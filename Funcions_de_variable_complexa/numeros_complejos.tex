\chapter{Números complejos}
\section{Introducción a los números complejos}

Los numeros complejos son los números de la forma
\[
	z = x + iy
\]
(con $x, y \in \real$) donde se tiene que $i^2 = -1$ y por lo tanto $i \notin \real$, además se tiene
que $\real \subset \cx$ ya que
\[
	x = x + 0i \in \cx.
\]

Estos números forman un cuerpo y por lo tanto, podemos definir la operación
división. Si $x_2 \neq 0$ o $y_2 \neq 0$
\[
	\frac{x_1 + iy_1}{x_2 + iy_2} = \frac{\left( x_1 + iy_1 \right)\left(
	x_2 - iy_2 \right)}{\left( x_2 + iy_2 \right)\left( x_2 - iy_2 \right)}
	= \frac{x_1x_2 + y_1y_2}{x^2_2 + y^2_2} + i \frac{y_1x_2 - x_1y_2}{x^2_2
	+ y^2_2}.
\]

Además, $\cx$ es un $\real$-espacio vectorial de dimensión 2, con base $\left\{
1, i \right\}$. Así, podemos definir las componentes de un número imaginario
\[
	z = \underbrace{x}_{\mathclap{\text{parte real}}} + i \overbrace{y}^{\mathclap{\text{parte
	imaginaria}}}.
\]
Basandonos en esta definición, podemos definir
\[
	\begin{aligned}
		\re \colon \cx &\to \real \\
		z = x + iy &\mapsto x,
	\end{aligned}
	\qquad
	\begin{aligned}
		\im \colon \cx &\to \real \\
		z = x + iy &\mapsto y.
	\end{aligned}
\]

Como todo cuerpo, el grupo multiplicativo de los complejos es $\cx^\ast = \cx
\setminus \left\{ 0 \right\}$. Quiere decir que $\cx^\ast$ con el producto es un
grupo.

Existe una biyección 
\[
	\begin{aligned}
		\cx &\leftrightarrow \real^2 \\
		z = x + iy &\leftrightarrow \left( x, y \right).
	\end{aligned}
\]
A veces nos interesará ver las funciones de una variable compleja como funciones
de dos variables reales y viceversa.

Una operación que nos resultará muy útil es la conjugación
\[
	\begin{aligned}
		\overline{\cdot} \colon \cx &\to \cx \\
		z = x + iy &\mapsto \overline{z} = x - iy.
	\end{aligned}
\]
Una de las propiedades que cumple esta operación es que se trata de un
automorfismo de grupos, es decir que
\[
	\overline{z + w} = \overline{z} + \overline{w}
	\quad
	\overline{z \cdot w} = \overline{z} \cdot \overline{w}.
\]
Además, también es una involución, es decir, $\overline{\cdot}^2 = \Id$ o dicho
de otro modo, $\overline{\overline{z}} = z$ $\forall z \in \cx$.

Se tiene pues la siguiente igualdad, si $z = x + iy$:
\[
	x = \frac{1}{2} \left( z + \overline{z} \right)
	\quad
	y = \frac{1}{2i}\left( z - \overline{z} \right).
\]

\begin{prop}
	Sea $P(t) \in \real[x]$ un polinomio con coeficientes reales y $z \in
	\cx$, entonces
	\[
		P\left( z \right) = 0 \iff P\left( \overline{z} \right) = 0.
	\]
\end{prop}
\begin{proof}
	Haremos tan solo la demostración hacia la derecha y la implicación hacia
	la izquierda quedará automaticamente demostrada.

	Se tiene
	\[
		\begin{aligned}
			P\left( \overline{z} \right) &= a_0 + a_1\overline{z} +
			\cdots + a_n \overline{z}^n = \\
			&= \overline{a_0} + \overline{a_1} \cdot \overline{z} +
			\cdots + \overline{a_n} \cdot \overline{z}^n = \\
			&= \overline{a_0} + \overline{a_1 z} + \cdots +
			\overline{a_n z^n} = \\
			&= \overline{a_0 + a_1 z + \cdots + a_n z^n} = \\
			&= \overline{P(z)} = \overline{0} = 0.
		\end{aligned}
	\]
\end{proof}

Nos será útil también dotar al espacio de una norma (a la que también nos
referiremos como valor absoluto o módulo) que definiremos como
\[
	\abs{z} = \sqrt{z \cdot \overline{z}} = \sqrt{x^2 + y^2}.
\]
Ese valor corresponde con la distancia euclidiana entre 0 y $z$.

Otra de las funciones características de los números complejos es el argumento,
el argumento es un ángulo $\alpha$ que representa la longitud del arco (de la
circunferencia unitaria) comprendido entre el semieje real positivo y la
semirecta que parte de 0 y pasa por $z$.

\begin{center}
	\begin{tikzpicture}
		\begin{axis}[axis equal image,
				axis lines=center,
				ticks=none,
				ymin=-2,ymax=2,
			xmin=-2,xmax=3]

			\addplot[color=orange,domain=0:100] {0.5*x};
			\addplot[mark=*,color=red] coordinates {(2,1)} node[right] {$z$};

			\addplot[mark=*,color=white] coordinates {(0,0)};
			\addplot[mark=o] coordinates {(0,0)};


			\addplot[domain=-180:180, samples=210, color=black] ({cos(x)},{sin(x)});
			\addplot[domain=0:26.565, samples=210, color=red, thick] ({cos(x)},{sin(x)})
			node[label=-80:$\arg z$] {};
		\end{axis}
	\end{tikzpicture}
\end{center}

Lamentablemente, no existe una función continua que cumpla estas
características, de tal forma que definiremos
\[
	\arg z  = \arg\left( x + iy \right) = \left\{ \alpha + 2 \pi k \mid k \in \z \right\}
\]
donde $\alpha$ cumple que $(x, y) = \left( r \cos \alpha, r \sin \alpha \right)$
y $r = \abs{z}$.

\begin{defi}[determinación!del argumento]
	Llamaremos determinación del argumento en un conjunto de valores $S
	\subseteq \cx^\ast$ a una aplicación $\Arg \colon S \to \real$ tal que
	\[
		\Arg\left( z \right) \in \arg \left( z \right).
	\]
\end{defi}

\begin{obs}
	Existe lo que conocemos como determinación principal que es la
	determinación que toma valores en $(-\pi, \pi]$ o la determinación
	alternativa, que toma valores en $[0, 2\pi)$.
\end{obs}

\begin{obs}
	Existe un problema con las determinaciones y es que no es continua (y
	por lo tanto tampoco derivable) ni existe ninguna determinación en todo
	$\cx^\ast$ que lo sea. Pero si que hay determinaciones en otros
	conjuntos, por ejemplo en $\re\left( z \right) > 0$.
\end{obs}

\begin{prop}
	Se tiene que $\abs{zw} = \abs{z} \abs{w}$ y que $\arg(zw) = \arg(z) +
	\arg(w)$.
\end{prop}
\begin{proof}
	\[
		\abs{zw} = \sqrt{z w \overline{z} \cdot \overline{w}} = \sqrt{z
		\overline{z} \cdot w \overline{w}} = \sqrt{z \overline{z}}
		\sqrt{w \overline{w}} = \abs{z} \abs{w}.
	\]

	Por otro lado, tomamos $z = r\left( \cos\alpha + i \sin\alpha \right)$ y
	$w = s\left( \cos\beta + i\sin\beta \right)$, entonces
	\[
		zw = rs\left( \underbrace{\left( \cos\alpha\cos\beta -
		\sin\alpha\sin\beta \right)}_{\cos(\alpha + \beta)} + i
		\underbrace{\left( \sin\alpha\cos\beta + \cos\alpha\sin\beta
		\right)}_{\sin(\alpha + \beta)} \right)
	\]
	y por lo tanto, $\arg(zw) = \arg(z) + \arg(w)$.
\end{proof}
\begin{col}
	Como consecuencia $\arg\left( z^{-1} \right) = - \arg(z)$, $\arg\left( \sfrac{z}{w}
	\right) = \arg(z) - \arg(w)$ y $\arg\left( z^n \right) = n \arg(z)$.
\end{col}

\begin{prop}
	Si $z \neq 0$ y $n \geq 1$, $z$ tiene exactamente $n$ ra\'ices
	$n$-ésimas diferentes. Además, se tiene que si $z - r \left( 
	\cos \alpha + i \sin \alpha \right)$ entonces
	\[
		\sqrt[n]{z} = \left\{ \sqrt[n]{r} \left( \cos
		\frac{\alpha + 2 \pi t}{n} + i \sin \frac{\alpha + 2 \pi t}{n}
		\right) \mid 0 \leq t < n \right\}.
	\]
\end{prop}

\begin{proof}
	Se tiene que, si $w = s \left( \cos\beta + i\sin\beta \right)$
	\[
		w = \sqrt[n]{z} \iff w^n = s^n \left( \cos n\beta + i \sin
		n\beta \right) = z = r \left( \cos\alpha + i \sin \alpha \right).
	\]
	De donde se obtiene que
	\[
		\begin{cases}
			r = s^n \iff s = \sqrt[n]{r} \\
			n\beta = \alpha + 2\pi k & k \in \z \iff \beta =
			\frac{\alpha+2\pi k}{n}.
		\end{cases}
	\]
\end{proof}

\begin{obs}
	Las raices $n$-ésimas de un número complejo forman un polígono regular
	centrado en el origen. La raiz se trata de una función multivaluada.
\end{obs}

\section{Coordenadas cartesianas y polares}

Las coordenadas cartesianas nos sirven para representar los números complejos de
la forma $\left( x, y \right)$ con $x, y \in \real$ y nos resultan muy útiles a
la hora de sumar números complejos.

Por otro lado existen las coordenadas polares, que nos permiten representar
$\cx^\ast$ de la forma $\left( r, \alpha \right)$ con $r \in \real^+$ y $\alpha
\in (-\pi, \pi]$. Está forma nos resulta muy icaz a la hora de multiplicar y
dividir números complejos.

Para pasar de coordenadas polares a cartesianas se tiene la fórmula
\[
	x = r \cos \alpha \quad y = r \sin \alpha.
\]

Sin embargo, pasar de la forma cartesiana a la polar no es tan trivial, ya que
no existen fórmulas cerradas. Pero se tiene que
\[
	r = \sqrt{x^2 + y^2}
	\quad
	\arg (z) \stackrel{\ast}{=} \arctan \left( \frac{y}{x} \right)
	\stackrel{\ast}{=} \arcsin\left( \frac{y}{r} \right) \stackrel{\ast}{=} \arccos\left( \frac{x}{r} \right).
\]
Usamos $\stackrel{\ast}{=}$ ya que no se da la igualdad entre ninguna de las
fórmulas ni con el argumento, la fórmula a usar dependerá del cuadrante.

Para notar los números complejos usaremos pues distintas notaciones
\begin{center}
	\begin{tabular}{lc}
		forma cartesiana & $z = x + iy$ \\
		forma polar & $z = r < \alpha$, $r_\alpha$ \\
		forma exponencial & $z = r e^{i\alpha}$
	\end{tabular}
\end{center}

Es fácil comprobar que la notacion exponencial se comporta bien con respecto a
las operaciones, aquí tan solo comprobamos la multiplicación (el uso de está
notación y su corrección se harán evidentes cuando definamos la
exponencial compleja).
\[
	\begin{rcases}
		z = re^{i\alpha} \\
		w = se^{i\beta}
	\end{rcases}
	zw = (rs) e^{i(\alpha+\beta)}.
\]

\section{Métrica y topología}

\begin{prop}
	Se tiene que
	\begin{enumerate}[i)]
		\item $\abs{z} \geq 0$ y $\abs{z} = 0 \iff z = 0$,
		\item $\abs{zw} = \abs{z} \abs{w}$,
		\item $\abs{z+w} \leq \abs{z} + \abs{w}$.
	\end{enumerate}
\end{prop}
\begin{proof}
	Las dos primeras afirmaciones ya las hemos visto y no deberían
	representar ningún reto, demostraremos la tercera afirmación:
	\[
		\begin{aligned}
			\abs{z+w}^2 &= (z+w)\left( \overline{z} + \overline{w} \right) =
			z\overline{z} + w\overline{w} + z \overline{w} +
			\underbrace{\overline{z}w}_{\overline{z\overline{w}}} =
			\\ &= \abs{z}^2 + \abs{w}^2 + 2 \re\left( z \overline{w}
			\right) \leq \abs{z}^2 + \abs{w}^2 + 2\abs{\re\left(
			z \overline{w} \right)} \leq \\ &\leq\abs{z}^2 + \abs{w}^2 +
			2\abs{z}\abs{w} = \left( \abs{z} + \abs{w} \right)^2.
		\end{aligned}
	\]
	Por último se tiene que 
	\[
		\abs{z+w} \leq \abs{z} + \abs{w}.
	\]
\end{proof}

\begin{defi}[distancia]
	Definiremos la distancia entre los números complejos $z$ y $w$ como
	\[
		\text{d}(z, w) = \abs{z - w}.
	\]
\end{defi}
\begin{obs}
	La distancia tal y como la hemos definido cumple estás tres propiedades
	\begin{enumerate}[i)]
		\item $\text{d}(z, w) \geq 0$, $\text{d}(z,w) = 0 \iff z = w$,
		\item $\text{d}(z, w) = \text{d}(w, z)$,
		\item $\text{d}(z, w) \leq \text{d}(z, p) + \text{d}(p, w)$.
	\end{enumerate}
	Por lo tanto, $(\cx, \text{d})$ se trata de un espacio métrico y por
	lo tanto de un espacio topológico.
\end{obs}

\begin{teo*}
	El espacio $\left( \cx, \text{d} \right)$ es un espacio completo, es
	decir, que dada una sucesión $\left( z_n \right) = \left( x_n
	\right) + i \left( y_n \right)$, entonces
	\[
		\left( z_n \right) \text{ es convergente} \iff \left( z_n
		\right) \text{ es de Cauchy}.
	\]
\end{teo*}

\begin{defi}[serie]
	Definimos una serie $\sum^\infty_{n=0} z_n$ como una pareja de
	succesiones $\left( s_n \right)$ y $\left( z_n \right)$ relacionadas por
	\[
		s_n = \sum^n_{j=0} z_j.
	\]
	Llamaremos a $s_n$ suma parcial $n$-ésima y a $z_n$ término $n$-ésimo de
	la serie.
\end{defi}
\begin{defi}[serie!convergente]\idx{serie!absolutamente convergente}\idx{serie!divergente}
	Sea $\sum z_n$, diremos que es convergente si la sucesión de sumas
	parciales $\left( s_n \right)$ es convergente, asimismo diremos que la
	serie es absolutamente convergente si la serie $\sum \abs{z_n}$ es
	convergente.

	Por otro lado, diremos que una serie es divergente si no es convergente.
\end{defi}
\begin{obs}
	Existen criterios para saber si una serie es convergente como
	\begin{itemize}
		\item Acotación de las sumas parciales.
		\item Comparación.
		\item Criterio del cociente.
		\item Criterio de la raíz.
	\end{itemize}
\end{obs}

\begin{defi}[producto de Cauchy]
	Dadas dos series $\sum z_n$ y $\sum w_n$, definimos el producto de
	Cauchy como la serie $\sum p_n$ con
	\[
		p_n = \sum^n_{k=0} z_kw_{n-k} = \sum_{r+s=n} z_rw_s.
	\]
\end{defi}
\begin{obs}
	Notaremos el producto de Cauchy como $\left( \sum p_n \right) = \left(
	\sum z_n \right) \left( \sum w_n \right)$. Notemos también que este
	producto es conmutativo.
\end{obs}
\begin{prop}
	Sean $\sum z_n$ y $\sum w_n$ dos series absolutamente convergentes, entonces su
	producto de Cauchy $\left( \sum p_n \right) = \left( \sum z_n \right) \left( w_n
	\right)$ también es convergente. Y la suma del producto de Cauchy es
	igual al producto de las sumas.
\end{prop}
\begin{proof}
	Primero veremos que es convergente. Por ser $\sum \abs{z_k}$
	convergente, existe $M_z$ una cota superior, analogamente, existe $M_w$
	cota de $\sum \abs{w_n}$, entonces se tiene que
	\[
		\begin{aligned}
			\sum^n_{k=0} \abs{p_k} &= \sum^n_{k=0}
			\abs{\sum_{r+s=k} z_rw_s} \leq \sum^n_{k=0}
			\sum_{r+s=k} \abs{z_r}\abs{w_s} \leq \\ &\leq
			\sum_{0 \leq r,s \leq n} \abs{z_r}\abs{w_s} =
			\left( \sum^n_{r=0} \abs{z_r} \right) \left(
			\sum^n_{s=0} \abs{w_s} \right) \leq M_zM_w.
		\end{aligned}
	\]
	Y por lo tanto, $\sum \abs{p_k}$ es acotada y por lo tanto convergente,
	así concluimos que $\sum p_k$ es convergente.

	Por otro lado, sea $s_n = \sum^n_{k=0} z_k$, $t_n = \sum^n_{k=0} w_k$,
	$u_n = \sum^n_{k=0} p_k$ y por último $v_n = \sum^n_{k=0} q_k$ donde
	$q_m = \sum_{r+s=m} \abs{z_r}\abs{w_s}$ entonce
	\[
		\begin{aligned}
			\abs{s_nt_n - u_n} &= \abs{\left( \sum^n_{r=0} z_r
			\right) \left( \sum^n_{s=0} w_s \right) -
			\sum^n_{t=0} p_t} = \\ &= \abs{\sum_{0 \leq r,s \leq n} z_rw_s -
			\sum_{0 \leq r+s \leq n} z_rw_s} \leq \\ &\leq
			\abs{\sum^{2n}_{r+s=n+1} z_rw_s} \leq
			\sum^{2n}_{r+s=n+1} \abs{z_r}\abs{w_s} = \\ &=
			v_{2n} - v_n.
		\end{aligned}
	\]
	Como $v_n$ es convergente, es de Cauchy y por lo tanto $\abs{s_nt_n -
	u_n} \leq v_{2n} - v_n < \varepsilon$ para algún $n$ suficientemente
	grande. Concluimos así que el producto de las series parciales tiende a
	la serie producto.
\end{proof}
\begin{ej}
	Ver que el resultado es cierto si y solo si al menos una de las dos series es
	convergente.
\end{ej}

\section{Infinito complejo}

\begin{defi*}
	Definiremos $\overline{\cx} = \cx \cup \left\{ \infty \right\}$ donde
	$\infty$ quiere decir $\abs{z} = \infty$.
\end{defi*}
\begin{obs}
	Este nuevo espacio $\overline{\cx}$ es un espacio compacto.
\end{obs}

Al ser $\overline{\cx}$ un espacio compacto, toda sucesión es convergente y
tiene límite, se tiene que $\lim_{n \to \infty} z_n = \infty \iff \forall M > 0
\exists N$ tal que $\forall n \geq N \abs{z_n} > M$. Además, al ser $\infty$ un
elemento más de nuestro espacio, podemos operar tranquilamente con él,
obteniendo las siguientes igualdades:
\[
	\frac{1}{\infty} = 0, \quad \frac{1}{0} = \infty, \quad \infty + z =
	\infty (\text{si } z \neq \infty), \quad \infty z = \infty (\text{si } z
	\neq 0).
\]
Sin embargo, al añadir el $\infty$ también nos encontramos con las siguientes
indeterminaciones:
\[
	\infty + \infty, \quad 0\infty, \quad \frac{0}{0}, \quad,
	\frac{\infty}{\infty}.
\]

Observamos también que la recta proyectiva compleja $\Po^1\left( \cx \right) =
\cx \cup \left\{ \infty \right\} = \overline{\cx}$. Tambien se tiene que
$\Sph^2 \cong \overline{\cx}$ a través de la proyección estereológica.
Recordamos que
\[
	\Sph^2 = \left\{ \left( x_1, x_2, x_3 \right) \in \real^3 \mid x^2_1 +
	x^2_2 + x^2_3 = 1 \right\}
\]
y la identificación es la correspondiente a trazar rectas desde el punto del
infito e identificar los puntos de corte de la esfera y el plano
\begin{center}
	\begin{tikzpicture}
		\begin{axis}[axis equal,
				ticks=none,
				width=13 cm,
				enlargelimits=0,
				axis lines=center,
				view={10}{20},
				ymin=-4,ymax=4,
			xmin=-4,xmax=4]

			\addplot3[patch,color=orange, patch type=rectangle, opacity=0.8] coordinates
			{(4,4,0) (4,-4,0) (-4,-4,0) (-4,4,0)};

			\addplot3[color=blue,mark=*] coordinates {(0,0,2)} node[pin=150:{$\infty$}]{};

			\addplot3[mark=*,color=blue] coordinates {(2,3,0)}
			node[pin=60:{$\Pi(z)$}]{};
			\addplot3[mark=*,color=blue] coordinates
			{(0.47,0.71,1.53)} node[pin=60:{$z$}]{};
			\draw (axis cs:0,0,2) [color=blue] -- (axis cs:2,3,0);

			\addplot3[mark=*,color=blue] coordinates {(2,-1.5,0)};
			\addplot3[mark=*,color=blue] coordinates {(0.78,-0.59,1.22)};
			\draw (axis cs:0,0,2) [color=blue] -- (axis cs:0.78,-0.59,1.22);

			\addplot3[%
				opacity = 0.5,
				surf,
				color=red,
				faceted color=red,
				z buffer = sort,
				samples = 21,
				variable = \u,
				variable y = \v,
				domain = 0:180,
				y domain = 0:360,
			]
			({cos(u)*sin(v)}, {sin(u)*sin(v)}, {cos(v)+1});

			\draw (axis cs:0.78,-0.59,1.22)[color=blue] -- (axis cs:2,-1.5,0);

		\end{axis}
	\end{tikzpicture}
\end{center}
Numéricamente, se tiene
\[
	\begin{aligned}
		\Pi \colon  \Sph^2 &\to \overline{\cx} \\
		\left( x_1, x_2, x_3 \right) &\mapsto
		\begin{cases}
			\frac{x_1}{1-x_3} + i \frac{x_2}{1-x_3} & \text{si } x_3
			\neq 1, \\
			\infty &\text{si } x_3 = 1.
		\end{cases}
	\end{aligned}
\]
Y la identificación inversa
\[
	\begin{aligned}
		\Pi^{-1} \colon \overline{\cx} &\to \Sph^2 \\
		z &\mapsto
		\begin{cases}
			\left( \frac{2\re(z)}{\abs{z}^2+1},
				\frac{2\im(z)}{\abs{z}^2+1}, \frac{\abs{z}^2-1}{\abs{z}^2+1}
			\right) &\text{si } z \in \cx, \\
			(0, 0, 1) &\text{si } z = \infty.
		\end{cases}
	\end{aligned}
\]

\begin{ej}
	Ver que $\Pi$ y $\Pi^{-1}$ corresponden a la definición geométrica dada.
	Además ver que están bien definidas y son inversas mutuas.
\end{ej}
\begin{obs}
	$\Pi^{-1}$ induce una topología de antiimagen en $\overline{\cx}$ de
	forma que $U$ es un abierto de $\overline{\cx} \iff \Pi^{-1}(U)$ es
	abierto de $\Sph^2$ con la topología estándar en $\Sph^2$.
\end{obs}
