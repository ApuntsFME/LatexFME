\chapter{Aparellaments}

\section{Teorema de König}

\begin{defi}[aparellament]
	Un aparellament $M$ en un graf $\Gamma$ és un conjunt d'arestes sense vèrtexs en comú.
	En particular, direm que:
	\begin{itemize}
		\item L'aparellament és maximal si no està contingut en cap altre aparellament.
		\item L'aparellament és perfecte si tots els vèrtexs són incidents a una aresta de l'aparellament.
	\end{itemize}
\end{defi}

\begin{defi}[camí!alternat]
	Un camí alternat respecte de $M$ és un camí que comença en un vèrtex no aparellat i que alterna
	arestes aparellades i no aparellades.
\end{defi}

\begin{defi}[camí!augmentador]
	Un camí augmentador és un camí alternat que acaba en un vèrtex no aparellat.
\end{defi}

\begin{example}
	Considerem els dos aparellaments del mateix graf mostrat a la figura inferior. Les arestes de
	l'aparellament estan marcades en vermell. 
	
	Els dos aparellaments són maximals, ja que no s'hi pot afegir cap aresta. 
	No obstant, el graf de l'esquerra té un camí augmentador, marcat en punts negres. 
	Si canviem el color de les arestes del camí augmentador obtenim l'aparellament
	de la dreta, que és un aparellament amb més arestes (i, de fet, perfecte). 
	Així, queda justificat l'ús del terme ``camí augmentador''.
	\begin{center}
		\begin{minipage}{0.4\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle,fill=black,inner sep=1pt}]
				\path (0,0) node (p0) {}
				(2,0) node (p1) {}
				(4,0) node (p2) {}
				(0,2) node (p3) {}
				(2,2) node (p4) {}
				(4,2) node (p5) {}
				(2,4) node (p6) {}
				(4,4) node (p7) {};
				
				\draw[color=blue,thick]
				(p0) -- (p1)
				(p1) -- (p2)
				(p0) -- (p3)
				(p0) -- (p4)
				(p3) -- (p1)
				(p3) -- (p4)
				(p4) -- (p5)
				(p2) -- (p5)
				(p1) -- (p4)
				(p3) -- (p6)
				(p4) -- (p6)
				(p5) -- (p6)
				(p5) -- (p7)
				(p6) -- (p7);
				\draw[color = red,thick]
				(p3) -- (p6)
				(p0) -- (p4)
				(p2) -- (p5);
				\draw[dotted,very thick]
				(p1) -- (p0) -- (p4) -- (p3) -- (p6) -- (p7);
			\end{tikzpicture}
		\end{minipage}
		\begin{minipage}{0.4\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle,fill=black,inner sep=1pt}]
				\path (0,0) node (p0) {}
				(2,0) node (p1) {}
				(4,0) node (p2) {}
				(0,2) node (p3) {}
				(2,2) node (p4) {}
				(4,2) node (p5) {}
				(2,4) node (p6) {}
				(4,4) node (p7) {};
				
				\draw[color=blue,thick]
				(p0) -- (p1)
				(p1) -- (p2)
				(p0) -- (p3)
				(p0) -- (p4)
				(p3) -- (p1)
				(p3) -- (p4)
				(p4) -- (p5)
				(p2) -- (p5)
				(p1) -- (p4)
				(p3) -- (p6)
				(p4) -- (p6)
				(p5) -- (p6)
				(p5) -- (p7)
				(p6) -- (p7);
				\draw[color = red,thick]
				(p6) -- (p7)
				(p3) -- (p4)
				(p0) -- (p1)
				(p2) -- (p5);
			\end{tikzpicture}
		\end{minipage}
	\end{center}
\end{example}

\begin{lema}
	Si existeix un camí augmentador respecte $M$, aleshores la mida de $M$ no és màxima.
\end{lema}
\begin{proof}
	Canviant les arestes del camí, obtenim un aparellament més gran.
\end{proof}

Definim ara l'últim concepte que ens falta per arribar al teorema de König, que és el
d'una cobertura de vèrtexs.

\begin{defi}[cobertura de vèrtexs]
	Una cobertura de vèrtexs (\textit{vertex cover}) $U$ d'un graf $\Gamma$ és un subconjunt dels vèrtexs de $\Gamma$
	tal que tota aresta és incident a un vèrtex de $U$.
\end{defi}

\begin{teo}[Teorema de König]
	Sigui $\Gamma$ un graf bipartit. Llavors, la mida mínima d'una cobertura de vèrtexs és
	igual a la mida màxima d'un aparellament.
\end{teo}
\begin{proof}
	Si el graf és bipartit, podem escriure $V(G) = A \sqcup B$, de manera que totes les arestes connecten
	un vèrtex de $A$ i un vèrtex de $B$. Al llarg de la demostració, denotarem amb $a$ els vèrtexs
	de $A$ i amb $b$ els de $B$.
	
	En primer lloc, constatem que tota cobertura de vèrtexs més gran o igual que qualsevol aparellament, 
	ja que cada vèrtex de la cobertura cobreix, com a molt, una aresta de l'aparellament.
	
	Sigui $M$ un aparellament de mida màxima. És suficient construir una cobertura de vèrtexs
	$U$ tal que $|U| = |M|$. Per a cada aresta $ab \in M$, posarem $b \in U$ si existeix
	un camí alternat de mida senar que acabi a $b$ i que no es pugui estendre a un altre
	camí alternat de mida superior. Altrament, posarem $a \in U$.
	
	És clar que $|U| = |M|$. Per tant, l'únic que cal demostrar és que $U$ és una cobertura de vèrtexs.
	Suposem que no ho és, i per tant que existeix una aresta $ab$ tal que $a,b \notin U$. Com que
	$M$ és maximal, almenys un dels dos $a,b$ ha de ser incident a $M$, ja que sinó podríem afegir
	$ab$ a l'aparellament. No obstant, $ab \notin M$, ja que tota aresta de $M$ té un extrem dins la cobertura.
	\begin{itemize}
		\item Suposem que $a$ no està cobert per una aresta de $M$. Aleshores, $ab$ és un camí
		alternat de mida 1, senar. Com que $b \notin U$, hem de poder estendre el camí a un nou
		camí alternat de mida senar.
		
		Aquest camí ha d'anar de $A$ a $B$ a través d'arestes que no són de $M$,
		i tornar de $B$ a $A$ amb arestes de $M$. El camí ha d'acabar a un vèrtex $b' \in B$. 
		Aquest vèrtex $b'$ no pot ser incident a $M$, ja que sinó es podria estendre el camí (per què?).
		Però, aleshores, el camí alternat que hem obtingut és augmentador, contradient la maximalitat de $M$.
		
		\item Suposem ara que $a$ està cobert per una aresta de $M$, diguem-li $ab'$ (necessàriament, $b \neq b'$).
		Com que $ab' \in M$ i $a \notin U$, s'ha de tenir $b' \in U$. Així, existeix un camí alternat $P$
		de longitud senar que acaba en $b'$. Però $P(b'a)(ab)$ és una extensió de $P$ a un camí alternat
		senar més llarg (per què això està ben definit?).
	\end{itemize}
	En qualsevol dels casos, arribem a contradicció, demostrant que $U$ és una cobertura de vèrtexs.
\end{proof}

\begin{ej}
	Completeu els detalls que s'han deixat a la demostració.
\end{ej}

\section{Teorema de Hall}
Al primer tema ja vam demostrar el teorema de Hall (\ref{teo:Hall}). Aquí, el tornarem a demostrar
en termes d'aparellaments. L'avantatge de la demostració que donarem tot seguit és que és algorísmica:
no només demostra l'existència d'un SRD, sinó que dona un algorisme per trobar-lo.

Recordem que, en el context del teorema de Hall, teníem $A_1,\dots,A_n$ subconjunts d'un conjunt $X$,
i que un sistema de representants distints (SRD) és una $n$-tupla $(x_1,\dots,x_n)$ tal que
$x_i \in A_i$ i que tots els $x_i$ són diferents.

Si $J \subseteq \{ 1,\dots, n\}$, definim $A(J) = \cup_{j\in J} A_j$. La condició de Hall
era que
\[
	|A(J)| \geq |J|
\]
per a tot $J$, i era equivalent a que existís un SRD per als conjunts $A_1,\dots,A_n$.

A partir dels conjunts $A_1,\dots,A_n$ i $X$ podem construir un graf bipartit $\Gamma$. Dividim els vèrtexs
en dos conjunts: el primer conjunt té com a vèrtexs $A_1,\dots,A_n$ i el segon conjunt té com a vèrtexs
els elements $x\in X$. Posem una aresta entre $x$ i $A_i$ si i només si $x \in A_i$. En aquest cas,
un aparellament que cobreixi tots els vèrtexs de $A$ es correspondrà amb un SRD. En efecte:

\begin{teo}[Teorema del matrimoni de Hall]
	El graf $\Gamma$ té un aparellament que cobreix tots els vèrtexs de $A$ si i només si se satisfà
	la condició de Hall.
\end{teo}
\begin{proof}
	És clar que si $\Gamma$ té tal aparellament, aleshores se satisfà la condició de Hall.
	
	Per a la implicació contrària, suposem que se satisfà la condició de Hall, i sigui $M$ un aparellament.
	Demostrarem que si algun vèrtex $a\in A$ no està aparellat, el podem aparellar canviant arestes de $M$
	però sense desaparellar cap vèrtex de $A$ prèviament aparellat.
	
	Suposem que $a_0 \in A$ no està cobert per $M$. Per la condició de Hall, existeix un veí $x_1 \in X$
	de $a_0$. Si $x_1$ no està cobert per $M$, parem. Altrament, $x_1$ està aparellat amb $a_1 \in A$.
	Aplicant la condició de Hall a $\{a_0,a_1\}$, existeix un $x_2 \in X \setminus \{x_1\}$ que és veí 
	de $a_0$ o $a_1$. Pot passar que:
	\begin{itemize}
		\item $x_2$ no estigui cobert per $M$ i sigui veí de $a_0$. En aquest cas, ja estem.
		\item $x_2$ no estigui cobert per $M$ i no sigui veí de $a_0$ (per tant, ho és de $a_1$).
		Si treiem $x_1 a_1$ de l'aparellament i posem $a_0 x_1$ i $a_1 x_2$, ja estem.
		\item $x_2$ estigui cobert per $M$. En aquest cas, aplicant el mateix raonament obtindríem
		uns nous $a_3 \in A$, $x_3 \in X \setminus \{x_1,x_2 \}$.
	\end{itemize} 
	En general, apliquem el raonament repetidament fins que arribem a un vèrtex $x_m$ no cobert
	per $M$ (hi arribarem en un nombre finit de passos). En aquest cas, podem canviar totes les
	arestes $x_i a_i$, amb $1 \leq i \leq m-1$ per $a_{i-1} x_i$ amb $1 \leq i \leq m$, augmentant
	la mida de $M$ de la forma desitjada (canviem les arestes blaves per les vermelles en la figura).
	\begin{center}
		\begin{tikzpicture}[every node/.style={draw,shape=circle,inner sep=1pt}]
			\path (0,2) node (a0) {$a_0$}
			(1,0) node (x1) {$x_1$}
			(2,2) node (a1) {$a_1$}
			(3,0) node (x2) {$x_2$}
			(4,2) node (a2) {$\dots$}
			(5,0) node (x3) {$\dots$}
			(6,2) node (a3) {$a_m$}
			(7,0) node (x4) {$x_m$};
			
			\draw[color=blue,thick]
			(x1) -- (a1)
			(x2) -- (a2)
			(x3) -- (a3);
			
			\draw[color=red,thick]
			(a0) -- (x1)
			(a1) -- (x2)
			(a2) -- (x3)
			(a3) -- (x4);
		\end{tikzpicture}
	\end{center}
\end{proof}

\section{Aparellaments estables}
Considerem ara que els vèrtexs tenen un ordre de preferència (estricte) sobre els seus veïns a l'hora de
ser aparellats, assumint que tots els vèrtexs prefereixen estar aparellats a no estar-ho.

\begin{defi}
	L'aresta $xy \notin M$ és inestable respecte $M$ si $x$ prefereix a $y$ sobre el vèrtex
	al qual està aparellat (si està aparellat), i $y$ prefereix a $x$ sobre el seu vèrtex aparellat.
\end{defi}
\begin{defi}[aparellament estable]
	Un aparellament es diu estable si no té cap aresta inestable.
\end{defi}

Observem que tot aparellament estable és maximal, ja que si hi haguessin dos vèrtexs no aparellats connectats
per una aresta, preferirien ajuntar-se que estar sols. En general, un aparellament estable no necessàriament
existeix, però en el cas dels grafs bipartits sí:

\begin{teo*}
	Tot graf bipartit admet un aparellament estable.
\end{teo*}
\begin{proof}
	Altra vegada, farem una demostració algorísmica.  Suposem que el graf està bipartit
	en dos conjunts $A,B$. L'algorisme consistirà en diversos torns, en els quals tot vèrtex
	de $A$ \textit{es proposarà} al seu vèrtex preferit de $B$ que no l'hagi rebutjat prèviament.
	\footnote{Històricament, als llibres de text s'associava $A$ amb els homes i $B$ amb les dones
	(tots heterosexuals, és clar), i a cada torn els homes es proposaven a les dones. L'analogia serveix
	per entendre l'algorisme, però comença a ser una mica casposa (segons el parer de l'autor).}
	Si $b \in B$ rep més d'una proposició, escollirà la preferida i rebutjarà les altres. Els vèrtexs
	rebutjats eliminaran $b$ de la seva llista de preferències i seguirem al següent torn.
	
	L'algorisme segueix fins que tot vèrtex de $B$ rep, com a molt, una proposició. Aparellant els vèrtexs
	segons aquestes últimes proposicions, demostrarem que l'aparellament obtingut és estable.
	
	Suposem, buscant una contradicció, que $xy \notin M$ és una aresta inestable, amb $x \in A$ i $y \in B$. Observem
	que si un vèrtex de $B$ no està aparellat, és que no ha obtingut mai cap proposició (ja que, altrament,
	sempre hi ha una proposició que no rebutja). De forma similar, si un vèrtex de $A$ no està aparellat, és que
	totes les seves proposicions han estat rebutjades. Els casos són:
	\begin{itemize}
		\item $x$ i $y$ no estan aparellats amb ningú. En aquest cas, la proposició de $x$ a $y$ ha d'haver estat rebutjada,
		però això vol dir que $y$ hauria d'estar aparellat!
		\item $x$ no està aparellat però $y$ sí. Això vol dir que $y$ ha rebutjat a $x$ en algun moment, quedant-se
		a algú que preferia per sobre de $x$. Per tant, $xy$ no és inestable!
		\item $x$ està aparellat i $y$ no. Si $x$ està aparellat a $y'$ però prefereix a $y$, això vol dir que $y$ l'ha rebutjat.
		Però això implica que $y$ ha d'estar aparellat!
		\item $x$ i $y$ estan aparellats (però no entre sí). En aquest cas, si $x$ està aparellat amb $y'$ però prefereix a $y$,
		això vol dir que $y$ l'ha rebutjat. Per tant, $y$ està aparellat amb algú a qui prefereix!
	\end{itemize}
	En qualsevol cas, arribem a contradicció.
\end{proof}

\section{Teorema de Tutte}

\begin{defi}[component!senar]
	Una component senar d'un graf $\Gamma$ és una component connexa amb un nombre senar
	de vèrtexs. Es denota el nombre de components senar per $\text{oc}(\Gamma)$.
\end{defi}

\begin{teo}[Teorema de Tutte]
	El graf $\Gamma$ té un aparellament perfecte si, i només si, per a tot subconjunt $S \subseteq V(\Gamma)$,
	se satisfà $\text{oc}(\Gamma \setminus S) \leq |S|$. 
\end{teo}
\begin{proof}
	Quant a la implicació directa, suposem que $\Gamma$ té un aparellament perfecte, i sigui $S$
	un subconjunt qualsevol de vèrtexs. A $\Gamma \setminus S$, s'hauran preservat exactament 
	tots aquells aparellaments que no involucrin vèrtexs de $S$. Cada component senar de $\Gamma \setminus S$
	té, almenys, un vèrtex que no està aparellat, fet que vol dir que estava aparellat amb un vèrtex de $S$ a $\Gamma$.
	En altres paraules, per a cada component senar de $\Gamma \setminus S$ hi ha d'haver un vèrtex de $S$, com volíem veure.
	
	Centrem-nos ara en la implicació recíproca. La primera observació que fem és que si afegim arestes a $\Gamma$,
	la condició $\text{oc}(\Gamma \setminus S) \leq |S|$ no queda afectada:
	\begin{itemize}
		\item Si una aresta uneix dos vèrtexs d'una component connexa, no passa res.
		\item Si una aresta uneix dues components senars, queda una component parella, disminuint $\text{oc}(\Gamma \setminus S)$.
		\item Si una aresta uneix una component senar i una de parella, queda una component senar i $\text{oc}(\Gamma \setminus S)$
		es queda igual.
		\item Si una aresta uneix dues components parelles, queda una component parella i altra vegada $\text{oc}(\Gamma \setminus S)$ 
		es queda igual.
	\end{itemize}
	Suposem, buscant una contradicció, que $\Gamma$ no té un aparellament perfecte. Sigui $\Gamma^*$ un graf obtingut
	afegint arestes a $\Gamma$, de manera que afegir una aresta més doni un graf que admet un aparellament perfecte.
	Per l'observació anterior, se segueix satisfent $\text{oc}(\Gamma^* \setminus S) \leq |S|$.
	
	Sigui $K$ el subconjunt de vèrtexs de $\Gamma^*$ que estan units a tots els altres vèrtexs. Separarem la demostració
	en dos casos, segons si $\Gamma^* \setminus K$ té totes les components connexes completes o no.
	
	Suposem primer que $\Gamma^* \setminus K$ té una component connexa no completa. Així doncs,
	existeix un subgraf $K_{1,2}$ induït:
	\begin{center}
		\begin{tikzpicture}[every node/.style={draw,shape=circle,inner sep=1pt}]
			\path (0,0) node (a) {$a$}
			(1,2) node (b) {$b$}
			(2,0) node (c) {$c$}
			(3,2) node (d) {$d$};

			\draw[thick]
			(a) -- (b)
			(b) -- (c);
		\end{tikzpicture}
	\end{center}
	Com que $b \notin K$, existeix algun vèrtex de $\Gamma^*$ al qual no està connectat, diguem-li $d$.
	Atès que afegir una aresta a $\Gamma^*$ dona un graf amb un aparellament perfecte, considerem els aparellaments
	$M_{ac}$ i $M_{bd}$ corresponents a afegir les arestes $ac$ i $bd$, respectivament.
	
	Observem que $ac \in M_{ac}$ i $bd \in M_{bd}$, ja que sinó, els aparellaments perfectes també
	ho serien de $\Gamma^*$. Observem també que cada vèrtex està cobert exactament per una aresta de $M_{ac}$
	i una de $M_{bd}$ (potser iguals).
	
	Sigui $P$ el camí que comença al vèrtex $d$ i que segueix únicament per arestes dels aparellaments,
	començant per $M_{ac}$ i després per $M_{bd}$. Sota aquestes condicions, el camí passa per una aresta
	de $M_{ac}$, després per una de $M_{bd}$, després per una de $M_{ac}$, i així successivament.
	Aquest camí ha de ser un cicle que no contingui cicles més petits (penseu-ho), i d'aquesta manera
	$P$ ha de tornar a $d$ a través de $M_{bd}$ (es pot arribar a cada vèrtex com a molt amb dues arestes
	diferents, les de $M_{ac}$ o $M_{bd}$ que siguin incident al vèrtex).
	
	Suposem en primer lloc que el camí arriba a $b$ abans d'arribar a $a$ o $c$. Considerem la figura de l'esquerra,
	on les arestes de $M_{bd}$ són les vermelles i les de $M_{ac}$, les blaves. Si en el cicle substituïm les arestes
	de $M_{bd}$ per les de $M_{ac}$ incidents a $P$ (és a dir, canviem el color), $M_{bd}$ passa a ser un
	aparellament perfecte que no utilitza $bd$. Però això vol dir que $\Gamma^*$ té un aparellament perfecte, arribant a contradicció.
	\begin{center}
		\begin{minipage}{0.4\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle,inner sep=1pt}]
				\path (0,0) node (a) {$a$}
				(1,2) node (b) {$b$}
				(2,0) node (c) {$c$}
				(2,2) node (d) {$d$}
				(2.5,2.866) node (e) {}
				(2,3.732) node (f) {}
				(1,3.732) node (g) {}
				(0.5,2.866) node (h) {};
				
				\draw[thick]
				(a) -- (b)
				(b) -- (c);
				
				\draw[color=red,thick]
				(b) -- (d)
				(e) -- (f)
				(g) -- (h);
				
				\draw[color=blue,thick]
				(d) -- (e)
				(f) -- (g)
				(h) -- (b);
			\end{tikzpicture}
		\end{minipage}
		\begin{minipage}{0.4\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle,inner sep=1pt}]
				\path (0,0) node (a) {$a$}
				(1,2) node (b) {$b$}
				(2,0) node (c) {$c$}
				(3.236,2) node (d) {$d$}
				(4.236,0) node (e) {}
				(3.236,-2) node (f) {}
				(1,-2) node (g) {};
				
				\draw[thick]
				(b) -- (c);
				
				\draw[color=red,thick]
				(b) -- (d)
				(e) -- (f)
				(g) -- (a);
				
				\draw[color=blue,thick]
				(d) -- (e)
				(f) -- (g);
				
				\draw[thick,color=green]
				(a) -- (b);
			\end{tikzpicture}
		\end{minipage}
	\end{center}
	Suposem ara que arribem a $a$ abans que a $b$ o a $c$. En aquest cas, substituïm les arestes de $M_{bd}$
	incidents a $P$ amb les de $M_{ac}$ fins que arribem a $a$, i afegim l'aresta $ab$. D'aquesta manera, hem
	obtingut un aparellament perfecte per $\Gamma^*$, arribant altra vegada a contradicció.
	
	Per tant, ens hem de trobar en el cas que totes les components de $\Gamma^* \setminus K$ siguin completes.
	En cada component completa parella de $\Gamma^* \setminus K$ podem aparellar tots els vèrtexs,
	mentre que en les components senars els podem aparellar tots menys un. Aquest vèrtex sobrant ha d'estar
	aparellat amb algun vèrtex de $K$ si volem assolir algun aparellament perfecte. Això sempre es pot fer,
	ja que $\text{oc}(\Gamma^* \setminus K) \leq |K|$ i tot vèrtex de $K$ està connectat amb tothom.
	
	Ara només cal comprovar que a $K$ ens queda un nombre parell de vèrtexs per aparellar (com que tots  els
	vèrtexs de $K$ estan connectats, sempre podem aparellar qualsevol subconjunt parell). Si apliquem la hipòtesi
	per $S = \emptyset$, veiem que $\text{oc}(\Gamma^*) \leq 0$, i per tant totes les components de $\Gamma^*$ són
	parelles. Per tant, el nombre total de vèrtexs és parell i podem fer un aparellament perfecte, arribant a contradicció. 
\end{proof}
