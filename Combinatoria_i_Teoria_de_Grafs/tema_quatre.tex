\chapter{Enumeració amb simetries}
\section{Accions d'un grup}

En aquesta secció repassarem el concepte d'acció d'un grup sobre un conjunt.
S'assumirà que el lector està familiaritzat amb els conceptes bàsics de la teoria de grups.
Si no fos així, convidem al lector a repassar els apunts d'Estructures Algebraiques, que
també contenen part de la teoria que desenvoluparem a continuació.

Atès que principalment estudiarem accions amb el grup simètric (o subgrups seus), 
al llarg del capítol notarem els elements dels grups amb la notació de permutacions.

\begin{defi}[acció d'un grup]
	Sigui $X$ un conjunt. Un grup $G$ actua sobre $X$ si per a tot element $\sigma \in G$,
	hi ha una aplicació de $X$ en $X$, satisfent que $\sigma (\tau (x)) = (\sigma \tau)(x)$
	 i $\Id (x) = x$ per a tot $\sigma, \tau \in G$, $x \in X$.
\end{defi}

Usarem la mateixa notació per un element $\sigma \in G$ i la seva corresponent aplicació
$\sigma \colon X \to X$.

\begin{obs}
	Equivalentment, es pot dir que hi ha un homomorfisme de $G$ a $\Sym(x)$.
	Observem que totes les aplicacions $\sigma \colon X \to X$ són bijectives,
	amb inversa $\sigma^{-1}$.
\end{obs}

\begin{example}
	Suposem que $G = \{ \Id, \sigma, \sigma^2 \}$ i $X = \{a,b \}$. És
	possible definir una acció tal que $\sigma(a) = b$? Si aquest fos el cas, com que
	$\sigma$ és bijectiva, hem de tenir $\sigma(b) = a$. Però, aleshores:
	\[
		\sigma^2 (a) = a \implies a = \sigma^3(a) = \sigma(a) = b,
	\]
	arribant a contradicció.
\end{example}

\begin{defi}[òrbita d'una acció]
	Per a $x \in X$, l'òrbita de $x$ sota l'acció de $G$ és
	\[
		\text{orb}_G (x) = \{\sigma(x) | \sigma \in G \}.
	\]
	Diem que l'acció és transitiva si totes les òrbites són iguals o, equivalentment,
	quan per a tots $x,y \in X$, existeix $\sigma \in G$ tal que $\sigma(x) = y$.
\end{defi}

\begin{example}
	Sigui $\Gamma = K_4$ el graf complet de 4 vèrtexs, i sigui $G$ el grup generat per
	la permutació $(1 2 3 4)$. Observem que $G$ actua tant en els vèrtexs de $\Gamma$
	(el vèrtex numerat amb $x$ va a la posició $\sigma(x)$) com en les arestes de $\Gamma$
	(enviant l'aresta $xy$ a l'aresta $\sigma(x) \sigma(y)$).
	
	L'acció de $G$ sobre els vèrtexs és transitiva: cadascuna de les permutacions de $G$
	envia cada vèrtex a un vèrtex diferent. En canvi, l'acció sobre les arestes no és transitiva:
	les òrbites són:
	\[
		\{ (1 2), (2 3), (3 4), (1 4) \}, \; \{(1 3), (2 4) \}.
	\]
\end{example}

\begin{defi}
	Suposi's que $G$ actua en $X$. Per a $\sigma \in G$, definim 
	\[
		\text{fix} (\sigma) = \{ x \in X | \, \sigma(x) = x\}.
	\]
\end{defi}

\begin{defi}[subgrup estabilitzador]
	Per a $x \in X$, definim el subgrup estabilitzador de $x$ com a:
	\[
		G_x = \{ \sigma \in G | \, \sigma(x) = x\}.
	\]
\end{defi}
Es deixa com a exercici comprovar que $G_x$ és, en efecte, un subgrup de $G$.

\begin{lema}[Lema d'enumeració d'òrbites]
	\label{lema:enumOrbites}
	El nombre d'òrbites diferents de l'acció de $G$ en $X$ és:
	\[
		\frac{1}{|G|} \sum_{\sigma \in G} |\text{fix} (\sigma)|.
	\]
\end{lema}

\begin{proof}
	En primer lloc suposarem que $G$ actua transitivament en $X$, i després
	demostrarem el cas general.
	
	En primer lloc, observem que si $G$ actua transitivament, aleshores
	per a tot $y \in X$ existeix $\sigma \in G$ tal que $\sigma(x) = y$, per a un
	$x \in X$ donat. Observem també que $\sigma G_x$ és una classe lateral de $G_x$. Afirmem
	que hi ha $|X|$ classes laterals de $G_x$, de la forma $\sigma G_x$, classificades
	segons el valor de $\sigma(x)$. En altres paraules, volem veure que 
	$\sigma G_x = \tau G_x \iff \sigma (x) = \tau (x)$. Però es té:
	\[
		\sigma G_x = \tau G_x \iff \sigma^{-1} \tau \in G_x \iff \sigma^{-1}(\tau (x)) = x
		\iff \tau(x) = \sigma(x).
	\]
	Per tant, hi ha $|X|$ classes laterals de $G_x$, és a dir, $|X| = \frac{|G|}{|G_x|}$.
	
	A continuació, comptem el nombre de parelles $(\sigma, x)$ tals que $\sigma(x) = x$
	de dues maneres (fixant $x$ o fixant $\sigma$), obtenint:
	\[
		\sum_{\sigma \in G} |\text{fix} (\sigma)| = \sum_{x \in X} |G_x| = 
		\sum_{x \in X} \frac{|G|}{|X|} = |G|.
	\]
	Per tant, el lema se satisfà si l'acció és transitiva. Suposem ara que $G$ té
	òrbites $X_1, ..., X_t$. Atès que $G$ actua sobre $X_i$ transitivament, pel
	cas anterior es té $\sum_{\sigma \in G} |\text{fix}_i (\sigma)| = |G|$, 
	on $\text{fix}_i (\sigma)$ denota els elements de $X_i$ fixos per $\sigma$.
	Aleshores, sumant per a tot $i$:
	\[
		t |G| = \sum_{i=1}^t \sum_{\sigma \in G} |\text{fix}_i (\sigma)|
		= \sum_{\sigma \in G} \sum_{i=1}^t |\text{fix}_i (\sigma)| =
		 \sum_{\sigma \in G} |\text{fix} (\sigma)|.
	\] 
\end{proof}

En l'exemple anterior de l'acció de $G = \langle (1234) \rangle$ sobre les arestes de $\Gamma = K_4$,
si comptem les arestes fixes per a cada permutació:
\begin{center}
       \begin{tabular}{|c|c|} \hline
           $G$ & $|\text{fix}(\sigma)|$ \\\hline
           $\Id$ & $6$ \\\hline
           $(1 2 3 4)$ & $0$ \\\hline
           $(1 3) (2 4)$ & $2$ \\\hline
           $(1 4 3 2)$ & $0$ \\\hline
       \end{tabular}
\end{center}
Aplicant el lema, surten $\frac{1}{4} (6 + 0 + 2 +0) = 2$ òrbites diferents, com hem comprovat
anteriorment.

\begin{example}
	Si ara considerem l'acció de $G = \langle (1 2 3 4 5 6) \rangle$ sobre les arestes de $\Gamma = K_6$,
	podem fer una taula similar:
	\begin{center}
		   \begin{tabular}{|c|c|} \hline
			   $G$ & $|\text{fix}(\sigma)|$ \\\hline
			   $\Id$ & $15$ \\\hline
			   $(1 2 3 4 5 6)$ & $0$ \\\hline
			   $(1 3 5) (2 4 6)$ & $0$ \\\hline
			   $(1 4) (2 5) (3 6)$ & $3$ \\\hline
			   $(1 5 3) (2 6 4)$ & $0$ \\\hline
			   $(1 6 5 4 3 2)$ & $0$ \\\hline
		   \end{tabular}
	\end{center}
	Comptant les òrbites amb el lema, surten $\frac{1}{6} (15+0+0+3+0+0) = 3$ òrbites diferents. 
\end{example}

\section{Grups actuant en funcions}

\begin{defi}[coloració d'un conjunt]
	Una $r$-coloració d'un conjunt $X$ és una aplicació de $X$ a un conjunt $C$ de
	$r$ colors. Denotem per $C^X$ aquest conjunt d'aplicacions.
\end{defi}

\begin{example}
	Sigui $\Gamma$ el graf cíclic de 6 vèrtexs. Volem comptar el nombre de formes diferents
	de 2-colorejar les arestes de $\Gamma$ que no siguin equivalents respecte l'acció d'algun
	grup $G$. Per exemple, si $G = C_6$ és el grup cíclic de 6 elements, totes les coloracions
	que es puguin obtenir fent una rotació de $\Gamma$ són equivalents. Si pintem les arestes
	de color blau o vermell, podem fer el següent recompte:
	\begin{center}
		 \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
			 Arestes blaves & $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ \\\hline
			 Coloracions & $1$ & $1$ & $3$ & $4$ & $3$ & $1$ & $1$ \\\hline
		 \end{tabular}
	\end{center}
	Sota $C_6$, hi ha 14 coloracions en total. Les quatre coloracions pel cas de 3 arestes blaves 
	són les següents:
	\begin{center}
		\begin{minipage}{.2\textwidth}
			\begin{tikzpicture}
			  %% Try to define a style, to prevent typing
			  [inner sep=2mm]

			  %% Now do the circle with nodes
			  \node at ( 60:1cm){};%{2}
			  \node at (120:1cm){};%{1}
			  \node at (180:1cm){};%{6}
			  \node at (240:1cm){};%{5}
			  \node at (300:1cm){};%{4}
			  \node at (360:1cm){};%{3}

			  %% Connect those circs
			  \draw [thick, blue] (120:1cm) to ( 60:1cm);
			  \draw [thick, blue] ( 60:1cm) to (360:1cm);
			  \draw [thick, red]  (360:1cm) to (300:1cm);
			  \draw [thick, red]  (300:1cm) to (240:1cm);
			  \draw [thick, red]  (240:1cm) to (180:1cm);
			  \draw [thick, blue] (180:1cm) to (120:1cm);
			\end{tikzpicture}
		\end{minipage}
		\begin{minipage}{.2\textwidth}
			\begin{tikzpicture}
			  %% Try to define a style, to prevent typing
			  [inner sep=2mm]

			  %% Now do the circle with nodes
			  \node at ( 60:1cm){};%{2}
			  \node at (120:1cm){};%{1}
			  \node at (180:1cm){};%{6}
			  \node at (240:1cm){};%{5}
			  \node at (300:1cm){};%{4}
			  \node at (360:1cm){};%{3}

			  %% Connect those circs
			  \draw [thick, blue] (120:1cm) to ( 60:1cm);
			  \draw [thick, red] ( 60:1cm) to (360:1cm);
			  \draw [thick, red]  (360:1cm) to (300:1cm);
			  \draw [thick, blue]  (300:1cm) to (240:1cm);
			  \draw [thick, red]  (240:1cm) to (180:1cm);
			  \draw [thick, blue] (180:1cm) to (120:1cm);
			\end{tikzpicture}
		\end{minipage}
		\begin{minipage}{.2\textwidth}
			\begin{tikzpicture}
			  %% Try to define a style, to prevent typing
			  [inner sep=2mm]

			  %% Now do the circle with nodes
			  \node at ( 60:1cm){};%{2}
			  \node at (120:1cm){};%{1}
			  \node at (180:1cm){};%{6}
			  \node at (240:1cm){};%{5}
			  \node at (300:1cm){};%{4}
			  \node at (360:1cm){};%{3}

			  %% Connect those circs
			  \draw [thick, blue] (120:1cm) to ( 60:1cm);
			  \draw [thick, blue] ( 60:1cm) to (360:1cm);
			  \draw [thick, red]  (360:1cm) to (300:1cm);
			  \draw [thick, blue]  (300:1cm) to (240:1cm);
			  \draw [thick, red]  (240:1cm) to (180:1cm);
			  \draw [thick, red] (180:1cm) to (120:1cm);
			\end{tikzpicture}
		\end{minipage}
		\begin{minipage}{.2\textwidth}
			\begin{tikzpicture}
			  %% Try to define a style, to prevent typing
			  [inner sep=2mm]

			  %% Now do the circle with nodes
			  \node at ( 60:1cm){};%{2}
			  \node at (120:1cm){};%{1}
			  \node at (180:1cm){};%{6}
			  \node at (240:1cm){};%{5}
			  \node at (300:1cm){};%{4}
			  \node at (360:1cm){};%{3}

			  %% Connect those circs
			  \draw [thick, red] (120:1cm) to ( 60:1cm);
			  \draw [thick, blue] ( 60:1cm) to (360:1cm);
			  \draw [thick, red]  (360:1cm) to (300:1cm);
			  \draw [thick, blue]  (300:1cm) to (240:1cm);
			  \draw [thick, red]  (240:1cm) to (180:1cm);
			  \draw [thick, blue] (180:1cm) to (120:1cm);
			\end{tikzpicture}
		\end{minipage}
	\end{center}
	Si ara considerem $G = D_6$, el grup diedral (és a dir, que admetem rotacions i reflexions),
	observem que els dos hexàgons centrals són equivalents sota reflexió. La resta queda igual:
	\begin{center}
		 \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
			 Arestes blaves & $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ \\\hline
			 Coloracions & $1$ & $1$ & $3$ & $3$ & $3$ & $1$ & $1$ \\\hline
		 \end{tabular}
	\end{center}
	En total, hi ha 13 coloracions diferents sota $D_6$.
\end{example}

Notem que una acció d'un grup en un graf colorejat no només actua en els vèrtexs i les
arestes, sinó que també ho fa de forma natural en els colors. Concretament:

\begin{defi}
	Suposi's que $G$ actua en $X$. Per a $\sigma \in G$ i $f \in C^X$, es defineix
	$\sigma(f)$ com l'element de $C^X$ tal que:
	\[
		\sigma(f(x)) := f(\sigma^{-1}(x)).
	\]
\end{defi}

\begin{lema}
	L'aplicació $f \to \sigma(f)$ és una acció de $G$ en $C^X$.
\end{lema}
\begin{proof}
	D'una banda, $Id(f(x)) = f(Id(x)) = f(x)$. 
	De l'altra:
	\begin{align*}
		\sigma(\tau(f))(x) = \tau(f) (\sigma^{-1}) (x) = f(\tau^{-1} \sigma^{-1} (x)), \\
		(\sigma \tau) (f(x)) = f((\sigma \tau)^{-1} (x)) = f(\tau^{-1} \sigma^{-1} (x)).
	\end{align*}
\end{proof}

\begin{example}
	Considerem el següent hexagon:
	\begin{center}
	\begin{tikzpicture}
		  %% Try to define a style, to prevent typing
		  [inner sep=2mm]

		  %% Now do the circle with nodes
		  \node[above] at ( 60:1cm){2};
		  \node[above] at (120:1cm){1};
		  \node[left] at (180:1cm){6};
		  \node[below] at (240:1cm){5};
		  \node[below] at (300:1cm){4};
		  \node[right] at (360:1cm){3};

		  %% Connect those circs
		  \draw [thick, red] (120:1cm) to ( 60:1cm);
		  \draw [thick, blue] ( 60:1cm) to (360:1cm);
		  \draw [thick, red]  (360:1cm) to (300:1cm);
		  \draw [thick, blue]  (300:1cm) to (240:1cm);
		  \draw [thick, blue]  (240:1cm) to (180:1cm);
		  \draw [thick, red] (180:1cm) to (120:1cm);
		\end{tikzpicture}
	\end{center}
	Es té que $f((12)) = f((34)) = f((61)) = $vermell, i $f((23)) = f((45)) = f((56)) = $blau.
	Si considerem l'acció de $\sigma = (135)(246)$ sobre el graf, les accions sobre els
	colors queden de la següent forma:
	\begin{align*}
		&(\sigma f) ((12)) = f (\sigma^{-1} (12)) = f((56)) = \text{blau}. \\
		&(\sigma f) ((23)) = \text{vermell}. \\
		&(\sigma f) ((34)) = \text{vermell}. \\
		&(\sigma f) ((45)) = \text{blau}. \\
		&(\sigma f) ((56)) = \text{vermell}. \\
		&(\sigma f) ((61)) = \text{blau}.
	\end{align*}
	Visualment, com que després d'aplicar $\sigma$, on hi havia l'aresta $(12)$ ara hi haurà
	l'aresta $(56)$, el color de l'aresta $(12)$ passa a ser el color de l'aresta $(56)$, en
	aquest cas de vermell a blau.
\end{example}

\section{Polinomi d'índexs de cicles}

Si $G$ actua en $X$, ens podem mirar $G$ com un subgrup de $\Sym (X)$ i, per tant,
podem considerar la seva descomposició en cicles disjunts.

\begin{defi}
	Per a $\sigma \in G$, definim $c_i^X (\sigma)$ com el nombre de cicles de longitud
	$i$ en la descomposició de $\sigma$.
\end{defi}

\begin{defi}[polinomi d'índexs de cicles]
	El polinomi d'índexs de cicles és:
	\[
		Z_G^X (X_1,\dots,X_n) = \frac{1}{|G|} \sum_{\sigma \in G} \prod_{i=1}^n X_i^{c_i^X(\sigma)},
	\]
	amb $n = |X|$.
\end{defi}

\begin{example}
	Considerem l'acció de $G = C_6$ sobre les arestes del graf cíclic de 6 vèrtexs. 
	Atès que $G = \{\Id, (123456), (135)(246), (14)(25)(36), (153)(264), (165432)\}$,
	tenim que el polinomi d'índexs de cicles és:
	\[
		Z_{C_6}^X (X_1,\dots,X_6) = \frac{1}{6} (X_1^6 + X_2^3 + 2X_3^2 + 2X_6).
	\]
\end{example}

\begin{example}
	Considerem ara l'acció de $G = D_6$ sobre les arestes del mateix graf. 
	A part de les rotacions considerades a l'exemple anterior, hem d'afegir 
	les possibles reflexions:
	\begin{itemize}
		\item Podem fer una reflexió a través de dos vèrtexs oposats. En aquest cas,
		la descomposició serà $\sigma = (..)(..)(..)$, amb multiplicitat 3.
		\item Podem fer una reflexió a través dels centres de dues arestes oposades.
		En aquest cas, la descomposició serà $\sigma = (.)(.)(..)(..)$, amb multiplicitat 3.
	\end{itemize}
	Així, el polinomi d'índexs de cicles queda:
	\[
		Z_{D_6}^X (X_1,\dots,X_6) = \frac{1}{12} (X_1^6 + 2 X_3^2 + 4 X_2^3 + 2 X_6 + 3 X_1^2 X_2^2).
	\]
\end{example}

\begin{teo}[Teorema de Pólya]
	Suposi's que $G$ actua en $X$, i sigui $C$ un conjunt de $r$ colors. Aleshores,
	el nombre de $r$-coloracions de $X$ diferents sota $G$ és $Z_G^X(r,\dots,r)$.
\end{teo}
\begin{proof}
	Observem que el nombre de $r$-coloracions de $X$ diferents sota $G$ és el nombre
	d'òrbites de $G$ actuant en $C^X$.
	
	Observem també que una coloració de $X$ queda fixada per $\sigma$ si en cada cicle
	en la descomposició de $\sigma$, tot element rep el mateix color.
	Així, $|\text{fix}(\sigma)| = r^{c(\sigma)}$, sent $c(\sigma) = \sum_{i} c_i^X (\sigma)$
	el nombre total de cicles.
	
	Aplicant el lema d'enumeració d'òrbites (\ref{lema:enumOrbites}), s'obté:
	\[
		\text{coloracions} = \frac{1}{|G|} \sum_{\sigma \in G} |\text{fix}(\sigma)|
		= \frac{1}{|G|} \sum_{\sigma \in G} r^{c(\sigma)} = Z_G^X (r,\dots,r).
	\]
\end{proof}

Així, aplicant el teorema als dos exemples anteriors, s'obté que el nombre de 2-coloracions
diferents és:
\begin{align*}
	Z_{C_6} (2,2,2,2,2,2) = \frac{1}{6} (2^6 + 2^3 + 2^3 + 2^2) = 14, \\
	Z_{D_6} (2,2,2,2,2,2) = \frac{1}{12} (2^6 + 2^3 + 2^5 + 2^2 + 3\cdot 2^4) = 13,
\end{align*}
com ja havíem recomptat anteriorment.

\begin{example}
	Comptem el nombre de 2-coloracions de $K_4$ diferents sota el grup cíclic.
	D'una banda, es pot fer el recompte manual:
	\begin{center}
		 \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
			 Arestes blaves & $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ \\\hline
			 Coloracions & $1$ & $2$ & $5$ & $6$ & $5$ & $2$ & $1$ \\\hline
		 \end{tabular}
	\end{center}
	D'altra banda, podem aplicar el teorema de Pólya:
	\begin{center}
		 \begin{tabular}{|c|c|c|c|} \hline
			 $\sigma$ & multiplicitat & vèrtexs & arestes\\\hline
			 $\Id$ & 1 & (.)(.)(.)(.) & (.)(.)(.)(.)(.)(.) \\\hline
			 $r_{90}$ & 2 & (....) & (....)(..) \\\hline
			 $r_{180}$ & 1 & (..)(..) & (..)(..)(.)(.) \\\hline
			 $r_{270}$ & 2 & (....) & (....)(..) \\\hline
		 \end{tabular}
	\end{center}
	El nombre de coloracions serà:
	\[
		Z_{C_4}^X = \frac{1}{4} (X_1^6 + 2 X_4 X_2 + X_1^2 X_2^2)
		\implies Z_{C_4}^X (2,\dots,2) = 22.
	\]
\end{example}

\begin{obs}
	El teorema de Pólya es pot usar per comptar el nombre de grafs no isomorfs de $n$ vèrtexs.
	Si comptem el nombre de 2-coloracions de $K_n$ diferents sota tot $\Sym(n)$, podem interpretar
	que un dels dos colors es correspon amb posar l'aresta i l'altre color amb no posar l'aresta.
	Per exemple, en el cas $n = 4$:
	\begin{center}
		 \begin{tabular}{|c|c|c|} \hline
			 multiplicitat & vèrtexs & arestes\\\hline
			 1 & (.)(.)(.)(.) & (.)(.)(.)(.)(.)(.) \\\hline
			 6 & (....) & (....)(..) \\\hline
			 3 & (..)(..) & (..)(..)(.)(.) \\\hline
			 8 & (.)(...) & (...)(...) \\\hline
			 6 & (.)(.)(..) & (.)(.)(..)(..) \\\hline
		 \end{tabular}
	\end{center}
	El nombre de grafs no isomorfs és $\frac{1}{24} (2^6 + 6 \cdot 2^2 + 3 \cdot 2^4 +
	8 \cdot 2^2 + 6 \cdot 2^4) = 11$.
\end{obs}

\section{Les rotacions del cub}
Volem enumerar les simetries rotacionals del cub, és a dir, les rotacions que deixin invariant
la ``forma'' del cub. Tenim tres opcions per escollir l'eix de rotació:

\begin{itemize}
	\item A través de dues cares oposades. En aquest cas, podem fer o bé una rotació de $\pm 90^{\circ}$
	o de 180$^{\circ}$.
	\item A través de dos vèrtexs oposats. En aquest cas, podem fer una rotació de $\pm 120^{\circ}$.
	\item A través de dues arestes oposades. En aquest cas, podem fer una rotació de $180^{\circ}$.
\end{itemize}
Podem escriure'ns la taula de l'acció d'aquestes rotacions sobre les cares del cub:

\begin{center}
	 \begin{tabular}{|c|c|c|} \hline
		 $\sigma$ & multiplicitat & cares\\\hline
		 $\Id$ & 1 & (.)(.)(.)(.)(.)(.)  \\\hline
		 Cara, $r_{\pm 90}$ & 6 & (.)(.)(....)  \\\hline
		 Cara, $r_{180}$ & 3 & (.)(.)(..)(..)  \\\hline
		 Vèrtex, $r_{\pm 120}$ & 8 & (...)(...)  \\\hline
		 Aresta, $r_{180}$ & 6 & (.)(.)(..)(..) \\\hline
	 \end{tabular}
\end{center}

\begin{ej}
	Completeu la taula amb l'acció sobre els vèrtexs i sobre les arestes del cub.
\end{ej}

És possible comprovar que aquestes 24 simetries són totes les possibles. Per exemple,
si ens fixem en les cares, podem posar qualsevol de les 6 cares a la part superior, i després tenim
4 possibilitats per la cara frontal. La resta de cares queden determinades a partir d'aquestes dues.
Així, hi ha com a molt 24 possibilitats.

Tot seguit, ens preguntem pel nombre de 2-coloracions de les cares del cub 
invariants per qualsevol d'aquestes rotacions. Es pot fer el recompte manualment:
\begin{center}
	 \begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
		 Cas blaves & $0$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ \\\hline
		 Coloracions & $1$ & $1$ & $2$ & $2$ & $2$ & $1$ & $1$ \\\hline
	 \end{tabular}
\end{center}
Surten 10 coloracions en total. Si calculem el polinomi d'índexs de cicles:
\[
	Z_{G}^X = \frac{1}{24} (X_1^6 + 6X_1^2 X_4 + 8 X_3^2 + 6 X_2^3 + 3 X_1^2 X_2^2),
\]
podem demostrar (amb Pólya) que el nombre de $r$-coloracions és:
\[
	\frac{r^2}{24} (r+1)(r^3-r^2+4r+8).
\]
En particular, si $r=2$, l'expressió anterior dona 10, com era d'esperar.

\section{Versió general del teorema de Pólya}
El teorema de Pólya ens permet calcular el nombre de coloracions diferents sota l'acció d'un grup,
però no ens dona cap informació de quantes coloracions hi ha per a cada combinació de colors.

\begin{obs}
	Recordem que, en el càlcul dels polinomi d'índexs de cicles per les coloracions de les
	cares d'un cub, surt el següent resultat:
	\[
		Z_{G}^X = \frac{1}{24} (X_1^6 + 6X_1^2 X_4 + 8 X_3^2 + 6 X_2^3 + 3 X_1^2 X_2^2).
	\]
	Si substituïm $X_1 = t_1 + t_2$ i $X_2 = t_1^2 + t_2^2$, ens surt:
	\[
		Z_{G}^X = t_1^6 + t_1^5 t_2 + 2t_1^4 t_2^2 +2t_1^3 t_2^3 + 2t_1^2 t_2^4 + t_1 t_2^5 + t_2^6.
	\]
	Observem que el coeficient de $t_1^i t_2^{6-i}$ és exactament el nombre de coloracions del cub
	amb $i$ cares d'un cert color.
\end{obs}

Aquest resultat no és casual, sinó que és conseqüència de la generalització del teorema de Pólya.
Abans de demostrar-ho, però, definim un concepte que ens serà necessari:

\begin{defi}
	Suposem que $G$ actua en $X$, i que tenim un conjunt $C = \{c_1,\dots,c_r \}$ de colors,
	on a cada color $c_j$ li assignem una variable $h(c_j) = t_j$. Aleshores, definim la funció
	$w \colon C^X \to \z [t_1,\dots,t_r]$, satisfent:
	\[
		w(f) = \prod_{i=1}^{r} t_i^{m_i(f)},
	\]
	sent $m_i(f)$ el nombre d'elements de $X$ colorejats amb el color $i$.
\end{defi}

\begin{example}
	Considerem el graf cíclic de 6 vèrtexs, i la permutació $\tau = (124)(356)$.
	Hi ha quatre 2-coloracions fixades per $\tau$, que són:
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|c|c|c|} \hline
			 $f$ & $1$ & $2$ & $3$ & $4$ & $5$ & $6$ & $w(f)$ \\\hline
			 $f_1$ & b & b & b & b & b & b & $t_1^6$ \\\hline
			 $f_2$ & b & b & r & b & r & r & $t_1^3 t_2^3$ \\\hline
			 $f_3$ & r & r & b & r & b & b & $t_1^3 t_2^3$ \\\hline
			 $f_4$ & r & r & r & r & r & r & $t_2^6$ \\\hline
		 \end{tabular}
	 \end{center}
	 En particular, es té $\sum_{f \in \text{fix}(\tau)} w(f) = t_1^6 + 2 t_1^3 t_2^3 + t_2^6 = (t_1^3 + t_2^3)^2$.
	 Apareixen dos nombres: l'exponent de $t_1$ i $t_2$ és 3, que és la longitud dels cicles,
	 i l'exponent de la suma és 2, que és el nombre de cicles. Això tampoc és casual.
\end{example}

\begin{teo}[Teorema de Pólya generalitzat]
	Suposem que $G$ actua en $X$, i sigui $C$ un conjunt de $r$ colors. Sigui $R$ un conjunt de
	representants de les òrbites de l'acció de $G$ en $C^X$. Aleshores:
	\[
		Z_G^X (t_1 + \dots + t_r, t_1^2 + \dots + t_r^2, \dots, t_1^n + \dots + t_r^n) =
		\sum_{f \in R} w(f).
	\]
\end{teo}
\begin{proof}
	Observem que $\prod_{x \in X} h(f(x)) = w(f)$. Si $f$ i $g$ són de la mateixa òrbita
	(és a dir, $g = \sigma f$), es té:
	\[
		w(g) = \prod_{x \in X} h(g(x)) = \prod_{x \in X} h(\sigma f(x)) = \prod_{x \in X} h(f(\sigma^{-1}(x)))
		= \prod_{x \in X} h(f(x)) = w(f).
	\]
	Per tant, en considerar $w(f)$, és indiferent quin representant agafem de l'òrbita de $f$.
	Suposem ara que $\sigma \in G$ fixa $f$. Això vol dir que, considerant la descomposició en cicles
	disjunts de $\sigma$, cada element del cicle rep el mateix color. Tenint en compte que
	cada cicle pot tenir qualsevol dels $r$ colors, es té:
	\[
		\sum_{f \in \text{fix}(\sigma)} w(f) = \sum_{f \in \text{fix}(\sigma)} (t_1 + \dots + t_r)^{c_1 (\sigma)}
		(t_1^2 + \dots + t_r^2)^{c_2 (\sigma)} \cdots (t_1^n + \dots + t_r^n)^{c_n (\sigma)},
	\]
	si $\sigma$ té $c_i (\sigma)$ cicles de longitud $i$. Recordant que 
	\[
	|G|Z_G^X(X_1,\dots,X_n)	= \sum_{\sigma \in G} X_1^{c_1(\sigma)} \cdots X_n^{c_n(\sigma)},
	\]
	obtenim que:
	\[
		\sum_{\sigma \in G} \sum_{f \in \text{fix}(\sigma)} w(f) = 
		|G| Z_G^X (t_1+\dots+t_r,\dots,t_1^n + \dots + t_r^n).
	\]
	Girant ara el primer sumatori:
	\[
		\sum_{\sigma \in G} \sum_{f \in \text{fix}(\sigma)} w(f) =
		\sum_{f \in C^X} \sum_{\substack{\sigma \in G \\ \sigma(f) = f}} w(f) = 
		\sum_{f \in C^X} w(f) \frac{|G|}{|\text{orb}(f)|} = 
		\sum_{f \in R} w(f) |G|.
	\]
	Ajuntant les dues igualtats, se segueix el resultat.
\end{proof}

Una conseqüència directa del teorema és que el coeficient de $t_1^{k_1} \dots t_r^{k_r}$
ens dona el nombre de $r$-coloracions amb $k_j$ elements de color $j$.

\begin{example}
	El polinomi d'índexs de cicles de l'acció del grup rotacional sobre les cares del cub és:
	\[
		Z_{G}^X = \frac{1}{24} (X_1^6 + 6X_1^2 X_4 + 8 X_3^2 + 6 X_2^3 + 3 X_1^2 X_2^2).
	\]
	Si substituïm $X_i = t_1^i + \dots t_6^i$, i ens mirem el coeficient de $t_1t_2\cdots t_6$,
	veiem que és:
	\[
		\frac{1}{24} \binom{6}{1\colon1\colon1\colon1\colon1\colon1} = \frac{6!}{4!} = 30.
	\]
	Per tant, el nombre de 6-coloracions de les cares del cub tals que cada cara rep un color
	diferent és 30.
\end{example}
