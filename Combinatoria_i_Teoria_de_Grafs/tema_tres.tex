\chapter[Funcions generadores exponencials]{Enumeració simbòlica. Classes combinatòries etiquetades i funcions generadores exponencials}
\section{Classes combinatòries etiquetades}

\begin{defi}[classe!combinatòria etiquetada]
    Diem que una classe combinatòria $\mathcal{A}$ és etiquetada si tot objecte $\alpha \in \mathcal{A}$, amb $\abs{\alpha} = n$, està composat per $n$ àtoms distingibles. Equivalentment, podem dir que els àtoms tenen etiquetes del conjunt $\lc 1, 2, \dots, n\rc$ i que no hi ha dos àtoms amb la mateixa etiqueta. Si $\alpha \in \mathcal{A}$, aleshores $\ell \lp \alpha\rp$ denota la tupla de les etiquetes dels seus àtoms.
    
    Sovint es consideren classes combinatòriques etiquetades que tenen per objectes grafs (que tant poden ser dirigits com no dirigits). En aquest cas, els àtoms que conformen un graf són els seus vèrtexs i la mida del graf és el nombre de vèrtex.
\end{defi}
Convé considerar la classe nul·la $\varepsilon$, que conté un únic objecte de mida zero (i, per tant, sense cap etiqueta) i la classe $\mathcal{N}$, que conté un únic objecte de mida $1$ (i, per tant, amb un sol àtom d'etiqueta $1$).

\begin{example}
    \begin{enumerate}[1.]
        \item La classe $\mathcal{U}$ d'urnes (que contenen boles, per exemple). Els objectes són grafs sense arestes, de manera que $u_n = 1$.
        \item La classe $\mathcal{P}$ de permutacions. Els objectes són camins dirigits. Es té que $p_n = n!$.
        \item La classe $\mathcal{C}$ de permutacions cícliques. Els objectes són cicles dirigits. Es té que $c_n = (n-1)!$. Notarem per $\combC_k$ la classe de cicles de longitud $k$.
    \end{enumerate}
\end{example}

\section{Funcions generadores exponencials}

\begin{defi}[funció!generadora exponencial]
    La funció generadora exponencial d'una classe combinatòria $\mathcal{A}$ és
    \[
        A\lp x\rp = \sum_{\alpha\in\mathcal{A}} \frac{z^{\abs{\alpha}}}{\abs{\alpha}!} = \sum_{n\geq 0} \frac{a_n}{n!}z^n.
    \]
    Quan es treballa amb classes combinatòries etiquetades, es fan servir les funcions generadores exponencials (FGE) en comptes de les funcions generadores ordinàries (FGO).
\end{defi}

\begin{obs}
    Si $C\lp z\rp = A\lp z\rp B\lp z\rp$,
    \[
        \lb z^n\rb C\lp z\rp = \sum_{k=0}^n \frac{a_k}{k!}\frac{b_{n-k}}{\lp n-k\rp!} = \frac{1}{n!}\sum_{k=0}^{n}\binom{n}{k}a_kb_{n-k}.
    \]
    L'expressió $\sum_{k=0}^{n}\binom{n}{k}a_kb_{n-k}$ s'anomena convolució binomial de $\lp a_n\rp$ i $\lp b_n\rp$.
\end{obs}

\section{Operacions de classes combinatòries etiquetades}

\begin{defi}
    Sobre les classes combinatòries etiquetades també es poden definir les operacions de suma, producte i seqüència, i la operació addicional conjunt.
    \begin{itemize}
        \item La suma $\mathcal{A} + \mathcal{B}$ de classes combinatòries etiquetades es defineix de la mateixa manera, és a dir, com llur unió disjunta $\mathcal{A}\cup\mathcal{B}$.
        \item Per definir el producte etiquetat de dues classes combinatòries $\mathcal{A}$ i $\mathcal{B}$, la idea és associar a cada parell $\lp \alpha, \beta\rp \in \mathcal{A} \times \mathcal{B}$ el conjunt de tots els possibles objectes les etiquetes dels quals siguin \emph{coherents} amb les etiquetes $\ell \lp \alpha\rp$ i $\ell\lp \beta\rp$. Per a fer-ho, convé introduir la següent notació. Donada una tupla $\alpha=\lp a_1, \dots, a_n\rp$ de naturals diferents, $\rho\lp a\rp = \lp b_1, \dots, b_n\rp$ és l'única tupla que preserva l'odre relatiu i tal que $\lc b_1, \dots, b_n\rc = \lc 1, \dots, n\rc$.
        
            Ara, donats $\alpha \in \mathcal{A}$ i $\beta\in\mathcal{B}$, definim
            \[
                \alpha \ast \beta = \lc \lp \alpha^{\prime}, \beta^{\prime}\rp \mid \rho\lp c_1, \dots, c_{\abs{\alpha}}\rp = \ell\lp \alpha\rp, \rho\lp c_{\abs{\alpha}+1}, \dots, c_{\abs{\alpha}+\abs{\beta}}\rp = \ell\lp \beta\rp \rc,
            \]
            on
            \[
                \lp c_1, \dots, c_{\abs{\alpha}+\abs{\beta}}\rp = \ell \lp \lp \alpha^{\prime}, \beta^{\prime}\rp\rp.
            \]
            
            Finalment, podem definir el producte etiquetat de classes combinatòries com
            \[
                \mathcal{A} \ast \mathcal{B} = \bigcup_{\substack{\alpha \in \mathcal {A} \\ \beta\in \mathcal{B}}} \alpha \ast \beta.
            \]
        \item La sequència $\seq\lp\combA\rp$ d'una classe combinatòria etiquetada es defineix com
            \[
                \seq\lp\combA\rp = \varepsilon + \combA + \combA \ast \combA + \combA \ast \combA \ast \combA + \cdots.
            \]
        \item El conjunt $\Set{\combA}$ d'una classe combinatòria etiquetada es defineix com
            \[
                \Set\lp\combA\rp = \varepsilon + \combA + \faktor{\combA \ast \combA}{\sim_2} + \faktor{\combA \ast \combA \ast \combA}{\sim_3} + \cdots,
            \]
            on $\sim_n$ és una relació que identifica les $n$-tuples que només difereixen en l'ordre.
    \end{itemize}
\end{defi}

\begin{prop}
    Siguin $\combA, \combB$ i $\combC$ classes combinatòries etiquetades i siguin $A\lp x\rp, B\lp z\rp$ i $C\lp z\rp$ llurs funcions generadores exponencials, respectivament. Aleshores,
    \begin{enumerate}[(i)]
        \item Si $\combC = \combA + \combB$, aleshores $C\lp z\rp = A\lp z\rp + B\lp z\rp$.
        \item Si $\combC = \combA \ast \combB$, aleshores $C\lp z\rp = A\lp z\rp  B\lp z\rp$.
        \item Si $\combC = \seq\lp\combA\rp$, aleshores $C\lp z\rp = \frac{1}{1-A\lp z\rp}$.
        \item Si $\combC = \Set\lp\combA\rp$, aleshores $C\lp z\rp = e^{A\lp z\rp}$.
    \end{enumerate}
\end{prop}
\begin{proof}
    \begin{enumerate}[(i)]
        \item[]
        \item $ \begin{aligned}[t]
                    C \lp z\rp = \sum_{\alpha\in \combA + \combB} \frac{z^{\abs{\alpha}}}{\abs{\alpha}!} = \sum_{n\geq 0} \frac{a_n + b_n}{n!}z^n = A\lp z\rp + B\lp z\rp.
                \end{aligned}$
        \item $ \begin{aligned}[t]
                    C \lp z\rp = \sum_{\lp \alpha, \beta\rp \in \combA \ast \combB} \frac{z^{\abs{\lp \alpha, \beta\rp}}}{\abs{\lp \alpha, \beta\rp}!} = \sum_{n\geq 0} \lp \sum_{k=0}^n \binom{n}{k}a_kb_{n-k}\rp z^n = A\lp z\rp  B\lp z\rp.
                \end{aligned}$
        \item $C\lp z\rp = 1 + A\lp z\rp + A\lp z\rp ^2 + \cdots = \frac{1}{1-A\lp z\rp}$.
        \item $\displaystyle C\lp z\rp = 1 + A\lp z\rp + \frac{A\lp z\rp^2}{2!} + \frac{A\lp z\rp^3}{3!} + \cdots = e^{A\lp z\rp}$.
    \end{enumerate}
\end{proof}


\begin{example}
    \begin{enumerate}[1.]
        \item[]
        \item La classe $\mathcal{U}$ d'urnes té per funció generadora exponencial
            \[
                U\lp z\rp = \sum_{n\geq 0} \frac{z^n}{n!} = e^z.
            \]
        \item La classe $\mathcal{P}$ de permutacions té per funció generadora exponencial
            \[
                P\lp z\rp = \sum_{n\geq 0} \frac{n!}{n!}z^n = \frac{1}{1-z}.
            \]
        \item La classe $\mathcal{C}$ de permutacions cícliques té per funció generadora exponencial
            \[
                C\lp z\rp = \sum_{n\geq 1} \frac{(n-1)!}{n!}z^n = \sum_{n\geq 1} \frac{z^n}{n} = \log\lp \frac{1}{1-z}\rp.
            \]
    \end{enumerate}
\end{example}

\section{Exemples}
\subsection{Permutacions}

Alternativament al que acabem de veure, podem entendre les permutacions com a productes de cicles disjunts, on l'ordre no importa. D'aquesta manera, $\mathcal{P} = \Set\lp \combC\rp$ i
\[
    P\lp z\rp = e^{C\lp z\rp} = e^{\log \lp \frac{1}{1-z}\rp} = \frac{1}{1-z}.
\]
\begin{defi}[desarranjament]
    Anomenem desarranjament a una permutació sense punts fixos. Notem per $\mathcal{D}$ la classe de desarranjaments.
\end{defi}
Observem que una permutació és un desarranjament si, i només si, la seva descomposició cicles disjunts no en conté cap de longitud $1$. Per tant,
\[
    \mathcal{D} = \Set\lp \combC \setminus \combC_1\rp,
\]
Tenim, doncs, que
\[
    D\lp z\rp = e^{\log\lp \frac{1}{1-z}\rp - z} = \frac{1}{1-z} e^{-z}.
\]
A més a més,
\begin{gather*}
    d_n = n!\lb z^n\rb D\lp z\rp = n!\lb z^n\rb \lp \lp 1 + z + z^2 + \cdots \rp \lp 1 - z + \frac{\lp -z\rp ^2}{2!} + \frac{\lp -z\rp ^3}{3!} + \cdots\rp \rp  = \\
    = n! \sum_{k=0}^n \frac{\lp-1\rp ^k}{k!} \sim \frac{n!}{e}.
\end{gather*}

\begin{defi}[involució]
    Anomenem involució a una permutació $\sigma$ que satisfaci que $\sigma^2 = \Id$. Notem per $\mathcal{I}$ la classe d'involucions.
\end{defi}
Observem que una permutació és una involució si, i només si, la seva descomposició en cicles disjunts està formada únicament per cicles de longitud $1$ o $2$. Per tant,
\[
    \mathcal{I} = \Set\lp \combC_1 + \combC_2\rp.
\]
Tenim, doncs, que
\[
    I\lp z\rp = e^{z+\frac{z^2}{2}}.
\]
A més a més,
\begin{gather*}
    i_n = n!\lb z^n\rb \lp e^z e^{\frac{z^2}{2}}\rp = n!\lb z^n\rb \lp \lp 1 + z + \frac{z^2}{2!} + \cdots\rp \lp 1 + \frac{z^2}{2} + \frac{\lp \frac{z^2}{2}\rp ^2}{2!} + \cdots\rp \rp = \\
    = n! \sum_{k=0}^{\left\lfloor\frac{n}{2}\right\rfloor} \frac{1}{k!\lp n-2k\rp ! 2^k}.
\end{gather*}
Notem per $\mathcal{P}^{\lp k\rp}$ la classe de permutacions que descomposen en $k$ cicles disjunts. Es té que
\[
    \mathcal{P}^{\lp k\rp} = \faktor{\overbrace{\combC \ast \cdots \ast \combC}^k}{\sim_k}
\]
i que
\[
    P^{\lp k\rp} \lp z\rp = \frac{1}{k!} C\lp z\rp ^k = \frac{1}{k!} \lp \log \frac{1}{1-z}\rp ^k.
\]
Per tant, el nombre de permutacions d'$n$ elements que descomposen en $k$ cicles disjunts és
\begin{gather}
    p^{\lp k\rp}_n = n! \lb z^n\rb \lp \frac{1}{k!}\lp \log \frac{1}{1-z}\rp^k \rp = \frac{n!}{k!} \lb z^n\rb \lp \lp z + \frac{z^2}{2} + \frac{z^3}{3} + \cdots \rp^k \rp = \stirlingi{n}{k},                        
\end{gather}
on $\stirlingi{n}{k}$ és el nombre d'Stirling de primera espècie.

\begin{defi}[nombres!de Stirling]
Els nombres de Stirling de primera espècie es defineixen com
\[
    \stirlingi{n}{k} = \lb x^k\rb \lp x \lp x+1\rp \cdots \lp x+n-1\rp \rp.
\]
\iffalse
i els de segona espècie es defineixen com
\[
    \stirlingii{n}{k} = \lb x^k\rb \lp x \lp x-1\rp \cdots \lp x-\lp n-1\rp \rp \rp,
\]
\fi
\end{defi}
Es pot comprovar que coincideixen amb el resultat anterior.
\begin{obs}
\[
    \sum_{k=1}^n \stirlingi{n}{k} = n!.
\]
\end{obs}

Notem per $\mathcal{E}$ la classe de permutacions que descomposen en un nombre parell de cicles. Es té que
\[
    \mathcal{E} = \varepsilon + \faktor{\combC \ast \combC}{\sim_2} + \faktor{\combC \ast \combC \ast \combC \ast \combC}{\sim_4} + \cdots
\]
i que
\[
    E\lp z\rp = \sum_{k\geq 0} \frac{C\lp z\rp ^2k}{\lp 2k\rp !}.
\]

Notem per $\mathcal{C}_{even}$ la classe de permutacions que descomposen en cicles de longitud parella. Es té que
\[
    C_{even} = \sum_{k\geq 1}\frac{z^{2k}}{2k} = \frac{C\lp z\rp + C\lp -z\rp}{2} = \frac{1}{2}\lp \log\lp \frac{1}{1-z}\rp + \log \lp \frac{1}{1+z}\rp \rp = \frac{1}{2}\log\lp \frac{1}{1-z^2}\rp.
\]

\subsection{Particions d'un conjunt}
\begin{defi}[partició!d'un conjunt]
    Donat un conjunt finit $X$, diem que $\lc X_1, \dots, X_k\rc$ és una partició d'$X$ si els conjunts $X_i$ són disjunts dos a dos i llur unió és $X$. Sense pèrdua de generalitat, podem considerar que $X=\lc 1, \dots, n\rc$, és a dir, només ens importa el cardinal del conjunt. Notem per $\mathcal{P}$ la classe de particions.
\end{defi}
Es té que
\[
    \mathcal{P} = \Set\lp \mathcal{U} \setminus \mathcal{U}_0\rp,
\]
on $\mathcal{U}_0 = \varepsilon$ és la classe de l'urna buida. Per tant,
\[
    P\lp z\rp = e^{U\lp z\rp - 1} = e^{e^z - 1} = \sum_{n\geq 1} \frac{B_n}{n!}z^n.
\]
Els coeficients $B_n$ s'anomenen nombres de Bell. $B_n$ és, per tant, el nombre de particions d'un conjunt d'$n$ elements. Recordem que els nombres d'Stirling de segona espècie $\stirlingii{n}{k}$ calculen el nombre de particions d'un conjunt d'$n$ elements en $k$ conjunts. Així doncs,
\[
    B_n = \sum_{k=1} ^n \stirlingii{n}{k}.
\]
Tornant a la funció generadora exponencial,
\begin{gather*}
    B_n = n!\lb z^n\rb e^{e^z - 1} = \frac{n!}{e}\lb z^n\rb e^{e^z} = \frac{n!}{e}\lb z^n\rb \lp \sum_{k\geq 0} \frac{\lp e^z\rp^k}{k!} \rp = \\
    = \frac{n!}{e}\lb z^n\rb \lp \sum_{k\geq 0} \frac{1}{k!} \sum_{m\geq 0} \frac{\lp kz\rp ^m}{m!} \rp = \frac{n!}{e} \lp \sum_{k\geq 0} \frac{k^n}{k!n!} \rp = \frac{1}{e} \sum_{k\geq 0} \frac{k^n}{k!}.
\end{gather*}

Notem per $\mathcal{P}_k$ la classe de particions en $k$ parts. Es té que
\[
    \mathcal{P}_k = \faktor{\overbrace{\lp \mathcal{U} \setminus \mathcal{U}_0\rp \ast\cdots\ast \lp \mathcal{U} \setminus \mathcal{U}_0\rp}^k}{\sim_k}
\]
i que
\[
    P_k\lp z\rp = \frac{1}{k!}\lp e^z - 1\rp ^k = \frac{1}{k!} \sum_{n=0}^k \binom{k}{n}e^{mz}\lp -1\rp ^{k-m}.
\]
En particular,
\[
    P_2\lp z\rp = \frac{1}{2}\lp e^z-1\rp^2 = \frac{1}{2}\lp e^{2z}-2e^z+1\rp
\]
i
\[
    \lp p_2\rp_n = \stirlingi{n}{2} = n!\lb z^n\rb \lp \frac{1}{2}\lp e^{2z} - 2e^z + 1\rp\rp = \frac{n!}{2}\lp \frac{2^n}{n!} - \frac{2}{n!}\rp = 2^{n-1}-1.
\]

    
\subsection{Arbres arrelats etiquetats}
\begin{defi}[arbre!etiquetat arrelat]
    Un arbre arrelat etiquetat és una arbre els nodes del qual són distingibles (és a dir, estan etiquetats) i amb un node destacat, que anomenem arrel. Notem per $\mathcal{T}$ la classe d'arbres arrelats etiquetats.
\end{defi}
Es té que
\[
    \mathcal{T} = \mathcal{N}\ast\Set\lp \mathcal{T}\rp
\]
i que
\[
    T\lp z\rp = z e^{T\lp z\rp}.
\]
Aplicant la fórmula d'inversió de Lagrange,
\[
    t_n = n!\lb z^n\rb T\lp z\rp = \frac{n!}{n}\lb t^{n-1}\rb\lp e^t\rp^n = \lp n-1\rp! \lb t^{n-1}\rb \lp \sum_{k\geq 0} \frac{\lp nt\rp^k}{k!} \rp= n^{n-1}.
\]
