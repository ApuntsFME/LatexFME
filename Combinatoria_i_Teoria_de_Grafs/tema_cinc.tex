\chapter{Connectivitat de grafs}
\section{Connectivitat per vèrtexs}
\begin{defi}[graf!connex]
    Diem que un graf és connex si tot parell de vèrtexs està connectat per un camí. Per convenció, un vèrtex està connectat a si mateix (per un camí de longitud nul·la).
\end{defi}
\begin{defi}[component!connex d'un graf]
    Anomenem component connex d'un graf a un subgraf connex maximal.
\end{defi}
\begin{obs}
    Tot graf és unió disjunta dels seus components connexos.
\end{obs}
\begin{defi}
    Sigui $G=\lp V, E\rp$ un graf i sigui $X\subseteq V$. Anomenem subgraf de $G$ induït per $X$ a 
    \[
        G\lb X\rb = \lp X, \lc \lc a, b\rc \in E \mid a, b\in X\rc\rp.
    \]
\end{defi}
\begin{prop}
    Un graf $G=\lp V, E\rp$ és connex si, i només si, existeix una ordenació $\lp v_1, \dots, v_n\rp$ dels vèrtexs tal que $G\lb \lc v1, \dots, v_i\rc\rb$ és connex per a tot $1\leq i\leq n$. En particular, $G$ és connex si, i només si, conté un arbre d'expansió.
\end{prop}
\begin{proof}
    La implicació inversa és trivial; vegem la directa. Començant amb un vèrtex qualsevol $v_1$, hom pot definir $v_{i+1}$ com el primer vèrtex de $V\setminus \lc v_1,\dots, v_i\rc$ connectat a $G\lb \lc v_1, \dots, v_i\rc\rb$, que existeix en virtut de la connexió de $G$. L'arbre d'expansió es construeix naturalment a partir d'aquesta tria de $v_i$.
\end{proof}

\section{Estructura de grafs $k$-connexos}
\begin{defi}[graf!$k$-connex]\idx{connectivitat!d'un graf}
    Diem que un graf $G=\lp V, E\rp$ és $k$-connex si $\abs{V}\geq k+1$ i $G\lb V\setminus X\rb$ és connex per a tot $X\subset V$ tal que $\abs{X} < k$. Anomenem connectivitat del graf $G$ al màxim $k$ tal que $G$ és $k$-connex i la notem per $\kappa\lp G\rp$.
\end{defi}
\begin{defi}[separador d'un graf]
    Sigui $G=\lp V, E\rp$ un graf i sigui $S\subseteq V$. Diem que $S$ és un separador si $G\lb V\setminus S\rb$ és no connex.
\end{defi}
\begin{obs}
    Sigui $G$ un graf no complet. El separador $S$ de $G$ de cardinal mínim té cardinal $\abs{S} = \kappa\lp G\rp$
\end{obs}
\begin{defi}[separació d'un graf]
    Sigui $G=\lp V, E\rp$ un graf, sigui $S$ un separador de $G$ i sigui $C$ un component connex de $G\lb V\setminus S\rb$. Anomenem separació de $G$ al parell $\lc G_1, G_2\rc$, on $G_1 = G\lb C\cup S\rb$ i $G_2 = G\lb V\setminus\lp C\cup S\rp\rb$.
\end{defi}
\begin{obs}
    Si $\lc G_1, G_2\rc$ és un separador d'un graf $G$, $G=G_1\cup G_2$ i totes les arestes de $G$ pertanyen, o bé a $G_1$, o bé a $G_2$.
\end{obs}
\begin{defi}[vèrtex de tall]
    Sigui $G=\lp V, E\rp$ un graf. Diem que $v\in V$ és un vertex de tall si $\lc v\rc$ és un separador.
\end{defi}
\begin{defi}[bloc d'un graf]
    Anomenem blocs d'un graf $G$ als subgrafs connexos i sense vèrtexs de tall de $G$ que són maximals.
\end{defi}
\begin{prop}
    Dos blocs intersequen en, com a molt, un vèrtex, que és de tall.
\end{prop}
\begin{proof}
    Si els blocs $B_1$ i $B_2$ tenen més d'un vèrtex en comú, $B_1\cup B_2$ és un bloc.
\end{proof}
\begin{col}
    Tot cicle està completament contingut en un bloc.
\end{col}
\begin{obs}
    Els blocs d'un graf són, necessàriament, d'alguna d'aquestes formes
    \begin{enumerate}[(i)]
        \item Un vèrtex aïllat.
        \item Dos vèrtexs units per una aresta.
        \item Un subgraf $2$-connex maximal.
    \end{enumerate}
\end{obs}
\begin{prop}
    Sigui $G$ un graf connex i sigui $A$ el conjunt de vèrtexs de tall de $G$. Considerem el graf bipartit $B\lp G\rp$ els conjunts de vèrtexs del qual són $V_1=A$ i $V_2=\lc B\subseteq G \mid B \text{ és un bloc de } G\rc$ i que té arestes $E=\lc \lc x, B\rc \mid x\in B\rc$. $B\lp G\rp$ és un arbre.
\end{prop}
\begin{proof}
    $B\lp G\rp$ és connex perquè ho és $G$ i si hi hagués algun cicle contradiria el fet que $V_2$ és el conjunt de blocs de $G$.
\end{proof}
\begin{teo*}
    Tot graf $2$-connex es pot construir partint d'un cicle i afegint, successivament, un camí (que pot contenir nous vèrtexs) entre dos vèrtexs existents.
\end{teo*}
\begin{proof}
    Suposem que $G$ és $2$-connex i no satisfà el teorema. Considerem un subgraf maximal $H$ de $G$ que es pugui construir d'aquesta manera. $H\neq G$ perquè hem suposat que $G$ no satisfà el teorema i $H$ és no buit perquè $G$ conté un cicle. Nogensmenys, com que $G$ és $2$-connex, existeix un vèrtex de $G$ que no és a $H$ i que es pot connectar a $H$ a través de dos camins que no comparteixen cap altre vèrtex. Això contradiu la maximalitat d'$H$.
\end{proof}
\begin{defi}[contracció d'una aresta]
    Sigui $G$ un graf i sigui $e$ una aresta de $G$. La contracció de $e$ consisteix a identificar els dos extrems d'$e$ i les arestes múltiples que crea aquesta identificació. El graf resultant es denota per $G/e$.
\end{defi}
\begin{prop}
    Sigui $G=\lp V, E\rp$ un graf $3$-connex de més de $4$ vèrtexs. Aleshores, existeix alguna aresta $e\in E$ tal que $G/e$ és $3$-connex.
\end{prop}
\begin{proof}
    Suposem que $G/e$ no és $3$-connex per a tota aresta $e$. Aleshores, existeix algun separador $S$ de $G/e$ de mida menor o igual a $2$. Notem per $v_{xy}$ el vèrtex de $G/e$ que resulta d'identificar els veïns $x$ i $y$ a $G$ ($e = xy$). Si $v_{xy}$ no pertany a $S$, aleshores $S$ és un separador de $G$, la qual cosa suposa una contradicció. Així doncs, $v_{xy}\in S$. Si $S=\lc v_{xy}\rc$, aleshores $\lc x, y\rc$ és un separador de $G$, la qual cosa també suposa una contradicció. Per tant, $S=\lc v_{xy}, u\rc$.
    
    D'entre totes les tripletes de vèrtexs $\lc x, y, u\rc$ en triem una tal que el menor component connex de $G/xy \setminus \lc v_{xy}, u\rc$ sigui de mida mínima, i anomenem $C$ a aquest component. Com que $\lc v_{xy}, u\rc$ és un separador minimal, $u$ és veí d'algun vèrtex $v\in C$. Necessàriament, tots els veïns de $v$ diferents de $x, y$ i $u$ pertanyen a $C$.
    
    Considerem, ara $G/uv$. Existeix algun vèrtex $w$ tal que $\lc v_{uv}, w\rc$ és separador de $G/uv$. Sigui $D$ un dels components connexos de $G/uv \setminus \lc v_{uv}, w\rc$ que no conté ni $x$ ni $y$. Sabem que $v$ ha de tenir un veí $d$ a $D$ i que aquest pertany a $C$. Com que tots els vèrtexs de $D$ són accessibles des de $d$ a través de camins que no passen per $x, y, u$ o $v$, $D\subsetneq C$, la qual cosa suposa una contradicció.
\end{proof}

\begin{teonom}[Teorema de Tutte]
	\label{teo:TutteT5}
    Un graf $G$ és $3$-connex si, i només si, existeix una seqüència $G_0, G_1, \dots, G_n = G$ tal que 
    \begin{enumerate}[(i)]
        \item $G_0 = K_4$,
        \item $G_i = G_{i+1}/xy$ per a alguns $x, y \in G_{i+1}$ tals que $d\lp x\rp, d\lp y\rp \geq 3$.
    \end{enumerate}
\end{teonom}
\begin{proof}
    La implicació directa és immediata a partir de la proposició anterior. Per veure la inversa, demostrarem que, si $G_i$ és $3$-connex, també ho és $G_{i+1}$. Suposem, doncs, que $G_i = G_{i+1}/xy$ és $3$-connex i que $G_{i+1}$ no ho és. Sigui $S$ un separador de $G/xy$ amb $\abs{S}\leq 2$. Observem que $S\neq \lc x, y\rc$ perquè, altrament, $v_{xy}$ seria un vèrtex de tall de $G_i$ i que $S\cap\lc x, y\rc\neq\varnothing$ perquè, altrament, $S$ seria un separador de $G_{i+1}$. Podem suposar, doncs, que $x\in S$ i que $y\notin S$. Considerem el component connex de $G_{i+1}$ que conté $y$. Aquest component no pot contenir un segon vèrtex $z$ perquè, si fos així, $S\setminus \lc x\rc \cup \lc v_{xy}\rc$ seria un separador de cardinal menor que $3$ de $G_i$. Per tant, $d\lp y\rp \leq 2$, la qual cosa suposa una contradicció.
\end{proof}

\section{Teorema de Menger}
\begin{defi}[$AB$-separador]\idx{$AB$-connector}
    Sigui $G=\lp V, E\rp$ un graf i siguin $A, B\subseteq V$ no buits. Diem que $S\subseteq V$ és un $AB$-separador si a $G\lb V\setminus S\rb$ no hi ha cap camí entre un vèrtex d'$A$ i un de $B$. Anomenem $AB$-connector a la unió de camins disjunts que uneixen $A$ i $B$ i que no tenen vèrtexs interiors a $A\cup B$.
\end{defi}
\begin{obs}
    Qualssevol $AB$-separador $S$ i $AB$-connector $C$ satisfan que $\abs{S}\geq \abs{C}$, atès que cal eliminar, almenys, un vèrtex de cadascun dels camins per a separar $A$ i $B$.
\end{obs}
\begin{teonom}[Teorema local de Menger]\label{teo:local_menger}
    Sigui $G=\lp V, E\rp$ un graf i siguin $A, B\subseteq V$. Aleshores, el mínim $AB$-seprador té el mateix cardinal que el màxim $AB$-connector.
\end{teonom}
\begin{proof}
    Sigui $S$ un $AB$-separador mínim i sigui $C$ un $AB$-connector màxim. Basta veure que $\abs{S}\leq \abs{C}$. Procedirem per inducció sobre el nombre d'arestes de $G$, $\abs{E}$. Si $\abs{E}=0$, aleshores $A\cap B$ és un $AB$-connector màxim i un $AB$-separador mínim. Si $\abs{E}\geq 0$, aleshores existeix alguna aresta $e=\lc x, y\rc \in E$, i sigui $s$ el cardinal d'un $AB$-separador mínim de $G$. El graf $G^{\prime} = \lp V, E\setminus \lc e\rc\rp$ satisfà l'enunciat en virtut de la hipòtesi d'inducció. Si un $AB$-separador mínim a $G^{\prime}$ té cardinal $s$, hem acabat perquè un connector a $G^{\prime}$ és un connector a $G$. Suposem que $S^{\prime}$ és un $AB$-separador a $G^{\prime}$ amb $\abs{S^{\prime}}\leq s$. Com que $S_x = S^{\prime} \cup \lc x\rc$ (o, alternativament, $S_y = S^{\prime}\cup\lc y\rc$) és un $AB$-separador a $G$, tenim que $\abs{S^{\prime}} = \abs{S_x} - 1 \geq s-1$, de manera que $\abs{S^{\prime}} = s-1$ i $\abs{S_x} = \abs{S_y} = s$. Sigui $S_1$ un $AS_x$-separador a $G^{\prime}$ i sigui $S_2$ un $BS_y$-separador a $G^{\prime}$. Observem que $S_1$ és un $AB$-separador a $G$, atès que tot camí entre $A$ i $B$ passa per $S_x$ i, per tant, passa també per $S_1$. Anàlogament, $S_2$ és un $AB$-separador a $G$, i tenim que $\abs{S_1}, \abs{S_2} \geq s$. En virtut de la hipòtesi d'inducció, existeixen un $AS_x$-connector $C_1$ a $G^{\prime}$ i un $BS_y$-connector $C_2$ a $G^{\prime}$ de cardinal $s$. Els $s$ camins de $C_1$ comencen a $A$ i acaben a $S_x$, i els $s$ camins de $C_2$ comencen a $B$ i acaben a $S_y$. Com que $x$ i $y$ estan units per $e$ a $G$, podem concatenar cada camí de $C_1$ amb un camí de $C_2$, obtenint un $AB$-connector de cardinal $s$.
\end{proof}

\begin{teonom}[Teorema global de Menger]\label{teo:global_menger}
    Sigui $G=\lp V, E\rp$ un graf amb $\abs{V}\geq k+1$. Aleshores, $G$ és $k$-connex si, i només si, tot parell de vèrtexs està unit per $k$ camins internament disjunts.
\end{teonom}
\begin{proof}
    La implicació inversa és immediata, vegem la directa. Siguin $x, y \in V$ i siguin $A$ i $B$ els conjunts de veïns de $x$ i de $y$, respectivament. Com que $G$ és $k$-connex, els $AB$-separadors tenen, almenys, $k$ vèrtexs. Per tant, existeix un $AB$-connector de $k$ camins disjunts, que determina un conjunt de $k$ camins internament disjunts entre $x$ i $y$.
\end{proof}
\begin{col}
    Per tot graf $G$ es té que $\kappa\lp G\rp \leq \delta\lp G\rp$, on $\delta\lp G\rp$ és el mínim dels graus dels vèrtexs de $G$.
\end{col}
\begin{teonom}[Teorema de Mader]
    Sigui $G=\lp V, E\rp$ un graf tal que la mitjana dels graus dels seus vèrtexs és, almenys, $4k$. Aleshores, $G$ conté un subgraf $k$-connex.
\end{teonom}
\begin{proof}
    Si $k\in \lc 0, 1\rc$, l'afirmació és trivialment certa. Pel cas general, demostrarem una versió més forta del teorema: si $k\geq 2$ i $G=\lp V, E\rp$ és un graf que satisfà que
    \begin{enumerate}[(i)]
        \item $n\geq 2k-1$,
        \item $m\geq \lp 2k-3\rp\lp n-k+1\rp +1$,
    \end{enumerate}
    on $n=\abs{V}$ i $m=\abs{E}$, aleshores $G$ conté un subgraf $k$-connex. En efecte, es tracta d'una versió més forta del teorema perquè, si la mitjana dels graus és, almenys, $4k$,
    \begin{enumerate}[(i)]
        \item $2\frac{n\lp n-1\rp}{2} \geq 4kn \implies n\geq 4k+1 \implies n\geq 2k-1$,
        \item $m \geq \frac{4kn}{2} = 2kn-1\geq \lp 2k-3\rp\lp n-k+1\rp +1$.
    \end{enumerate}
    Procedirem per inducció dobre $n$. Si $n=2k-1$,
    \[
        m \geq \lp 2k-3\rp \lp n-k+1\rp + 1 = \lp n-2\rp \lp n-\frac{n+1}{2}+1\rp+1 = \frac{n\lp n-1\rp}{2}
    \]
    i $G=K_n\supseteq K_{k+1}$. Suposem, doncs, que $n\geq 2k$. Si existeix algun vèrtex $v\in V$ de grau $d\lp v\rp \leq 2k-3$, el graf $G^{\prime} = G\lb V \setminus \lc v \rc \rb$ satisfà que $m^{\prime} \geq m - \lp 2k-3\rp \geq \lp 2k-3\rp \lp \lp n-1\rp -k +2\rp +1$, de manera que $G^{\prime}$ conté algun subgraf $k$-connex en virtut de la hipòtesi d'inducció. Suposem, doncs, que tots els vèrtexs tenen grau major o igual a $2k-2$ i que $G$ no és $k$-connex. Existeix un separador $S$ de $G$ de menys de $k$ vèrtexs. Podem considerar una separació $\lc G_1, G_2\rc$ de $G$ produïda per $S$. Com que els vèrtexs de $G_1$ que no són a $S$ tenen, almenys, $2k-2$ veïns i tots són a $G_1$, $n_1\geq 2k-1$. Anàlogament, $n_2\geq 2k-1$. Com que $n\geq 2k$, podem aplicar la hipòtesi d'inducció tant a $G_1$ com a $G_2$. Si cap dels dos conté un subgraf $k$-connex, aleshores
    \[
        m_1 \leq \lp 2k-3\rp \lp n_1-k+1\rp
    \]
    i, anàlogament,
    \[
        m_2 \leq \lp 2k-3\rp \lp n_2-k+1\rp.
    \]
    Però
    \[
        m \leq m_1 + m_2 \leq \lp 2k-3\rp \lp n_1+n_2-2k+2\rp \leq \lp 2k-3\rp \lp n-k+1\rp,
    \]
    la qual cosa suposa una contradicció. Per tant, o bé $G_1$ o bé $G_2$ contenen un subraf $k$-connex.
\end{proof}

\begin{defi}[sistema!comú de representants distints]
    Sigui $X$ un conjunt i siguin $A_1, \dots, A_m, B_1, \dots, B_m \subseteq X$. Anomenem sistema comú de representants distints (SCRD) a un conjunt $\lc x_1, \dots, x_m\rc \subseteq X$ tal que $x_i \in A_i \cap B_{\sigma\lp i\rp}$, per a certa $\sigma \in S_m$.
\end{defi}

\begin{teonom}[Teorema de Ford-Fulkerson]
    Sigui $X$ un conjunt i siguin $A_1, \dots, A_m, B_1, \dots, B_m \subseteq X$. Existeix un sistema comú de representants distints si, i només si,
    \[
        \abs{A\lp I\rp \cap B\lp J\rp} \geq \abs{I} + \abs{J} - m,
    \]
    per a qualssevol $I, J \subseteq \lc 1, \dots, m\rc$.
\end{teonom}
\begin{proof}
    Comencem per la implicació directa. Sabem que existeix un conjunt \newline $\lc x_1, \dots, x_m\rc \subseteq X$ tal que $x_i \in A_i \cap B_{\sigma\lp i\rp}$, per a certa $\sigma \in S_m$. Considerem conjunts $I, J\subseteq \lc 1, \dots, m\rc$ qualssevol i prenem un $i\in I\cap \sigma^{-1}\lp J\rp$. Tenim que $x_i \in A_i \cap B_{\sigma\lp i\rp} \subseteq A\lp I\rp \cap B\lp J\rp$. Finalment,
    \[
        \abs{A\lp I\rp \cap B\lp J\rp} \geq \abs{I \cap \sigma^{-1}\lp J\rp} = \abs{I} + \abs{\sigma^{-1}\lp J\rp} - \abs{I \cup \sigma^{-1}\lp J\rp} \geq \abs{I} + \abs{J} - m.
    \]
    Vegem, ara, la implicació inversa. Considerem el graf $G=\lp V, E\rp$ que té vèrtexs
    \[
        V = X \cup \lc A_1, \dots, A_m\rc \cup \lc B_1, \dots, B_m\rc
    \]
    i que té arestes
    \[
        E = \lc \lc x, A_i\rc \mid x\in A_i\rc \cup \lc \lc x, B_i\rc \mid x\in B_i\rc.
    \]
    Que existeixi un sistema comú de representants distints és equivalent al fet que existeixin $m$ camins disjunts d'$A$ a $B$ al graf $G$. Per tant, en virtut del teorema local de Menger (\ref{teo:local_menger}) cal veure que tots els $AB$-separadors tenen cardinal almenys $m$. Sigui $S$ un $AB$-separador de $G$ i considerem els conjunts
    \[
        I=\lc i\in \lc 1, \dots, m\rc \mid A_i \notin S\rc
    \]
    i
    \[
        J=\lc j\in \lc 1, \dots, m\rc \mid B_j \notin S\rc.
    \]
    Com que $S$ separa $A$ de $B$, qualsevol $x\in A\lp I\rp \cap B\lp J\rp$ pertany, necessàriament, a $S$. Per tant, fent ús de la hipòtesi,
    \begin{gather*}
        \abs{S} \geq \abs{A\lp I\rp \cap B\lp J\rp} + \overbrace{\abs{\lc A_i \mid A_i \in S\rc}}^{m-\abs{I}} + \overbrace{\abs{\lc B_i \mid B_i \in S\rc}}^{m-\abs{J}} = \\
        = \abs{A\lp I\rp \cap B\lp J\rp} + 2m - \abs{I} - \abs{J} \geq m.
    \end{gather*}
\end{proof}

\section{Connectivitat per arestes}
\begin{defi}[graf!$k$-aresta-connex]\idx{connectivitat!per arestes}
    Diem que un graf $G=\lp V, E\rp$ és $k$-aresta-connex si, per tot $S\subseteq E$ amb $\abs{S}<k$ , $G^{\prime}=\lp V, E\setminus S\rp$ és connex. Anomenem connectivitat per arestes del graf $G$ al màxim $k$ tal que $G$ és $k$-aresta-connex i la notem per $\lambda\lp G\rp$.
\end{defi}
\begin{lema}
    Sigui $G$ un graf. Es té que
    \[
        \kappa\lp G\rp \leq \lambda\lp G\rp \leq \delta\lp G\rp.
    \]
\end{lema}
\begin{teo*}
    Un graf $G=\lp V, E\rp$ és $k$-aresta-connex si, i només si, tota parell de vèrtexs està connectat per $k$ camins que no comparteixen arestes.
\end{teo*}
\begin{proof}
    La implicació inversa és immediata, vegem la directa. Considerem el graf $L\lp G\rp$ que té per vèrtex les arestes de $G$ i on existeix una aresta entre dos vèrtexs si llurs corresponents arestes a $G$ comparteixen un vèrtex. Un tall d'arestes a $G$ és un tall de vèrtexs a $L\lp G\rp$, de manera que $L\lp G\rp$ és $k$-connex. En virtut del teorema global de Menger (\ref{teo:global_menger}), tot parell de vèrtexs a $L\lp G\rp$ està connectat per $k$ camins disjunts, que es tradueixen a camins que no comparteixen arestes a $G$.
\end{proof}
