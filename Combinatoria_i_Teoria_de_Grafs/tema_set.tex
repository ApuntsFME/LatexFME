\chapter{Planaritat}

\section{Preliminars topològics}
Començarem aquest capítol amb algunes propietats topològiques dels grafs planars.
No les demostrarem totes, ja que algunes de les demostracions serien més pròpies d'un curs
de topologia que no pas de teoria de grafs. El lector està convidat a explorar-les pel seu compte,
bé sigui amb els apunts oficials de l'assignatura o pel seu compte.

\begin{defi}[graf!planar]
	Un graf $G = (V,E)$ és planar si existeix una aplicació $\phi \colon V \to \real^2$ i, per a
	cada aresta $e = xy$, existeix també una corba simple (i.e. que no té autointerseccions) 
	$\phi_e \colon [0,1] \to \real^2$, de manera que $\phi_e(0) = \phi(x)$, $\phi_e(1) = \phi(y)$ i 
	$\Im \phi_e \cap \Im \phi_{\tilde e} \subseteq e \cap \tilde{e}$.
	
	En aquest context, una cara d'un graf planar és una component arc-connexa de $\real^2 \setminus G$.
\end{defi}

En altres paraules, un graf és planar si es pot dibuixar al pla sense que les arestes es creuin.
Addicionalment, es pot suposar que les arestes són rectes:

\begin{lema}
	Si $G$ és planar, existeix una immersió de $G$ en el pla on totes les arestes són línies rectes.
\end{lema}

No ho demostrarem. Tot seguit, enunciem un parell de resultats que poden ser interessants
però que tampoc demostrarem, i després demostrarem la fórmula d'Euler.

\begin{teo}[Teorema de la corba de Jordan]
	Una corba simple i tancada divideix $\real^2$ en dues components connexes,
	una d'elles fitada i l'altre no fitada.
\end{teo}

\begin{prop}
	Si $G$ és planar i 2-connex, aleshores tota cara de $G$ (en alguna immersió de $G$ en el pla)
	està delimitada per un cicle de $G$.
\end{prop}

\begin{teo}[Fórmula d'Euler]
	Sigui $G$ un graf planar connex, amb $n$ vèrtexs, $m$ arestes i $f$ cares. Aleshores:
	\[
		n-m+f = 2.
	\]
\end{teo}
\begin{proof}
	Ho farem per inducció en $m$. El cas base és $m = n-1$, quan $G$ és un arbre. En aquest cas,
	hi ha una sola cara, l'exterior. Així doncs, se satisfà la fórmula:
	\[
		n-m+f = n-(n-1) + 1 = 2.
	\]
	Suposem que $m > n-1$. En aquest cas, seleccionem una aresta $e$ d'algun cicle de $G$.
	Aquesta aresta separa dues cares, així que quan la treiem, $G \setminus e$ té una aresta
	menys i una cara menys (les dues cares ja no queden separades). Se segueix el resultat per inducció.
\end{proof}

\begin{col}
	Si $n > 2$, tot graf planar satisfà $m \leq 3n-6$. Si, addicionalment, el graf és bipartit, se satisfà $m \leq 2n-4$.
\end{col}
\begin{proof}
	Tota aresta és incident a dues cares, i tota cara és incident, com a mínim, a tres arestes. Per tant, $2m \geq 3f$.
	Així,
	\[
		6 = 3n-3m+3f \leq 3n-3m+2m = 3n-m \implies m \leq 3n-6.
	\]
	Si el graf és bipartit, la condició és que $2m \geq 4f$, i el raonament és anàleg.
\end{proof}

En particular, $K_5$ té $10 > 3 \cdot 5 - 6$ arestes, i per tant no pot ser planar. De forma similar,
$K_{3,3}$ tampoc ho és: té $9 > 2 \cdot 6 - 4$ arestes.

\section{Teorema de Kuratowski}

\begin{defi}[subdivisió d'una aresta]
	Donat un graf $G$, una subdivisió d'una aresta $e = xy$ del graf és la substitució de l'aresta
	per un camí entre $x$ i $y$ on tots els vèrtexs tenen grau 2, i una subdivisió de $G$ és un graf obtingut
	subdivint algunes de les seves arestes. 
	
	En aquest context, direm que $G$ és un menor topològic de $H$ si $H$ conté un subgraf que és una subdivisió de $G$,
	i escriurem $G \mt H$.
\end{defi}

Podem pensar que una subdivisió és, senzillament, posar vèrtexs al mig de les arestes. És clar, per exemple,
que si un graf no és planar, qualsevol subdivisió seva tampoc ho és i, per tant, qualsevol graf planar
no pot contenir cap subdivisió de $K_5$ o de $K_{3,3}$. De fet, això és suficient:

\begin{teo}[Teorema de Kuratowski]
	Un graf és planar si, i només si, no conté cap subdivisió de $K_5$ o de $K_{3,3}$ (o, alternativament,
	$K_5, K_{3,3} \not \mt G$).
\end{teo}

La demostració es farà a través de dos lemes:

\begin{lema}
	Si $G$ és 3-connex i $K_5, K_{3,3} \not \mt G$, aleshores $G$ és planar. A més, $G$ admet una
	immersió planar convexa (i.e. totes les cares són polígons convexos, llevat potser de la cara exterior,
	el complement de la qual és un polígon convex).
\end{lema}
\begin{proof}
	Ho farem per inducció en $n = |V(G)|$. Per $n = 4$, l'únic cas és $G = K_4$, en el cas l'enunciat és cert.
	
	Suposem que $n > 4$. Per un teorema de Tutte (\ref{teo:TutteT5}), existeix una aresta $e = xy$ tal
	que $G' = G/e$ és 3-connex. $G'$ no conté $K_5, K_{3,3}$ com a menors topològics. Per tant, per la
	hipòtesi d'inducció, $G'$ és planar, i admet una immersió planar convexa.
	
	Sigui $v_{xy}$ el vèrtex de $G'$ obtingut contraient $x$ i $y$. Com que $G \setminus v_{xy}$ és
	2-connex, $v_{xy}$ es troba a l'interior d'un cicle $C$. Siguin $x_1,\dots,x_k$ els vèrtexs del cicle,
	numerats en sentit horari, i sigui $N_G(y)$ els veïns de $y$ en $G$. Considerem 3 casos:
	\begin{enumerate}
		\item $N_G (y) \subseteq C \cap [x_i,x_{i+1}]$ per a cert $i$ (mòdul $k$). En aquest cas,
		identificant $v_{xy}$ amb $x$ i posant $y$ dins la cara determinada per $v_{xy},x_{i},x_{i+1}$
		i ajuntant-lo amb tots els seus veïns s'obté una immersió planar convexa.
		\item $|N_G(x) \cap N_G (y)| \geq 3$ (i no estem al primer cas). Siguin $z_1,z_2,z_3$ veïns
		comuns de $x$ i $y$. Aleshores, aquests 5 vèrtexs formen una subdivisió de $K_5$, en contradicció
		amb les hipòtesis.
		\item $|N_G(x) \cap N_G (y)| \leq 2$ (i no estem al primer cas). Llavors, com que $y$ ha de tenir 
		algun veí $z$ en un segment $x_i x_{i+1}$ i un altre veí $z'$ en un segment
		diferent. Aleshores, $x,z,z';y,x_i,x_{i+1}$ és una subdivisió de $K_{3,3}$.
	\end{enumerate}
	A la figura següent es mostren els tres casos. En vermell, es mostren les arestes de les subdivisions,
	i les línies ratllades indiquen que és una aresta subdividida.
	\begin{center}
		\begin{minipage}{0.3\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
				\path (0,0) node (x) {$x$}
				(2,0) node (x1) {$x_1$}
				(1,-1.732) node (x2) {$x_2$}
				(-1,-1.732) node (x3) {$x_3$}
				(-2,0) node (x4) {$x_4$}
				(-1,1.732) node (x5) {$x_5$}
				(1,1.732) node (x6) {$x_6$}
				(1,0.5) node (y) {$y$}
				(1.333,1.154) node (a) {}
				(1.667,0.577) node (b) {};
				
				\draw
				(x) -- (x1)
				(x) -- (x2)
				(x) -- (x3)
				(x) -- (x4)
				(x) -- (x5)
				(x) -- (x6)
				(x1) -- (x2)
				(x2) -- (x3)
				(x3) -- (x4)
				(x4) -- (x5)
				(x5) -- (x6)
				(x6) -- (x1)
				(x) -- (y)
				(y) -- (x1)
				(y) -- (a)
				(y) -- (b);
				
			\end{tikzpicture}
		\end{minipage}
		\hspace{7mm}
		\begin{minipage}{0.3\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
				\path (0,0) node (x) {$x$}
				(2,0) node (x1) {$x_1$}
				(1,-1.732) node (x2) {$x_2$}
				(-1,-1.732) node (x3) {$x_3$}
				(-2,0) node (x4) {$x_4$}
				(-1,1.732) node (x5) {$x_5$}
				(1,1.732) node (x6) {$x_6$}
				(4,0) node (y) {$y$};
				
				\draw
				
				(x) -- (x3)
				(x) -- (x4)
				(x) -- (x5);
				
				\draw[color=red,dashed,thick]
				(x2) -- (x3)
				(x3) -- (x4)
				(x4) -- (x5)
				(x5) -- (x6);
				
				\draw[color=red]
				(x) -- (x1)
				(x) -- (x2)
				(x) -- (x6)
				(x1) -- (x2)
				(x6) -- (x1)
				(y) -- (x1)
				(y) -- (x2)
				(y) -- (x6)
				(y) to[out=150,in=30] (x);
				
			\end{tikzpicture}
		\end{minipage}
		
		\vspace{4mm}
		\begin{minipage}{0.3\textwidth}
			\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
				\path (0,0) node (x) {$x$}
				(2,0) node (x1) {$x_1$}
				(1,-1.732) node (x2) {$x_2$}
				(-1,-1.732) node (x3) {$x_3$}
				(-2,0) node (x4) {$x_4$}
				(-1,1.732) node (x5) {$x_5$}
				(1,1.732) node (x6) {$x_6$}
				(4,0) node (y) {$y$}
				(1.5,-0.866) node (a) {};
				
				\draw
				(x) -- (x3)
				(x) -- (x4)
				(x) -- (x5);
				
				\draw[color=red,dashed,thick]
				(x2) -- (x3)
				(x3) -- (x4)
				(x4) -- (x5)
				(x5) -- (x6);
				
				\draw[color=red]
				(x1) -- (x2)
				(x6) -- (x1)
				(x) -- (x1)
				(x) -- (x2)
				(x) -- (x6)
				(y) -- (x6)
				(y) -- (a)
				(y) to[out=150,in=30] (x);
				
			\end{tikzpicture}
		\end{minipage}
	\end{center}
\end{proof}

\begin{lema}
	Si $G$ té un nombre maximal d'arestes satisfent que $K_5, K_{3,3} \not \mt G$, aleshores
	$G$ és 3-connex (o $|V(G)| \leq 3$).
\end{lema}
\begin{proof}
	%Aquesta demo és un xurro
	Ho farem per inducció en $n = |V(G)|$. El cas base per $n = 4$ és $G = K_4$, que és 3-connex.
	
	Suposem que $n > 4$, i sigui $S$ un conjunt separador mínim de $G$, de manera que el graf quedi separat
	en dues components $G_1,G_2$. Siguin $x \in V(G_1) \setminus V(G_2)$, $y \in V(G_2) \setminus V(G_1)$, que no
	estiguin connectats. Si afegim l'aresta $xy$ a $G$, per maximalitat, ens apareixerà un menor topològic 
	$X \in \{K_5,K_{3,3}\}$.
	
	Buscant una contradicció, suposarem que $|S| \leq 2$. Com que $X$ és 3-connex (en els dos casos), es té
	que tots els vèrtexs de $X$ (els ``originals'', no necessàriament els obtinguts per subdivisió)
	han d'estar continguts en una de les dues components, suposem que $G_1$. Es deixa al lector comprovar aquest detall.
	
	Llavors, separem per casos:
	\begin{itemize}
		\item Si $|S| = 0$, aleshores la subdivisió de les arestes no pot passar per $xy$, i llavors es té
		$X \mt G_1 \subseteq G$, que contradiu les hipòtesis.
		\item Si $|S| = 1$, $S = \{ u\}$, suposem que una subdivisió d'una de les arestes de $X$ passa per $xy$.
		Aquest camí, quan torni de $G_2$ a $G_1$, ha de passar necessàriament per $u$. Així, substituïnt el camí
		$x \to y \to u$ per un camí en $G_1$ de $x \to u$, es tindrà $X \mt G_1 \subseteq G$, contradicció.
		\item Si $|S| = 2$, $S = \{u,v \}$, tenim dos casos depenent de si $uv \in E(G)$ o no. Si $uv \notin E(G)$,
		aleshores $G + uv$ conté $X$ com a menor topològic. No obstant, si la subdivisió de $X$ conté $uv$, podem substituir
		l'aresta $uv$ per un camí en $G$ agafant un vèrtex $w \in G_2$ i reemplaçant $uv$ per $u \to w \to v$. Per tant,
		$X \mt G$, contradicció.
		
		En conseqüència, podem suposar que $uv \in E(G)$. Aleshores, $G_1$ i $G_2$ són maximals sense contenir $K_5$ ni $K_{3,3}$ i,
		per inducció, són 3-connexos (o petits), i pel lema anterior són planars. Escollim una immersió del pla de manera que
		$uv$ caigui en la cara exterior de $G_1$, i agafem $x$, $y$ en $G_1$ i $G_2$ en les cares externes dels grafs respectius.
		Suposem que en $X$, el camí que passa per $y$ torna a $G_1$ a través de $v$. Aleshores, substituint $xy$ per
		$x \to u \to y$ obtenim un nou camí que no conté $xy$. Aleshores, $X \mt G$, contradicció.
	\end{itemize}
\end{proof}

Així doncs, els dos lemes impliquen el teorema de Kuratowski, ja que podem assumir sense pèrdua de generalitat
que el graf té un nombre maximal d'arestes.

\section{Immersions}

Considerem els dos grafs de la figura següent:
\begin{center}
	\begin{minipage}{0.4\textwidth}
		\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
			\path (0,0) node (a) {}
			(2,0) node (b) {}
			(1,1.732) node (c) {}
			(3,1.732) node (d) {}
			(1,0.7) node (e) {};
			
			\draw
			(a) -- (b)
			(b) -- (c)
			(a) -- (c)
			(c) -- (d)
			(b) -- (d)
			(c) -- (e);
			
		\end{tikzpicture}
	\end{minipage}
	\begin{minipage}{0.4\textwidth}
		\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
			\path (0,0) node (a) {}
			(2,0) node (b) {}
			(1,1.732) node (c) {}
			(3,1.732) node (d) {}
			(-0.5,1.732) node (e) {};
			
			\draw
			(a) -- (b)
			(b) -- (c)
			(a) -- (c)
			(c) -- (d)
			(b) -- (d)
			(c) -- (e);
			
		\end{tikzpicture}
	\end{minipage}
\end{center}
En termes únicament de vèrtexs i arestes, són isomorfs, però observem que en el graf de la dreta la cara exterior
conté tots 5 vèrtexs, cosa que no passa en cap cara del graf de l'esquerra. Per tant, en termes intuïtius, aquestes dues
immersions en el pla haurien de ser ``diferents''. Formalitzem aquest concepte:
\begin{defi}
	Dues immersions de dos grafs planars $G_1$ i $G_2$ són equivalents si existeix una bijecció
	$\phi \colon V(G_1) \to V(G_2)$ que preservi incidències d'arestes i cares.
\end{defi}

\begin{teo}[Teorema de Whitney]
	Si $G$ és 3-connex, aleshores $G$ admet com a molt una immersió al pla (llevat d'equivalència).
\end{teo}

\noindent Ho demostrarem a través del següent lema:

\begin{lema}
	Sigui $G$ un graf 3-connex. Un cicle $C$ de $G$ és una cara en una immersió de $G$ en el pla
	si, i només si, $C$ és induït (és a dir, no té cordes) i és no separant ($G \setminus C$ és connex).
\end{lema}
\begin{proof}
	Suposem que $C$ és la frontera d'una cara. Siguin $x,y$ dos vèrtexs no consecutius de $C$ units
	per una corda. Aquesta corda ha de passar ``per fora'' del cicle, fet que impedeix que hi hagi
	3 camins diferents unint dos vèrtexs en components diferents de $C \{x,y \}$, contradient que
	$G$ sigui 3-connex.
	\begin{center}
		\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
			\path
			(2,0) node (x1) {$x$}
			(1,-1.732) node (x2) {}
			(-1,-1.732) node (x3) {}
			(-2,0) node (x4) {}
			(-1,1.732) node (x5) {$y$}
			(1,1.732) node (x6) {};
			
			\draw
			(x6) -- (x1)
			(x1) -- (x2)
			(x2) -- (x3)
			(x3) -- (x4)
			(x4) -- (x5)
			(x5) -- (x6)
			(x1) to[out=60,in=45] (x5);
			
		\end{tikzpicture}
	\end{center}
	Quant a la propietat de no ser separant, considerem dos vèrtexs qualssevol $u,v$. Com que $G$ és 3-connex,
	existeixen tres camins disjunts que els uneixen. Considerant la unió d'aquests tres camins, $C$ pot estar
	delimitada com a molt per dos d'aquests camins, i per tant $C$ no separa $u$ i $v$.
	
	El recíproc es deixa com a exercici.
\end{proof}

A partir d'aquest lema, la demostració del teorema de Whitney és immediata: tot isomorfisme de grafs preserva
cicles induïts i no-separants, i per tant ha de preservar també les cares.
