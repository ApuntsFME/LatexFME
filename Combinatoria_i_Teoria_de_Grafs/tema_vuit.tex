\chapter{Coloració}

\section{Coloració de vèrtexs}

\begin{defi}[coloració!de vèrtexs]
	Sigui $G$ un graf. Una coloració de vèrtexs és una aplicació $c \colon V(G) \to \{1, \dots, k\}$,
	per a algun cert $k$ enter positiu.
	
	La coloració es diu pròpia si els dos extrems de tota aresta del graf tenen colors diferents.
\end{defi}

Si no s'especifica el contrari, sempre que parlem d'una coloració en aquest tema farem referència
a una coloració pròpia.

\begin{defi}[nombre cromàtic]
	El nombre cromàtic $\chi(G)$ d'un graf és el mínim nombre de colors necessaris per a obtenir
	una coloració pròpia.
\end{defi}

Així, per exemple, un graf és bipartit si, i només si, $\chi(G) = 2$. Tampoc és difícil veure que el
nombre cromàtic d'un arbre és 2, o que si $C_n$ és el cicle de $n$ vèrtexs, $\chi(C_n)$ és 2 si $n$ és parell
i 3 si $n$ és senar (o 1 si $n = 1$).

Un teorema important, però que no demostrarem aquí a causa de la seva complexitat, és el teorema dels quatre
colors, que afirma que si $G$ és planar, aleshores $\chi(G) \leq 4$; és a dir, tot graf planar es pot pintar amb com
a molt 4 colors.

Donem tot seguit algunes fites pel nombre cromàtic $\chi(G)$:

\begin{prop}
	Si $G$ té $n$ vèrtexs, aleshores:
	\[
		\chi(G) \geq \max\left\{ \omega(G),  \frac{n}{\alpha(G)}\right\},
	\]
	on $\omega(G)$ és el nombre de clique del graf (la mida del subgraf complet més gran de $G$),
	i $\alpha(G)$ és el nombre de coclique del graf (la mida del subgraf estable - sense arestes -
	més gran de $G$ o, alternativament, el nombre de clique del complementari de $G$).
\end{prop}
\begin{proof}
	D'una banda, si tenim un subgraf complet de $\omega(G)$ vèrtexs, és clar que necessitem pintar-los
	tots d'un color diferent per a obtenir una coloració pròpia.
	
	D'altra banda, el subgraf format pels vèrtexs pintats d'un cert color és un graf estable, sense arestes.
	Aquests grafs poden tenir, com a molt, $\alpha(G)$ vèrtexs, de manera que $n \leq \alpha(G) \chi(G)$, d'on
	se segueix el resultat.
\end{proof}

\begin{teo}[Teorema de Szekeres-Wilf]
	\label{teo:Szekeres-Wilf}
	Per a tot graf $G$, se satisfà
	\[
		\chi(G) \leq 1 + \max_{H \subseteq G} \delta(H),
	\]
	on $\delta(H)$ és el mínim grau del subgraf $H$.
\end{teo}
\begin{proof}
	Construïrem una coloració del graf que satisfaci la fita. Ho farem amb un algorisme voraç (\textit{greedy}):
	ordenarem els vèrtexs $v_1,\dots,v_n$, acolorirem $v_1$ amb 1 i, un cop haguem acolorit $v_i$,
	acolorirem $v_{i+1}$ amb el mínim color disponible que mantingui que $G[v_1,\dots,v_{i+1}]$ estigui
	pròpiament acolorit.
	
	Falta ordenar els vèrtexs. Escollirem $v_n$ tal que $d(v_n) = \delta(G)$ (és a dir, amb mínim grau).
	Un cop escollit $v_{n-i}$, escollirem $v_{n-i-1}$ tal que en el subgraf $H = G[v_1,\dots,v_{n-i-1}]$,
	$d_H(v_{n-i-1}) = \delta(H)$. D'aquesta manera, quan fem la coloració amb l'algorisme voraç, a cada moment
	tindrem prohibits com a molt $\delta(G[v_1,\dots,v_i])$ colors, de manera que podem satisfer la fita superior.
\end{proof}

Observem que si $G = K_n$, la fita s'assoleix. També s'assoleix la fita si $G = C_{2n+1}$. Justament aquests dos
grafs seran les excepcions pel següent teorema:

\begin{teo}[Teorema de Brooks]
	Suposem que $G \neq K_n, C_{2n+1}$ és un graf connex. Llavors,
	\[
		\chi(G) \leq \Delta(G),
	\]
	on $\Delta(G)$ representa el màxim grau de $G$.
\end{teo}
\begin{proof}
	Ho farem en diferents casos:
	\begin{enumerate}[i)]
		\item Suposem que $G$ és 3-connex. Com que $G \neq K_n$, existeix $x$
		amb dos veïns $z,z'$ no adjacents entre ells. Utilitzarem l'algorisme voraç anterior posant $v_1 = z$
		i $v_2 = z'$, de manera que tots dos quedaran acolorits amb el color 1. Escollim també $v_n = x$, i
		un cop hem escollit $v_{i+1}$, escollim $v_i$ tal que tingui una aresta amb algun dels $v_{i+1}, \dots v_n$
		(tal aresta existeix perquè $G$ és 3-connex: si eliminem $z,z'$, el graf continua sent connex, i per tant
		ha d'existir alguna aresta entre els conjunts $v_3,\dots,v_i$ i $v_{i+1},\dots,v_n$).
		
		Amb aquesta ordenació, sempre que volguem acolorir $v_i$ ($i < n$), tindrem com a molt $\Delta-1$
		colors prohibits. En el cas de $v_n = x$, com que els seus veïns $z,z'$ tenen el mateix color,
		també té com a molt $\Delta-1$ colors prohibits. Per tant, existeix una coloració amb $\Delta$ colors com a molt.
		
		\item Si $G$ és 1-connex però no 2-connex, existeix $x$ tal que $G \setminus \{ x\}$ no és connex.
		Siguin $G_1, \dots, G_m$ les components connexes de $G \setminus \{ x\}$. Per inducció, podem fer una coloració
		de $G_i \cup \{ x\}$ (si són cicles senars o grafs complets han de tenir grau menor que $\Delta$).
		Permutant els colors, podem suposar que $x$ sempre rep el color 1. Per tant, ajuntant les coloracions,
		$G$ és $\Delta$-colorable.
		
		\item Si $G$ és 2-connex però no 3-connex, existeixen $x,y$ tal que $G \setminus \{x,y\}$ no és connex.
		Com abans, existeixen components connexes $G_1,\dots,G_m$. La idea serà acolorir $G_i \cup \{x_i,y_i \}$,
		on $x_i,y_i$ són còpies de $x,y$, i ajuntar les coloracions. Si $d(x_i), d(y_i) < \Delta -1$ per a tot $i$,
		podem aconseguir coloracions de manera que $x_i$ i $y_i$ rebin colors diferents (suposem que són 1 i 2). Ajuntant
		totes les components, ja ho tindríem.
		
		L'únic cas en el qual no es dona això és quan $m=2$ i $d(x_1) = d(y_1) = \Delta-1$ i $x_1,y_1$ sempre
		reben el mateix color. En aquest cas, $d(x_2) = d(y_2) = 1$, i per tant podem permutar els colors en $G_2$
		per aconseguir que $x_2$ i $y_2$ tinguin el mateix color. En aquest cas, també podem ajuntar les dues coloracions.
	\end{enumerate}
\end{proof}

\section{Grafs planars}

Tot seguit, particularitzem alguns dels nostres resultats de coloració al cas de grafs planars.
Com ja s'ha esmentat anteriorment, tot graf planar és 4-colorable, però la demostració requereix comprovar
molts casos i no la farem aquí. No obstant, podem obtenir resultats més febles amb els coneixements que tenim.

Recordem que tot graf planar de $n$ vèrtexs té, com a molt, $3n-6$ arestes. Així doncs, hi ha d'haver algun
vèrtex que tingui grau estrictament inferior a 6, ja que:
\[
	\frac{1}{n} \sum_{x \in V} d(x) \leq \frac{2(3n-6)}{n} < 6.
\]
Així, $\delta(G) \leq 5$ per a tot graf planar $G$, i pels resultats anteriors deduïm que $\chi(G) \leq 6$.
Podem millorar aquesta fita:

\begin{teo*}
	Si $G$ és planar, aleshores $\chi(G) \leq 5$.
\end{teo*}
\begin{proof}
	Suposarem sense pèrdua de generalitat que $G$ és maximalment planar. Farem la demostració per inducció
	en $n = V(G)$. Els casos base es poden comprovar fàcilment (per exemple, si $n = 4$, $G = K_4$, que és
	4-colorable).
	
	Suposem doncs que $n > 4$. Si $\delta(G) \leq 4$, pel teorema de Szekeres-Wilf (\ref{teo:Szekeres-Wilf}) ja haurem acabat.
	Per tant, suposem que existeix $x \in V(G)$ amb grau $d(x) = 5$. Per inducció, podem acolorir $G \setminus \{x \}$
	amb 5 colors. Podem suposar que els veïns de $x$ reben els 5 colors diferents, ja que altrament només caldria
	assignar a $x$ el color que falta per completar la coloració. 
	\begin{center}
		\begin{tikzpicture}[every node/.style={draw,shape=circle, inner sep = 1pt}]
			\path
			(0,2) node (x1) {$x_1$}
			(-1.902,0.618) node (x2) {$x_2$}
			(-1.1756,-1.618) node (x3) {$x_3$}
			(1.1756,-1.618) node (x4) {$x_4$}
			(1.902,0.618) node (x5) {$x_5$}
			(0,0) node (x) {$x$};
			
			\draw
			(x) -- (x1)
			(x) -- (x2)
			(x) -- (x3)
			(x) -- (x4)
			(x) -- (x5)
			(x1) -- (x2)
			(x2) -- (x3)
			(x3) -- (x4)
			(x4) -- (x5)
			(x5) -- (x1);
			
			\draw[color=red]
			(0,2) arc(36:288:2.35107);
			
		\end{tikzpicture}
	\end{center}
	Sigui $x_i$ el veí de $x$ que rep el color $i$, $1 \leq i \leq 5$. Considerem el subgraf format pels vèrtexs
	 de color 1 i 3, $G'[1,3]$. Considerem dos casos:
	\begin{itemize}
		\item Si $x_1$ i $x_3$ estan en components connexes diferents de $G'[1,3]$, canviem tots els colors en una
		de les components connexes: per exemple, en la que conté $x_1$ canviem el color 1 pel 3 i viceversa. En aquest
		cas, podem assignar el color 1 a $x$ i seguir tenint una coloració pròpia.
		\item Si $x_1$ i $x_3$ estan en la mateixa component connexa de $G'[1,3]$, aleshores $x_2$ i $x_4$ estan en components
		connexes diferents (com es pot deduir de la figura), i podem aplicar el raonament anterior.
	\end{itemize}
	En qualsevol cas, obtenim una coloració pròpia amb 5 colors de $G$.
\end{proof}

\begin{defi}[graf!dual]
	Donat un graf planar $G = (V,E,F)$, el seu graf dual és $G^* = (F,E^*)$, on $E^*$ és la incidència entre cares.
\end{defi}
En altres paraules, els vèrtexs del graf dual són les cares del graf planar, i dos vèrtexs del graf dual
comparteixen una aresta si les dues cares corresponents són incidents en el graf original.

\begin{prop}
	Sigui $G$ un graf planar 2-connex. Llavors, $G$ és bipartit si, i només si, totes les cares tenen un nombre parell
	d'arestes (i.e. tots els vèrtexs del graf dual tenen grau parell).
\end{prop}
\begin{proof}
	Si $G$ és bipartit, tots els cicles de $G$ són parells, però tota cara en un graf 2-connex està delimitada per un cicle.
	
	En el cas recíproc, observem que tot cicle conté dins seu diferents cares. No és difícil veure que si dues cares amb un 
	nombre parell d'arestes són incidents, quan eliminem l'aresta que uneix ambdues cares resulta una sola cara amb un 
	nombre parell d'arestes. Si fem això repetidament amb totes les arestes que són dins del cicle, ens acaba quedant
	que el cicle té un nombre parell d'arestes.
\end{proof}

\begin{teo*}
	Sigui $G$ un graf maximal planar. Aleshores, $\chi(G) = 3$ si, i només si, $G$ és Eulerià.
\end{teo*}
\begin{proof}
	Suposem que $\chi(G) = 3$. Si agafem un vèrtex $v$ numerat amb 1, tots els seus veïns han d'estar numerats
	amb 2 i 3. Aleshores, els veïns de $v$ formen un cicle (com que $G$ és maximal planar, totes les cares són triangles;
	en particular les incidents a $x$ - feu un dibuix si no ho veieu). El cicle necessàriament ha de tenir longitud parella,
	de manera que $d(v)$ és parell. Per tant, $G$ és Eulerià.
	
	La implicació recíproca es deixa com a exercici.
\end{proof}

Finalment, enunciem un resultat que no demostrarem:
\begin{teo}[Teorema de Grötzsch]
	Si $G$ és planar i sense triangles, aleshores $\chi(G) \leq 3$.
\end{teo}


