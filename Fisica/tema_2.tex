
\chapter{Lleis de conservació}
 A priori, el problema del moviment queda resolt.
 A partir de les forces i la massa, utilitzo la segona llei de Newton per arribar a un sistema d'EDO's, i amb l'ajuda d'unes condicions inicials arribo a una solució.

\defi Les \textbf{lleis de conservació} proporcionen el mètode d'extreure informació del moviment sense haver d'integrar el sistema d'EDO's. Aquestes estableixen que certes magnituts, sota certes condicions, es mantenen constants al llarg del moviment.
 
\section{Llei de conservació del moment lineal}

\defi S'anomena \textbf{moment lineal (o quantitat de moviment)} d'una partícula respecte un sistema de referència $S$ qualsevol:
\[
\vec{p} = m \vec{v},
\]on $\vec{v}$ és el vector velocitat segons $S$.
 A més:
 \[
 {(\frac{d\vec{p}}{dt})}_S = \vec{F} + \vec{F}^{\text{inèrcia}}.
\]

\defi Si tenim un sistema de $N$ partícules, per moment lineal del sistema s'entén
\[
\vec{P} = \sum_{i=1}^n \vec{p}_i = \sum_{i=1}^n m_i \vec{v}_i.
\]
\teo[Llei de conservació del moment lineal] En qualsevol sistema de referència $S$ 
\[
{(\frac{d\vec{P}}{dt})}_S = \vec{F}^{\text{ext}} + \vec{F}^{\text{ine}}. 
\] on $\vec{F}^{\text{ext}} = \sum_i \vec{F}^{\text{ext}}_i$ suma de forces externes que actuen sobre la partícula $i$ i l'altre terme és la suma de forces d'inèrcia que actuen sobre la partícula $i$.

\proof En efecte, apliquem-ne la segona llei de Newton a cada partícula $i$:
\[
 {(\frac{d\vec{p}_i}{dt})}_S = \vec{F}^{\text{ext}}_i + \vec{F}^{\text{ine}}_i + \sum_{j\neq i} \vec{F}_{j\to i},
\] on $\vec{F}_{j\to i}$ és la força que fa $j$ sobre $i$.
Sumant:
\[
{(\frac{d\vec{P}}{dt})}_S = \sum_i {(\frac{d\vec{p}_i}{dt})}_S = \sum_i \vec{F}^{\text{ext}}_i + \sum_i \vec{F}^{\text{ine}}_i + \sum_{i=1}^N \sum_{j\neq i} \vec{F}_{j\to i}, 
\] 
observem que $(a_{ij}) = \vec{F}_{j\to i}$ és una matriu $N \times N$ antisimètrica per la tercera llei de Newton, aleshores $\sum_{j\neq i} \vec{F}_{j\to i} = 0$.
\prop (Corol·lari)
La variació de $\vec{P}$ d'un sistema entre $t_1, t_2$:
\[
\triangle \vec{P}(t_1,t_2) = \vec{P}(t_2) - \vec{P}(t_1) = \int_{t_1}^{t_2} \vec{F}^{\text{ext}}(t)dt + \int_{t_1}^{t_2} \vec{F}^{\text{ine}}(t)dt.
\]
 \obs A l'integrar vectors respecte $t$ hi ha implícit un sistema de referència $S$. Si $\{\h{u}_x, \h{u}_y, \h{u}_z \}$ base associada.
 \[
 \int_{t_1}^{t_2} A(t)dt = (\int_{t_1}^{t_2} A_x(t)dt) \h{u}_x + ...
 \]
 \prop (Corol·lari) Si $S$ sistema de referència inercial aleshores \[
 \vec{P} = \text{cte.} \iff \vec{F}^{\text{ext}} = 0,
 \] ja que en un sistema de referència inercial $\vec{F}^{\text{ine}} = 0.$
 
 \obs 
 \begin{enumerate}
     \item[]
     \item Es conserva $\vec{P}$ però no $\sum_{i=1}^{N} \vec{v}_i$.
     \item El que es conserva és $\vec{P}$, no cada $\vec{p}_i$ per separat; de fet, la interacció entre les partícules del sistema es pot interpretar com que estan "intercanviant" el moviment lineal.
     \item Si $\h{u}$ direcció qualsevol, pot passar que $\vec{F}^{\text{ext}} \neq 0$ però $\vec{F}_u^{\text{ext}} = 0 \implies P_u = 0$.
     \item La llei de conservació de $\vec{P}$ és invariant per transformacions galil·leanes.
     Si $\vec{v}_i$ vectors velocitat segons $S$ i $\vec{v}'_i$ vectors velocitat segons $S'$ i $\vec{P} = \sum_i m_i v_i = \text{cte.}$ aleshores $\vec{P}' = \sum_i m_i v_i' = \text{cte.}$, ja que:
     \[
     \vec{v}_i = \vec{v}_{O'} + \vec{v}'_i.
     \]
 \end{enumerate}
 \newpage
\section{Xocs}
Situació general des d'un sistema de referència inercial: 
 \begin{figure}[!ht]
 \centering
  \includegraphics[width=0.5\textwidth]{diagramesAmalia/xocs.png}
  \caption{$m_1, m_2$ s'apropen i no sotmeses a forces externes fins "xocar".}
 \end{figure}
 Hi ha 3 fases:
\begin{itemize}
    \item Fase I: $m_1, m_2$ són lluny l'una de l'altra i es mouen segons rectes $r_1, r_2$ amb velocitats constants $v^i_1, v^i_2$.
    \item Fase II (fase de xoc): interaccionen les dues partícules provocant canvis en la trajectòria i en les velocitats.
    \item Fase III: Tornen a ser lluny l'una de l'altra i a moure's seguint rectes $s_1, s_2$ amb velocitats $v^f_1, v^f_2$.
\end{itemize}
\defi Angle de dispersió de $m_1$: 
\[
\phi_1 = \text{angle}(\h{r_1 s_1}) = \text{angle}(\vec{v}^i_1, \vec{v}^f_1).
\] Si $m_2$ té $\vec{v_2^i} = 0$, l'angle de resposta és:
\[
\alpha = \text{angle} (\vec{v}^i_2, \vec{v}^f_2).
\]
\defi Energia cinètica d'una partícula:
\[
K := \frac{1}{2} m v^2 \quad \text{ (depèn de S). }
\] En el cas d'un sistema de $N$ partícules:
\[
K := \sum^N_{i=1} \frac{1}{2} m_i v_i^2 \quad \text{ (depèn de S). }
\]
\defi Tipus de xocs:
\begin{itemize}[(a)]
	\item \textbf{Xocs elàstics}, si $\triangle K = K($després del xoc$) - K($abans del xoc$) = 0$.
	\item[(b)] \textbf{Xocs inelàstics}, si $\triangle K < 0$. Hi ha transferència de part de l'energia cinètica del sistema
	a "energia interna".
\end{itemize}
\subsection{Problema general dels xocs:}
Donades $\vec{v}_1^i, \vec{v}^i_2$ i un \textbf{paràmetre d'impacte} (distància entre $r_1, r_2$), determinar les rectes
$s_1, s_2$ i les velocitats $\vec{v}_1^f, \vec{v}_2^f$.
La solució depèn dels detalls de la interacció. En general, serà un problema complex. Es pot deduir alguna cosa usant $P =$
constant i, si elàstic, $\triangle K = 0$.
Tot i això, hi ha dos casos particulars on el problema es pot resoldre íntegrament usant $\triangle P = 0$ i, si elàstic, $\triangle K = 0$:
\begin{itemize}
	\item \textbf{Xocs absolutament inelàstics}. Quan, arran del xoc, les dues masses es fusionen en una de sola.
	\[
	\vec{P} = \text{cte.} \quad m_1 \vec{v}^i_1 + m_2 \vec{v}^i_2 = (m_1 + m_2) \vec{v}^f \implies \vec{v}^f = \frac{ m_1 \vec{v}^i_1 + m_2 \vec{v}_2^i}{(m_1+ m_2)\vec{v}^f}. 
	\]
	Es pot comprovar que en aquest cas:
	\[
	\triangle K = -\frac{1}{2} \mu \cdot {\norm{\vec{v}^i_1 - \vec{v}_2^i}}^2,
	\] on $\mu = \frac{m_1m_2}{m_1 + m_2}$.
	\item \textbf{Xocs elàstics frontals}. Un xoc és frontal si $\exists S$ (inercial) des del qual les dues masses es mouen tant abans com després
	del xoc al llarg d'una mateixa recta $r$.
	\obs Si és un xoc frontal, no es veu frontal des de qualsevol sistema de referència.
\end{itemize}
En general, ni tan sols es poden trobar $\vec{v}_1^f, \vec{v}_2^f$, però si es poden deduir-ne fets.
Només considero ara xocs elàstics. En aquest cas, 
 \[
 \begin{cases*}
 \vec{P} = \text{cte.} \quad m_1 \vec{v}^i_1 + m_2 \vec{v}^i_2 = m_1 \vec{v}_1^f + m_2 \vec{v}_2^f \\
 \triangle K = 0. \quad m_1 (v_1^i)^2 + m_2 (v_2^i)^2 = m_1 (v_1^f)^2 + m_2(v_2^f)^2, \\
 \end{cases*}
 \] és un sistema de 4 equacions i 6 incògnites, i partir d'aquest sistema no sol ser una bona idea. 
 
 \textbf{Recordatori}. Un espai afí euclidià és una terna $(A,V,\phi)$, on $A$ conjunt, $V$ espai vectorial euclidià i $\phi : A \times A \rightarrow V$ de manera que:
\begin{enumerate}
    \item $\phi(P, -) : A \to V$ és bijectiva.
    \item (Regla del paral·lelogram). $\phi(P,Q) + \phi(Q,R) = \phi(P,R)$ $\forall P,Q,R \in A$.
\end{enumerate}
Sigui $\mathcal{S} = \{$ sistemes de referència inercials $\}$. Definirem una relació d'equivalència tal que si $S,S' \in \mathcal{S}$, $S \equiv S'$ si $\vec{v}_{S'/S} = 0$. Denotarem com a $[S]$ la classe de $S$.
\prop Sigui $A = \frac{\mathcal{S}}{\equiv}$, $V=$ e.v. de les possibles velocitats d'una partícula 3-dim i euclidià ($\norm{\vec{v}} = \sqrt{v_x^2 + v_y^2 + v_z^2}$.
\[
\phi : A \times A \rightarrow V
\]
\[
([S], [S']) \to \phi([S], [S']) := \vec{v}_{S'/S}
\] és un espai afí.
\proof 
\[
\phi([S], -) : A \rightarrow V
\]
\[
[S'] \to \vec{v}_{S'/S}
\]és bijectiva.
A més, 
\[
\phi([S],[S'']) = \phi([S],[S']) + \phi([S'],[S'']
\] Vol dir veure que:
\[
\vec{v}_{S''/S} = \vec{v}_{S'/S} + \vec{v}_{S''/S'},
\] però això és la llei clàssica d'addició de velocitats ($\vec{v} = \vec{v}_{O'} + \vec{v}'$).

\defi S'anomena \textbf{graf cinemàtic d'un xoc} a la quaterna $G = (P_1, P_2, P'_1, P'_2) \in \mathbb{V}^4$ definida per:
\[
k = 1,2, \quad P_k = [S_k],
\] on $S_k$ sistema de referència inercial en el qual $\vec{v}^i_k = 0$.
\[
k = 1,2, \quad P'_k = [S'_k],
\]on $S'_k$ sistema de referència inercial en el qual $\vec{v}^f_k = 0$.
\obs $G$ és 'absolut'; la seva definició no exigeix fixar cap sistema de referència prèviament, cosa que he de fer per definir les velocitats abans i després.
\newline Conèixer $G$ em permet conèixer les $\vec{v}_1^i, \vec{v}_2^i, \vec{v}_1^f, \vec{v}_2^f$ respecte qualsevol $S$:
\[
\overset{\text{resp.} S}{\implies} \vec{v}_1^i = \vec{v}_{S_1/S} = \phi([S],[S_1]) = \vec{OP_1}.
\]
El $G$ d'un xoc no és arbitrari (qualsevol quaterna de $\mathbb{V}^4$. De fet, compleix
\begin{enumerate}
    \item $P_1, P_2, P'_1, P'_2$ són coplanaris.
    \item $\bar{P_1 P_2}$ (representa el segment de $P_1$ a $P_2$).   $\bar{P_1 P_2} \cap \bar{P'_1 P'_2} = \{ X \}$, on $X$ és tal que:
    \[
    \frac{|XP_1|}{|XP_2|} = \frac{|XP'_1|}{|XP'_2|} = \frac{m_2}{m_1},
    \] anomenada la regla de la palanca cinemàtica. A més, $| -|$ és una velocitat (obtinguda a $\mathbb{V}$).
    \item Si és un xoc elàstic:
    \[
    |XP_k| = |XP'_k|, \quad k = 1,2.
    \]
\end{enumerate}
\proof
\begin{enumerate}
    \item Coplanaritat.
    \defi Direm que un xoc és \textbf{bidimensional} si $\exists S$ sistema de referència inercial tal que 
    \[
    \dim <\vec{v}^i_1, \vec{v}^i_2, \vec{v}^f_1, \vec{v}^f_2> \text{ } \leq 2.
    \]Vol dir que el moviment té lloc en un mateix pla o en plans paral·lels. Diré que $S$ és adaptat al xoc.
    \prop Tot xoc és bidimensional. És a dir, $\exists S$ tal que l'observador de $S$ veu el xoc en dues dimensions. 
    \obs Equival a que $P_1, P_2, P'_1, P'_2$ siguin coplanaris.
    
    \proof És conseqüència del teorema de conservació del moment lineal. Només cal agafar $S = S_1$, és a dir el sistema de referència inercial des del qual $\vec{v}^i_1 = 0$.
    Ja que $\vec{P} = \text{cte.}$, dedueixo
    \[
    m_1 \vec{v}^i_1 + m_2 \vec{v}^i_2 = m_1 \vec{v}^f_1 + m_2 \vec{v}^f_2 \implies <\vec{v}^i_1, \vec{v}^f_1, \vec{v}^i_2, \vec{v}^f_2> = <\vec{v}_1^f, \vec{v}_2^f>.
    \]
    Incís sobre el centre de masses (c.d.m) d'un sistema de partícules:
    \defi Donat sistema de $N$ partícules de masses $m_1, ..., m_N$, el seu centre de masses és el punt $C$ tal que:
    \[
    \sum_{i=1}^N m_i \vec{C\vec{P}_i} = 0,
    \] on $P_1, ..., P_N$ posicions dels punts del sistema.
    \prop Aquesta equació sempre té solució i és única. 
    \proof Si $O$ un punt qualsevol, $C$ és l'únic punt tal que $\vec{R} \equiv \vec{OC} = \frac{1}{M} \sum m_i \vec{OP_i}$.
    \obs S'assembla al baricentre, però aquí hi intervenen les masses.
    \obs A $C$, en general, no hi ha res, és un punt merament geomètric.
    \prop $S$ sistema de referència qualsevol, $\vec{P}$ moment lineal del sistema respecte $S$, $\vec{V}$ velocitat del centre de masses respecte $S$, $\vec{A}$ acceleració del c.d.m respecte $S$.
    
    Aleshores 
    \begin{enumerate}
        \item $\vec{V} = \frac{1}{M} \vec{P}$.
        \item $M\vec{A} = \vec{F}^{\text{ext}} +\vec{F}^{\text{ine}}$.
    \end{enumerate}
    \defi S'anomena sistema de referència del c.d.m. ($S_C$) d'un sistema de partícules al sistema de referència tal que:
    \begin{enumerate}
        \item $\vec{\omega}_S = 0$. No gira respecte sist. de referències inercials.
        \item $C$ roman immòbil a $S$.
    \end{enumerate}
    \obs Hi ha més d'un, segons on agafo orígen, temps i espai i com agafo els eixos. \\
    \underline{Propietat fonamental de $S_C$}. Si $\vec{P}_C =$ moment lineal del sistema respecte $S_C$. 
    
    Aleshores $\vec{P}_C = 0$. 
    \proof 
    \[
    \vec{P} = \sum_{i=1}^N m_i \vec{v}_{i,C} = \sum m_i {(\frac{d\vec{r}_{i,C}}{dt})}_{S_C} = {(\frac{d}{dt}(\sum m_i \vec{r}_{i,C}))}_{S_C} = 0.
    \] On hem usat:
    \[
    \sum m_i \vec{r}_{i,C} = \sum m_i \vec{CP_i} = 0.
    \]
    \obs $S_C$ pot ser inercial... o no. 
    
    De fet, $S_C$ inercial $\iff \vec{F}^{\text{ext}} = 0$.
    \item[2.] Regla de la palanca cinemàtica.
    \[
    \bar{P_1P_2} \cap \bar{P'_1 P'_2} = \{ X \}, \quad \text{amb } X \text{ t.q.}:
    \]
    \[
    \frac{|XP_1|}{|XP_2|} = \frac{|XP'_1|}{|XP'_2|} = \frac{m_2}{m_1}.
    \]
    \proof Siguin $\mathbb{V} \ni X = [S_C]$ inercial per $\vec{F}^{\text{ext}} = 0$.
    \[
    |XP_1| = \norm{\vec{XP_1}} = \norm{\phi([S_C],[S_1])} = \norm{\vec{v}_{S_1/S_C}} = \norm{\vec{v^i_{1,C}}}.
    \] Anàlogament amb els altres 3 punts.
    Per tant, la regla correspon a que:
    \[
    \frac{v^i_{1,C}}{v^i_{2,C}} = \frac{v^f_{1,C}}{v^f_{2,C}} = \frac{m_2}{m_1}.
    \] Ara bé, com que $\vec{P}_C = 0$:
    \[
    m_1 \vec{v^i_{1,C}} + m_2 \vec{v}^i_{2,C} = 0,
    \]
    \[
     m_1 \vec{v^f_{1,C}} + m_2 \vec{v}^f_{2,C} = 0,
    \]
    Però aleshores 
    \[
    m_2 \vec{XP_2} = -m_1 \vec{XP_1}
    \] i anàleg per $P'_i$, $i = 1,2$. Aleshores ja hem acabat.
    \item[3.] Si és un xoc elàstic, $|XP'_k|= |XP_k|$.
    Però, per l'apartat anterior,
    \[
    |XP'_k| = \norm{\vec{v}^f_{k,C}} 
    \]
    \[ |XP'_k| = \norm{\vec{v}^i_{k,C}} 
    \]
    \proof Imposar $K_{i,C} = K_{f,C}$.
    \[
    K_{i,C} = \frac{{p^i}^2_{1,C}}{2m_1} + \frac{{p^i}^2_{2,C}}{2m_2} = \frac{{p^f}^2_{1,C}}{2m_1} + \frac{{p^f}^2_{2,C}}{2m_2} = K_{f,C}.
    \] D'aquí, provo $\norm{p^f_{k,C}} = \norm{p^i_{k,C}}$ i per tant acabo.
\end{enumerate}

\textbf{Tècnica} (usada en l'exercici 2.9). Usar $\vec{P} = \text{cte.}$ i $\triangle K = 0$. 

\textbf{Tècnica} (usada en l'exercici 2.10). Usar com a argument el graf cinemàtic per comparar angle de dispersió amb angle de resposta des del centre de masses. A partir d'aquí, dibuixar els vectors velocitat i establir les relacions geomètricament.
 
\textbf{Tècnica} (usada en l'exercici 2.11). Important recordar a l'hora d'usar equacions la fórmula de $\vec{OC}$, on $C$ centre de masses. En general, usar l'espai de $\mathbb{V}$ ens és útil per deduir resultats físics a partir de propietats geomètriques i també viceversa.

\textbf{Tècnica} (usada en l'exercici 2.04). Important recordar $M\vec{A} = \vec{F}^{\text{ine}} + \vec{F}^{\text{ext}}$, on $\vec{A}$ és l'acceleració en el c.d.m.
\newpage

\begin{figure}[h]
  \includegraphics[width=1.1\textwidth]{diagramesAmalia/e6.png}
  \includegraphics[width=1.05\textwidth]{diagramesAmalia/e7.png}
 \end{figure}
\section{Conservació de l'energia}

\underline{Objectiu}: Introduir concepte de \textbf{l'energia mecànica} d'una partícula. Vegem com depèn de l'entorn on es troba, no és pròpia de la partícula i, sota certes condicions, es conserva.
\[
E := K + U.
\] On $U$ té codificat part de l'entorn, és la suma de les energies potencials de les forces conservatives que actúen sobre la partícula.
Si sobre la partícula només actuen forces conservatives $\implies$ $E = \text{cte.}$. 
En general:
\[
\triangle E = W_{nc},
\] on $W_{nc}$ és el treball fet per les forces no conservatives. 

\textbf{Recordatori}. $\gamma : [a,b] \rightarrow \mathbb{R}^n$ camí, $\gamma \in \mathcal{C}^1$ ($\exists \gamma'$ i és contínua). $\vec{A}$ camp vectorial definit a $Im \gamma$ i continu.
\[
\text{Integral de línia:} \quad \int_{\gamma} \vec{A} := \int_a^b \vec{A}(\gamma(t)) \cdot \gamma'(t)dt.
\]
\begin{enumerate}
    \item[(1)] $\int_{\gamma} \vec{A}$ és lineal en $\vec{A}$.
    \item[(2)] $\int_{\gamma} \vec{A}$ és additiva en $\gamma$ (i.e., $\int_{\gamma} \vec{A} = \int_{\gamma_1} \vec{A} + \int_{\gamma_2} \vec{A}$).
    \item[(3)] $\int_{\gamma}\vec{A}$ és invariant per reparametritzacions 'positives' ($\phi'>0$), i.e., $\phi : [c,d] \rightarrow [a,b]$ bijectiva i $\tilde{\gamma} = \gamma \circ \phi$. 
\end{enumerate}
En el nostre cas, $\gamma =$ trajectòria de la partícula i $\vec{A} = \vec{F}$ que actua sobre la partícula.
\defi S'anomena \textbf{treball} fet sobre la partícula per $\vec{F}$ al llarg de $\gamma$ a:
\[
W(\vec{F}, \gamma) := \int_{\gamma} \vec{F}.
\]
En general, depèn de $\gamma$, no només de $\gamma(a)$ i $\gamma(b)$.

\textbf{Exemples}.

\begin{enumerate}
    \item $\vec{F} = \text{cte.} $
    \[
    W(\vec{F},\gamma) = \int_a^b \vec{F} \gamma'(t)dt = \vec{F} \cdot (\int_a^b \gamma'(t)dt) = \vec{F} \cdot (\gamma(b) - \gamma(a)) = \vec{F} \cdot \triangle \vec{r},
    \] on $\triangle \vec{r}$ és el vector desplaçament.
    \item $\vec{F} = F \h{u}_t$ ($\h{u}_t = \frac{\gamma'(t)}{\norm{\gamma'(t)}}$), amb $F = \text{cte.}$ (pot ser positiva o negativa).
    \[
    F \cdot l = W(\vec{F}, \gamma) = \int_{\gamma}\vec{F} = \int_a^b \vec{F}(\gamma(t)) \cdot \gamma'(t)dt = \int_a^b F \norm{\gamma'(t)}dt = \int_a^b \norm{\gamma'(t)}dt.
    \] (l és la longitud de la corba).
    \item $\vec{F} = (y^2 -x^2, 3xy)$, $n=2$.
    \[
    \gamma(a) = (0,0), \quad \gamma(b) = (2,4).
    \] Veurem que aquesta força no serà conservativa, ja que el treball, de fet, dependrà del camí realitzat. 
    
    De fet, amb parametrització respecte $x$ i respecte $y$ tenim resultats diferents; amb la primera, $\vec{F}(\gamma(x)) = (3x^2, 6x^2)$: 
    \[
    W(\vec{F}, \gamma) = \int_0^2 (3x^2+ 12x^2)dx = 40 J.
    \]
    \obs En general, $\vec{F} = \vec{F}(\vec{r}, \vec{v}, t) \implies$ he de parametritzar $\gamma$ pel temps.
    En aquest cas, $\gamma'(t)dt = \vec{v}dt = d\vec{r}$.
    
    \obs $W(\vec{F},\gamma)$ correspon a una energia, concretament, $W(\vec{F},\gamma) > 0$ (vol dir $\vec{F} d\vec{r}$ majoritàriament positiu $\implies$ $(\h{\vec{F},d\vec{r}}) < \pi/2  \implies \vec{F}$ empeny.
    Si $W(\vec{F},\gamma) < 0$, és la partícula que transforma energia a l'agent extern que exerceix $\vec{F}$.
\end{enumerate}
\teo[Teorema de l'energia cinètica o forces vives] Si sota l'acció d'una força neta $\vec{F}$ una partícula recorre la trajectòria $\gamma$ entre $t = t_0$ i $t = t_1$ aleshores:
\[
W(\vec{F}, \gamma) = \triangle K = K(t_1) - K(t_0).
\]
\proof En efecte:
\[
W(\vec{F}, \gamma) \overset{(1)}{=} \int_{t_0}^{t_1} \vec{F}(\gamma(t)) \cdot\gamma'(t)dt = \int_{t_0}^{t_1} \vec{F} \vec{v}dt \overset{(2)}{=} m \int_{t0}^{t_1} (\vec{a} \cdot \vec{v})dt =
\]
\[
= m \int_{t_0}^{t_1} (\frac{d\vec{v}}{dt} \vec{v}) dt = m \int_{t_0}^{t_1} (\frac{1}{2} \frac{d}{dt}(\vec{v}\vec{v}))dt = \frac{1}{2}m \vec{v}^2 |^{t_1}_{t_0}.
\] 
On (1): parametritzem per $t$ i on (2): $\vec{F} = m \vec{a}$.
\obs Si es parla de $\triangle K$, és que hi ha un sistema de referència implícit. Pot ser qualsevol, inercial o no. Però si no és inercial, aleshores:
\[
\vec{F} = \vec{F}^{\text{real}} + \vec{F}^{\text{ine}}.
\] Vegi's (1).

\obs $\int_{t_0}^{t_1} \vec{F} dt = \triangle \vec{p}$; $\int_{\gamma} \vec{F} = \int \vec{F} d\vec{r} = \triangle K$.
\defi $\vec{F}$ és una força qualsevol que actúa sobre una partícula. Direm que $\vec{F}$ és conservativa si:
\begin{enumerate}
    \item $\vec{F}$ només depèn de la posició (si és que en depèn) [i és contínua].
    \item $\forall \gamma : [a,b] \rightarrow \mathbb{R}^n$,  $W(\vec{F}, \gamma)$ només depèn de $\gamma(a)$ i de $\gamma(b)$.
\end{enumerate}

\obs Si $\vec{F}(\vec{r},\vec{v}, t)$, cal parametritzar entorn de $\gamma$, aconsellablement respecte $t$, d'aquesta manera $\vec{F}(\gamma(t), \gamma'(t),t)$.

\obs (vista a l'exercici 2.15). Per calcular el treball fet sobre la força neta, la integral de línia és lineal, aleshores equival a calcular el treball realitzat per cadascuna de les forces.


\textbf{Tècnica} (usada en l'exercici 2.15). Vull calcular $v_B$, sabent $\vec{F}_c$ constant i $v_A = 0$. Aleshores amb $\triangle K = K_B - K_A = K_B = W(\vec{F},\gamma)$, on $\vec{F}$ és la força neta i $\gamma$ la corba descrita. Aleshores 
\[
W(\vec{F},\gamma_{A\to B} = W(m\vec{g}, \gamma_{A \to B}) + W(\vec{F}_c, \gamma_{A \to B} + W(\vec{N}, \gamma_{A \to B}) = m\vec{g} \cdot \triangle \vec{r}_{A \to B} + \vec{F}_c \cdot \triangle \vec{r}_{A \to B},
\] on $W(\vec{N}, \gamma_{A \to B}) = 0$ ja que $\vec{N} \cdot \vec{v} = 0$, i.e. són ortogonals.

\textbf{Tècnica} (usada en l'exercici 2.17). Quan calculem $\triangle K = K_B - K_A$, vegem com a la definició de $K$, quan hem obtingut una velocitat $\vec{v}$ en termes de $\h{u}_x, \h{u}_y$, aleshores la $v$ que s'usa en la fórmula és el mòdul de la velocitat $\vec{v}$ (i.e., $\norm{\vec{v}} = v)$. 
\obs (vista a l'exercici 2.17). La força de Coriolis no altera el mòdul de la velocitat, només la direcció, i.e., la força de Coriolis NO fa treball (és $\perp \vec{v} '$).

\textbf{Exemples}:
\begin{enumerate}
    \item $\vec{F} = \text{cte.}$ és conservativa:
    \[
    W(\vec{F}, \gamma) = \int_{\gamma} \vec{F} = \vec{F} \cdot (\gamma(b) - \gamma(a)) = \vec{F} \cdot  \triangle \vec{r}.
    \]
    \item $\vec{F}$ central si $\exists$ punt $O$ tal que:
    \[
    \vec{F}(\vec{r}) = F(\norm{\vec{r}- \vec{r}_O}) \cdot \frac{\vec{r}-\vec{r}_O}{\norm{\vec{r}-\vec{r}_O}}. 
    \] Si $O$ origen de coordenades: 
    \[
    \vec{F}(\vec{r}) = F(r) \h{u}_r.
    \] Si $\vec{F}$ és central aleshores és conservativa.
    Utilitzo base $\{ \h{u}_r, \h{u}_{\phi}, \h{u}_{\theta}\}$, on $(r,\phi, \theta)$ coordenades esfèriques.
    \[
    \gamma(t) = r(t) \cdot \h{u}_r(\phi(t), \theta(t)).        
    \]
    \[
    W(\vec{F},\gamma) = \int_{t_0}^{t_1} \vec{F}(\gamma(t)) \cdot \gamma'(t)dt.
    \] Vegem com:
    \[
    \gamma'(t) = \frac{dr}{dt}\h{u}_r + r\frac{d\h{u}_r}{dt} = \frac{dr}{dt}\h{u}_r + r \frac{d\h{u}_r}{d\phi}\frac{d\phi}{dt} + \frac{d\h{u}_r}{d\theta} \frac{d\theta}{dt},
    \] on $\frac{d\h{u}_r}{d\phi} = \sin \theta \h{u}_{\phi}$ i $\frac{d\h{u}_r}{d\theta} = \h{u}_{\theta}$.
    Aleshores vegem primer:
    \[
    \gamma'(t) = \frac{dr}{dt}\h{u}_r + r\frac{d\h{u}_r}{dt} = \frac{dr}{dt} \h{u}_r + r\sin \theta \frac{d\phi}{dt}\h{u}_{\phi} + r\frac{d\theta}{dt} \h{u}_{\theta}
    \] I per tant:
    \[
    W(\vec{F}, \gamma) = \int_{t_0}^{t_1} \vec{F}(\gamma(t)) \cdot \gamma'(t)dt = \int_{t_0}^{t_1} \vec{F}(r(t)) \cdot \frac{dr}{dt} dt = \int_{r_0 = r(t_0)}^{r_1 = r(t_1)} \vec{F}(r) \cdot dr.
    \]
    \item $\vec{F}_{fr}$ depèn del vector $\vec{v}$ velocitat de la partícula, sobretot de la seva direcció. Malgrat sigui de mòdul constant, no és conservativa. Encara fins i tot que el moviment fos rectilini, tampoc seria conservativa, ja que segueix depenent del vector velocitat, degut al seu sentit.
    \item Les forces d'inèrcia: $\vec{F}^{\text{ine}}$:
    
    La força de Coriolis $\vec{F}_{\text{cor}}$ mai és conservativa ja que depèn de $\vec{v}$. 
    
    La força centrífuga $\vec{F}_{\text{cen}}$ és conservativa si $\vec{\omega}$ és constant.
    
    La força d'arrossegament $\vec{F}_{\text{arro}} = -m\vec{a}_{O'}$ és conservativa.
    
     \textbf{Exercici}. La força azimutal $\vec{F}_{\text{eul}} = -m \vec{\omega}\wedge \vec{r} '$? 
\end{enumerate}

\teo[Caracteritzacions de força conservativa]  Sigui $D \subseteq \mathbb{R}^n$ obert i arc-connex i $\vec{F}: D \rightarrow \mathbb{R}^n$ contínua. TFAE:
\begin{enumerate}
    \item $\vec{F}$ és conservativa.
    \item $\forall \gamma$ camí tancat, $\int_{\gamma} \vec{F} = 0$.
    \item $\vec{F}$ és el gradient d'alguna funció $D \to \mathbb{R}$.
\end{enumerate}
Si $\vec{F}$ de classe $\mathcal{C}^1$ i $D$ convex (en general, $D$ simplement connex [el camí és únic llevat homotopia]), també és equivalent a
\begin{enumerate}
    \item[4.] $\frac{dF_i}{dx_j} = \frac{dF_j}{dx_i} \quad \forall i,j$ (si $n = 3$, es diu que $rot(\vec{F}) = 0$).
\end{enumerate}
\defi Si $\vec{F}$ conservativa, s'anomena \textbf{energia potencial} de $\vec{F}$ qualsevol funció $U: D\rightarrow \mathbb{R}$:
\[
\vec{F} = - \vec{\bigtriangledown}U.
\]
\obs
\begin{enumerate}
    \item[]
    \item $U$ definida llevat constant additiva.
    En efecte, si $\vec{F} = - \bigtriangledown U_1 = - \bigtriangledown U_2$:
    \[
    \bigtriangledown (U_1-U_2) = 0 \implies U_1-U_2 = \text{cte.}
    \]
    El valor de la constant determinat un cop trio un punt de referència que suposo d'energia potencial $= 0$.
    \item El perquè del signe el veurem més endavant. En tot cas, el signe $-$ implica que $\vec{F}$ sempre apunta en la direcció de màxim decreixement de $U$.
    \item El fet que $\vec{F}$ conservativa vingui totalment descrita per la seva $U$ és un indici del fet que la mecànica clàssica es pot reformular sense el concepte de força.
\end{enumerate}
\underline{Càlcul de $U$}. Hi ha dos procediments:
\begin{itemize}
    \item[1.] Fixo $O$ punt de referència i defineixo energia potencial en qualsevol altre punt P que sigui:
    \[
    U(P) = - \int_{\gamma} \vec{F},
    \] on $\gamma$ camí qualsevol de $O$ a $P$. $O$ serà el punt tal que $U(O) = 0$ (origen de l'energia potencial).
    
    \item[2.] Usant que $F_i(x_1,.., x_n) = - \frac{dU}{dx_i} \quad \forall i = 1..n$. Aleshores:
    \[
    U(x_1,..,x_n) = - \int F_i (x_1,..,x_n) dx_i + A_i (x_1,..\h{x}_i,..,x_n), \quad \text{depèn de totes menys $x_i$}.
    \] Per tant busco $A_1, .., A_n$ funcions tals que 
    \[
    -\int F_i (x_1..,x_n)dx_i + A_i(x_1,..,\h{x}_i,.., x_n) 
    \] sigui independent de $i$.
\end{itemize}

\defi Sigui $\vec{F}_1, ..., \vec{F}_k$ totes les forces conservatives que actúen sobre una partícula determinada i $U_1, ..., U_k$ les respectives energies potencials (no tenim perquè escollir el mateix origen de l'energia potencial a totes les $U_k$, podem prendre qualsevol). Anomenem \textbf{energia mecànica de la partícula} a:
\[
E := K + \sum_{i=1}^k U_i.
\]
\prop Suposem que entre $t_0$ i $t_1$ la partícula descriu una trajectòria $\gamma : [t_0, t_1] \rightarrow \mathbb{R}^3$. Sigui $W'(\gamma) =$ suma dels treballs fets per totes les forces no conservatives quan la partícula recorre $\gamma$. Aleshores:
\[
\triangle E ( = E(t_1) - E(t_0)) = W'(\gamma).
\]
\teo[Llei de la conservació energia mecànica] Si sobre una partícula només actúen forces conservatives, aleshores l'energia mecànica es conserva; $E$ és constant, i per tant:
\[
\triangle E = 0.
\] El valor concret de $E$ dependrà de les condicions inicials (C.I.).
\proof Siguin $\vec{F}_1, ..., \vec{F}_k$ forces conservatives que actúen sobre la partícula i $\vec{F}'_1, ..., \vec{F}'_{k'}$ les no conservatives.

Pel teorema de la variació de l'energia cinètica:
\[
\triangle K (K(t_1) - K(t_0)) = W(\vec{F}, \gamma),
\] on $\vec{F} = \sum F_i + \sum F'_i$. Però això és el mateix que:
\[
= \sum_i W(\vec{F}_i, \gamma) + \sum_i W(\vec{F}'_i, \gamma) = \sum_i W(\vec{F}_i, \gamma) + W'(\gamma).
\] Ara, definim $\gamma_{i,j}$ camí de la força realitzada per la força $i$ i en direcció al punt $P_j = \gamma(t_j)$:
\[
W(\vec{F}_i,\gamma) = W(\vec{F}_i,\bar{\gamma}_{i,0}) + W(\vec{F}_i,{\gamma}_{i,1}) = W(\vec{F}_i,{\gamma}_{i,1}) - W(\vec{F}_i,{\gamma}_{i,0}) = U_i (P_0) - U_i (P_1).   
\] Per tant:
\[
\triangle K = \sum_i (U_i(P_0) - U_i (P_1)) + W'(\gamma) \implies 
\]
\[
\implies K(P_1) + \sum_i U_i(P_1) - (K(P_0) + \sum_i U_i (P_0)) = W'(\gamma) = E(P_1) - E(P_0) = W'(\gamma).
\] 

\textbf{Tècnica} (usada en l'exercici 24). 
\begin{enumerate}
    \item La tensió no és una força conservativa.
    \item En el punt més alt, un pèndol no té energia cinètica, té energia potencial gravitatòria i de fet és màxima en aquell instant.
    \item Per determinar paràmetres d'abans i després del xoc, faig 'instantànies':
    Considero $t_0$ moment que deixo anar una molla, $t_1$ instant just abans del xoc entre $m_1, m_2$, $t_2 = t_1 + \varepsilon$ instant just després del xoc i $t_3$ quan el pèndol arriba a altura màxima. Fixem-nos com s'interpreten els $t_i$ just abans i just després dels moments crítics. Aleshores suposo $\varepsilon << 1$ de manera que el moment lineal del sistema $m_1 + m_2$ es conserva i $m_1, m_2$ no es mouen durant el xoc. Per tant puc interpretar els resultats amb fórmules que hem vist i trobo relacions entre $t_i, t_j$.
\end{enumerate}
\textbf{Tècnica} (usada en l'exercici 25). Jugo amb els instants de temps. Per exemple: suposo $\varepsilon << 1$ de manera que no hi ha moviment durant el xoc ni variació de $\vec{P}$ durant aquest. Per tant, $\vec{P}(t_0) = \vec{P}(t_0 + \varepsilon)$. A més, puc usar coses com velocitats de partícules JUST DESPRÉS del xoc.

\textbf{Tècnica} (usada en l'exercici 25). La partícula penjada d'una molla $M$ fa volta completa si $v_1 = $velocitat en el punt més alt és tal que:
\[
\frac{Mv_1^2}{L} \geq Mg.
\] Per tant:
\[
v_1^{min} = \sqrt{Lg}.
\] Busco ara la $v^{min}_0$ usant que l'energia mecànica es conserva entre $t_1, t_2$ perquè $T_1$, malgrat no ser conservativa, no fa treball. Trobarem que $v_0^{min} = \sqrt{5Lg}$.

\textbf{Important!} No fer els exercicis del 27 al 29. Del 30 al 34, són de moment angular.

\subsection{Llei de la conservació del moment angular}
\defi $S$ sistema de referència qualsevol, $Q$ punt fix a $S$ qualsevol, $\vec{p} (= m\vec{v})$ moment lineal de la partícula segons $S$. S'anomena \textbf{moment angular de la partícula respecte $S$ i respecte $Q$} a:
\[
\vec{L}_{S,Q} \equiv \vec{L}_Q = \vec{r}_Q \wedge \vec{p},
\] on $\vec{r}_Q$ és el vector posició de la partícula respecte $Q$.
\obs $\vec{L}_Q = 0$ si $\vec{r}_Q$ és paral·lel a $\vec{p}$ (passa si la partícula s'està simplement apropant o allunyant de $Q$). 

Per tant, $\vec{L}_Q \neq 0$ si la partícula està girant al voltant de $Q$. A més, $\vec{L}(Q) \perp$ pla del moviment.

Volem saber com varia en el temps aquest moment angular i deduir sota quines condicions es conserva. Recordem:
\[
\begin{cases*}
    \frac{d\vec{p}}{dt} = \vec{F}.\\
    \frac{dK}{dt} = \vec{F}\vec{v}. \\
    \frac{dE}{dt} = \frac{dW_{nc}}{dt}.
\end{cases*}
\]
\defi Si $\vec{A}$ vector lligat amb punt d'aplicació(o origen) $P$ (és important on està ancorat, quin és el punt d'aplicació...), $Q$ punt qualsevol. S'anomena \textbf{moment de $\vec{A}$ respecte $Q$} a:
\[
\vec{M}_Q(\vec{A}) = \vec{QP}\wedge \vec{A}.
\]
\defi S'anomena \textbf{moment d'una força $\vec{F}$ de punt d'aplicació $P$ i respecte el punt $Q$} al moment $\vec{M}_Q(\vec{F}) = \vec{QP}\wedge \vec{F}$.

\prop $S$ sistema de referència qualsevol, $Q$ punt fix a $S$, $\vec{L}_Q = $ moment angular de la partícula respecte $S$, $Q$:
Aleshores:
\[
\frac{\vec{L}_Q}{dt} = \vec{M}_Q(\vec{F}),
\] on $\vec{M}_Q(\vec{F})$ és el moment de la força neta que actúa sobre la partícula.
\proof
En efecte, 
\[
\frac{\vec{L}_Q}{dt} = \frac{d}{dt}(\vec{r}_Q \wedge \vec{p}) = \frac{d\vec{r}_Q}{dt}\wedge \vec{p} + \vec{r}_Q \wedge \frac{d\vec{p}}{dt} = \frac{d\vec{r}_Q}{dt}\wedge \vec{p} + \vec{r}_Q \wedge \vec{F} = \vec{M}_Q(\vec{F}),
\] on $\vec{r}_Q = \vec{QP} = \vec{QO} + \vec{OP}$, on $\vec{QO}$ és constant ja que $Q$ és fix a $S$, per tant $\frac{d\vec{r}_Q}{dt} = \frac{d\vec{r}}{dt} = \vec{v}$.
\teo[Conservació moment angular] Sota les mateixes hipòtesis i notacions d'abans:
\[
\vec{L}_Q \text{ és constant} \iff \vec{M}_Q(\vec{F})=0.
\]
\obs Que $\vec{L}_Q$ es conservi o no, depèn del punt $Q$!
\obs Si $\vec{F} = F(r)\h{u}_r$ és força central (p.ex. el moviment dels planetes) aleshores:
\[
\vec{L}_O = \text{constant},
\] on $O$ centre de forces (suposo que és l'origen).

\textbf{Tècnica} (usada en l'exercici 31). Busco $Q$ tal que $\vec{L}_Q$ és constant (es redueix, primer, a trobar la \underline{força neta} que actúa) i a veure que $\vec{M}_Q(\vec{F}) = 0 $.

\textbf{Important} Calcular el rotacional del vector per saber si una força es conservativa o no.


