\part{Gravitació i Electromagnetisme}
\chapter{Teoría de la gravitación Newtoniana}
\section{Introducción}
Estudiaremos la gravitación y el electromagnetismo como teoría de campos, en su forma moderna.
La teoría de campos describe las cuatro interacciones de la naturaleza:
\begin{enumerate}
    \item \underline{Gravitatoria}.
    \item \underline{Electromagnética}.
    \item Nuclear débil.
    \item Nuclear fuerte.
\end{enumerate}
La gravitatoria y la electromagnética son las 'clásicas'. Las otras dos no las estudiaremos en este curso.

\subsection{Antecedentes}
\begin{enumerate}
    \item Primera ley de Kepler. Los planetas se mueven en torno al Sol siguiendo órbitas elípticas con el Sol en uno de sus focos.
    \item Segunda ley de Kepler. La recta que une un planeta con el Sol barre áreas iguales en tiempos iguales; la velocidad areolar de los planetas es constante.
    \item Tercera ley de Kepler. El cuadrado del período de la órbita es proporcional al cubo de su distancia media al Sol. 
    
    \defi \textbf{Perihelio}. Punto de la trayectoria del planeta tal que consigue distancia mínima al Sol.
    \defi \textbf{Afelio}. Punto de la trayectoria del planeta tal que consigue distancia máxima al Sol.
\end{enumerate} Las tres leyes son deducidas por Newton en su libro, 'Principia Mathematica'.

\textbf{Ley de la Gravitación Universal}. Dos cuerpos masivos (puntuales) se ejercen una fuerza mútua de atracción, en la dirección de la recta que los une, cuyo valor es directamente proporcional al producto de sus masas e inversamente proporcional a la distancia de separación entre ambos cuerpos; i.e., si las masas son $m_1, m_2$:
\[
\vec{F} = -G\frac{m_1 m_2}{r^2} \cdot \frac{\vec{r}}{r},
\] donde $\vec{r}$ vector posición de un cuerpo respecto al otro y $r$ su módulo, por lo tanto; $\h{u}_r = \frac{\vec{r}}{r}$ vector unitario en la dirección de $\vec{r}$, y $G$ es una constante positiva.
\obs De hecho, el signo negativo de $\vec{F}$ indica que efectivamente, es una ley de atracción.

\obs A partir de esta fórmula, se podrá deducir las tres leyes de Kepler. 
\obs $\vec{F}$ es un campo irrotacional y conservativo.
\subsection{Postulados de la gravitación newtoniana}

\begin{enumerate}
    \item[0.] El espacio y el tiempo son independientes. El tiempo es absoluto y en cada instante de tiempo, el espacio es absoluto. Se identifica el espacio tiempo como un espacio afín 4-dimensional.
    \item[1.] Principio de relatividad de Galileo. Existe una clase de sistemas de referencia (inerciales) caracterizadas porque, en ellos, son válidas las leyes de la Mecánica y se desplazan entre ellos a velocidad constante o en reposo relativo y son equivalentes.
\end{enumerate}
\defi Un \textbf{campo} es una función de varias variables. Físicamente, suelen ser las coordenadas espaciales, una magnitud física; un observable. Se manifiesta físicamente porque ejerce una fuerza sobre otras partículas.

\section{Teoría de campos estándar}
Estas son todas las ecuaciones de una teoría de campos estándar.
\begin{itemize}
    \item Ecuación de la divergencia (e.d.p.s).
    \item Ecuación del rotacional (e.d.p.s).
    \item Ecuación de la fuerza + $2^a$ ley de Newton = ecuación de las trayectorias (e.d.o).
\end{itemize} 
\subsection{Ecuaciones de la Gravitación}

$2^o$ Postulado. 
\[
g: \mathbb{R}^3 \times \mathbb{R} \rightarrow \mathbb{R}^3.
\]
\begin{itemize}
    \item[(1)] Ecuación de la divergencia.
    \[
        \nabla \cdot g = - 4\pi G\cdot \rho(x,y,z,t).
    \] Donde $\rho(x,y,z,t)$ es la densidad de materia. 
\end{itemize}
\begin{enumerate}
    \item[(2)] Ecuación del rotacional (el campo es irrotacional; i.e. no tiene vórtices).
    \[
        \nabla \times g = 0.
    \]
\end{enumerate}

\begin{enumerate}
    \item[(3)] Ecuación de la fuerza.
    \[
        F_{\text{gravitatoria}} = m_g\cdot g.
    \]
\end{enumerate} $m_g$, por ahora, no tiene porque ser la misma magnitud física que la asociada a la densidad de materia $\rho$. La llamaremos \textbf{masa gravitatoria pasiva}. 

A la masa asociada a la densidad de materia $\rho$ la llamaré, por contra, \textbf{masa gravitatoria activa}, ya que es la que produce el campo gravitatorio $g$. 

Por la segunda ley de Newton: $F = m_i \cdot a$. Ahora, en principio, $m_i$ representa \emph{otra} masa, la \textbf{masa inercial}. Combinando ambas ecuaciones, obtengo:
\[
m_i \cdot a = m_g \cdot g \implies m_i \frac{d^2x}{dt^2} = m_g \cdot g.
\]
\obs $(2) \iff \exists U \subseteq \mathbb{R}^3\times\mathbb{R}$ tal que $g = - \nabla U$, llamado el \textbf{potencial gravitatorio}.

\prop $(1) \implies - \nabla \cdot (\nabla U) = -4\pi G \rho \iff \nabla^2 U = 4\pi G \rho $. \textbf{Ecuación de Poisson del campo gravitatorio}. Obsérvese como $\nabla^2$ es el operador diferencial laplaciano.

\prop Ley de Gauss de la Gravitación (de ésta, obtendremos la Ley de Gravitación Universal). 

Obtendremos la ecuación (1) en forma integral, usando el Teorema de Gauss-Ostrogradski (Véase Cálculo Integral).

\[
- \int_{V} 4\pi G \rho \cdot dV = -4\pi G \int_V \rho \cdot dV = -4\pi G M,
\] donde $M$ masa total que produce el campo graviatorio, es decir, la masa gravitatoria activa, $M_g$.
\[
\int_V \nabla \cdot g = \int_{S = dV} g \cdot dS.
\]
Por lo tanto,
\[
\int_S g\cdot dS = -4\pi G M.
\]
Cómo, de aquí, podemos deducir la Ley de la Gravitación Universal?
Distribución esférica de materia homogénea y estática, es decir, $\rho = \text{cte.}$ Sea $R$ el radio de la distribución esférica y sea $r > R$, de manera que queremos determinar el campo gravitatorio creado por esta distribución sobre un punto a $r$ unidades de distancia del centro de la distribución esférica.

\obs El campo gravitatorio es un campo vectorial; en realidad, deberíamos escribir $\vec{g}$, aunque se deduce cuando nos referimos al módulo según el contexto.

Bajo estas condiciones, el término de la derecha es el mismo, no como el de la izquierda. Además, si la distribución tiene simetría esférica, el campo gravitatorio también va a tener una simetría esférica, es decir, es un campo radial ($\vec{g} = g(r) \cdot \h{u}_r$).
\[
-4\pi G M = \int_{S} g \cdot dS = \int_S g(r) \h{u}_r \cdot dS = g(r) \int_S \h{u}_r \cdot dS = g(r) \int_S dS = g(r) \cdot \mathcal{A}(S).
\] Pero la superfície es una esfera de radio $r$, entonces:
\[
= g(r) \cdot 4\pi r^2.
\]
Obtenemos pues:
\[
-GM = g(r) \cdot r^2 \iff g(r) = -\frac{GM}{r^2} \implies \vec{g} = -\frac{GM}{r^2}\h{u}_r.
\]
La Ley de la Gravitación Universal se deduce de este resultado junto con esta ecuación que acabamos de hallar:
\[
\vec{F}_{\text{gravitatoria}} = -G \frac{M \cdot m_g}{r^2} \h{u}_r.
\]

La \textbf{energía potencial gravitatoria} de una partícula de masa $m_g$ sometida a un campo $g$:
\[
U := m_g U_g,
\] donde $U_g = -G \frac{M}{r}$.

\obs Recordemos, para el ejercicio 5 de la lista, la tercera ley de Kepler:
\[
T^2 = \frac{4\pi^2}{GM} \cdot r^3.
\] 
Además, también hacemos uso del momento angular respecto al centro de la Tierra de un satélite geoestacionario (siempre en la vertical de un cierto punto de la Tierra).
\[
\vec{L} = \vec{r} \times m_g \vec{v}, \quad |\vec{L}| = L = r \cdot m_g v.
\]
\subsection{Reflexiones sobre el concepto de 'masa'. Principio de equivalencia de Galileo}

Han aparecido tres masas distintas. Vamos a llegar a la conclusión que son la misma magnitud física. Recordemos:
\begin{itemize}
    \item \textbf{Masa gravitatoria activa}, medida de la intensidad del campo gravitatorio creado por la partícula.
    \item \textbf{Masa gravitatoria pasiva}, medida de la reacción de la materia a la acción del campo gravitatorio.
\end{itemize}
Consideremos un sistema aislado de dos partículas.
\[
\vec{F}_{1\to2} = g^{1} m^2_g =  \frac{GM_g^1 m^2_g}{r^2} \h{u}_r.
\] Del mismo modo,
\[
\vec{F}_{2\to1} = g^2 m^1_g = \frac{GM^2_gm^1_g}{r^2}\h{u}_r.
\] Al ser un sistema aislado, por la 3a Ley de Newton, estas fuerzas estan apareadas. Como consecuencia $F_{2\to1} = -F_{1\to2}$. Se obtiene pues que:
\[
\frac{m_g^1}{M^1_g} = \frac{m_g^2}{M^2_g}.
\] Ya que esto vale para dos partículas cualesquiera, el resultado es constante, i.e. $M_g = k m_g$. De hecho, ajustando la constante de la gravitación universal, puede tomarse $k=1$. Por lo tanto, masa gravitatoria activa y pasiva son la misma magnitud física, la \textbf{masa gravitatoria}.

Qué hay de la \textbf{masa inercial}? 
Ahora
\[
m_i \vec{a} = \vec{F} = m_g \vec{g}.
\]
De nuevo,
\[
\vec{a} = \frac{m_g}{m_i}\vec{g}. 
\] A veces a este cociente se denomina \emph{carga gravitatoria} de la partícula. Medidas empíricas concluyeron que $\vec{a} = c \vec{g}$, por lo tanto la carga gravitatoria es constante $m_g = c m_i$. Redefiniendo las unidades con las que mido la masa, puedo tomar $c =1$, por lo tanto, $m_i = m_g \equiv m$.

Es decir, $\vec{a} = \vec{g}$, y se deduce

\prop \textbf{Principio de equivalencia (débil o de Galileo)}. El efecto del campo gravitatorio sobre cualquier objeto material es independiente de su masa y se mueve con la misma aceleración si no hay otras fuerzas que actúe sobre él.


\section{Movimiento en campos centrales y newtonianos} 

\prop Repaso sobre las cónicas. 

\defi \textbf{Parábola}: lugar geométrico de los puntos del plano que equidista de una recta fija (\textbf{directriz}) y de un punto fijo (\textbf{foco}) que no pertenece a la directriz. La distancia del foco a la directriz se denomina \textbf{parámetro}. La recta perpendicular a la directriz y que contiene el foco se llama \textbf{eje focal}. El punto de la parábola que pertenece al eje focal y que está a la mínima distancia ($a/2$) de la directriz es el \textbf{vértice} de la parábola.

La ecuación de la parábola (de la forma $y^2 = 2ax$ o $x^2 = 2ay$) en coordenadas polares es:
\[
r = \frac{a}{1+\cos\theta}.
\]
\defi \textbf{Elipse}: lugar geométrico de los puntos del plano tales que la suma de sus distancias $r$ y $r'$ a dos puntos fijos $F, F'$ (\textbf{focos}) es constante: $r+r' = 2a$. El punto medio entre los dos focos se llama \textbf{centro} de la elipse. Los puntos de máxima ($a$) y mínima distancia ($b$) al centro se denominan \textbf{vértices}, y los segmentos rectilíneos que los unen se llaman \textbf{semiejes}. Los que unen los de máxima distancia (\textbf{mayores}) y los otros son los \textbf{menores}. El valor $e < 1$ tal que $a\cdot e$ es la distancia del centro a los focos se llama \textbf{excentricidad} y se tiene que $b = a \sqrt{1-e^2}$. 

La ecuación de la elipse (de la forma $x^2/a^2 + y^2/b^2 = 1$) en coordenadas polares es:
\[
r = \frac{a(1-e^2)}{1+e\cos\theta}.
\] Casos particulares: 
\begin{itemize}
    \item Si $e = 0$ entonces se trata de una circumferencia y se obtiene $r=a$.
    \item Si $e \to 1$ entonces la elipse degenera en una parábola cuando $F'$ se aleja hacia el infinito y en una recta cuando $F'$ permanece a distancia finita de $F$.
\end{itemize}
\prop Teorema del coseno. 
\[
a^2 + b^2 - 2ab\cos \alpha = c^2.
\] Donde $\alpha$ es el ángulo entre $a$ y $b$.

\defi La \textbf{hipérbola} es el lugar geométrico de los puntos del plano tales que la diferencia de sus distancias $r$, $r'$ a dos puntos fijos $F,F'$ (los \textbf{focos}), es constante. 

Una hipérbola tiene dos \textbf{ramas}: cuando $r-r'=2a$, que denominaremos \textbf{rama positiva} y cuando $r-r' = -2a$, la \textbf{rama negativa}. 

Además, el punto medio del segmento que une los focos se denomina \textbf{centro}, y los puntos de la hipérbola que estan a mínima distancia del centro se denominan \textbf{vértices}. 

Por último, los segmentos rectilíneos (de longitud $a$) que unen cada vértice con el centro son los \textbf{semiejes transversales} de la hipérbola y el valor $e > 1$ tal que $a\cdot e = c$, que es la distancia del centro a los focos, se llama \textbf{excentricidad}.

La ecuación de la hipérbola (de la forma $\frac{x^2}{a^2} - \frac{y^2}{b^2} = 1$) en coordenadas polares escogiendo el origen de coordenadas en uno de sus focos, $F$, entonces (obtenida mediante el teorema del coseno) es:
\[
r = \frac{a(e^2-1)}{\pm 1 + e\cos \theta}.
\] El signo negativo da la rama positiva y el signo positivo, la rama negativa.

\prop La ecuación general de una cónica en coordenadas polares con centro en uno de sus focos es:
\[
\frac{1}{r} = B + A \cos(\theta - \theta_{O}), \quad A>0,
\] donde $\theta_O$ es el ángulo formado por el semieje $OX^+$ y el eje de la cónica (o uno de ellos). Si $\theta_O = 0$, el eje de la cónica coincide con el eje $OX$.

\begin{enumerate}
    \item Si $B > A$ es una \textbf{elipse}, y 
    \[
    B = \frac{1}{a(1-e^2)}, \quad A = \frac{e}{a(1-e^2)}.
    \]
    \item Si $B = A$ es una \textbf{parábola}, y
    \[
    B = A = \frac{1}{a},
    \]
    \item Si $0 < B < A$ entonces es la \textbf{rama negativa de una hipérbola} y
    \[
    B = \frac{1}{a(e^2-1)}, \quad A = \frac{e}{a(e^2-1)}.
    \]
    \item Si por contra, $-A < B < 0$ entonces es la \textbf{rama positiva de una hipérbola}, y
    \[
    B = - \frac{1}{a(e^2-1)}, \quad A = \frac{e}{a(e^2-1)}.
    \]
\end{enumerate}
% Inserir les figures del Narciso.

En todos los casos, $e = \frac{A}{|B|}$. En particular, para elipses y hipérbolas, $a = \left |\frac{B}{A^2-B^2}\right |$. Y los puntos a máxima y mínima distancia del foco se obtienen cuando $\cos (\theta-\theta_O) = \pm 1$ y los valores son, por tanto:
\[
\frac{1}{r_{max}} = B-A\quad \frac{1}{r_{min}} = B+A.
\]


En este curso, se considerará el problema de los dos cuerpos ($M$ y $m$ están sometidas a la acción de los campos gravitatorios creados por ambas partículas). De hecho, solo se considerará $M >> m$, entonces, se puede asumir:
\begin{itemize}
    \item El campo gravitatorio creado por $m$ es despreciable comparado con el otro.
    \item La masa $M$ está fija en un punto (origen de coordenadas espaciales).
\end{itemize}

\defi{Un campo vectorial $\vec{F}$ es un \textbf{campo central} si es conservativo y su potencial escalar (y por tanto, el módulo del campo) \textbf{sólo depende de la distancia $r$ a un punto fijo denominado \textbf{centro del campo}}, por tanto, se tiene que:}
\[
-\nabla U(r) = \vec{F}(r) = F(r)\h{u}_r.
\]
\defi{En particular, un campo central se denomina \textbf{newtoniano o cowlombiano} si su potencial escalar es inversamente proporcional a $r^2$ y, por tanto, el campo depende intrínsecamente de $r$. Es decir, $\vec{F}(r) = \frac{F(r)}{r^2}\h{u}_r$.}
\defi{Las trayectorias de las partículas sometidas a la acción de campos centrales se denominan \textbf{órbitas}}.
\teo[Las órbitas se encuentran en un plano]{El momento angular de la partícula es constante. Por lo tanto, la órbita de una partícula está contenida en un plano que contiene el centro del campo.}

\begin{proof}
Si $\vec{r}$ es el vector posición de la partícula respecto al centro del campo, el momento de la fuerza central que actúa sobre la partícula es:
\[
\vec{N} = \vec{r} \times \vec{F} = \vec{r} \times F(r) \h{u}_r = 0,
\] con lo que, de acuerdo con la ecuación de la dinámica de rotación, para el momento angular se tiene que:
\[
\frac{\partial}{\partial t} \vec{L} = \vec{N} = 0,
\] por tanto $\vec{L}$ es constante (es constante su dirección, sentido y módulo).

Por otra parte, ya que $\vec{L} = m (\vec{r} \times \vec{v})$, y como el vector $\vec{L}$ es constante, $\vec{r},\vec{v}$ deben permanecer en el plano fijo perpendicular a $\vec{L}$, que contiene el centro del campo. 
\end{proof}
\obs Las órbitas NO son alabeadas, son PLANAS.
\obs Ya que el campo es radial, el sistema es invariante por rotaciones en el plano del movimiento, y se puede escoger coordenadas cartesianas para que éste sea $z = 0$. Dado que la fuerza está dirigida según la recta que une el centro con el campo con la partícula, sus componentes son:
\[
F_x = F(r) \cos \theta, \quad F_y = F(r)\sin\theta, \quad F_z = 0.
\] Por la segunda ley de Newton, para una trayectoria $\vec{r}(t) = (x(t), y(t))$:
\[
m\ddot{x} = F(r) \cos \theta, \quad m\ddot(y) = F(r)\sin\theta.
\] Haciendo el cambio $x(t) = r(t)\cos\theta, y(t) = r(t)\sin\theta$, entonces:
\[
m(\ddot{x}\cos\theta + \ddot{y}\sin\theta) = m\ddot{r} - mr\dot{\theta}^2 = F(r), \quad m(-\ddot{x}\sin\theta + \ddot{y}\cos\theta) = mr\ddot{\theta} + 2m\dot{r}\dot{\theta} = 0.
\] \textbf{Esta es la expresión de las ecuaciones dinámicas en coordenadas polares en el plano $z = 0.$}

\teo[Conservación de la energía mecánica total] $L = |L|$ y la energía mecánica total de la partícula, $E = K+U$, son constantes del movimiento (o cantidades conservadas).
\begin{proof}
La conservación del módulo del momento angular es una consecuencia del teorema anterior. También se obtiene multiplicando por $r$ la segunda ecuación, ya que:
\[
0 = mr^2 \ddot{\theta} + 2mr\dot{r}\theta = \frac{d}{dt} (mr^2\dot{\theta}) = \frac{dL}{dt} \implies mr^2\dot{\theta} = L \equiv \text{cte.}
\] La energía mecánica total es una constante del movimiento como consecuencia de que el campo es conservativo, recordemos:
\[
W = K(f) - K(i) = -(U(f)-U(i)) \implies K(f) - K(i) = -(U(f)-U(i)) \implies K(f)+U(f) = K(i) + U(i).
\]
\end{proof}
\obs Obsérvese que los valores de $L$ y $E$ se obtienen de las condiciones iniciales de $r,\theta, \dot{r}, \dot{\theta}$.

\prop La solución de las ecuaciones del movimiento  en coordenadas polares en el plano $z=0$ es:
\[
\int^r_{r_O} \frac{dr}{{\left ( E- U(r) - \frac{L^2}{2mr^2} \right )}^{1/2}} = \sqrt{\frac{2}{m}t}, \quad \theta = \theta_0 + \int_{t_0}^t \frac{L}{mr^2}dt.
\]
\obs Si $L = 0$, la solución es una semirecta y $\theta$ es constante.

\begin{proof}
De la ecuación de la prueba del \textbf{Teorema 4} se tiene que $\dot{\theta} = \frac{L}{mr^2}$, e integrando se obtiene $\theta(t)$. Por otra parte, sustituyendo en la expresión de la energía en coordenadas polares se tiene:
\[
E = \frac{1}{2} m \dot{r}^2 + \frac{1}{2}mr^2\dot{\theta}^2 + U(r) = \frac{1}{2}m\dot{r}^2 + \frac{L^2}{2mr^2} + U(r),
\] de donde se obtiene:
\[
\dot{r} = \sqrt{\frac{2}{m}(E-U(r)-\frac{L^2}{2mr^2}},
\] de aquí, integrando, se consigue el valor de $r(t)$.
\end{proof}

\obs Combinando las ecuaciones obtengo la segunda ley de Newton en dirección radial:
\[
m\ddot{r} = F(r) + \frac{L^2}{mr^3} \equiv \vec{F}(r).
\] 
\obs El sumando $\frac{L^2}{mr^3}$ se identifica como \textbf{fuerza centrífuga}, fuerza fictícia que realmente forma parte del producto masa por aceleración una vez expresado en coordenadas polares.
\obs Por eso mismo se puede tratar este problema dinámico como el de un movimiento en una dimensión con una fuerza $\vec{F}(r)$, también conservativa, y asociada al potencial:
\[
\tilde{U}(r) = - \int \left ( F(r) + \frac{L^2}{mr^3}\right ) dr = U(r) + \frac{L^2}{2mr^2} + k,
\]
 en el que el segundo sumando se asocia como \textbf{la energía potencial asociada a la fuerza centrífuga}, y se puede tomar la constante $k=0$ pertinientemente fijando el origen del potencial.
 
 \defi La función $\tilde{U}(r)$ se llama \textbf{potencial efectivo} del campo central.
 
 \prop Si $L \neq 0$, haciendo el cambio $u(t) =\frac{1}{r(t)}$, e introduciendo la función $u(\theta)$ tal que $u(t) = u(\theta(t))$, se obtiene:
 \[
 \frac{d^2u}{d\theta^2} = -u-\frac{m}{L^2u^2} F\left( \frac{1}{u}\right ).
 \]
 
 % Problema 3.
\textbf{Técnica}. Usada para describir las ecuaciones del movimiento de una partícula bajo la acción del campo gravitatorio dentro de la distribución esférica en el \textbf{problema 3}.

Recordemos el resultado del problema 2 para la fuerza:
\[
F = -\frac{4}{3}G \rho\pi m r = -\frac{GMm}{R^3}r.
\] Ésto es todo lo que necesitamos para obtener la ecuación del movimiento; lo haremos por la segunda ley de Newton.

\[
F = m\ddot{r} = \frac{-GMm}{R^3}r.
\] Equivalentemente:
\[
m\ddot{r} + \frac{GMm}{R^3}r = 0.
\] Esta ecuación es la misma que la de un oscilador armónico simple, es decir movimiento periódico cuya constante recuperadora es la que acompaña a $r$.
Cuál es el periodo? $T = 2\pi \sqrt{\frac{m}{k}}$. Sustituyendo $k = \frac{GMm}{R^3}$, obtenemos $T= 84,3$ minutos.

% Problema 4.
\textbf{Técnica}. Usada para determinar la energía potencial gravitatoria formada por $4$ masas situadas en los vértices en un cuadrado de perímetro $4l$, en el \textbf{problema 4}.

\textbf{NO} el potencial gravitatorio, sino la energía potencial gravitatoria! 

\obs Consideremos un sistema formado sólo por dos partículas situadas en una distancia $d$. Cuál es el potencial gravitatorio? Por la tercera ley de Newton, sabremos que la fuerza ejercida por ambos es la misma en módulo y de sentidos contrarios. 
\[
U_{1,2} = - \frac{Gm_1m_2}{d}.
\]
El potencial gravitatorio, en cambio:
\[
P_1 = -\frac{Gm_1}{d}, \quad P_2 = -\frac{Gm_2}{d}.
\]
Es por eso que la energía potencial gravitatoria del sistema será la suma de cada par de energías potenciales, es decir $U_T = U_{1,2}+ U_{1,3}+ U_{1,4} + U_{2,3} + U_{3,4} + U_{2,4}$, donde:
\[
U_{ij} = -\frac{Gm_i m_j}{{d_{ij}}}.
\]
Cuál es la energía potencial gravitatoria de una partícula cualquiera?
Para la partícula $i$,,
\[
U_i = \sum_{j\neq i} U_{ij}.
\] Obviamente, $U_i \neq U_T$.


\subsection{Análisis cualitativo sobre campos conservativos}
Supongamos que $F$ es un campo conservativo. Sea $U$ la energía potencial del campo, tal que $F = -\nabla U$.
La energía total del sistema $E = K + U$ es conservada.

\obs Caso particular: sistema unidimensional. 

Supongamos que el sistema tiene una energía total $E$ dada. Ésta es constante durante todo el movimiento. Además, el grafo de la energía potencial (con, como eje, $OX$, unidimensional) tendrá un máximo y un mínimo locales ($U_{min} \leq E \leq U_{max}$). La velocidad máxima se logrará en $U_{min}$ y, dependiendo de la posición inicial de la partícula y analizando el grafo de su energía potencial, podemos predecir el movimiento de ésta.

\subsection{Volvemos a los campos centrales}
Recordamos, un campo central, $F = F(r) \h{u}_r$. Además, los campos centrales newtonianos eran los que tenían como $F(r) = F(\frac{1}{r^2})$. Por lo tanto, $U(\frac{1}{r})$. 

Voy a poder trabajar con campos conservativos unidimensionales si uso, en vez de $U(\frac{1}{r})$, el potencial efectivo y, sólo entonces, voy a poder representar gráficamente el potencial efectivo, y describir cualitativamente el movimiento de las partículas. Ésto haremos próximamente. 

