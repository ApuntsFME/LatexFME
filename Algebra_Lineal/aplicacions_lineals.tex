\chapter{Aplicacions lineals}
\section{Definició i tipus}
\begin{defi}[aplicació! lineal]
	Una aplicació $f$ entre dos $\k$-e.v. $E$ i $F$,
	\begin{equation*}
	\begin{aligned}
		f \qcolon & E &&\to &&F\\
				 & v &&\mapsto &&f(v)\,,
	\end{aligned}
	\end{equation*}
	és una aplicació lineal (\textit{linear map} o \textit{linear 
	transformation}) si i només si
	\begin{gather*}
		\forall u, v \in E\quad f(u + v) = f(u) + f(v)\,,\\
		\forall c\in\k\ \forall v \in E \quad f(c\cdot v) = c\cdot f(v)\,.
	\end{gather*}
\end{defi}

\begin{defi}[aplicació! identitat]
	L'aplicació lineal entre un $\k$-e.v. $E$ i sí mateix que relaciona cada 
	$v\in E$ amb sí mateix, és a dir,
	\begin{equation*}
	\begin{aligned}
		\Id \qcolon & E &&\to &&E\\
				    & v &&\mapsto &&v \quad \forall v\in E\,,
	\end{aligned}
	\end{equation*}
	s'anomena aplicació identitat i es denota amb $\Id$.
\end{defi}

\begin{defi}[homotècia]
	Sigui $E$ un $\k$-e.v., i sigui $\lambda \in \k$ un escalar. Tota 
	aplicació lineal de la forma
	\begin{equation*}
	\begin{aligned}
		f \qcolon & E &&\to &&E\\
		& v &&\mapsto &&\lambda v \quad \forall v\in E
	\end{aligned}
	\end{equation*}
	s'anomena \textit{homotècia}.
\end{defi}

\begin{prop}
	Si una aplicació $f$ entre dos $\k$-e.v. $E$ i $F$ és lineal, 
	necessàriament $f(0_E) = 0_F$, on $0_E$ i $0_F$ són, respectivament, el 
	vector nul d'$E$ i de $F$.
	\begin{proof}
		Sigui $v$ un vector d'$E$ tal que $f(v) = u \in F$. Per la linearitat 
		de $f$, 
		$f(0_E) = f(0\cdot v) = 0 \cdot f(v) = 0\cdot u = 0_F$\,.
	\end{proof}
\end{prop}

\begin{prop}
	Siguin $E$ i $F$ dos $\k$-e.v.; sigui $\{u_1,\ldots,u_n\}$ una base d'$E$ i 
	siguin $v_1,\ldots,v_n\in F$ vectors qualssevol de $F$. Llavors existeix 
	una única aplicació lineal $f$ tal que
	\[
		f(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,.
	\]
	És a dir, que la imatge d'una base determina unívocament una aplicació 
	lineal.
	\begin{proof}
		Suposem que existeixen dues aplicacions lineals, $f\colon E \to F$ i 
		$g\colon E \to F$, tals que
		\begin{gather*}
			f(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,,\\
			g(u_i) = v_i\quad \forall i \in \{1,\ldots,n\}\,.
		\end{gather*}
		Per cada vector $w\in E$, existeixen uns únics coeficients 
		$\lambda_1,\ldots, \lambda_n\in\k$ tals que
		\[
			w = \sum_{i=1}^n \lambda_i u_i\,,
		\]
		ja que els vectors $u_1,\ldots,u_n$ formen una base (i per tant les 
		coordenades de $w$ respecte aquesta \hyperref[teo:unique-coord]{són 
		úniques}).
	
		Llavors, emprant la linearitat de $f$,
		\[
			f(w) = f(\lambda_1 u_1 + \cdots + \lambda_n u_n) = \lambda_1 f(u_1) 
			+ \cdots + \lambda_n f(u_n) = \sum_{i=1}^{n} \lambda_i v_i\,.
		\]
		D'altra banda, $g$ també és lineal, i per tant
		\[
			g(w) = g(\lambda_1 u_1 + \cdots + \lambda_n u_n) = \lambda_1 g(u_1) 
			+ \cdots + \lambda_n g(u_n) = \sum_{i=1}^{n} \lambda_i v_i\,.
		\]
		Com que els coeficients $\lambda_1,\ldots,\lambda_n$ són únics, 
		concloem que $f(w) = g(w)$. Però $w$ és un vector arbitrari, ergo
		\[
			\forall w \in E\quad f(w) = g(w) \Leftrightarrow f \equiv g\,.
		\]
	\end{proof}
\end{prop}

\begin{defi}[monomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{monomorfisme} si i només 
	si és injectiva.
\end{defi}

\begin{defi}[epimorfisme]
	Direm que una aplicació lineal $f$ és un \textit{epimorfisme} si i només si 
	és suprajectiva (exhaustiva).
\end{defi}

\begin{defi}[isomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{isomorfisme} si i només si 
	és un monomorfisme i un epimorfisme alhora---és a dir, si és bijectiva.
\end{defi}

\begin{defi}[endomorfisme]
	Direm que una aplicació lineal $f$ és un \textit{endomorfisme} si i només 
	si és de la forma $f\colon E \to E$---és a dir, si l'espai d'arribada i 
	l'espai de sortida coincideixen.
\end{defi}

\begin{defi}[automorfisme]
	Direm que una aplicació lineal $f$ és un \textit{automorfisme} si i només 
	si és un endomorfisme i un isomorfisme alhora---és a dir, si és un 
	endomorfisme bijectiu.
\end{defi}

\section{Nucli i imatge}
\begin{defi}[imatge! d'una aplicació]
	Sigui $f\colon E \to F$ una aplicació lineal. La imatge de $f$ es defineix 
	com
	\[
		\im f \defeq \{v \in F \mid \exists u \in E \colon f(u) = v \}\,;
	\]
	és a dir, com el conjunt de tot vector de $F$ que és la imatge d'[almenys 
	]un vector d'$E$.
\end{defi}


\begin{defi}[nucli]
	Sigui $f\colon E \to F$ una aplicació lineal. El nucli de $f$ es defineix 
	com
	\[
		\nuc f \defeq \{u\in E \mid f(u) = 0_F \}\,;
	\]
	és a dir, el conjunt de vectors d'$E$ que es transformen en el vector nul 
	de $F$ quan se'ls hi aplica $f$.
\end{defi}

\begin{prop}
	Una aplicació lineal $f\colon E \to F$ és suprajectiva (exhaustiva) si i 
	només si $\im f = F$.
	\begin{proof}
		D'una banda, per definició, $\im f \subseteq F$, sigui quina sigui 
		l'aplicació $f$. A més, si $f$ és suprajectiva, per tot $v\in F$ 
		existeix un $u\in E$ tal que $f(u) = w$. Per tant, per la definició 
		d'imatge, tot $v \in F$ pertany a $\im f\,$; ergo $F \subseteq \im f$. 
		Per tant $\im f = F$.
		
		La implicació recíproca és directa: si $\im f = F$, per definició 
		d'imatge, tot vector $v\in F$ compleix $\exists u \in E \colon f(u) = 
		v$. Aquesta és la definició de que $f$ sigui suprajectiva.
	\end{proof}
\end{prop}

\begin{prop}
Una aplicació lineal $f\colon E \to F$ és injectiva si i 
només si $\nuc f = \{0_E\}$.

\begin{proof}
	Suposem que $f$ és injectiva i que $\exists w \in \nuc f\colon w \ne 0_E$. 
	Llavors, per tot vector $u\in E$,
	\[
		u + w \ne u \quad\land\quad f(u + w) = f(u) + f(w) = f(u) + 0_F = 
		f(u)\,,
	\]
	cosa que contradiu la definició d'injectivitat (és a dir,  $f(a) = f(b) 
	\rightarrow a = b$). Per tant, si $f$ és injectiva, $\nuc f = \{0_E\}$.
	
	D'altra banda, suposem que $\nuc f = \{0_E\}$ però que $f$ no és injectiva.
	Llavors, existeixen vectors $u,v\in E$ tals que 
	\[
		u \ne v \quad \land \quad f(u) = f(v) \sRarr	f(u) - f(v) = f(u-v) = 
		0_F \,.
	\]
	Com que per hipòtesi $\nuc f = \{0_E\}$, $u-v$ ha de ser per força $0_E$, i 
	per tant $u - v = 0_E \sRarr u = v$, cosa que contradiu la hipòtesi que 
	$u\ne v$. Per tant queda demostrat el recíproc.
\end{proof}
\end{prop}


\begin{prop}
	El nucli d'una aplicació lineal $f\colon E\to F$ és un subespai vectorial 
	d'$E$.
	\begin{proof}
		Per tot parell de vectors $u,v\in \nuc f$,
		\[
			f(u + v) = f(u) + f(v) = 0_F + 0_F = 0_F \sRarr (u+v)\in \nuc f\,.
		\]
		Per tot escalar $c \in \k$ i vector $v \in \nuc f$,
		\[
			f(c\cdot v) = c\cdot f(v) = c\cdot 0_F = 0_F \sRarr c\cdot v \in 
			\nuc f\,.
		\]
	\end{proof}
\end{prop}

\begin{defi}[imatge! d'un subconjunt]
	Donada una aplicació lineal $f\colon E \to F$, la imatge d'un subconjunt 
	$W\subseteq E$ es defineix com
	\[
		f(W) \defeq \{v \in F\mid \exists u\in W \colon f(u) = v\}\,.
	\]
\end{defi}

\begin{prop}\label{prop:image-sev}
	Donada una aplicació lineal $f\colon E \to F$, si $W$ és un subespai 
	vectorial d'$E$, $f(W)$ és un subespai vectorial de $F$.
	\begin{proof}
		Siguin $u,v \in f(W)$ vectors tals que $u = f(w)$ i $v = f(y)$, on $w,y 
		\in W$ (els vectors $w,y$ existeixen per la definició de $f(W)$). 
		Llavors
		\[
			u + v = f(w) + f(y) = f(w+y)\,.
		\]
		Com que $W$ és un espai vectorial i per tant és tancat per la suma, 
		tenim que $w+y \in W$; ergo
		existeix un vector de $W$ la imatge del qual sigui $(u+v)$; és a dir, 
		$(u+v)\in f(W)$. A més, per tot escalar $c \in \k$,
		\[
			c\cdot u = c\cdot f(w) = f(c\cdot w)\,.
		\]
		Novament, raonem que $W$ és un e.v. i per tant és tancat per la 
		multiplicació escalar: $c\cdot w \in W$. Concloem que existeix un 
		vector de $W$ (ergo, $c\cdot w$) tal que la seva imatge sigui $c\cdot 
		u$; és a dir, $c\cdot u \in f(W)$.
	\end{proof}
\end{prop}

\begin{col}
	La imatge d'una aplicació lineal $f\colon E \to F$ és un subespai vectorial 
	de $F$.
	\begin{proof}
		$E\subseteq E$, i per tant, per la proposició \ref{prop:image-sev}, 
		$f(E) \doteq \im f$ és un s.e.v. de $F$.
	\end{proof}
\end{col}

\begin{defi}[preimatge]
	Donada una aplicació lineal $f\colon E \to F$, definim la preimatge d'un 
	subconjunt $W\in F$ com
	\[
		f^{-1}(W) \defeq \{u \in E \mid f(u) \in W \}\,.
	\]
\end{defi}

\section{Composició}
\begin{defi}[composició d'aplicacions]
	Donades dues aplicacions lineals $f\colon E \to F$ i $g\colon F \to G$ 
	definirem la \textit{composició de g amb f} com l'aplicació lineal
	\begin{equation*}
	\begin{aligned}
		g\circ f \qcolon & E &&\to &&G\\
		& v &&\mapsto &&(g\circ f)(v) \defeq g(f(v))\,.
	\end{aligned}
	\end{equation*}
	Usarem la notació $f_n \circ f_{n-1} \circ \cdots \circ f_1$ per 
	referir-nos a $f_n\circ (f_{n-1} \circ (\cdots \circ(f_2 \circ f_1) )$.
\end{defi}

\begin{defi}[inversa d'una aplicació]
	Direm que una aplicació lineal $f\colon E \to F$ és invertible si existeix 
	una aplicació lineal $g\colon F \to E$ tal que $g \circ f = f \circ g = 
	\Id_E$. En aquest cas direm que $g$ és l'inversa de $f$.
\end{defi}

\begin{prop}
	Una aplicació lineal $f$ és invertible si i només si és un isomorfisme.
\end{prop}
\section{Matriu d'una aplicació lineal}
Sigui $f \colon U \rightarrow V$ una aplicació lineal entre dos $\mathbb{K}$-espais vectorials de dimensió finita.
Siguin $\mathfrak{B}_u = \{ u_1, \dots , u_n \}$ i $\mathfrak{B}_v = \{ v_1, \dots, v_n \}$ bases dels espais $U$ i $V$,
respectivament.
\begin{defi} [matriu! d'una aplicació lineal]
La matriu de $f$ en les bases $\mathfrak{B}_u$ i $\mathfrak{B}_v$ és la matriu $( a_{ij} ) \in \mathcal{M}_{m \cross n}(\mathbb{K})$ amb
entrades determinades per:
\[
f(u_j) = \sum_{i=1}^m a_{ij}v_i, \quad j = 1, \dots, n.
\] En altres paraules, és la matriu que té per columnes les coordenades dels vectors $f(u_j)$.
\end{defi}
\begin{teo}[Teorema d'isomorfisme]
Per tota aplicació lineal $f \colon U \rightarrow V$ es considera l'espai quocient $U \backslash \Ker f$. Aleshores
l'aplicació $U \backslash \Ker f \rightarrow \im f \subseteq V$ que envia cada classe $[u]$ al vector $f(u)$ està
ben definida i és un isomorfisme d'espais vectorials.
\end{teo}

\begin{proof}
Sigui $f_{\ast}$ l'aplicació definida a l'enunciat. Si $[u] = [v]$ aleshores $u-v \in \Ker f$, per definició. 
Aleshores $f(u-v)=0$, però $f_{\ast}([u]= f(u) = f(v) = f_{\ast}([v])$. Per tant l'aplicació està ben definida.

Anem a veure que és lineal. $f_{\ast}([u] + [v]) = f_{\ast}([u+v]) = f(u+v) = f(u) + f(v) = f_{\ast}([u]) + f_{\ast}([v])$.
Però també $f_{\ast}(x[u]) = f(xu) = xf(u) = xf_{\ast}([u]).$
És exhaustiva ja que, donat $w \in \im f$ (i per tant $w = f(v)$ per algun $v \in V$) aleshores $w = f_{\ast}([v]) \in Im f_{\ast}.$
A més, és injectiva ja que si $f_{\ast}([u]) = f(u) = 0$ tenim $u \in \Ker f$ i aleshores $[u] = [0]$.  
\end{proof}

\section{Espai dual}
\begin{defi}[forma! lineal]
Sigui $V$ un $\mathbb{K}$-e.v. Una \textit{forma lineal} a $V$ és una aplicació lineal 
$\phi \colon V \rightarrow \mathbb{K}$.
\end{defi}

\begin{defi}[espai! dual]
Sigui $V$ un $\mathbb{K}$-e.v. L'espai dual de $V$ és el $\mathbb{K}$-e.v. de les formes lineals 
$\mathfrak{L}(V,\mathbb{K})$. Es denota $V^{\ast}$.
\end{defi}

\begin{prop}
Sigui $v_1, \dots, v_n$ una base d'un espai vectorial de dimensió finita $V$. Aleshores les aplicacions
lineals $v^{\ast}_{1}$  $\colon$ $V \rightarrow \mathbb{K}$ definides posant 
\[
v^{\ast}_i(\sum x_i v_i) = x_i
\]  són una
base de l'espai dual $V^{\ast}$, anomenada \textit{base dual}.
\end{prop}

\begin{proof}
Les aplicacions $v^{\ast}_i$, per construcció, són elements de $V^{\ast}$ donat que $v_i$ són una base
i són lineals. Donada una forma lineal $\phi \in V^{\ast}$, per cada $v = \sum x_i v_i \in V$ és:
\[
\phi(v) = \sum^{n}_{i=1} x_i \phi(v_i) = \sum_{i=1}^n \phi(v_i) v^{\ast}_i(v) \implies 
\phi = \sum^n_{i=1} \phi(v_i) v^{\ast}_i,
\] i per tant és la combinació lineal de les formes lineals $v^{\ast}_i$, amb coeficients $\phi(v_i)$.
Per veure que la independència lineal, considerem $\sum_{i=1}^{n} x_i v^{\ast}_i = \bar{0}$, 
on $\bar{0}$ és la forma lineal zero. En particular, per cada $v_j$ tenim
 $\sum_{i=1}^n x_i v^{\ast}_{i}(v_j) = 0.$ Ja que aquest sumatori, alhora, és igual a $x_j$, de fet, tots els
 coeficients de la combinació lineal són zero.
\end{proof}

\begin{col}
Si $V$ és de dimensió finita, aleshores $\dim V = \dim V^{\ast}$.
\end{col}

\begin{defi}[aplicació! lineal dual]
Sigui $f \colon U \rightarrow V$ una aplicació lineal. La seva aplicació lineal dual associada és l'aplicació lineal
$f^{\ast} \colon V^{\ast} \rightarrow U^{\ast}$ definida tal que $f^{\ast}(\phi) = \phi \circ f$, per cada $\phi \colon V \rightarrow \mathbb{K}$.
\end{defi}

\begin{prop}
Siguin $\mathfrak{B}_u$ i $\mathfrak{B}_v$ bases dels espais $U$ i $V$ i $\mathfrak{B}_u^{\ast}$ i
$\mathfrak{B}_v^{\ast}$ les bases duals respectives. Prenent les seves aplicacions $f$ i $f^{\ast}$ associades tenim:
\[
\text{Mat}(f^{\ast}; \mathfrak{B}_v^{\ast}, \mathfrak{B}_u^{\ast}) = \text{Mat}(f; \mathfrak{B}_u,
 \mathfrak{B}_v)^{t}.
\]
\end{prop}

\begin{proof}
Siguin $f(u_j) = \sum_{i=1}^m a_{ij} v_{i}$ i $f^{\ast}(v_j^{\ast}) = \sum_{i=1}^n b_{ij}u_i^{\ast}$, on $(a_{ij})$ són les 
entrades de la matriu Mat$(f; \mathfrak{B}_u,\mathfrak{B}_v)$ i $(b_{ij})$ coordenades 
de la matriu associada a l'aplicació dual. Aleshores, per cada vector $u = \sum_{i=1}^n x_i u_i \in U$:
\[
f^{\ast}(v_j^{\ast})(u) = (v_j^{\ast} \circ f)(u) = v_j^{\ast}(f(\sum_{k=1}^{n} x_i u_i)) = v_j^{\ast}(\sum_{i=1}^n x_i f(u_i)) =
\]
\[
= v_j^{\ast}(\sum_{i=1}^n x_i \sum_{k=1}^m a_{ki}v_k) = \sum_{i=1}^n x_i \sum_{k=1}^m a_{ki}v_j^{\ast}(v_k) = 
\sum_{i=1}^n x_i a_{ji} = \sum_{i=1}^n a_{ji} u_i^{\ast}(u).
\]
Aleshores $f^{\ast}(v_j^{\ast}) = \sum_{i=1}^n a_{ji}u_i^{\ast} = \sum_{i=1}^n b_{ij}u_{i}^{\ast}$ i deduïm $b_{ij} = a_{ji}$
\end{proof}

\begin{col}
Siguin $\mathfrak{B}_u$ i $\mathfrak{B}_v$ dues bases d'un e.v. $V$ de dimensió finita. Aleshores:
\[
\text{M}(\mathfrak{B}_u^{\ast} \rightarrow \mathfrak{B}_v^{\ast}) = (\text{M}(\mathfrak{B}_u \rightarrow  
\mathfrak{B}_v)^{-1})^{t}.
\]
\end{col}
\begin{col}
Tota base del dual $V^{\ast}$ d'un e.v. de dimensió finita $V$  és la base dual d'alguna base de $V$.
\end{col}
\begin{proof}
Siguin $\mathfrak{B}_{\phi}^{\ast}  = \{ \phi_1, \dots, \phi_n \}$ una base de $V^{\ast}$. Prenent qualsevol base
$\mathfrak{B}_u$
$ = \{ u_i \}$ de $V$ i la seva base dual $\mathfrak{B}_u^{\ast} = \{ u^{\ast}_{i} \}$. Sigui 
$A =$ M$(\mathfrak{B}_{\phi} \rightarrow \mathfrak{B}^{\ast}_u$) la matriu canvi de base. Si 
$\mathfrak{B}_{v} = \{v_{i}\}$
la base de $V$ tal que M$(\mathfrak{B}_{\phi} \rightarrow \mathfrak{B}^{\ast}_{u}$) =
$(A^{-1})^{t}$. 
Aleshores 
$\mathfrak{B}^{\ast}_{v}$ 
$= \{ v^{\ast}_{i} \} $
  compleix 
  M$(\mathfrak{B}^{\ast}_{v} \rightarrow \mathfrak{B}^{\ast}_{u}$) =$A$,
i per tant $\phi_i = v^{\ast}_{i}$ per tot $i$.
\end{proof}
