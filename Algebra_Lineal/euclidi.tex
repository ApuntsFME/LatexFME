

\chapter{Espai euclidià}
\section {Formes bilineals}
\begin{defi}[forma! bilineal] 
    Sigui $\E$ un $\mathbb{R}$-e.v. Una $\textbf{forma bilineal}$ a $\E$ és una aplicació $\phi \colon \E \cross \E \rightarrow \mathbb{R}$ tal que:
\begin{enumerate}[(i)]
	\item\label{defi:linealitat suma} $\phi(u+v,w) = \phi(u,w) + \phi(v,w)$.
	\item \label{defi:linealitat prod} $\phi(\lambda u, w) = \lambda \phi(u,w)$.
	\item Compleix \ref{defi:linealitat suma} i \ref{defi:linealitat prod} per l'altre terme anàlogament.
\end{enumerate} 
\end{defi}
A més, és simètrica si compleix $\phi(u,v) = \phi(v,u)$.

\begin{defi}[producte! escalar]
    Sigui $\E$ un $\mathbb{R}$-e.v. Un $\textbf{producte escalar}$ és una forma bilineal simètrica $\phi \colon \E \cross \E \rightarrow \mathbb{R}$ que compleix $\phi(u,u) > 0$, on $u$ és vector tal que $\forall u \neq 0$. 
    Aquesta condició s'anomena ser una forma bilineal definida positiva. Per notació, $\phi(u,v) = \vbrack{u, v}$ en aquest cas.
\end{defi}

\begin{defi}[espai! euclidià]
    L'espai euclidià és un $\mathbb{R}$-e.v on hi ha un producte escalar determinat. S'anota com a parell espai vectorial-producte escalar.
\end{defi}

\begin{defi}[norma!]
    Sigui $(V, \langle \cdot, \cdot \rangle)$ un espai euclidià. Es defineix la norma d'un vector $v \in V$ com el nombre real  $\|v\|$ = $\sqrt{\langle v,v \rangle }$. Els vectors de norma 1 es diuen unitaris.
\end{defi}

\begin{teo}[Desigualtat de Cauchy-Schwarz] \label{teo:cauchysch}
    Per a tot parell de vectors $u$, $v$ d'un espai euclidià es compleix: 
    \[
    |\langle u,v \rangle | \leq \|u\| \|v\|.
    \]
    La igualtat es compleix si i només si els vectors eren linealment dependents.
\end{teo}
\begin{proof}
Si $v = 0$, trivial. Altrament, es considera un vector $w = \frac{\langle u,v \rangle}{\langle v,v \rangle}v$. Aleshores 
\[
    \langle u-w, v\rangle = \langle u,v \rangle - \frac{\langle u,v \rangle }{ \langle v,v \rangle } \langle v,v \rangle = 0 \implies \langle u-w,w \rangle = 0.
\]
Aleshores tenim
\[
    \langle u,u \rangle = \langle w + (u-w), w + (u-w)\rangle = \langle w,w \rangle + \langle u-w, u-w \rangle \geq \langle w,w \rangle = \frac{\langle u,v \rangle^2}{\langle v,v \rangle^2}\langle v,v \rangle.
\]   
\end{proof}

\begin{prop} \label{prop:norma}
	Sigui $(V, \vbrack{\cdot, \cdot})$ un espai vectorial euclidià. L'aplicació $v \mapsto \|v\| \colon V \rightarrow \mathbb{R}$ satisfà:
	\begin{enumerate}[(i)]
		\item $\|v\| \geq 0, \|v\| = 0 \iff v = 0.$
		\item $\|xv\| = |x| \|v\|,$ on $|x|$ denota el valor absolut de l'escalar $x \in \mathbb{R}$,
		\item $\|u + v\|$ $\leq \|u\| + \|v\|$ (desigualtat triangular).
	\end{enumerate}

\end{prop}

\begin{proof}
	\begin{enumerate}[(i)]
		\item $\|v\| = 0 \iff \|v\|^2 = 0 \iff \vbrack{v,v} = 0 \iff v = 0$.
		\item $\|xv\|^2 = \vbrack{xv,xv} = x^2 \vbrack{v,v}.$ Només cal treure arrels.
		\item Aplicant el Teorema \ref{teo:cauchysch} tenim $\|u+v\|^2 = \vbrack{u+v,u+v} = \vbrack{u,u} + 2\vbrack{u,v} + \vbrack{v,v} \leq \|u\|^2 + 2 |\vbrack{u,v}| + \|v\|^2 \leq \|u\|^2 + 2 \|u\|\|v\| + \|v\|^2 = (\|u\| + \|v\|)^2.$ Només cal treure arrels quadrades.
	\end{enumerate}
\end{proof}

\begin{defi}[distància euclidiana]
	La distància (euclidiana) entre dos vectors d'un espai vectorial euclidià es definix com d($u,v$) = $\|u-v\|$. Observem com, gràcies a les propietats de \ref{prop:norma}
	és també definida positiva, és simètrica i compleix la desigualtat triangular.
\end{defi}
\begin{defi}[angle]
	L'angle que formen dos vectors no nuls d'un espai euclidià $V$ es defineix com l'angle $\phi \in [0, \pi]$ tal que:
	\[
	\cos\text{ }\phi = \frac{\vbrack{u,v}}{\|u\|\|v\|}.
	\]
\end{defi}

\begin{defi}[matriu! d'una forma bilineal]
Sigui $V$ un $\mathbb{K}$-e.v i sigui $\mathcal{B} = {v_1, \dots, v_n}$ una base. La matriu d'una forma bilineal $\Phi \colon V \cross V \rightarrow \mathbb{K}$ en aquesta base és definida com:
\[
\text{Mat}(\Phi; \mathcal{B}) := (\Phi(v_i,v_j))_{1 \leq i,j \leq n} = 
\begin{pmatrix}
\Phi(v_1,v_1) & \Phi(v_1, v_2) & \dots & \Phi(v_1, v_n) \\
\Phi(v_2,v_1) & \Phi(v_2, v_2) & \dots & \Phi(v_2, v_n) \\
\vdots & \vdots & & \vdots \\
\Phi(v_n, v_1) & \Phi(v_n, v_2) & \dots & \Phi(v_n, v_n) \\
\end{pmatrix} \in \mathcal{M}_n (\mathbb{K}).
\]
Observem que, si la forma bilineal és simètrica, aleshores la seva matriu associada és sempre simètrica en qualsevol base.
A més, si diem $\mathbf{A} = (a_{ij}) \in \mathcal{M}_n(\mathbb{K})$ a Mat$(\Phi, \mathcal{B})$, tenim $\Phi(u,v) = u^t A v$,
on $u = \sum x_iv_i$ i $v = \sum y_iv_i$ són expressats en la base $\mathcal{B}$.
\end{defi}
\begin{prop}[Criteri de Sylvester]
	Sigui $A = $Mat$(\phi, \mathfrak{B}_u) \in \mathcal{M}_n(\mathbb{R})$ la matriu d'una forma bilineal simètrica en un $\mathbb{R}$-e.v de dimensió $n$. 
	La forma $\phi$ és un producte escalar (definida positiva) $\iff \det((a_{ij})_{1 \leq i,j \leq k}) > 0$ per tot $k = 1,2, \dots ,n$.
\end{prop}
\begin{proof}
Donada la matriu $A = (a_{ij})_{1 \leq i,j \leq n} \in \mathcal{M}_n (\mathbb{K}$, per cada $k$ es denota $A_k$ com la submatriu $(a_{ij})_{1 \leq i,j \leq k}$, tal que
els menors principals de $A$ són els determinants de cada $A_k$.
Si $a_{11} \neq 0$ es pot construir una nova base $\mathfrak{B}_v$ d'aquesta manera:
\[
v_1 = u_1, \text{  } v_2 = u_2 - \frac{a_{12}}{a_{11}}u_1, \text{ \dots,  } v_n = u_n - \frac{a_{1n}}{a_{11}}u_1.
\]
I és base ja que s'obté fent transformacions elementals dels vectors. La matriu canvi de base és 
\[
P = \text{Mat}(\mathfrak{B}_v \rightarrow \mathfrak{B}_u) = 
\begin{pmatrix}
1 & -\frac{a_{12}}{a_{11}} & \hdots & -\frac{a_{1n}}{a_{11}} \\
0 & 1 & \hdots & 0 \\
\vdots & & \ddots & \vdots \\
0 & 0 & \hdots & 1 \\
\end{pmatrix}
\]
Només cal veure que els determinants són estrictament positius. Ho veurem per inducció sobre la dimensió $n$. 
Si $n = 1$ aleshores $a_{11} = \phi(u_1, u_1) > 0 \implies det(A) = a_{11} > 0$. Suposem demostrat per $n-1$. 
Definim una nova base $\mathfrak{B}_v$ com abans. Sigui $B =$ Mat$(\phi;\mathfrak{B}_v)$ = $P^t A P$. Per
cada índex $j>1$ tenim:
\[
\phi(v_1, v_j) = \phi(u_1, u_j - \frac{a_{1j}}{a_{11}}u_1) = a_{1j} - \frac{a_{1j}}{a_{11}a_{11}} = 0.
\]
Per tant 
\[
B = (\phi(v_i, v_j)) = 
\begin{pmatrix}
a_{11} & 0 & 0 & \hdots & 0 \\
0 & b_{22} & b_{23} & \hdots & b_{2n} \\
0 & b_{23} & b_{33} & \hdots & b_{3n} \\
\vdots & \vdots & \vdots & & \vdots \\
0 & b_{2n} & b_{3n} & \hdots & b_{nn} \\
\end{pmatrix} =
\begin{pmatrix}
a_{11} & 0 \\
0 & B' \\
\end{pmatrix}
\] amb $B' \in \mathcal{M}_{n-1}(\mathbb{R})$ la matriu del producte escalar $\phi$ restringit al subespai $\vbrack{v_2, \dots, v_n}$.
Per hipòtesi d'inducció $det(B') > 0$ i per tant $det(B) = a_{11}det(B') > 0$. Ja que $det(P) = 1$ es dedueix que $det(A) > 0$.
Ara anem a demostrar el recíproc. També ho farem per inducció sobre $n$. Suposem que la matriu $A$ té tots els seus menors 
principals estrictament positius. En particular, $a_{11} > 0$. Per tant podem definir una nova base com abans i tenim la mateixa
matriu $B$ definida a dalt. La matriu $B$ també compleix la condició de Sylvester ja que $B_k = P_k^t A_k P_k$ per tot $k$.
La matriu $B'$, associada a la restricció de la forma bilineal $\phi$ al subespai $W = \vbrack{v_2, \dots, v_n}.$ $B'$ també compleix
Sylvester ja que $a_{11}det(B_k') = det(B_{k+1}) > 0 \implies det(B_k) > 0$. Per hipòtesi, la restricció de $\phi$ al subespai $W$ de 
dimensió $n-1$ és un producte escalar. A més, $V = \vbrack{v_1} \oplus W$, per tant $V \ni v = xv_1 + w$, on $x \in \mathbb{R}$ i 
$w \in W$, i només pot ser el vector zero si $x=0$ i $w = 0$. I com que $\phi(v_1,w) = 0$, 
\[
\phi(v,v) = \phi(xv_1, xv_1) + \phi(w,w) = x^2a_{11} + \phi(w,w) > 0
\] 
si $x \neq 0$ o bé si $w \neq 0$. En altres paraules, si $v \neq 0$.
\end{proof}


\section{Projecció ortogonal}
Sigui $(V, \vbrack{\cdot, \cdot})$ un espai vectorial euclidià.
\begin{defi}[ortogonalitat]
 Dos vectors $u,v \in V$ es diuen ortogonals (o perpendiculars) si $\vbrack{u,v} = 0$. Es denota $u \perp v$.
 Dos subconjunts $S, T \subseteq V$ es diuen ortogonals, i es denota $S \perp T$, si $u \perp v$ $\forall u \in S, v \in T$.
 Una base $\mathfrak{B} = \{ v_i \}$ de $V$ es diu ortogonal si $v_i \perp v_j $ $\forall i \neq j$. La base $\{ w_i \}$ es diu
 ortonormal si és ortogonal i tots els seus vectors són unitaris.
\end{defi}
\begin{prop}[Gram-Schmidt]
Sigui ${u_1, \dots, u_n}$ una base de l'espai vectorial euclidià $(V, \vbrack{\cdot, \cdot}$. Es construeix una base ortonormal de l'espai de manera que els vectors es defineixen recursivament
d'aquesta manera:
\[
w_k = \frac{ v_k}{\|v_k\|}, \text{  on } v_k = u_k - \sum_{i = 1}^{k-1} \vbrack{u_k, w_i}w_i, \text{ per a } k = 1, \dots, n.
\]
\end{prop}

\begin{proof}
Primerament, $w_k$ són base ja que s'obtenen de $u_k$ fent transformacions elementals. A més, estan normalitzats per construcció.
Per veure que són ortogonals hem de veure que cadascun és ortogonal a tots els anteriors. Ho provarem per inducció.
La hipòtesi d'inducció és que, de fet, és ortogonal fins a $w_{k-1}$. Aleshores per tot $j < k$:
\[
\vbrack{w_k,w_j} = \frac{1}{\|v_k\|} \vbrack{v_k,w_j} = \frac{1}{\|v_k\|} \vbrack{u_k - \sum_{i=1}^{k-1}\vbrack{u_k,w_i}, w_j} =
\]
\[
= \frac{ 1}{\|v_k\|} ( \vbrack{u_k, w_j} - \sum_{i=1}^{k-1} \vbrack{u_k, w_i}\vbrack{w_i, w_j} ).
\]
Per hipòtesi d'inducció cada terme $\vbrack{w_i, w_j}$ del sumatori és 1 si $i = j$ i 0 altrament. Aleshores el valor del sumatori és exactament $\vbrack{u_k,w_j}$.
Aleshores $\vbrack{w_k,w_j} = 0$.
\end{proof}
\begin{col}
Tot espai euclidià de dimensió finita té alguna base ortonormal i tota base ortonormal d'un subespai euclidià pot ampliar-se a una base ortonormal de tot l'espai.
\end{col}

\begin{defi}[matriu! ortogonal]
Una matriu $A \in \mathcal{M}_n(\mathbb{K})$ és ortogonal si $A^tA = Id_n \in \mathcal{M}_n(\mathbb{K})$.
Això equival a $A^{-1} = A^{t}$, i per tant també a $AA^t = Id_n$. Les matrius ortogonals són invertibles.
Ser una matriu ortogonal equival a que les seves columnes formin una base ortonormal de l'espai euclidià $\mathbb{R}^n$.
\end{defi}

\begin{prop}
Sigui $\mathfrak{B}_w = \{ w_1, \dots, w_n \}$ una base ortonormal de l'espai euclidià $V$. Aleshores una altra base $\mathfrak{B}_u = \{ u_1, \dots, u_n \}$ 
és també ortonormal $\iff$ les matrius canvi de base són matrius ortogonals.
\end{prop}
\begin{proof}
Que $\mathfrak{B}_w$ sigui ortonormal equival a Mat$(\vbrack{\cdot,\cdot}; \mathfrak{B}_w) = Id_n$. 
En fer el canvi de base, Mat$(\vbrack{\cdot,\cdot}; \mathfrak{B}_u) = P^t Id_n P = P^tP$, amb $P =$ Mat($\mathfrak{B}_u \rightarrow \mathfrak{B}_w$).
La base $ \mathfrak{B}_u$ és ortonormal quan la matriu és $Id_n$, però aleshores $P$ i $P^{-1}$ són ortonormals.
\end{proof}

\begin{defi}[subespai! ortogonal]
Sigui $W \subseteq V$ d'un espai euclidià. Aleshores el conjunt 
\[
W^{\perp} = \{ v \in V \colon v \perp w, \forall w \in W \}
\] s'anomena ortogonal del conjunt $W$. Això implica que si $W = \vbrack{ w_1, \dots, w_r}$, aleshores
$v \in W^{\perp} \iff v \perp w_i$ $\forall i = 1, \dots, r$.
\end{defi}

\begin{defi}[suma! ortogonal]
La suma $W = W_1 + W_2$ de dos subespais $W_1, W_2 \subseteq V$ que siguin ortogonals
s'anomena suma ortogonal. La suma ortogonal sempre és directa. A vegades es denota $W_1 \perp W_2$.

És directa ja que si $W = W_1 \perp W_2$, prenem $w_1 + w_2 = 0$ una suma de vectors dels dos subespais que
és igual a zero. Aleshores per cada índex $j$ tenim $0 = \vbrack{w_j, 0} = \vbrack{w_j,\sum w_i} = \vbrack{w_j,w_j} \implies
w_j = 0$. 
\end{defi}

\begin{teo}[Teorema de la projecció ortogonal]
Per a tot subespai $W \subseteq V$ d'un espai euclidià de dimensió finita, l'espai $V$ és la suma ortogonal
\[
V = W \perp W^{\perp}.
\]
\end{teo}
\begin{proof}
La condició de definit positiu del producte escalar $\implies W \cap W^{\perp} = \{ 0 \}$, i per tant la suma és directa.
Només cal veure que $V \ni v = u + s$, on $u \in W$ i $s \in W^{\perp}$. Sigui $w_1, \dots, w_r$ base ortogonal de $W$
i sigui $w_{r+1}, \dots, w_n$ una ampliació a una base ortogonal de tot $V$. Aleshores tot vector $v \in V$ s'escriu com $\sum x_iw_i$,
on els coeficients són $x_i = \vbrack{v, w_i}$, aleshores $v$ és suma de $\sum_{i=1}^r \in W$ i $\sum_{i=r+1}x_iw_i \in W^{\perp}$.
\end{proof}

\begin{col}
Si $W \subseteq \mathbb{R}^n$ és 
\[
W = \left \{ (x_1, \dots,x_n) \in \mathbb{R}^n \colon \sum_{j=1}^n a_{ij}x_j = 0, \text{ } \forall i = 1, \dots,m \right \},
\] aleshores el seu ortogonal $W^{\perp}$ és el subespai generat pels $m$ vectors 
\[
v_1 = (a_{11}, a_{12}, \dots, a_{1n}), \dots, v_m = (a_{m1}, \dots, a_{mn}).
\]
\end{col}
\begin{proof}
Cada $v_i$ és ortogonal a tot vector de $W$ i per tant es té la inclusió: \\ $\vbrack{v_1, \dots, v_m} \subseteq W^{\perp}$.
Si $r$ és el rang del sistema, aleshores dim $W = n-r$ i pel teorema de la projecció ortogonal dim$W^{\perp} = r$. Per tant
la inclusió és una igualtat.
\end{proof}
\begin{col}
$\forall W \subseteq V$ d'un espai euclidià de dimensió finita, $(W^{\perp})^{\perp} = W$.
\end{col}
\begin{proof}
Sigui $w \in W$. Per tot vector $w' \in W^{\perp}$ tenim $w \perp w'$. Aleshores $w \in (W^{\perp})^{\perp}$. 
D'altra banda, $V = W \perp W^{\perp}$ i $V = W^{\perp} \perp (W^{\perp})^{\perp}$. Per tant tenen la mateixa dimensió,
i per tant són iguals.
\end{proof}

\begin{defi}[projecció ortogonal sobre un subespai]
Sigui $W \subseteq V$ un subespai vectorial d'un espai euclidià. La projecció ortogonal de $V$ sobre $W$ és l'aplicació
$\pi_W \colon V \rightarrow W$ tal que si $v = w_1 + w_2$ i $w_1 \in W$ i $w_2 \in W^{\perp}$, $\pi_W(v) = w_1$.
\end{defi}
A més, la podem caracteritzar mitjançant:
\begin{prop}
d($v, \pi_W(v)) =$ min\{d($v,w) \colon w \in W$\}.
\end{prop}
\begin{proof}
Ja que $V = W \perp W^{\perp}$, el vector $v$ s'escriu de manera única com $v = w_1 + w_2$, les projeccions sobre $W$ i 
sobre el seu ortogonal. Per tot $w \in W$ tenim:
\[
\text{d}(v,w)^2 = \vbrack{v-w, v-w} = \vbrack{w_1+w_2-w, w_1+w_2 - w} = \vbrack{w_1 - w, w_1-w} + \vbrack{w_2, w_2}.
\]
El valor mínim es dona quan el primer sumant és 0 ($w = w_1$), aleshores d$(v,w_1) = \sqrt{\vbrack{w_2,w_2}} = \|\pi_{W^{\perp}}(v)\|$.
Però la distància es definia com la diferència de normes, aleshores d($v,W$) = $\|\pi_{W^{\perp}}(v)\| = \|v - \pi_{W}(v)\|$.
\end{proof}
Per calcular les coordenades de la projecció ortogonal: si $w_1, \dots, w_r$ és base de $W \subseteq V$ i $\pi_W(v) = x_1w_1 + \dots + x_rw_r$,
aleshores les coordenades són la solució a $Ax = b$ on:
\[
A = 
\begin{pmatrix}
\vbrack{w_1,w_1} & \vbrack{w_1,w_2} & \dots  & \vbrack{w_1,w_r} \\
\vbrack{w_2, w_1} & \vbrack{w_2, w_2} & \dots & \vbrack{w_2, w_r} \\
\vdots & \vdots & & \vdots \\
\vbrack{w_r, w_1} & \vbrack{w_r, w_2} & \dots& \vbrack{w_r, w_r} \\
\end{pmatrix}
, \text{ }
x = \begin{pmatrix}
x_1 \\
x_2 \\
\vdots \\
x_r \\
\end{pmatrix}
,  \text{ }
b = \begin{pmatrix}
\vbrack{w_1,v} \\
\vbrack{w_2, v} \\
\vdots \\
\vbrack{w_r, v} \\
\end{pmatrix}.
\]
\section{Endomorfismes simètrics}
\begin{defi}[endomorfisme! simètric]
Un endomorfisme $f \in \End(V)$ d'un espai vectorial euclidià $(V, \vbrack{\cdot, \cdot})$ es diu simètric si
$\forall u,v \in V$ es compleix $\vbrack{f(u),v} = \vbrack{u,f(v)}.$
\end{defi}
\begin{prop}
Endomorfisme simètric $\iff$ la seva matriu associada en una base ortonormal és simètrica.
\end{prop}
\begin{proof}
La matriu del producte escalar en una base ortonormal $\mathfrak{B} = \{ w_i \}$ és la identitat. Prenem $A = (a_{ij}) =$ Mat$(f;\mathfrak{B})$,
de manera que $f(w_j) = \sum_{i=1}^{n} a_{ij}w_{i}$. Per linealitat, $\vbrack{f(u),v} = \vbrack{v,f(u)}$ per tot $u,v \in V$ si i només si es compleix
per tots els vectors de la base. Però:
\[
\vbrack{f(w_r), w_s} = \vbrack{\sum_{i=1}^{n}a_{ir}w_i, w_s} = \sum_{i=1}^{n}a_{ir} \vbrack{w_i,w_s} = a_{sr},
\]
\[
\vbrack{w_r, f(w_s)} = \vbrack{w_r, \sum_{i=1}^{n}a_{is}w_i} = \sum_{i=1}^{n}a_{is} \vbrack{w_i,w_s} = a_{rs},
\] i per tant equival a que la matriu $A$ sigui simètrica.
\end{proof}

\begin{prop} \label{prop:veps reals}
Tota matriu simètrica $A \in \mathcal{M}_n(\mathbb{R})$ té tots els VEPs reals. Equivalentment, el polinomi
característic té tantes arrels reals com el seu grau, comptant multiplicitats.
\end{prop}
\begin{proof}
També podríem pensar que $A \in \mathcal{M}_n(\mathbb{C})$. Pel teorema fonamental de l'àlgebra, el polinomi
característic té alguna arrel complexa, i per tant existeix algun VAP complex $\lambda$. Sigui $z = (z_1, \dots, z_n) \in \mathbb{C}^n$
un VEP de VAP $\lambda$ ($Az = \lambda z)$. Com que les entrades de la matriu són nombres reals, prenent conjugats es veu
$A \bar{z} = \bar{\lambda} \bar{z}$, amb $\bar{z} = (\bar{z_1}, \dots, \bar{z_n})$ el vector que té les components conjugades de $z$.
Observem a més com 
\[
\sum_{i=1}^{n} |z_i|^2 = \sum_{i=1}^n z_i \bar{z_i} = z^{t}\bar{z} = \bar{z}^tz = \sum_{i=1}^n \bar{z_i}z_i
\] és un real positiu. Però $A$ era simètrica:
\[
\lambda  \sum_{i=1}^n |z_i|^2 = \lambda \bar{z}^t z = \bar{z}^t \lambda z = \bar{z}^t A z = (\bar{z}^tAz)^t = z^tA^t\bar{z} = z^tA\bar{z} = z^t \bar{\lambda} \bar{z} = \bar{\lambda}z^t\bar{z} = \bar{\lambda} \sum_{i=1}^n |z_i|^2.
\] Per tant $\lambda = \bar{\lambda}$ i per tant el VAP $\lambda$ és un nombre real.
\end{proof}

\begin{teo}[Teorema espectral]
Tot endomorfisme simètric d'un espai vectorial euclidià diagonalitza i admet una base de VEPs ortonormal.
\end{teo}

\begin{proof}
Per inducció sobre la dimensió de l'espai ($n$). Si $n=1$ només cal normalitzar el VEP no nul. Per hipòtesi
d'inducció, suposem demostrat per $n-1$ i sigui $f \in \End(V)$ un endomorfisme simètric d'un espai euclidià de dimensió $n$.
Per \ref{prop:veps reals}, l'endomorfisme té VAPs reals. Sigui $\lambda \in \mathbb{R}$ un VAP i sigui $v \in V$ un VEP de VAP $\lambda$.
Sempre, a més, el podem normalitzar i seguirà sent VEP. El teorema de la projecció ortogonal ens assegura que
$\vbrack{v} \oplus \vbrack{v}^{\perp} = V$. El subespai $\vbrack{v}^{\perp}$ és invariant per l'endomorfisme $f$.
Això és així ja que 
\[
u \in \vbrack{v}^{\perp} \iff \vbrack{u,v} = 0 \implies \vbrack{f(u),v} = \vbrack{u, f(v)} = \vbrack{u, \lambda v} = \lambda \vbrack{u,v} = 0 \implies
\]
\[ \implies f(u) \in \vbrack{v}^{\perp}. 
\]
Per hipòtesi d'inducció, la restricció al subespai $\vbrack{v}^{\perp}$ de dimensió $n-1$, té una base ortonormal de VEPs. En afegir $v$, es té una base ortonormal de VEPs
de tot l'espai.
\end{proof}


\section{SVD}
\begin{teo}[Singular value decomposition]
Sigui $A$ una matriu real $m \cross n$. Aleshores existeix una descomposició $A = U \cdot D \cdot V^t$, on
$U$ és $m \cross m$, $V$ és $n \cross n$, $U,V$ són ortogonals i $D$ és:
\[
D = \begin{pmatrix}
\sigma_1 & & 0 & \dots & 0 \\
&  \ddots & & & \vdots \\
0 & & \sigma_r & & \vdots \\
0 & \dots & 0 & \dots & 0 \\
\vdots & &  & & \vdots \\
0 & \dots & \dots0 & \dots & 0 \\
\end{pmatrix},
\] amb $\sigma_1 \geq \sigma_2 \geq \dots \geq \sigma_r > 0$ i $ r = \rang A$. Els $\sigma$ s'anomenen valors 
singulars de $A$ i són unívocament determinats per $A$.
\end{teo}
\begin{proof}
La demostració, (vista en profundidtat a Àlgebra Lineal Numèrica), es fa per construcció. Primer vegem que $S = A^t \cdot A$ 
és sempre una matriu simètrica. 
En segon lloc, si $\lambda_1 \geq \dots \geq \lambda_r$ són els VAPs diferents de zero de $S$ 
$\implies$ els valors singulars són $\sigma_1 = \sqrt{\lambda_1}$, \dots, $\sigma_r = \sqrt{\lambda_r}$, i vegem com $A^tA$ té 
sempre VAPs no negatius ja que si $B$ és una matriu quadrada, té els mateixos VAPs que la seva transposada ja que tenen el mateix
polinomi característic. A més, les columnes de $V$ són una base ortonormal de $v_1, \dots, v_n$ de VEPs de $S$.
Per últim, per construir $U$, cal prendre $u_1 = \frac{1}{\sigma_1}Av_1, \dots , u_r = \frac{1}{\sigma_r}Av_r$, i formen les columnes de $U$.
\end{proof}

\begin{defi}[norma! 2-matricial]
$\|A\|_2 = \max_{\|x\| = 1} \|Ax\|$.
\end{defi}

\begin{prop}
\begin{enumerate}[(i)]
	\item[]
	\item $\|A\|_2 = \max_{x\neq 0} \frac{\|Ax\|}{\|x\|}.$
	\item $\|Av\| \leq \|A\|_2\|v\|$ $\forall v$.
	\item $\|AX\|_2 = \|A\|_2$ si $X$ és una matriu ortogonal.
	\item $\|YA\|_2 = \|A\|_2$ si $Y$ és una matriu ortogonal.
	\item $\|AB\|_2 \leq \|A\|_2 \|B\|_2$.
\end{enumerate}
\end{prop}
\begin{proof}
\begin{enumerate}[(i)]
	\item Vegem com $\max_{\|x\| = 1} \|Ax\| =\max_{x\neq 0} \frac{\|Ax\|}{\|x\|}$ ja que prenem una $x$ qualsevol no nul·la i la normalitzem,
	de manera que $\|x\| = 1$.
	\item  $\|A\|_2 \|x\| = \|x\| \max_{y \neq 0} \|Ay\|/\|y\| \geq \|x\| \|Ay\|/\|y\|.$ Ja que $y$ és arbitrari, prèn $y = x$, aleshores:
	$\|A\|_2 \|x\| \geq \|Ax\|$.
	\item Prenem $\|Qx\| = \sqrt{(Qx)^t(Qx)} = \sqrt{x^tQ^tQx} = \sqrt{x^tx} = \|x\|$. Per tant podem fer: $\|AQ\|_2 = \max_{\|x\| = 1} \|AQx\| = \max_{\|Qx\| = 1} \|AQx\|_2 = \max_{\|y\| = 1} \|Ay\|_2 = \|A\|_2.$
	\item $\|YA\|_2 = \max_{\|x\| = 1} \|YAx\| = \max \sqrt{(YAx)^t (YAx)} =\max \sqrt{x^t A^t Y^t Y A x }=$\\ $ \max \sqrt{x^t A^tA x} = \|A\|_2.$
	\item $\|AB\|_2 = \max_{\|x\|=1} \|ABx\| \leq \max\|A\| \|Bx\| = \|A\| \max \|Bx\| = \|A\|_2 \|B\|_2$. 

\end{enumerate}
\end{proof}
\begin{teo}[Interpretació geomètrica de la SVD]
\begin{enumerate}[(i)]
	\item[]
	\item $\|A\|_2$ = $\sigma_1$.
	\item $\max_{\|x\| = 1} \|Ax\| = \|Av_1\|.$
	\item  $
	\min_{\|x\|=1} \|Ax\| =
	\begin{cases}
	\sigma_n \text{ si $A$ té rang $n$.} \\
	0 \text{ altrament.} \\
	\end{cases}
	$
	\item Si $A$ és invertible, $\|A^{-1}\|_2 = \frac{1}{\sigma_r}$.
\end{enumerate}
\end{teo}

\begin{proof}
\begin{enumerate}[(i)]
	\item $\|A\|_2 = \max_{\|x\| = 1} \|Ax\| = \max_{\|x\| = 1} \|UDV^tx\| = \max_{\|y\| = 1} \|UDx\| = \max_{\|z\| = 1} \|Dz\| = \|D\|_2$. Ara bé, 
	podem prendre $e_1$, que té norma 1, i, ja que $\sigma_1$ és el més gran en mòdul, $\|A\|_2 = \sigma_1$.
	\item max$_{\|x\| = 1} \|Ax\| =$ max$_{\|x\| = 1} \|Dx\|$ = max$_{\|x\| = 1} \|\sigma_j x\|$, però ja que busquem el màxim, $j = 1$, i per tant $x$ ha de ser VEP de
	VAP $\sigma_1$, és a dir, $v_1$. Per tant, max$_{\|x\| = 1} \|Ax\| =$ max$_{\|x\| = 1} \|Dx\|$ = $\|Av_1\|$.
	\item Anàleg. Però en aquest cas distingim dos possibles resultats. Si la matriu $D$ no és $n \cross n$, això significa que hi ha almenys $1$ fila de $0$'s, aleshores
	ja que els $\sigma$ són no negatius, en particular, sabem que hi haurà almenys un que serà el mínim i serà $0$. Altrament, serà el $\sigma$ amb menor mòdul, 
	és a dir, l'últim.
	\item Només cal observar que la inversa d'una matriu diagonal és la mateixa però invertida, és a dir d'aquesta forma:
	\[
	\begin{pmatrix}
	\frac{1}{\sigma_1} & 0 & \hdots & 0 \\
	0 & \frac{1}{\sigma_2} & \hdots & 0 \\
	\vdots & & \ddots & \\
	0 & 0 & 0 & \frac{1}{\sigma_r} \\
	0 & 0 & \hdots & 0 \\
	\vdots & \vdots & \vdots & \vdots \\
	\end{pmatrix}
	\]
	Per tant, el màxim valor s'atany al terme $\frac{1}{\sigma_r}$, aleshores $\|A\|^{-1} = \frac{1}{\sigma_r}$.
\end{enumerate}
\end{proof}
