\definecolor{ForestGreen}{rgb}{0,0.6,0}

\newcommand{\bl}{\color{blue}}
\newcommand{\rd}{\color{red}}
\newcommand{\gre}{\color{ForestGreen}}

\newcommand{\ucarr}{\mathbin{\text{\rotatebox[origin=c]{270}{$\curvearrowleft$}}}}
\newcommand{\dcarr}{\mathbin{\text{\rotatebox[origin=c]{270}{$\curvearrowright$}}}}

\chapter{Matrius i sistemes lineals}
\section{Teorema de Laplace}
\begin{defi}[determinant]
	Sigui $M \in \mathcal{M}_n(\mathbb{R})$ una matriu $n\times n$. Si $n>1$, anomenarem determinant de $M$ (abreviat $\det M$) a l'expressió següent:
	\[
		\det M \defeq \sum_{j=1}^{n} m_{1,j}\cdot(-1)^{1+j}\det M_{1,j}  \,,
	\]
	on $M_{i,j}$ denota la submatriu que s'obté traient la fila $i$ i la columna $j$ de $M$. En el cas $n=1$, el determinant serà igual a l'únic coeficient de la matriu.
\end{defi}

\begin{teo}[Teorema de Laplace]\label{teo:laplace}
	L'expansió del determinant per la primera fila és equivalent a a l'expansió del determinant per qualsevol fila o columna.
	
\end{teo}

\begin{proof}
	La demostració del teorema parteix de la demostració de cada un dels lemes \ref{lema:columna}, \ref{lema:intercanvi}, i \ref{lema:iesimafila}---que s'enuncien i es demostren a continuació del teorema.
	
	Un cop demostrats els lemes \ref{lema:columna}, \ref{lema:intercanvi}, i \ref{lema:iesimafila} la demostració del teorema complet és gairebé immediata. Ja s'ha demostrat que l'expansió per la $i$-èsima fila és equivalent a l'expansió per primera fila (lema \ref{lema:iesimafila}), i que aquesta és equivalent a l'expansió per primera columna (lema \ref{lema:columna}). Podem fer exactament el mateix raonament que hem fet per demostrar el lema \ref{lema:iesimafila}, però per columnes en lloc de files, per demostrar que l'expansió per primera columna és equivalent a l'expansió per la $j$-èsima columna. Per tant queda demostrat que expandir per la $i$-èsima fila és equivalent a expandir per la $j$-èsima columna.
\end{proof}

\begin{lema}\label{lema:columna}
	L'expansió del determinant per la primera fila és equivalent a l'expansió del determinant per la primera columna:
	\[
		\det M = \sum_{j=1}^{n} m_{1,j}\cdot(-1)^{1+j}\det M_{1,j} =
		\sum_{i=1}^{n} m_{i,1}\cdot(-1)^{i+1}\det M_{i,1}
	\]
	\begin{proof}
		Sigui $A \in \mathcal{M}_n(\mathbb{R})$. Per $n = 2$ és cert:
		\begin{equation*}
			\begin{gathered}		
				\det A = \sum_{j=1}^{2} a_{1j}\cdot (-1)^{1+j}\det A_{1j} = a_{11}
				\begin{vmatrix}
					a_{22} 
				\end{vmatrix} - a_{12}
				\begin{vmatrix}
					a_{21} 
				\end{vmatrix} =\\
				= a_{11}a_{22} - a_{12}a_{21} =\\
				= a_{11}
				\begin{vmatrix}
					a_{22} 
				\end{vmatrix} - a_{21}
				\begin{vmatrix}
					a_{12} 
				\end{vmatrix} = \sum_{i=1}^{2} a_{i1}\cdot (-1)^{i+1}\det A_{i1}\,.
			\end{gathered}
		\end{equation*}
		
		Suposem que és cert per $n-1$, per un valor arbitrari de $n>2$ (hipòtesi d'inducció). El determinant d'$A\in\mathcal{M}_n(\mathbb{R})$ vindrà donat per
		\begin{equation}\label{eq:det1}
			\det A = \sum_{j=1}^{n} a_{1,j}\cdot(-1)^{1+j}\det A_{1,j}  \,.
		\end{equation} 
		El primer terme d'aquesta expressió serà $a_{1,1}\cdot C_{1,1}$, on $C_{1,1} =(-1)^{1+1}\cdot\det A_{1,1} = \det A_{1,1}$. Aquest coincideix exactament amb el primer terme de l'expansió per primera columna. 
		
		Analitzem la resta de termes ($j>1$). Sigui $\tilde{A} \defeq A_{1,j}$ i siguin $\tilde{a}_{\ell,m}$ els seus elements. La matriu $\tilde{A}$ és d'ordre $n-1$; per tant, per hipòtesi d'inducció, el seu determinant es pot calcular expandint per la primera columna: 
		\begin{equation}\label{eq:subdet}
			\det \tilde{A} = \sum_{\ell=1}^{n-1}\tilde{a}_{\ell,1}\cdot (-1)^{\ell+1}\det \tilde{A}_{\ell,1}\,. 
		\end{equation} 
		Com que aquesta matriu s'obté eliminant la primera fila i la $j$-èsima columna (on, recordi's, $j>1$), es té l'equivalència $\forall \ell \le n-1 : {\tilde{a}_{\ell,1} = a_{\ell+1,1}}\,$. De manera similar, $\tilde{A}_{\ell,1} = (A_{1,j})_{\ell,1}= A_{(1,\ell+1),(j,1)}$, on la notació $M_{(a,b),(\alpha,\beta)}$ denota la submatriu que s'obté eliminant les files $a$, $b$ i les columnes $\alpha$, $\beta$ de la matriu $M$. Visualitzant-ho:
		\begin{equation*}
			\begin{split}
				A =&
				\begin{pmatrix}
					a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
					a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
					\vdots  & \vdots  & \ddots & \vdots  \\
					a_{n,1} & a_{n,2} & \cdots & a_{n,n} 
				\end{pmatrix}\\
				%
				A_{1,j} =&\
				\begin{blockarray}{rcccccc}
					\begin{block}{*{7}{>{\scriptstyle\color[gray]{0.6}}c<{}}}
						1&	\cdots & j-1& j& \cdots & n-1 & \\
					\end{block}
					\begin{block}{(cccccc)*{1}{>{\scriptstyle\color[gray]{0.6}}l<{}}}
						\mathbf{a_{2,1}} & \cdots & a_{2,j-1} & a_{2,j+1} & \cdots & a_{2,n} & 1 \\
						\vdots & \ddots & \vdots & \vdots &	\ddots & \vdots & \vdots \\
						\mathbf{a_{n,1}}&   \cdots&	a_{n,j-1}&	a_{n,j+1}&	\cdots&		a_{n,n}&	n-1\\
					\end{block}
				\end{blockarray}\\
				%
				\tilde{A}_{\ell,1} =&
				\begin{pmatrix}
					a_{2,2} & \cdots & a_{2,j-1} & a_{2,j+1} & \cdots & a_{2,n} \\
					\vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
					a_{\ell,2} & \cdots & a_{\ell,j-1} & a_{\ell,j+1} & \cdots & a_{\ell,n} \\
					a_{\ell+2,2} & \cdots & a_{\ell+2,j-1} & a_{\ell+2,j+1} & \cdots & a_{\ell+2,n} \\
					\vdots & \ddots & \vdots & \vdots & \ddots & \vdots \\
					a_{n,2}      & \cdots & a_{n,j-1}      & a_{n,j+1}      & \cdots & a_{n,n}      \\
				\end{pmatrix} = A_{(1,\ell+1),(j,1)}
				\,.
			\end{split}
		\end{equation*}
		Per tant podem escriure \eqref{eq:subdet}, fent el canvi d'índex $k\defeq \ell+1$, com 
		\begin{equation}\label{eq:jthsubdet}
			\det A_{1,j} = \sum_{k=2}^{n} a_{k,1}\cdot(-1)^{k}\det A_{(1,k),(j,1)}\,.
		\end{equation}
		
		A continuació, separant el primer terme de \eqref{eq:det1}, i substituint $\det A_{1,j}$ per l'expressió \eqref{eq:jthsubdet} a la resta de termes, obtenim
		\begin{equation}\label{eq:parrafada}
			\begin{split}
				\det A ={}& a_{1,1}\cdot A_{1,1} \\
				&+\sum_{j=2}^{n} \left(a_{1,j} (-1)^{1+j}  \sum_{k=2}^{n} a_{k,1} (-1)^{k} \det A_{(1,k),(j,1)}\right)\,.
			\end{split}
		\end{equation}
		Ara manipulem algebraicament el sumatori  ($\sum_{j=2}^n [\cdots]$) per tal de ``girar'' els sumatoris:
		\begin{multline}\label{eq:sumofjth}
			\sum_{j=2}^{n} \left(a_{1,j} (-1)^{1+j}  \sum_{k=2}^{n} a_{k,1} (-1)^{k} \det A_{(1,k),(j,1)}\right)=\\
			=\sum_{j=2}^{n} \left(  \sum_{k=2}^{n} a_{1,j}\cdot a_{k,1} (-1)^{1+j+k} \det A_{(1,k),(j,1)}\right)=\\
			=\sum_{k=2}^{n} \left(  \sum_{j=2}^{n} a_{1,j}\cdot a_{k,1} (-1)^{1+j+k} \det A_{(1,k),(j,1)}\right)=\\
			=\sum_{k=2}^{n} \left(a_{k,1} (-1)^{k+1}  \sum_{j=2}^{n} a_{1,j} (-1)^{j} \det A_{(1,k),(j,1)}\right)\,.
		\end{multline}
		
		Ara, fent el mateix raonament que ens ha fet arribar a \eqref{eq:jthsubdet} però en direcció inversa, notem que el sumatori $\sum_{j=2}^{n}[\cdots]$ és l'expansió per primera fila del determinant de la matriu $A_{k,1}$. Per tant, l'expressió \eqref{eq:sumofjth} queda així:
		\begin{equation}\label{eq:simplesumofjth}
			\sum_{k=2}^{n} a_{k,1} (-1)^{k+1} \cdot A_{k,1} \,.
		\end{equation}
		Finalment, tornant a substituir \eqref{eq:simplesumofjth} en l'equació \eqref{eq:parrafada}, obtenim
		\begin{equation}
			\begin{split}
				\det A = a_{1,1}\cdot A_{1,1}
				+\sum_{k=2}^{n} a_{k,1}\cdot (-1)^{k+1} A_{k,1}\,.
			\end{split}
		\end{equation}
		Noteu que ara podem tornar a introduir el primer terme ($a_{1,1}\cdot A_{1,1}$, que és igual a $a_{1,1}\cdot(-1)^{1+1} A_{1,1}$) al sumatori:
		\begin{equation}
			\begin{split}
				\det A = \sum_{i=1}^{n} a_{i,1}\cdot (-1)^{i+1} A_{k,1}\,.
			\end{split}
		\end{equation}
		Aquesta darrera expressió és l'expansió per primera columna del determinant d'$A$.
	\end{proof}
\end{lema}

\begin{lema}\label{lema:intercanvi}
	Sigui $A \in \mathcal{M}_n(\mathbb{R})$. El determinant d'una matriu $B$ que s'obté intercanviant dues files adjacents de la matriu $A$ és $\;-\det A$.
	\begin{proof}
		Per $n=2$, és cert: 
		\[
			\begin{vmatrix}
				a & b \\
				c & d 
			\end{vmatrix} = ad - cb = -(cb - ad) = -
			\begin{vmatrix}
				c & d \\
				a & b 
			\end{vmatrix}\,. 
		\]
		Suposem que és cert per $n - 1$, per un valor arbitrari de $n > 2$ (hipòtesi d'inducció). Sigui $A$ una matriu $n\times n$ i $B$ la matriu obtinguda intercanviant les files $r$ i $r+1$ d'$A$. 
		\[A =
			\begin{pmatrix}
				a_{1,1}   & \cdots & a_{1,n}   \\
				\vdots    & \ddots & \vdots    \\
				a_{r,1}   & \cdots & a_{r,n}   \\
				a_{r+1,1} & \cdots & a_{r+1,n} \\
				\vdots    & \ddots & \vdots    \\
				a_{n,1}   & \cdots & a_{n,n}   \\
			\end{pmatrix}\,, \quad
			%
			B =
			\begin{pmatrix}
				a_{1,1}   & \cdots & a_{1,n}   \\
				\vdots    & \ddots & \vdots    \\
				a_{r+1,1} & \cdots & a_{r+1,n} \\
				a_{r,1}   & \cdots & a_{r,n}   \\
				\vdots    & \ddots & \vdots    \\
				a_{n,1}   & \cdots & a_{n,n}   \\
			\end{pmatrix}\,.
		\]
		
		A partir del lema \ref{lema:columna}, podem calcular el determinant de $B$ expandint per la primera columna: 
		\begin{equation}\label{eq:det2}
			\det B = \sum_{i=1}^{n} b_{i,1}\cdot(-1)^{i+1}\det B_{i,1}\,.
		\end{equation}
		Per tots els índex $i$ tals que $i\ne r \land i\ne r+1$, els coeficients $b_{i,1}$ i $a_{i,1}$ són equivalents; d'altra banda, 
		\begin{equation}\label{eq:submat}
			\forall i \notin \{r,r+1\} :\quad B_{i,1} = (A_{i,1})^{f_r\rightleftarrows f_{r+1}}\,,
		\end{equation}
		on el superíndex $f_r\rightleftarrows f_{r+1}$ denota que s'intercanvien de posició les files $r$ i $r+1$. Intuïtivament, això és el mateix que constatar que intercanviar primer les files (d'on s'obté la matriu $B$) i després obtenir la submatriu (ergo, $B_{i,1}$) és equivalent a obtenir en primer lloc la submatriu (ergo, $A_{i,1}$) i intercanviar les files després (d'on s'obté $(A_{i,1})^{f_r\rightleftarrows f_{r+1}}$).
		
		La submatriu $B_{i,1}$ és d'ordre $n-1$; llavors, utilitzant l'hipòtesi d'inducció en el fet \eqref{eq:submat}, es té que $\;\det B_{i,1} = - \det A_{i,1}\,$. Per tant, utilitzant aquestes equivalències en l'eq. \eqref{eq:det2},
		\begin{equation}\label{eq:dev1}
			\begin{split}
				\det B =& 	-\left(\sum_{i\notin \{r, r+1\}} a_{i,1}\cdot(-1)^{i+1}\det A_{i,1}\right)\\
				&	+ b_{r,1}\cdot(-1)^{r+1}\det B_{r,1}\\
				&	+ b_{r+1,1}\cdot(-1)^{(r+1)+1}\det B_{r+1,1}\,
			\end{split}
		\end{equation}
		(s'han separat els termes $i=r$ i $i=r+1$ de la suma, ja que per aquests no valen les equivalències anteriors).
		En el cas on $i = r$, es té que $b_{r,1} = a_{r+1,1}$---ja que $B$ s'ha obtingut intercanviant les files $r$ i $r+1$ en $A$. Per la mateixa raó, $B_{r,1} = A_{r+1, 1}\,$, ja eliminar la fila $r$ en $B$ és equivalent a eliminar la fila $r+1$ en $A$ (aquí ``equivalent'' vol dir que s'obté la mateixa submatriu):
		%
		\begin{alignat*}{2}
			B_{\color[gray]{0.6}r,1} = &
			\begin{pmatrix}
			\color[gray]{0.6}
			a_{1,1}                     & a_{1,2}                    & \cdots                   & a_{1,n}                    \\
			\color[gray]{0.6}
			\vdots                      & \vdots                     & \ddots                   & \vdots                     \\
			\color[gray]{0.6}
			a_{r-1,1}                   & a_{r-1,2}                  & \cdots                   & a_{r-1,n}                  \\
			\color[gray]{0.6}
			a_{r+1,1}                   & \color[gray]{0.6}a_{r+1,2} & \color[gray]{0.6}	\cdots & \color[gray]{0.6}a_{r+1,n} \\
			\color[gray]{0.6}
			a_{r,1}                     & a_{r,1}                    & \cdots                   & a_{r,n}                    \\
			\color[gray]{0.6}
			a_{r+2,1}                   & a_{r+2,2}                  & \cdots                   & a_{r+2,n}                  \\
			\color[gray]{0.6}
			\vdots                      & \vdots                     & \ddots                   & \vdots                     \\
			\color[gray]{0.6}
			a_{n,1}                     & a_{n,2}                    & \cdots                   & a_{n,n}                    \\
			\end{pmatrix}
			%
			\\ \\
			%
			A_{\color[gray]{0.6}r+1,1} = &
			\begin{pmatrix}
			\color[gray]{0.6}
			a_{1,1}                     & a_{1,2}                    & \cdots                   & a_{1,n}                    \\
			\color[gray]{0.6}
			\vdots                      & \vdots                     & \ddots                   & \vdots                     \\
			\color[gray]{0.6}
			a_{r-1,1}                   & a_{r-1,2}                  & \cdots                   & a_{r-1,n}                  \\
			\color[gray]{0.6}
			a_{r,1}                     & a_{r,1}                    & \cdots                   & a_{r,n}                    \\
			%
			\color[gray]{0.6}	a_{r+1,1} & \color[gray]{0.6}a_{r+1,2} & \color[gray]{0.6}	\cdots & \color[gray]{0.6}a_{r+1,n} \\
			%
			\color[gray]{0.6}
			a_{r+2,1}                   & a_{r+2,2}                  & \cdots                   & a_{r+2,n}                  \\
			\color[gray]{0.6}
			\vdots                      & \vdots                     & \ddots                   & \vdots                     \\
			\color[gray]{0.6}
			a_{n,1}                     & a_{n,2}                    & \cdots                   & a_{n,n}                    \\
			\end{pmatrix}\,.
		\end{alignat*}
		De manera similar, $b_{r+1, 1} = a_{r, 1}\;$ i $\;B_{r+1,1} = A_{r,1}$.
		
		Per tant, els últims dos termes de l'expressió \eqref{eq:development} es poden reescriure com
		\begin{equation*}\label{eq:penult}
			\begin{gathered}
				a_{r+1,1}\cdot(-1)^{r+1}\det A_{r+1,1}\\
				a_{r,1}\cdot(-1)^{(r+1)+1}\det A_{r,1}\,.
			\end{gathered}
		\end{equation*}
		Observem que ara en cada terme tot està ``en funció de'' $r$ i $r+1$, respectivament, exceptuant l'exponent del $-1$. Utilitzant la identitat $\forall a\in\mathbb{Z} : (-1)^a = -(-1)^{a\pm 1}$, podem tornar a reescriure com
		\begin{equation*}\label{eq:last}
			\begin{gathered}
				-a_{r+1,1}\cdot(-1)^{(r+1)+1}\det A_{r+1,1}\\
				-a_{r,1}\cdot(-1)^{r+1}\det A_{r,1}\,.
			\end{gathered}
		\end{equation*}
		
		Finalment, a partir d'això podem simplificar l'expressió \eqref{eq:dev1} reintroduint aquests termes al sumatori:
		\begin{equation}\label{eq:development}
			\begin{split}
				\det B =& 	-\left(\sum_{i\notin \{r, r+1\}} a_{i,1}\cdot(-1)^{i+1}\det A_{i,1}\right)\\
				&	-a_{r+1,1}\cdot(-1)^{(r+1)+1}\det A_{r+1,1}\\
				&	-a_{r,1}\cdot(-1)^{r+1}\det A_{r,1} = \\
				=&-\left(\sum_{i=1}^{n} a_{i,1}\cdot(-1)^{i+1}\det A_{i,1}\right)\,.
			\end{split}
		\end{equation}
		L'expressió entre parèntesis és el determinant de la matriu $A$ (expansió per primera columna). Per tant, $\det B = -\det A$.
	\end{proof}
	
	\begin{col} \label{col:intercanvi}
		És conseqüència directa d'aquest últim lema (\ref{lema:intercanvi}) que el determinant de la matriu obtinguda intercanviant dues files $r$ i $s$ qualssevol (no necessàriament adjacents) d'una matriu $A$ és $\ -\det A$, ja que intercanviar la posició de dues files és equivalent a fer un nombre imparell d'intercanvis entre files adjacents.
		
		\begin{proof}
			Primer cal fer $s-r$ (suposant que $s>r$) intercanvis per posar la fila $r$ a la posició de $s$. Ara la fila $s$ es troba a la posició $s-1$, per tant només cal fer $(s-1)-r = s-r-1$ intercanvis de files adjacents. Llavors el nombre total d'intercanvis és $(s-r)+(s-r-1) = 2(s-r) - 1$, que és imparell.
			\[
				\begin{blockarray}{cccc}
					\begin{block}{(ccc)*{1}{>{\color{red}}l<{}}}
						a_{1,1}&	\cdots&		a_{1,n}& 		\\
						\vdots&		\ddots&		\vdots& 		\\
						a_{r-1,1}&	\cdots&		a_{r-1,n}&    	\\
						\rd a_{r,1}&\rd\cdots&	\rd a_{r,n}&    \dcarr \\
						a_{r+1,1}&	\cdots&		a_{r+1,n}&    	\dcarr\\
						\vdots&		\ddots&		\vdots& 		\vdots\\
						a_{s-1,1}&	\cdots&		a_{s-1,n}&    	\dcarr \\
						\gre a_{s,1}&\gre\cdots&	\gre a_{s,n}&    \\
						a_{s+1,1}&	\cdots&		a_{s+1,n}&    	\\
						\vdots&		\ddots&		\vdots& 		\\
						a_{n,1}&	\cdots&		a_{n,n}& 		\\	
					\end{block}
				\end{blockarray}
				%
				\quad
				%
				\begin{blockarray}{cccc}
					\begin{block}{(ccc)*{1}{>{\color{ForestGreen}}l<{}}}
						a_{1,1}&	\cdots&		a_{1,n}& 		\\
						\vdots&		\ddots&		\vdots& 		\\
						a_{r-1,1}&	\cdots&		a_{r-1,n}&    	\\
						a_{r+1,1}&	\cdots&		a_{r+1,n}&    	\ucarr\\
						\vdots&		\ddots&		\vdots& 		\vdots\\
						a_{s-1,1}&	\cdots&		a_{s-1,n}&    	\ucarr\\
						\gre a_{s,1}&\gre\cdots&	\gre a_{s,n}&    \ucarr\\
						\rd a_{r,1}&\rd\cdots&	\rd a_{r,n}&    \\
						a_{s+1,1}&	\cdots&		a_{s+1,n}&    	\\
						\vdots&		\ddots&		\vdots& 		\\
						a_{n,1}&	\cdots&		a_{n,n}& 		\\	
					\end{block}
				\end{blockarray}
				%
				\quad
				%	
				\begin{pmatrix}
					a_{1,1}      & \cdots     & a_{1,n}      \\
					\vdots       & \ddots     & \vdots       \\
					a_{r-1,1}    & \cdots     & a_{r-1,n}    \\
					\gre a_{s,1} & \gre\cdots & \gre a_{s,n} \\
					a_{r+1,1}    & \cdots     & a_{r+1,n}    \\
					\vdots       & \ddots     & \vdots       \\
					a_{s-1,1}    & \cdots     & a_{s-1,n}    \\
					\rd a_{r,1}  & \rd\cdots  & \rd a_{r,n}  \\
					a_{s+1,1}    & \cdots     & a_{s+1,n}    \\
					\vdots       & \ddots     & \vdots       \\
					a_{n,1}      & \cdots     & a_{n,n}      \\
				\end{pmatrix}
			\]
		\end{proof}
	\end{col}
\end{lema}



\begin{lema}\label{lema:iesimafila}
	Sigui $M\in\mathcal{M}_n(\mathbb{R})$. L'expansió del determinant de $M$ per la primera fila és equivalent a l'expansió per la $i$-èsima fila:
	\begin{multline*}
		\forall i \in \{1,2,\ldots, n\}:\\
		\det M = \sum_{j=1}^{n} m_{1,j}\cdot(-1)^{1+j}\det M_{1,j} = \sum_{j=1}^{n} m_{i,j}\cdot(-1)^{i+j}\det M_{i,j}\,.
	\end{multline*}
	\begin{proof}
		Sigui $A\in\mathcal{M}_n(\mathbb{R})$ i sigui $B$ la matriu obtinguda movent la fila $i$ d'$A$ a la primera fila mitjançant $i-1$ intercanvis consecutius de files adjacents:
		\[
			A =\
			\begin{blockarray}{cccc}
				\begin{block}{(ccc)*{1}{>{\color{blue}}l<{}}}
					a_{1,1}&	\cdots&		a_{1,n}& 		\\
					a_{2,1}&	\cdots&		a_{2,n}& 		\ucarr\\
					\vdots&		\ddots&		\vdots& 		\vdots\\
					a_{i-1,1}&	\cdots&		a_{i-1,n}&    	\ucarr \\
					\bl a_{i,1}&\bl\cdots&	\bl a_{i,n}&    \ucarr \\
					a_{i+1,1}&	\cdots&		a_{i+1,n}&    	\\
					\vdots&		\ddots&		\vdots& 		\\
					a_{n,1}&	\cdots&		a_{n,n}& 		\\	
				\end{block}
			\end{blockarray}
			%
			\qquad
			%
			B =
			\begin{pmatrix}
				\bl a_{i,1} & \bl\cdots & \bl a_{i,n} \\
				a_{1,1}     & \cdots    & a_{1,n}     \\
				a_{2,1}     & \cdots    & a_{2,n}     \\
				\vdots      & \ddots    & \vdots      \\
				a_{i-1,1}   & \cdots    & a_{i-1,n}   \\
				a_{i+1,1}   & \cdots    & a_{i+1,n}   \\
				\vdots      & \ddots    & \vdots      \\
				a_{n,1}     & \cdots    & a_{n,n}     \\
			\end{pmatrix}\,.
		\]
		Llavors, com a conseqüència directa del lema \ref{lema:intercanvi} i del seu corol·lari (\ref{col:intercanvi}), 
		\begin{equation}\label{eq:detrel}
			\det A = (-1)^{i-1}\det B\,.
		\end{equation}
		
		Desenvolupem $\det B$ segons la seva definició.
		\begin{equation}\label{eq:det-iesima}
			\det B = \sum_{j=1}^{n} b_{1,j}\cdot (-1)^{1+j}\det B_{1,j}\,.
		\end{equation}
		Per construcció, $b_{1,j} = a_{i,j}$ per tot $j$. D'altra banda, la matriu $B_{1,j}$ és equivalent a la matriu $A_{i,j}$---ja que, novament per construcció, la primera fila de $B$ és la $i$-èsima fila d'$A$; vegi's el diagrama anterior per visualitzar-ho. Per tant, \eqref{eq:det-iesima} es pot reformular com
		\begin{equation}\label{eq:det-iesima2}
			\det B = \sum_{j=1}^{n} a_{i,j}\cdot (-1)^{1+j}\det A_{i,j}\,.
		\end{equation}
		
		Unint les equacions \eqref{eq:detrel} i \eqref{eq:det-iesima2}, obtenim
		\begin{multline}
			\det A = (-1)^{i-1}\sum_{j=1}^{n} a_{i,j}\cdot (-1)^{1+j}\det A_{i,j} =\\
			=\sum_{j=1}^{n} (-1)^{i-1}\cdot a_{i,j}\cdot (-1)^{1+j}\det A_{i,j}=\\
			=\sum_{j=1}^{n} a_{i,j}\cdot (-1)^{i+j}\det A_{i,j}\,.
		\end{multline}
		Aquesta darrera expressió és l'expansió del determinant d'$A$ per la $i$-èsima fila.
	\end{proof}
\end{lema}



\section{Propietats del determinant}
\begin{prop}\label{prop:filaescalada}
	El determinant de la matriu $A'$ que s'obté multiplicant una fila o una columna sencera d'una matriu $A\in\mathcal{M}_n(\real)$ per un escalar $\lambda\in\real$ és \[\det A' = \lambda\det A\,.\]
	\begin{proof}
		A partir del \hyperref[teo:laplace]{teorema de Laplace}, podem calcular el determinant d'$A'$ mitjançant l'expansió per la fila o columna que ha estat multiplicada; per exemple, si la s'ha multiplicat per $\lambda$ la fila $k$:
		\[\det A' = \sum_{j=1}^{n} {a'}_{k,j}\cdot C_{k,j} =\sum_{j=1}^{n} \lambda {a}_{k,j} \cdot C_{k,j} = \lambda \sum_{j=1}^{n} {a'}_{k,j} {a}_{k,j} \cdot C_{k,j} = \lambda\det A\,. \]
	\end{proof}
\end{prop}

\begin{prop}\label{prop:sumafila}
	Siguin $A,B\in\matspace{n}$ matrius que \textit{difereixen només en els coeficients d'una fila o d'una columna} (com a màxim). Sigui la matriu $M$ la matriu que s'obté sumant les dues files/columnes diferents D'$A$ i $B$ i deixant la resta de coeficients iguals (que a $A$ i a $B$). Llavors \[\det M = \det A + \det B \]
	\begin{proof}
		Sigui $k$ l'índex de la columna diferent entre $A$ i $B$. Pel \hyperref[teo:laplace]{teorema de Laplace}, podem expandir el determinant de $M$ per la $k$-èsima columna:
		\begin{multline*}
			\det M = \sum_{i=1}^{n} {m}_{i,k}\cdot C_{i,k} = \sum_{i=1}^{n} ({a}_{i,k}+ {b}_{i,k})\cdot C_{i,k}=\\
			=\sum_{i=1}^{n}{a}_{i,k}\cdot C_{i,k} +\sum_{i=1}^{n} {b}_{i,k}\cdot C_{i,k}=\det A + \det B\,.
		\end{multline*}
		Es pot fer exactament el mateix raonament per files. Noteu, a més, que els cofactors $C_{i,k}$ són iguals per totes tres matrius, ja que, excepte la $k$-èsima columna/fila, tota la resta de columnes/files són iguals.
	\end{proof}
\end{prop}

\begin{prop}\label{prop:filaigual}
	Per qualsevol $A\in\matspace{n}$, si $A$ conté dues files (o columnes) iguals entre elles, llavors $\det A = 0$.
	\begin{proof}
		Siguin $r$ i $s$ els índex de les files idèntiques en $A$. Sigui $B$ la matriu que s'obté intercanviant les files $r$ i $s$ d'$A$. Llavors, per una banda, $\det B = \det A$ ja que les dues matrius són idèntiques. D'altra banda, pel lema \ref{lema:intercanvi} $\det B = -\det A$. Per tant, \[\det A = -\det A \quad \therefore \det A = 0\,. \] El mateix raonament és vàlid per columnes també.
	\end{proof}
\end{prop}

\begin{prop}
	Si sumem un múltiple d'una fila/columna d'$A\in\matspace{n}$ a una fila/columna diferent, el determinant d'$A$ no canvia.
	\begin{proof}
		Sigui $B$ la matriu que s'obté sumant la fila $r$ d'$A$ a la fila $s$ (on $r\ne s$). Sigui $\tilde{A}$ la matriu que s'obté reemplaçant la $r$-èsima fila d'$A$ per la $s$-èsima fila multiplicada per l'escalar $\lambda \in\real$. És a dir,
		\[
		A = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{r,1}&	\cdots&		a_{r,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,,
		\quad
		\tilde{A} = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		\lambda a_{s,1}&	\cdots&		\lambda a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,.
		\]
		
		Per la propietat que s'enuncia en \ref{prop:sumafila}, 
		\begin{equation}\label{eq:decomposition}
			\det B = \det A + \det \tilde{A}\,. 
		\end{equation}
		Sigui la matriu $\tilde{A}'$ la matriu que s'obté reemplaçant la $r$-èsima fila d'$A$ per la $s$-èsima fila (sense multiplicar per $\lambda $): 
		\[\tilde{A}' = 
		\begin{pmatrix}
		a_{1,1}&	\cdots&		a_{1,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{s,1}&	\cdots&		a_{s,n}\\
		\vdots&		\ddots&		\vdots\\
		a_{1,1}&	\cdots&		a_{1,n}\\
		\end{pmatrix}\,.\]
		Aplicant la proposició \ref{prop:filaescalada} en \eqref{eq:decomposition}, obtenim \[\det B = \det A + \lambda \cdot\det \tilde{A}'\,.\] Per la propietat de \ref{prop:filaigual}, $\det \tilde{A}' = 0$, ja que $\tilde{A}'$ conté una fila duplicada (la fila $s$ d'$A$). Per tant, \[\det B=\det A\,. \]
	\end{proof}	
\end{prop}

\begin{lema}\label{lema:elementals}
	Direm que una matriu elemental és de tipus $E_1$ si, quan multiplica per l'esquerra a una altra matriu, n'intercanvia la posició de dues files, de tipus $E_2$ si en multiplica escalarment una fila, i de tipus $E_3$ si en suma un múltiple d'una fila a una fila diferent. Llavors:
	\begin{enumerate}[i)]
		\item Les matrius de tipus $E_1$ tenen determinant -1.
		\item Les matrius de tipus $E_2$ tenen determinant $\lambda$, on $\lambda$ és l'escalar pel qual es multiplica la fila.
		\item Les matrius de tipus $E_3$ tenen determinant 1.
		\item Per qualsevol matriu elemental $E\in\matspace{n}$ i per qualsevol matriu $M\in\matspace{n}$, \[\det (EM) = \det E \cdot \det M\,.\]
	\end{enumerate}
	\begin{proof}
		Demostrarem cada una de les proposicions a continuació.
		\begin{enumerate}[i)]
			\item Sigui $E$ una matriu elemental de tipus $E_1$. Les matrius d'aquest tipus tenen dues files intercanviades---en concret, tenen la $r$-èsima i la $s$-èsima fila de la matriu identitat intercanviades, on $r$ i $s$ són els índex de les files que $r$ i $s$ intercanvia en la matriu $M$ quan la multiplica per l'esquerra:
			\[
			E =
			\begin{pmatrix}
			1&		0&&&		\cdots&&&		  &0\\
			0& 		1&&&		&&&					\\
			&		&	\ddots&&&&&&&	 	  		\\
			&		& 		&0&\cdots&1&&		  	\\
			\vdots&	&		&\vdots&&\vdots&&&\vdots\\
			&		& 		&1&\cdots&0&&		  	\\
			&		&		&&&&		\ddots&		\\
			&		&		&&&&		&		 1&0\\
			0&		&		&&	\cdots&&	&	 0&1\\
			\end{pmatrix}\,.
			\]
			Per tant, pel lema \ref{lema:intercanvi}, $\det E = - \det I_n = -1$.
			%
			%
			\item Sigui $E$ una matriu elemental de tipus $E_2$. Aquestes matrius són la identitat amb una fila multiplicada per $\lambda\in\real$, un escalar:
			\[
			E =
			\begin{pmatrix}
				1&		0&		\cdots&		\cdots&		\cdots& 0\\
				0&		1&		&			&&\\
				\vdots&	&		\ddots&		&&\\
				\vdots&	&		&			\lambda&&\\
				\vdots&	&		&			&			\ddots&\\
				0&		&		&			&			&		1
			\end{pmatrix}\,.
			\]
			Llavors, per la proposició \ref{prop:filaescalada}, $\det E = \lambda \det \Id = \lambda$.
			%
			%
			\item Les matrius de tipus $E_3$ són la identitat amb una de les files multiplicada per un escalar $\lambda$ sumada a una de diferent. Per exemple,
			\[
			E =
			\begin{pmatrix}
				1&		0&		\cdots&		\lambda&		\cdots& 0\\
				0&		1&		&			&&\\
				\vdots&	&		\ddots&		&&\\
				\vdots&	&		&			1&&\\
				\vdots&	&		&			&			\ddots&\\
				0&		&		&			&			&		1
			\end{pmatrix}\,.
			\]
			Sigui $E$ una matriu arbitrària d'aquest tipus. Per la proposició \ref{prop:sumafila}, el determinant d'$E$ és el mateix que el de la identitat, i per tant $\det E = 1$.
			%
			%
			\item Sigui $M\in\matspace{n}$ una matriu qualsevol, siguin $\tilde{M}_1$ $\tilde{M}_2$ i $\tilde{M}_3$ les matrius que s'obtenen aplicant transformacions elementals qualssevol de tipus 1, 2 i 3 respectivament i siguin $E_1$, $E_2$ i $E_3$ les matrius elementals corresponent (respectivament). D'una banda, per les proposicions \ref{lema:intercanvi}, \ref{prop:filaescalada} i \ref{prop:sumafila}, tenim que 
			\[\det \tilde{M}_1 = -\det M,\ \det \tilde{M}_2 = \lambda \det M \text{ i } \det \tilde{M}_3 = \det M\,,\]
			respectivament. D'altra banda, per les proposicions de i), ii) i iii), tenim que 
			\[\det E_1 = -1,\ \det E_2 = \lambda \text{ i } \det E_3 = 1\,.\]
			Per tant és cert que
			\[\det \tilde{M}_1 = \det{E_1}\det M,\ \det \tilde{M}_2 = \det{E_2} \det M \text{ i } \det \tilde{M}_3 =\det E_3 \det M\,.\]
		\end{enumerate}
	\end{proof}
\end{lema}

\begin{lema}\label{lema:descomp-elem}
	Qualsevol matriu $A\in\matspace{n}$ es pot ``descompondre'' en un seguit de matrius elementals $E_1, \ldots,E_m \in \matspace{n}$ de manera que
	\[
		A = E_m\cdots E_1\Id_n
	\]
\end{lema}

\begin{teo*}
	Per dues matrius $A,B \in \matspace{n}$ qualssevol, \[\det(AB) = \det A\cdot \det B = \det (BA)\,. \]
	\begin{proof}
		Qualsevol matriu $A\in\matspace{n}$ es pot descompondre en un seguit d'operacions elementals sobre la matriu identitat (lema \ref{lema:descomp-elem}). És a dir, existeixen matrius elementals $E_1,\ldots,E_m\in\matspace{n}$ tals que
		\[
			A = E_m\cdots E_2E_1\,.
		\]
		Per tant
		\[
			AB = (E_m\cdots E_2E_1)B\,.
		\]
		Aplicant el lema \ref{lema:elementals} recursivament:
		\begin{multline}\label{eq:det-prod}
			\det (AB) = \det E_m \cdot \det (E_{m-1}\cdots E_1B)= \\= \det E_m\cdot\det E_{m-1} \cdot \det(E_{m-2}\cdots E_1 B) = \cdots \\\cdots  = \det E_m \cdot \cdots \cdot \det E_1 \cdot \det B\,.
		\end{multline}
		Ara, tornant a aplicar \ref{lema:elementals} recursivament però en sentit recíproc,
		\begin{multline*}
			\det (AB) = \left[\det E_m \cdot \cdots \cdot \det E_2 \cdot \det E_1\right] \cdot \det B =\\= \left[\det E_m \cdot \cdots \cdot \det E_3 \cdot\det (E_2E_1)\right]\det B =\\= \left[\det E_m \cdot \cdots \cdot \det E_4 \cdot\det (E_3E_2E_1)\right]\cdot B = \cdots \\
			 \cdots = \det(E_m\cdots E_1)\det B = \det A \cdot \det B\,.
		\end{multline*}
		
		El fet que $\det(AB) = \det(BA)$ és conseqüència directa d'això últim i de la commutabilitat de la multiplicació entre escalars.
	\end{proof}
\end{teo*}

\section{Sistemes simultanis}
L'objectiu d'aquesta secció és resoldre sistemes amb la mateixa matriu $A \in \mathcal{M}_{m,n}$ però variant els termes independents, d'aquesta forma:
\[
    Ax^{(1)} = b^{(1)}, \hdots , Ax^{(r)} = b^{(r)}.
\]

Equivalentment, es tractaria de trobar la matriu $X \in \mathcal{M}_{m,r}$ tal que 
\[
    AX = ( \underbrace{b_{1} \cdots b_{r}}_{B} ).
\]

La manera més raonable i eficient de resoldre aquest sistema ($AX = B$) és mitjançant Gauss-Jordan a la matriu:
\[
\begin{pmatrix}
	A & | & b^{(1)} & \cdots & b^{(r)} \\ 
\end{pmatrix}
\]

En matrius $M \in \mathcal{M}_{m,n}$ (incloent el cas $m = n$), el mètode consisteix en aplicar eliminació gaussiana fins obtenir la forma reduïda de la matriu: atenció, l'ampliada! Observem com, depenent del cas, podria no donar-se un sistema compatible determinat.

Suposem que ara ja tenim la matriu en forma reduïda, ara haurem de substituir cap enrere ("back-substitution") de tal manera que ens quedi ($Id_n | S$). Aleshores cada vector columna de la matriu $S$ és cada vector solució del sistema simultani.


Una aplicació d'aquest mètode és el problema de trobar l'inversa d'una matriu quadrada (de dimensió $n$). Podem pensar que estem resolent un sistema simultani, ja que estaríem resolent alhora:
\[
Ax^{(i)} = e_i \quad \forall i = 1, ..., n
\]
On $e_i$ és el vector columna amb '1' a la entrada i-èssima i amb '0' pertot altra. Aleshores usaríem Gauss-Jordan i la matriu resultant seria, de fet, $\inv{A}$ = $S$.











