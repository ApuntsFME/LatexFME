\chapter{Programació lineal}



\section{Poliedres}
\begin{defi}[hiperplà]\label{defi:hiperpla}
	Un hiperplà és un subconjunt de $\real^n$ definit com \[H = \{\vb{x}\in\real^n \mid \vb{a}'\vb{x}=b \}\,, \] on $\vb{a}\in\real^n$ i s'anomena el vector normal de l'hiperplà, i $b\in\real$.
\end{defi}

\begin{defi}[semiespai]\label{defi:semiespai}
	Donat un hiperplà $H = \{\vb{x}\in\real^n \mid \vb{a}'\vb{x}=b \}$, es defineixen dos semiespais: 
	\begin{align*}
		S_{\ge} &= \{\vb{x}\in\real^n \mid \vb{a}'\vb{x} \ge b \}\\
		S_{\le} &= \{\vb{x}\in\real^n \mid \vb{a}'\vb{x} \le b \}\,.
	\end{align*}
\end{defi}

En referència a la definició \ref{defi:semiespai}, es diu informalment que el semiespai $S_\ge$ és el conjunt de punts que estan ``per sobre'' de l'hiperplà, i $S_\le$, dels que estan ``per sota''.

\begin{defi}[poliedre]\label{defi:poliedre}
	Un poliedre $P$ és un subconjunt de $\real^n$ definit per una intersecció de semiespais: \[P = \{\vb{x}\in\real^n \mid \vb{a}_i'\vb{x} \le b_i \quad \forall i\in \{1,\ldots,m\} \}\,,\] on $n,m\in\n\,$, $\vb{a}_1,\ldots,\vb{a}_m \in \real^n$ i $b_1,\ldots,b_m\in\real$.
\end{defi}

\begin{defi}[conjunt! convex]
	Un conjunt convex és un conjunt $\mathcal{S} \in \mathbb{R}^{n}$ tal que $\forall x,y  \in \mathcal{S}$, $\forall \lambda \in  [0, 1]$, $\lambda x + (1- \lambda x)y \in \mathcal{S}$.
	Intuïtivament, podem pensar en que totes les cordes que poguem imaginar entre dos punts qualssevol del conjunt, pertanyen al conjunt.
\end{defi}

\begin{defi}[embolcall convex]
	L'embolcall convex és un conjunt tal que:
	\[
	CH(x^1,...,x^k) = \left \{ x \in \mathbb{R}^n \text{ | } x = \sum_{i=1}^k \lambda_i x^i , \lambda_i \geq 0 , \sum_{i=1}^k \lambda_i = 1 \right \}.
	\]
\end{defi}

\begin{defi}[punts! extrem]
	Sigui el políedre $P$. Un vector $x \in P$ és un punt extrem de $P$ si no existeix cap parell de vectors $y,z \in P$ ni cap $\lambda \in [0,1]$ tals que $x = \lambda y + (1-\lambda)z$.
\end{defi}
\begin{defi}[línia]
	Direm que el políedre $P \subseteq \mathbb{R}^n$ conté una línia si existeix el vector $x \in P$ i el vector no nul $d \in \mathbb{R}^n$ tals que $x + \lambda d \in P$ $\forall \lambda$.
\end{defi}
\begin{lema}
	$P \neq \varnothing$ té un punt extrem $\iff$ $P$ no conté cap línia.
\end{lema}


\section{Problemes de programació lineal}
\begin{defi}[problema!de programació lineal]
	Un problema de programació lineal és un problema de tipus
	\[
		\min\limits_{\vb{x}\in P} \{\vb{c}'\vb{x}\}\,,
	\]
	on $P$ és un poliedre. Denotarem el problema corresponent com $(P)$.
\end{defi}

\begin{defi}[problema!infactible]
	Si un problema de PL té un poliedre buit ($P=\emptyset$), direm que és 
	infactible.
\end{defi}

\begin{defi}
	Sigui $(P)$ un problema de PL. Si
	\[
		\forall k \in \real \text{ } \exists \vb{x} \in P \colon \vb{c} \vb{x} \le k \,
	\]
	direm que $(P)$ és il·limitat.
\end{defi}

\begin{defi}[problema!amb solució òptima]
	Si un problema $(P)$ no és ni infactible ni il·limitat, direm que té 
	solució òptima, o alternativament que té òptim.
\end{defi}
\begin{defi}[conjunt! solució]
	Es denomina conjunt solució a $x^{*} \in \mathcal{X}^{*}$, i està format per solucions òptimes $x\*$.
\end{defi}
\begin{defi}[notació]
	
	\[ (PL) = \left\{\begin{matrix}
	\min\limits_{\vb{x}\in P} z = c'x & &  (1)\\ 
	\text{s.a.:} & & (2) \\
	& Ax \geq ó \leq  b  & (3) \\ 
	& l \leq x \leq u & (4)\\ 
	\end{matrix}\right.
	\]
	On:
	\begin{itemize}
	\item[(1)] representa la funció objectiu, allò que volem optimitzar.
	\item[(2)] representa Subjecte a.
	\item[(3)] representa les constriccions d'igualtat o desigualtat.
	\item[(4)] són les fites inferior (l) i/o superior (u).
	\end{itemize}
\end{defi}

\section{Solucions bàsiques factibles}

El fet que hi hagi solucions òptimes que siguin punts extrems no és de gran ajuda computacional per sí sol. Per tenir una caracterització computacionalment assequible cal el concepte de solució bàsica factible, que es defineix a continuació.

\begin{defi}[solució!bàsica]\label{defi:SB}
	Donat un poliedre en forma estàndard $P_e$ associat a un problema de programació lineal, una solució bàsica és un vector $\vb{x}\in P_e$ del qual es pot fer la partició $\vb{x} = \left[\vb{x}_\B\mid\vb{x}_\N\right]$ tal que
	\begin{enumerate}[i)]
		\item $\B,\N\subseteq\{1,\ldots,n \}$ són conjunts complementaris 
		d'índexos i $|\B| = m$, on $m$ és el nombre de restriccions (el nombre 
		de files de la matriu $A$). $\B$ s'anomena el conjunt d'índexos de 
		variables bàsiques o \textbf{base}, i $\N$ s'anomena el conjunt 
		d'índexos de variables no bàsiques.
		%
		\item $\vb{x}_\B \defeq 
		\begin{bmatrix}x_{\B(1)} & x_{\B(2)} & \cdots & x_{\B(m)}\end{bmatrix}\,$
		i
		$\,\vb{x}_\N \defeq 
		\begin{bmatrix}x_{\N(1)} & x_{\N(2)} & \cdots & 
		x_{\N(n-m)}\end{bmatrix} = \vb{0}\,$, on $\B(i)$ i $\N(i)$ denoten, 
		respectivament, l'$i$-èsim índex de $\B$ i $\N$.
		%
		\item La matriu definida per
		\[
			B \defeq
			\begin{bmatrix}
			\arrowvert& 	\arrowvert& 	  & 	\arrowvert\\
			A_{\B(1)}&		A_{\B(1)}&	\cdots&		A_{\B(m)}\\
			\arrowvert& 	\arrowvert& 	  & 	\arrowvert\\
			\end{bmatrix}
		\]
		és no singular. Aquesta matriu s'anomena matriu bàsica.
	\end{enumerate}
\end{defi}

Aquesta definició no és gaire intuïtiva; a continuació es presenta una versió alternativa basada en el poliedre general.

\begin{defi}[solució!bàsica]\label{defi:SB-alt}
	Donat un poliedre (general) $P\subseteq\real^n$ associat a un problema de 
	programació lineal, direm que $\vb{x}\in P$ és una solució bàsica si és la 
	intersecció de $n$ hiperplans (associats cadascun a una restricció) 
	linealment independents.
\end{defi}

\section{Direccions bàsiques factibles}
\begin{defi}[direcci\'o b\`asica factible]
    Una DBF sobre la SBF $\vb{x} \in P_e$ associada a $q \in \N$ és $\vb{d} =
    \begin{bmatrix}
        \vb{d}_{\B} \\
        \vb{d}_{\N}
    \end{bmatrix}
    \in \real^n$ tal que:
    \begin{itemize}
        \item $d_{N\left(i\right)} \defeq
            \begin{cases}
                1 & N\left(i\right) = q \\
                0 & N\left(i\right) \neq q
            \end{cases}
            ,\, \forall i \in \left\{1, \dots, n-m \right\}$,
        \item $A \left(\vb{x} + \theta \vb{d}\right) = b$ per algun $\theta \in \real^+ \implies \vb{d}_{\B} \defeq -B^{-1} A_q$.
    \end{itemize}
\end{defi}

\begin{prop}[Càlcul de $\theta^*$]
    Calculem $\theta^* \defeq \max \left\{ \theta > 0 \, |\, \vb{y} = \vb{x} + \theta \vb{d} \in P_e\right\}$:
    \begin{enumerate}
        \item $A\left(\vb{x} + \theta \vb{d}\right) = b,\, \forall \theta$ és cert.
        \item $\vb{y} = \vb{x} + \theta \vb{d} \geq 0$:
            \begin{gather*}
                \vb{x}_{B\left(i\right)} + \theta d_{B\left(i\right)} \geq 0 \iff \theta \geq -\frac{\vb{x}_{B\left(i\right)}}{d_{B\left(i\right)}}, \\
                \theta^* = \min_{\{i \in \left\{1, \dots, m \right\} \mid 
                d_{B\left(i\right)} < 0\}} \left\{ - 
                \frac{\vb{x}_{B\left(i\right)}}{d_{B\left(i\right)}} \right\}.
            \end{gather*}
    \end{enumerate}
\end{prop}
\begin{prop}
    Sigui $\vb{d}$ una DBF sobre $\vb{x}$, una SBF de $P_e$,
    \begin{enumerate}
        \item Si $P_e$ és no degenerat, $\vb{d}$ és factible:
            \begin{enumerate}[a)]
                \item $\vb{d}_{\B} \ngeq 0 \implies \theta^* > 0$.
                \item $\vb{d}_{\B} \geq 0 \implies \theta^*$ no definida, $\forall \theta > 0, \vb{x} + \theta \vb{d} \in P_e,\, \vb{d}$ és un raig extrem.
            \end{enumerate}
        \item Si $P_e$ degenerat ($\exists i \in \B$ tal que $\vb{x}_{B\left(i\right)} = 0$) $\vb{d}$ pot no ser factible:
            \begin{gather*}
                \min \left\{ -\frac{\vb{x}_{B\left(i\right)}}{d_{B\left(i\right)}} \right\} = 0 \implies \nexists \theta > 0 \tq \vb{y} = \vb{x}+\theta \vb{d} \implies \vb{d} \text{ infactible}.
            \end{gather*}
    \end{enumerate}
\end{prop}
\begin{prop}
    Siguin $q$ i $B\left(p\right)$ les variables que entren i surten de la base, respectivament,
    \begin{gather*}
        \bar{\B} := \left\{ \bar{B}\left(1\right), \dots, \bar{B}\left(m\right) \right\}, \text{ on } \bar{B}\left(i\right) =
        \begin{cases}
            B\left(i\right) & i \neq p \\
            q & i = p
        \end{cases},
    \end{gather*}
    i la nova matriu bàsica és
    \[ \bar{B} = \left[A_{B\left(1\right)}, \dots, A_{B\left(p-1\right)}, A_q, A_{B\left(p+1\right)}, \dots, A_{B\left(m\right)} \right]. \]
\end{prop}
\begin{defi}[direcci\'o b\`asica factible!de descens]
    \begin{itemize}
        \item[]
        \item $\vb{d}$ és una DBF de descens si $\forall \theta > 0, \vb{c}'\left(\vb{x} + \theta \vb{d}\right) < \vb{c}'\vb{x} \iff \vb{c}'\vb{d} < 0$.
        \item Si $\vb{d}$ és DBF sobre $\vb{x}$ (SBF), $\vb{c}'\vb{x} + \theta^*\vb{c}'\vb{d} = \vb{c}'\vb{x} + \theta^*r_q$ i
            \begin{itemize}
                \item $r_q = \vb{c}'\vb{d}$.
                \item Si $P_e$ no degenerat, llavors la DBF $\vb{d}$ associada a $q \in \N$ és de descens $\iff r_q < 0$.
            \end{itemize}
    \end{itemize}
\end{defi}
\begin{teo*}[Condicions d'optimalitat de SBF]
    \begin{enumerate}[a)]
        \item[]
        \item $r \geq \vb{0} \implies \vb{x}$ és SBF òptima.
        \item $\vb{x}$ SBF i no degenerada $\implies \vb{r} \geq \vb{0}$.
    \end{enumerate}
\end{teo*}

\section{Algorisme del símplex primal}
\begin{alg}[Algorisme del símplex primal]\label{alg:asp}
    \begin{enumerate}
        \item[]
        \item {\bf Inicialització}: Trobem una SBF ($\B, \N, \vb{x}_{\B}, z$).
        \item \label{simp_pri_pas2} {\bf Identificació de la SBF òptima i selecció VNB entrant}:
            \begin{itemize}
                \item Calculem els costos reduïts: $\vb{r}' = c_N' - c_B'B^{-1}A_n$.
                \item Si $\vb{r}' \geq \vb{0}$, llavors és la SBF òptima. {\bf STOP!}
            \end{itemize}
            Altrament seleccionem una $q$ tal que $r_q < 0$ (VNB entrant).
        \item {\bf Càlcul de DBF de descens}:
            \begin{itemize}
                \item $\vb{d}_{\B} = -B^{-1}A_q$
                \item Si $\vb{d}_{\B} \geq \vb{0}$, DBF de descens il·limitat $\implies$ $\left(PL\right)$ il·limitat. {\bf STOP!}
            \end{itemize}
        \item {\bf Càlcul de $\theta^*$ i $B\left(p\right)$}:
            \begin{itemize}
                \item Càlcul de $\theta^*$: 
                    \[\theta^* = \min_{i \in \left\{ 1, \dots, m \right\} \,|\, d_{B\left(i\right)} < 0} \left\{-\frac{x_{B\left(i\right)}}{d_{B\left(i\right)}} \right\}.\]
                \item Variable bàsica de sortida: $B\left(p\right)$ tal que $\theta^* = -\frac{\vb{x}_{B\left(p\right)}}{d_{B\left(p\right)}}$.
            \end{itemize}
        \item {\bf Actualitzacions i canvi de base}:
            \begin{itemize}
                \item $\vb{x}_{\B} := \vb{x}_{\B} + \theta^*\vb{d}_{\B}$, \\
                    $x_q := \theta^*$, \\
                    $z := z + \theta^* r_q$.
                \item $\B := \B \setminus \left\{B\left(p\right)\right\} \cup \left\{q\right\}$, \\
                    $\N := \N \setminus \left\{q\right\} \cup \left\{B\left(p\right)\right\}$.
            \end{itemize}
        \item {\bf Anar} a \ref{simp_pri_pas2}.
    \end{enumerate}
\end{alg}
\begin{obs}[Fase 1 del símplex]
    A la fase 1 del símplex resolem el problema:
    \begin{equation*}
        \lp P_I \rp \begin{dcases*}
            \min\, \sum_{i = 1}^m \vb{y}_i \\
            \text{s.a.: } \\
            \quad \left( 1 \right) \quad A\vb{x} + I\vb{y} = \vb{b} \\
            \quad \left( 2 \right) \quad \vb{x}, \vb{y} \geq 0 \\
        \end{dcases*}
    \end{equation*}
    El resultat pot ésser:
    \begin{itemize}
        \item $z_I^* > 0 \implies$ $\left(P\right)$ infactible.
        \item $z_I^* = 0 \implies$ $\left(P\right)$ factible. Dos casos:
            \begin{itemize}
                \item $\B_I^*$ no conté variables $\vb{y} \implies \B_I^*$ és SBF de $\left(P\right)$.
                \item $\B_I^*$ conté alguna variable $\vb{y}$. Tenim que $\vb{y}_B^* = \vb{0} \implies \B_I^*$ és SBF degenerada de $\left(P_I\right)$ i per tant podem obtenir una SBF de $\left(P\right)$ a partir de $\B_I^*$.
            \end{itemize}
    \end{itemize}
\end{obs}
\begin{prop}[Regla de Bland] \addcontentsline{toc}{subsection}{Regla de Bland}\label{rgl:bland}
    Usem la regla de Bland per assegurar la terminació del símplex en resoldre 
    un problema degenerat.
    \begin{enumerate}
        \item Seleccionem com VNB d'entrada la VNB d'índex menor 
        (lexicogràficament) que compleix $r_q < 0$.
        \item Si al seleccionar la variable de sortida hi ha empat, seleccionem la VB amb índex menor.
    \end{enumerate}
\end{prop}
