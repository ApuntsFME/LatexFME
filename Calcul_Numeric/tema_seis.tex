\chapter{Introducción a los m\'etodos num\'ericos para EDPs}

\section{Introducción}
\begin{itemize}
    \item M\'etodo de los elementos finitos
    \item Diferencias finitas (1D, 2D, 3D)
    \item Volumenes finitos
\end{itemize}
Veremos el m\'etodo de las diferencias finitas en 1D.
\subsection{Ecuación parabólica}
EDP:
\begin{equation}\label{eq:EDP}
    \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}, \quad x\in \left( a,b \right), t>0.
\end{equation}
Condiciones de contorno:
\[
    \begin{cases}
        u\left( a, t \right) = u_a\left( t \right) \\
        u\left( b, t \right) = u_b\left( t \right)
    \end{cases}
\]
La condición inicial (ci) es: $u\left( x, 0 \right) = u^0\left( x \right)$.

Análogamente a como se hace en el m\'etodo de los elementos finitos, haremos primero solo discretización para el espacio ($X$) y más tarde la solución en tiempo. Cojamos $N$ subintervalos de tamaño $\Delta x = \frac{b-a}{N}$.

\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
                xmin=0.5, xmax=11.5,
                hide y axis,
                height = 0.1\linewidth,
                width = \linewidth,
                axis lines = center,
                xtick={1,2,...,11},
                xticklabels={$a=x_0$, $x_1$, $x_2$, $x_3$, , , , , , $x_{n-1}$, $b=x_n$},
        ]
            \addplot[domain=0:10, thick, opacity=0]{x-5};
        \end{axis}
    \end{tikzpicture}
\end{center}
La siguiente expressi\'on es una aproximación de la derivada segunda de una funci\'on $f$ gen\'erica:
\begin{equation}\label{eq:der2}
    f^{\prime\prime}\left( x_i \right) = \frac{f\left( x_{i-1} \right) - 2f\left( x_i \right) + f\left( x_{i+1} \right)}{\Delta x^2} + \O\left( \Delta x^2 \right),
\end{equation}
la demostraci\'on se encuentra en un fichero de Atenea.
\begin{obs}[Notación]
    Usaremos la siguiente notación:
    \begin{itemize}
        \item $u_i := u\left( x_i, \cdot \right)$
        \item $u_i\left( t \right) := u\left( x_i, t \right)$ es el valor desconocido (exacto/analítico)
        \item $U_i\left( t \right) \approx u_i\left( t \right)$ es la aproximación del valor.
    \end{itemize}
\end{obs}
Sustituyendo la ecuación \ref{eq:der2} en la EDP \ref{eq:EDP} evaluada en $x = x_i$ (y obviando los errores $\O\left( \Delta x^2 \right)$), tenemos que
\[
    \frac{\dif U_i}{\dif t} = \frac{1}{\Delta x^2} \left[ U_{i-1} - 2U_i + U_{i+1} \right], \quad i= 1,\dots, N-1.
\]
Para $i=1$ e $i=N-1$, la equación se modifica teniendo en cuenta las condiciones de contorno, $U_0 = u_a$ y $U_N = u_b$:
\begin{itemize}
    \item $i=1$: $\frac{\dif U_1}{\dif t} = \frac{1}{\Delta x^2}\left[ -2U_1 + U_2 \right] + \frac{U_a}{\Delta x^2}$.
    \item $i=N-1$: $\frac{\dif U_{N-1}}{\dif t} = \frac{1}{\Delta x^2}\left[ U_{N-2} - 2U_{N-1} \right] + \frac{U_b}{\Delta x^2}$.
\end{itemize}
Por lo tanto tenemos el problema del valor inicial siguiente
\[
    \begin{cases}
        \frac{\dif \bar{U}}{\dif t} = \underset{\sim}{A}\bar{U} + \bar{F} \\
        \bar{u}\left( 0 \right) = \bar{u}^0
    \end{cases}
\]
que es
\begin{align*}
    \frac{\dif}{\dif t} \underbrace{
    \begin{bmatrix}
        U_1 \\
        \vdots \\
        \vdots \\
        U_{N-1}
    \end{bmatrix}
    }_{\bar{U}} &= \frac{1}{\Delta x^2} \underbrace{
    \begin{bmatrix}
        -2 & 1 & & \\
        1 & \ddots & \ddots & \\
        & \ddots & \ddots & 1 \\
        & & 1 & -2
    \end{bmatrix}
    }_{=: \underset{\sim}{A}} \underbrace{
    \begin{bmatrix}
        U_1 \\
        \vdots \\
        \vdots \\
        U_{N-1}
    \end{bmatrix}
    }_{\bar{U}} + \underbrace{
    \begin{bmatrix}
        \frac{U_a}{\Delta x^2} \\
        \vdots \\
        \vdots \\
        \frac{U_b}{\Delta x^2}
    \end{bmatrix}
    }_{=: F}, \\
    \bar{u}^0 &= 
    \begin{bmatrix}
        u^0\left( x_1 \right) \\
        \vdots \\
        u^0\left( x_{N-1} \right)
    \end{bmatrix},
    \\
    \bar{U}\left( 0 \right) &=
    \begin{bmatrix}
        U_1\left( 0 \right) \\
        \vdots \\
        U_{N-1}
    \end{bmatrix}
    =
    \begin{bmatrix}
        U\left( x_1, 0 \right) \\
        \vdots \\
        U\left( x_{N-1}, 0 \right)
    \end{bmatrix}.
\end{align*}
El vector $F$ tiene todo $0$s excepto en los extremos que se muestran en la ecuación.

\begin{example}
    \[
        \begin{cases}
            \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}, \quad x\in\left( 0, \pi \right), t>0 \\
            u\left( x, 0 \right) = \sin\left( x \right) \\
            u\left( 0, t \right) = 0 \\
            u\left( \pi, t \right) = 0
        \end{cases}
    \]
    La soluci\'on es $u\left( x, t \right) = e^{-t}\sin\left( x \right)$. Vamos a suponer que no sabemos la soluci\'on y lo solucionaremos con diferencias finitas (DF).
    \[
        \frac{\dif \bar{u}}{\dif t} = \underset{\sim}{A}\bar{U} + \bar{F}
    \]
    con
    \begin{align*}
        \bar{U}\left( 0 \right) &= 
        \begin{bmatrix}
            \sin\left( x_1 \right) \\
            \vdots \\
            \sin\left( x_{N-1} \right)
        \end{bmatrix}
        , \\
        \bar{F} &= 
        \begin{bmatrix}
            \frac{0}{\Delta x^2} \\
            \vdots \\
            \frac{0}{\Delta x^2}
        \end{bmatrix}.
    \end{align*}
    Podemos resolver el problema del valor inicial de dos formas
    \begin{enumerate}[i)]
        \item Anal\'iticamente:
            \[
                U\left( t \right) = \underbrace{\sum_{i=1}^{N-1} \bar{\alpha}_i e^{\lambda_i t}}_{\text{soluci\'on homogenia}} + \underbrace{U_p\left( t \right)}_{\text{soluci\'on particular}}
            \]
        \item M\'etodos num\'ericos para PVI (EDOs).
    \end{enumerate}
\end{example}

\begin{obs}
    \[
        \begin{cases}
            \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}, \quad x\in\left( a, b \right), t>0 \\
            u\left( x, 0 \right) = u^0\left( x \right) \\
            u\left( a, t \right) = u_a\left( t \right) \\
            u\left( b, t \right) = u_b\left( t \right)
        \end{cases}
    \]
    Imponiendo la EDP en $x = x_i$:
    \[
        \frac{\dif U_i}{\dif t} = \frac{1}{\Delta x^2}\left( \underbrace{U_{i-1}}_{u_a \text{ si } i=1} - 2U_i + \underbrace{U_{i+1}}_{u_b \text{ si } i=N-1} \right),\quad i=1,\dots, N-1
    \]
    El problema del valor inicial es
    \[
        \begin{cases}
            \frac{\dif \bar{U}}{\dif t} = \underset{\sim}{A}\bar{U} + \bar{F}, \\
            \bar{U}\left( 0 \right) = \bar{U}^0
        \end{cases}
    \]
    donde
    \begin{align*}
        \bar{U} &=
        \begin{bmatrix}
            U_1 \\
            \vdots \\
            U_{N-1}
        \end{bmatrix}
        , \\
        \underset{\sim}{A} &=
        \begin{bmatrix}
            -2 & 1 & & \\
            1 & \ddots & \ddots & \\
            & \ddots & \ddots & 1 \\
            & & 1 & -2
        \end{bmatrix}
        \frac{1}{\Delta x^2} \\
        \bar{F} &= \frac{1}{\Delta x^2}
        \begin{bmatrix}
            u_a \\
            0 \\
            \vdots \\
            0 \\
            u_b
        \end{bmatrix} \\
        \bar{U}\left( t \right) &= \sum \bar{\alpha}_i e^{\lambda_i t} + U_p\left( t \right)
    \end{align*}
    Soluci\'on num\'erica:
    \begin{itemize}
        \item Euler ($\O(\Delta t )$):
            \[
                \bar{U}^{n+1} = \bar{U}^n + \Delta t\left[\underset{\sim}{A}\bar{U}^n + \bar{F}^n\right],\quad n=0,1, \dots.
            \]
            Los vaps de $\underset{\sim}{A}\cdot \Delta x^2$ son: $-4\leq \lambda_i < 0 \implies \frac{-4}{\Delta x^2} \leq \lambda_i <0$, $\min\left( \lambda_i \right) \rightarrow \frac{-4}{\Delta x^2}$ cuando $N\rightarrow +\infty$. Por lo tanto Euler es estable si $\Delta t \leq \Delta t_{\text{cr\'itico}} = \frac{1}{2}\Delta x^2$.
            \[
                r:= \frac{\Delta t}{\Delta x^2} \leq \frac{1}{2}.
            \]
        \item Euler hacia atr\'as ($\O(\Delta t )$):
            \begin{align*}
                \bar{U}^{n+1} &= \bar{U}^n + \Delta t\left[ \underset{\sim}{A}\bar{U}^{n+1} + \bar{F}^{n+1} \right], \\
                \left[ \underset{\sim}{\Id} - \Delta t \underset{\sim}{A} \right]\bar{U}^{n+1} &= \bar{U}^n + \Delta t \bar{F}^{n+1}.
            \end{align*}
    \end{itemize}
\end{obs}

\section{Condiciones de contorno de Neumann: nodos ficticios}
EDP:
\[
    \frac{\partial u}{\partial t} = \nu \frac{\partial^2 u}{\partial x^2}, \quad x\in (a, b),\, t>0.
\]
En general, $\frac{\partial}{\partial x}\left( \nu\left( x \right)\frac{\partial u}{\partial x} \right)$.
\begin{itemize}
    \item Condiciones iniciales: $u\left( x, 0 \right) = f\left( x \right)$.
    \item Condiciones de contorno:
        \[
            \begin{cases}
                -\nu\frac{\partial u}{\partial x} \left( a \right) = q_a & \text{flujo (de calor) en el contorno.} \\
                \nu \frac{\partial u}{\partial x} \left( b \right) = q_b & \text{Neumann (CC en }\frac{\partial u}{\partial x}\text{).}
            \end{cases}
        \]
\end{itemize}
Hacemos la discretitzación en el espacio ($\Delta x = \frac{b-a}{N-1}$):
\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
                xmin=0.5, xmax=11.5,
                hide y axis,
                height = 0.1\linewidth,
                width = \linewidth,
                axis lines = center,
                xtick={2,3,...,10},
                xticklabels={$a=x_1$, $x_1$, $x_2$, $x_3$, , , , $x_{N-1}$, $b=x_N$},
        ]
            \addplot[domain=0:10, thick, opacity=0]{x-5};
        \end{axis}
    \end{tikzpicture}
\end{center}
Ahora $U_i\left( t \right)$ es incógnita en todos los nodos en $\left[ a, b \right]$, $i=1,\dots, N$. Imponemos la aproximación de la EDP en $\left\{ x_i \right\}_{i=1}^N$:
\[
    \frac{\dif U_i}{\dif t} = \frac{\nu}{\Delta x^2} \left[ U_{i-1}-2U_i+U_{i+1} \right],\quad i=1,\dots,N
\]
\begin{center}
    \begin{tikzpicture}
        \begin{axis}[
                xmin=0.5, xmax=11.5,
                hide y axis,
                height = 0.1\linewidth,
                width = \linewidth,
                axis lines = center,
                xtick={1,2,...,11},
                xticklabels={$x_0$, $a=x_1$, $x_1$, $x_2$, $x_3$, , , , $x_{N-1}$, $b=x_N$, $x_{N+1}$},
        ]
            \addplot[domain=0:10, thick, opacity=0]{x-5};
            \addplot[mark=*,color=black] coordinates {(1, 0)} node[]{};
            \addplot[mark=*,color=black] coordinates {(11, 0)} node[]{};
        \end{axis}
    \end{tikzpicture}
\end{center}
Introducimos un nodo ficticio para cada contorno de Neumann ($x_0$ y $x_{N+1}$). Tenemos una incógnita adicional para cada nodo ficticio, pero la despejaremos en función de $\left\{ U_i \right\}_{i=1}^N$ haciendo servir las condiciones de contorno. En $x=a$:
\[
    q_a = -\nu\frac{\partial u}{\partial x} \left( a \right) = -\nu \left[ \frac{u_2-u_0}{2\Delta x} + \O\left( \Delta x^2 \right) \right].
\]
Menospreciando $\O\left( \Delta x^2 \right)$ obtenemos la aproximación deseada
\[
    q_a = -\nu\left[ \frac{U_2-U_0}{2\Delta x} \right] \implies U_0 = U_2 + q_a\frac{2\Delta x}{\nu}.
\]
Sustituyendo en la ecuación por $i=1$ tenemos
\[
    \frac{\dif U_1}{\dif t} = \frac{\nu}{\Delta x^2} \left[ -2U_1 + 2U_2 \right] + \frac{2}{\Delta x} q_a.
\]
Análogamente en $x=b$:
\begin{align*}
    q_b &= \nu\frac{\partial u}{\partial x}\left( b \right) = \nu\left[ \frac{U_{N+1} - U_{N-1}}{2\Delta x} + \O\left( \Delta x^2 \right) \right], \\
    q_b &= \nu\left[ \frac{U_{N+1} - U_{N-1}}{2\Delta x} \right] \implies U_{N+1} = U_{N-1} + q_b\frac{2\Delta x}{\nu}.
\end{align*}
y tenemos
\[
    \frac{\dif U_N}{\dif t} = \frac{\nu}{\Delta x^2} \left[ 2U_{N-1} - 2U_N \right] + \frac{2}{\Delta x}q_b.
\]
Por lo tanto, el problema del valor inicial es
\[
    \begin{cases}
        \frac{\dif \bar{U}}{\dif t} = \underset{\sim}{A}\bar{U} + \bar{F}\\
        \bar{U}\left( 0 \right) = \bar{U}^0
    \end{cases}
\]
con
\begin{align*}
    \bar{U} &=
    \begin{bmatrix}
        U_1 \\
        \vdots \\
        U_N
    \end{bmatrix}
    , \\
    \underset{\sim}{A} &= 
    \begin{bmatrix}
        -2 & 2 & & & \\
        1 & -2 & 1 & & \\
        & \ddots & \ddots & \ddots & \\
        & & 1 & -2 & 1 \\
        & & & 2 & -2
    \end{bmatrix}
    \frac{\nu}{\Delta x^2}, \\
    \bar{F} &= 
    \begin{bmatrix}
        q_a \\
        0 \\
        \vdots \\
        0 \\
        q_b
    \end{bmatrix}
    \frac{2}{\Delta x}, \\
    \bar{U}^0 &=
    \begin{bmatrix}
        f\left( x_1 \right) \\
        \vdots \\
        f\left( x_N \right)
    \end{bmatrix}.
\end{align*}

\section{Otras EDPs}
\begin{itemize}
    \item Ecuación de la calor transitoria: ecuación parabólica (condición de contorno en todo el contorno),
        \[
            \frac{\partial u}{\partial t} = \nu\underbrace{\frac{\partial^2 u}{\partial x^2}}_{\mathclap{\text{difusión}}}.
        \]
    \item Ecuación de convección/transporte: ecuación hiperbólica,
        \[
            \underbrace{\frac{\partial u}{\partial t} + a\overbrace{\frac{\partial u}{\partial x}}^{\mathclap{\text{convección}}}}_{u\left( x, t \right) = u^0\left( x-at \right)} = 0.
        \]
\end{itemize}
Discretización en el espacio:
\[
    \frac{\dif U_i}{\dif t} + \frac{a}{2\Delta x}\left[ U_{i+1} - U_{i-1} \right] = 0.
\]
El error es $\O\left( \Delta x^2 \right)$ y por lo tanto
\[
    \frac{\dif \bar{U}}{\dif t} = \underset{\sim}{A}\bar{U} + \bar{F},\quad \text{con }\underset{\sim}{A} =
    \begin{bmatrix}
        0 & 1 & & \\
        -1 & \ddots & \ddots & \\
        & \ddots & \ddots & 1 \\
        & & -1 & 0
    \end{bmatrix}
    \frac{-a}{2\Delta x}.
\]
Con la aproximación centrada en $\frac{\partial u}{\partial x}$ vemos que los vaps de $A$ son $\lambda_i = a_i$ (imaginarios puros). Si hacemos aproximación ``upwind'' (contracorriente):
\begin{align*}
    \frac{\partial u}{\partial x}_{\vert x=x_i} &= \frac{u_i - u_{i-1}}{\Delta x} + \O\left( \Delta x \right) \\
    \underset{\sim}{A} &= -
    \begin{bmatrix}
        1 & & & \\
        -1 & \ddots & & \\
        & \ddots & \ddots & \\
        & & -1 & 1
    \end{bmatrix}
    \frac{a}{\Delta x} \implies \lambda_i = \frac{-a}{\Delta x}
\end{align*}
y por lo tanto podemos aplicar el m\'etodo de Euler.
