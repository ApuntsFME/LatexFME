\chapter{Introducció. Notació i EDPs més importants}

\section{Notació}

A les EDPs on no apareix el temps denotarem per $x = (x_1, \dots, x_n) 
\in \real^n$ la variable independent i per $u = u(x) = u(x_1, \dots, x_n)$
la funció incògnita o variable dependent. A les EDPs on apareix el temps
(anomenades equacions d'evolució) denotarem per $(x, t) = (x_1, \dots, 
x_n, t)$ les variables independents i per $u = u(x, t)$ les dependents. 
En general, escriurem les derivades parcials d'una funció com $\pdv{u}{x_1}
=\partial_{x_1} u = u_{x_1}$. Habitualment utilitzarem aquesta última
notació però en alguna ocasió també farem servir $u_{x_1} = u_1$ per fer-la
més lleugera. Anàlogament, per les derivades parcials en més d'una
variable, $\frac{\partial^2 u}{\partial x_1 \partial x_2} =\partial_{x_1 x_2} 
u = u_{x_1 x_2} = u_{12}$. Tota EDP es pot escriure com 
$F(t, x_1, \dots, x_n, u_t, u_{x_1},\dots, u_{x_n}, u, u_{tt}, 
u_{tx_1}, \dots) = 0$, \'es a dir, \'es una
equació en termes de les variables independents, la
funció incògnita i un nombre finit de les seves respectives derivades parcials.

\begin{defi}[ordre d'una EDP]
    L'ordre d'una EDP \'es l'ordre de les derivades parcials m\'es gran
    que apareix a l'equació.
\end{defi}

\begin{defi}[EDP lineal]
    Diem que una EDP \'es lineal si l'equació que la defineix \'es lineal
    en la variable $u$ i en totes les seves derivades parcials (no cal que sigui lineal en les variables independents). Diem que \'es no lineal en cas contrari.
\end{defi}

\begin{example}
    Vegem un exemple d'una EDP no lineal de segon ordre:
    \[
        tu_{x_1} + (u_{tx_2})^2 - 3 = 0,
    \]
    
    Un exemple d'una EDP de segon ordre que és lineal (encara que la funció $F$ sigui no lineal en la variable $t$) és el següent:
    \[
        t^2 u_{x_1} + u_{tx_2} - 3 = 0.
    \]
\end{example}

\section{EDPs principals}
En aquest curs tractarem EDPs de primer i segon ordre. Veurem ara les EDPs 
m\'es rellevants.

\begin{defi}[equació!general de primer ordre]
    Una equació general de primer ordre \'es de la forma
    \[
        F(x, u, \nabla u) = 0,
    \]
    amb $u = u(x)$ i $F$ no necessàriament lineal.
\end{defi}
\begin{obs}
    En general, quan parlem del gradient $\nabla u$ i del Laplacià $\Delta u$
    d'una funció $u = u(x, t)$, aquests seran nom\'es respecte $x$, \'es a dir, 
    $\nabla u = \nabla_x u$ i $\Delta u = \Delta_x u$.
\end{obs}
\begin{defi}[equació!de Laplace]
    L'equació de Laplace per $u$ (on $u = u(x)$) \'es
    \[
        \Delta u = 0.
    \]
    On $\Delta u$ \'es el Laplacià, \'es a dir, $\Delta u = u_{x_1 x_1} + \dots +
    u_{x_n x_n} = \tr D^2 u = \diver \grad u$. Si $u$ satisfà l'equació de Laplace es diu que \'es harmònica.
\end{defi}
\begin{defi}[equació!de Poisson]
    L'equació de Poisson \'es el cas no homogeni de la de Laplace, \'es a dir,
    \[
        -\Delta u = f(x).
    \]
    
    L'Equació de Poisson no lineal és
    \[
        -\Delta u = f(u).
    \]
\end{defi}
\begin{defi}[equació!de difusió]
    L'equació de difusió o de la calor per $u$ \'es
    \[
        u_t - D \Delta u = 0,
    \]
    on $D > 0$ s'anomena la constant de difusió, o b\'e
    \[
        u_t -D \Delta u = f(x, t).
    \]
    en el cas no homogeni. Habitualment s'anomena equació de la calor quan $u$ \'es
    una temperatura i equació de difusió quan $u$ \'es una concentració.
\end{defi}
\begin{obs}
    L'equació de difusió amb $D = 1$ i quan $u$ \'es independent del temps \'es
    l'equació de Laplace.
\end{obs}
\begin{defi}[equació!d'ones]
    L'equació d'ones per $u$ \'es
   \[
       u_{tt} - c^2 \Delta u = 0,
   \]
   on $c > 0$ \'es la velocitat d'ones, una constant.
\end{defi}
Presentem dues equacions m\'es. La primera prov\'e dels models de la
mecànica quàntica i la segona de models financers (governa el preu d'una opció). Les tractarem en alguns problemes.
\begin{defi}[equació!de Schrodinger]
    L'equació de Schrodinger per $u = u(x, t) \in \cx$ \'es
    \[
        iu_t + \Delta u = 0.
    \]
\end{defi}
\begin{defi}[equació!de Black-Scholes]
    L'equació de Black-Scholes per $u$ \'es
    \[
        u_t + \frac{1}{2} \sigma^2 x^2 u_{xx} + r x u_x - r u = 0.
    \]
    on $u$ és el preu d'una opció com a funció del seu preu de mercat $x$ i el temps $t$, $r$ és la taxa d'interès (lliure de risc) i $\sigma$ és la volatilitat del mercat.
\end{defi}
Veiem finalment dues equacions que no estudiarem aquest curs però que són tamb\'e
rellevants. Serveixen per modelar el comportament dels fluids.
\begin{defi}[equació!d'Euler]
    L'equació d'Euler per $\vec{u}$ \'es el sistema
    \[
        \begin{cases}
            \vec{u}_t + (\vec{u} \cdot \nabla) \vec{u} = - \nabla p \\
            \diver \vec{u} = 0.
        \end{cases}
    \]
    on $p$ \'es la pressió i on $\vec{u} \cdot \nabla = u_1 \partial_{x_1} +
    \dots + u_n \partial x_n$ (aquí $\vec{u} = (u_1, ..., u_n)$ i $u_i$ indica la $i$-èssima funció component de $\vec{u}$ i no la derivada parcial respecte de la $i$-èssima component).
\end{defi}
\begin{defi}[equació!de Navier-Stokes]
    L'equació de Navier-Stokes per $\vec{u}$ \'es el sistema
    \[
        \begin{cases}
            \vec{u}_t + (\vec{u} \cdot \nabla) \vec{u} - \nu \Delta u = - \nabla p \\
            \diver \vec{u} = 0.
        \end{cases}
    \]
    on $\nu$ \'es la constant de viscositat i $p$, com abans, \'es la pressió.
\end{defi}

