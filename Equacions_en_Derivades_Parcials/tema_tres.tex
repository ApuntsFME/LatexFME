\chapter{Espais de Banach, operadors i semigrups}
Al llarg del capítol $(E, ||\cdot||)$ serà un espai de Banach.
\section{Espais de Banach}
\begin{obs}
    Recordem que un espai de Banach \'es un espai vectorial normat i complet, i
    que una norma \'es una aplicació $||\cdot|| \colon E \to \real$ tal que
    \begin{itemize}
        \item $||u|| \geq 0$.
        \item $||u + v|| \leq ||u|| + ||v||$.
        \item $||\lambda u|| = |\lambda| ||u||$.
        \item $||u|| = 0 \iff u = 0$.
    \end{itemize}
\end{obs}
\begin{example}
    \begin{enumerate}
        \item[]
        \item Per $K \subset \real^n$ compacte, $E = \C^0(K) = \C(K)$ amb la norma
            $||u||_{\infty} = \max_{x \in K} |u(x)|$ \'es un espai de Banach.
        \item $E = L^2(a, b)$, amb norma $||u||_2 = \lp \int_a^b |u|^2 (x) \dif x \rp^
            {\frac{1}{2}}$ \'es un espai de Banach.
    \end{enumerate}
\end{example}
\begin{obs}
    Donat $\Omega \subset \real^n$ obert i fitat aleshores $\overline{\Omega}$ \'es compacte i
    $\C\lp\overline{\Omega}\rp$ \'es un espai de Banach. Si $\Omega$ no \'es fitat, cal
    considerar $\C_b \lp \overline{\Omega} \rp$ (el subíndex $b$ prové de ``bounded''), l'espai de funcions contínues i fitades,
    que sí \'es un espai de Banach. Anàlogament $\C^k \lp \overline{\Omega} \rp$ \'es
    un espai de Banach quan $\Omega$ \'es fitat i cal imposar que les derivades d'ordre
    menor o igual a $k$ siguin fitades en cas contrari. Finalment, l'espai $\C^{\infty}$
    no \'es de Banach.
\end{obs}
\begin{obs}
    Per $\Omega \subset \real^n$ no necessàriament fitat, l'espai $L^p(\Omega)$, $p \in \real$,
    $1 \leq p < \infty$ \'es un espai de Banach amb la norma
    \[
        ||u||_p = ||u||_{L^p} = \lp \int_{\Omega} |u|^p \rp^\frac{1}{p}.
    \]
    I l'espai $L^\infty(\Omega)$ de funcions mesurables i essencialment fitades
    (fitades gaireb\'e arreu per una constant) amb la norma
    \[
        ||u||_\infty = \inf\{b \in \real \colon |\{|u(x)| > b\}| = 0\}
    \]
    (on $|\cdot|$ és la mesura de Lebesgue a $\real^n$) tamb\'e ho \'es.
\end{obs}
\begin{teo}[Teorema del punt fix de Banach]
    Sigui $E$ un espai de Banach i $T \colon \overline{B_M}(u) \subset E \to
    \overline{B_M}(u)$ una contracció, \'es a dir, $||Tu - Tv|| \leq \lambda ||u-v||$ per
    a tot $u, v \in \overline{B_M}(u)$, amb $0 \leq \lambda < 1$. Aleshores $T$ t\'e un
    únic punt fix.
\end{teo}
\begin{proof}
    La demostració es deixa com exercici per al lector. Consisteix
    a iterar un punt qualsevol i veure que la successió donada per aquesta iteració
    \'es de Cauchy fitant les diferències amb una progressió geomètrica.
\end{proof}
\section{Operadors}
\begin{defi}[operador]
    Un operador \'es una aplicació entre espais de funcions.

    Es diu que un operador és lineal quan l'aplicació ho és i no lineal altrament.
\end{defi}
\begin{example}
    La diferenciació \'es un operador lineal
    \[
        \begin{aligned}
            D \colon \C^1 \lp [0, 1] \rp & \to \C^0 \lp [0, 1] \rp \\
            u = u(x) &\mapsto u' = u'(x).
        \end{aligned}
    \]
\end{example}
\begin{prop}
    Siguin $E, F$ espais de Banach i $A\colon E \to F$ un operador lineal. Aleshores
    són equivalents
    \begin{enumerate}[(i)]
        \item A \'es continu.
        \item A \'es un ``operador fitat'', \'es a dir, existeix $C$ tal que
            $||Au||_F \leq C ||u||_E$ per a tot $u \in E$ \footnote{ Si $A \not \equiv 0$ això no vol dir que $A$ sigui una aplicació fitada a $E$, doncs $||A(\lambda u)|| = |\lambda| ||Au|| \to \infty$ si $|\lambda | \to \infty$ i $Au \neq 0$.}.
    \end{enumerate}
\end{prop}
\begin{proof}
    Vegem primer que la segona condició implica la primera. Donat $u_k \to u$, aleshores
    $||u_k - u|| \to 0$, d'on $||Au_k - Au|| \leq C ||u_k - u|| \to 0$, i per tant $Au_k
    \to Au$, \'es a dir, $A$ \'es continu. La implicació conversa es deixa com a exercici.
    Per demostrar-la, cal veure que la norma que definirem a continuació està ben definida
    per operadors lineals i continus.
\end{proof}
\begin{defi}[norma d'un operador lineal]
    Donat un operador $A \colon E \to F$ lineal i continu, es defineix com la norma de
    $A$ la constant $C \in \real$ m\'es petita que fita l'operador. \'Es a dir,
    \begin{gather*}
        ||A|| = \inf \{C \geq 0 \colon ||Au||_F  \leq C ||u||_E \ \forall u
        \in E \} = \sup_{u \in E \setminus \{0 \}} \frac{||Au||_F}{||u||_E} = \\
        = \sup_{v \in E,\, ||v||_E = 1} ||Av||_F = \sup_{v \in E,\, ||v||_E \leq 1} ||Av||_F.
    \end{gather*}
\end{defi}
Aquesta norma indueix un altre espai de Banach que definim a continuació.
\begin{defi}
    El conjunt d'operadors lineals i continus entre espais de Banach, $\mathcal{L}(E, F)$,
    \'es un espai vectorial i amb la norma $||A||$ que hem definit anteriorment \'es
    un espai de Banach.
\end{defi}
%TODO revisar utilitat de l'exemple i posar-lo
%\begin{example}
%    Donat un espai de Banach $E$ i el conjunt d'aplicacions contínues $\C^0([a, b], E)$,
%    aquest últim conjunt admet la norma $||u|| = \max_{t \in [a,b]} ||v(t)||_E$ i amb
%    aquesta norma \'es un espai de Banach. Aleshores, si volem resoldre una EDO de la
%    forma $v'(t) =
%\end{example}
\begin{example}
    \begin{enumerate}[1.]
        \item[]
        \item \emph{Diferenciació}: Donat $u = u(t)$ amb $t \in [a,b]$, l'operador
            $D \colon u \to u'$ \'es un operador lineal. Malgrat això, no \'es un
            endomorfisme, per exemple, envia $\C^1([a,b])$ a $\C^0([a, b])$, però
            no $\C^1([a, b])$ a $\C^1([a, b])$, i això veurem que crearà greus dificultats quan vulguem usar teoremes de punt fix per trobar solucions d'EDPs no lineals.

            Tamb\'e podem comprovar que no existeix $C$ tal que
            $||Du||_\infty \leq C ||u||_\infty$, prenent per exemple la família
            de funcions $e^{at}$, que necessitaria constants $C$ arbitràriament grans,
            i per tant tampoc \'es un operador continu quan és mirat amb la mateixa
            norma de sortida i d'arribada calculant-lo en funcions $\C^\infty$.
        \item \emph{Integració}: Donat $u = u(t)$, l'operador ``integració'' (o ``primitiva'')
            \[
                (I(u))(t) = \int_a^t u
            \]
            \'es un endomorfisme continu de $\C([a, b])$ a si mateix. En efecte, si $u \in \C([a, b])$, pel teorema fonamental del càlcul $I(u) \in \C([a, b])$. A més, l'operador és trivialment lineal i és fitat perquè donat $u \in \C([a, b])$, 
            \begin{align*}
                ||I(u)||_{\infty} & = \max_{t \in [a,b]} \left\{ \left| \int_{a}^t u(x) \dif x\right |\right\} \leq \max_{t \in [a,b]} \left\{ \int_{a}^t \left| u(x) \right| \dif x \right\} \\
                & \leq \max_{t \in [a,b]} \left\{ \int_{a}^t || u ||_{\infty} \dif x \right\} = (b-a) || u ||_{\infty},
            \end{align*}
            i, per tant, és continu.
    \end{enumerate}
\end{example}
\begin{ej}
    Demostreu que l'operador integració \'es un endomorfisme continu a l'espai
    $E = L^p(a, b)$.
\end{ej}
\section{Resolució d'EDPs per punt fix}
\begin{defi}[funció!localment Lipschitz]
    Diem que una funció $F:E \to E$ \'es localment Lipschitz si per a tot conjunt $T$ tancat
    i fitat de $E$, existeix una constant $C_T$ tal que
    \[
        ||F(u) - F(v)|| \leq C_T ||u-v||,
    \]
    per a tot $u, v \in T$.
\end{defi}
\begin{prop}
    \label{prop:solucio_edo_banach}
    Donat $u = u(t)$ de la forma $u \colon [a, b] \to E$ i $F \colon E \to E$ localment
    Lipschitz,
    existeix una única solució
    de l'EDO per un cert interval (petit) de temps $I$ (obert i amb $a \in I$)
    \[
        \begin{cases}
            u' = F(u) \\
            u(a) = g \in E.
        \end{cases}
    \]
\end{prop}
\begin{proof}
    La idea fonamental consisteix a escriure l'EDO a l'espai de Banach $E$ de forma integral (mètode de Picard).
    Volem resoldre aleshores
    \[
        u(t) - u(a) = \int_a^t u'(s) \dif s = \int_a^t F(u(s)) \dif s.
    \]
    \'Es a dir, cal resoldre
    \[
        u(t) = g + \int_a^t F(u(s)) \dif x
    \]
    per a tot $t \in \overline{I}$. Si definim $\tilde{E} = \C^0(\overline{I}, E)$ espai de Banach, podem interpretar
    les solucions d'aquest problema com punts fixos de l'operador $\mathcal{F}
    \colon \tilde{E} \to \tilde{E}$
    \[
        \lp\mathcal{F}(u)\rp(t) = g + \int_a^t F(u(s)) \dif s.
    \]
    Si veiem que, per un interval $\overline{I}$ adequat, $\mathcal{F}$ restringida a una bola adequada envia aquesta bola a si mateixa
    i \'es una contracció, aleshores podrem aplicar el teorema del punt fix i trobar una solució. En primer lloc, observem que, donada $u \in \tilde{E}$,
    aleshores $\mathcal{F}(u) \in \C^0(\overline{I}, E)$, \'es m\'es, $\mathcal{F}(u) \in \C^1(\overline{I}, E)$.
    Prenguem ara una bola de radi $M$ centrada en $g$ (entesa $g$
    com la funció constant en $t$). Vegem que, si
    $u \in \overline{B_M}(g)$, aleshores $\mathcal{F}(u) \in \overline{B_M}(g)$. Per a veure això, sigui $C_M$ la constant de Lipschitz de $F$ a la bola
    $\overline{B_M}(g)$ (entesa $g$ aquí com element de $E$). Prenent $M := ||F(g)||$ i $|I|\leq \frac{1}{2}$, tenim
    \begin{gather*}
        ||\mathcal{F}(u(t)) - g|| = \left \lVert \int_a^t F(u(s)) \dif s \right \rVert
        \leq \int_{\min(a, t)} ^{\max(a,t)} ||F(u(s))|| \dif s \leq \\
        \leq \int_{\min(a, t)} ^{\max(a,t)} \lp ||F(u(s)) - F(u(a))|| + ||F(g)|| \rp \dif s \leq \\
        \leq \int_{\min(a, t)} ^{\max(a,t)} \lp C_M ||u(s) - u(a)|| + ||F(g)|| \rp \dif s \leq \\
        \leq |I| \, (C_M M + ||F(g)||) \leq  |I| C_M M + \frac{1}{2} M \leq M
    \end{gather*}
    si fem $|I|$ prou petit. Per tant, prenent suprem per a tot $t$, tenim que $\mathcal{F}(u) \in \overline{B_M}(g)$.
    Vegem que \'es una contracció per a tot $u, v \in \overline{B_M}(g)$.
    \begin{gather*}
        ||\mathcal{F}(u)(t) - \mathcal{F}(v)(t)|| = \left \lVert \int_0^t F(u(s)) - F(v(s)) \dif
        s \right \rVert \leq \int_{\min(a, t)}^{\max(a,t)} ||F(u(s)) - F(v(s))|| \dif s \leq \\ \leq
        C_M \int_{\min(a, t)}^{\max(a,t)} ||u(s) - v(s)|| \dif s \leq C_M \, |I| \, ||u-v||_{\tilde{E}}.
    \end{gather*}
    Finalment, $\lambda = C_M |I|$ \'es menor que $1$ si $|I|$ \'es prou petit, i podem
    prendre suprem per a tot $t$ per obtenir
    \[
        ||\mathcal{F}(u) - \mathcal{F}(v)||_{\tilde{E}} \leq \lambda ||u - v||_{\tilde{E}}.
    \]
    Per tant, ja hem vist que $\mathcal{F}$ \'es una contracció que envia la bola
    $\overline{B_M}(g)$ a si mateixa. Si apliquem el teorema del punt fix de Banach obtenim
    una solució $u \in \tilde{E}$ única a la bola definida.
\end{proof}
\begin{obs}
    \label{obs:solucio_edo_banach}
    Si la funció $F$ \'es globalment Lipschitz, podem prendre $\mathcal{F}$ sobre tot
    $\tilde{E}$ i obtenir que $\mathcal{F}$ \'es una contracció amb una fita independent
    de la condició inicial, \'es a dir, la longitud de l'interval $I$ necessària perquè
    $\mathcal{F}$ sigui una contracció no dependrà de la condició inicial.
    Aleshores es pot prolongar la solució a tot $\real$ (nom\'es cal afegir tants intervals
    de mida |I| com sigui necessari).
\end{obs}
Estudiem ara el mateix problema restringint-nos al cas on $F$ \'es lineal i
contínua. Veurem que, com en el cas de les EDOs, la solució ve donada per l'exponencial.
En primer lloc, caldrà definir l'exponencial adequadament.
\begin{defi}[exponencial d'un operador]
    Donada una aplicació $A \colon E \to E$ lineal i contínua, definim l'aplicació
    $e^{tA} \colon E \to E$ com
    \[
        e^{tA} := \sum_{k = 0}^\infty \frac{t^kA^k}{k!}.
    \]
\end{defi}
\begin{prop}
    Donada una aplicació $A \colon E \to E$ lineal i contínua, $e^{tA}$ està ben definida
    i \'es una aplicació lineal i contínua.
\end{prop}
\begin{proof}
    Per comprovar que està ben definida hem de veure que la sèrie corresponent \'es
    convergent. Veurem, de fet, que \'es absolutament convergent.
    \[
        \sum_{k = 0}^\infty \left \lVert\frac{t^kA^k}{k!} \right \rVert \leq \sum_{k = 0}^\infty
        \frac{|t|^k}{k!}||A^k|| \leq \sum_{k = 0}^\infty \frac{|t|^k}{k!}||A||^k  =
        e^{|t| ||A||} < \infty.
    \]
    \'Es clar que aleshores tamb\'e \'es lineal, i per veure que \'es contínua cal
    observar que $||e^{tA}|| \leq e^{|t| ||A||}$.
\end{proof}
\begin{prop}
    Sigui $A \colon E \to E$ una aplicació lineal i contínua amb $E$ espai de Banach.
    L'única solució de
    \[
        \begin{cases}
            u_t = Au, & t \in \real, \\
            u(0) = g \in E,
        \end{cases}
    \]
    ve donada per $u(t) = e^{tA} g$.
\end{prop}
\begin{proof}
    La unicitat ja l'hem vista mitjançant el teorema del punt fix de Banach
    (\ref{prop:solucio_edo_banach} i \ref{obs:solucio_edo_banach}). Hem de veure per tant
    que $u(t) = e^{tA} g$ \'es efectivament una solució. En primer lloc,
    \[
        u(0) = e^{0A} g= \Id g = g.
    \]
    En segon lloc,
    \[
        \frac{\dif u}{\dif t} (t) = \sum_{k=1}^{\infty} \frac{kt^{k-1}}{k!} A^k g
        = \sum_{k=1}^{\infty} A \frac{t^{k-1}A^{k-1}}{(k-1)!} g = A e^{tA} g,
    \]
    on hem pogut derivar terme a terme perquè $\sum_{k=1}^{\infty}
    \frac{t^{k-1}A^{k-1}}{(k-1)!} A g$ \'es absolutament convergent.
\end{proof}
Volem ara resoldre l'EDO no homogènia
\begin{equation}
    \label{eq:EDONHBanach}
    \begin{cases}
        u_t = Au + f(t) \\
        u(0) = g \in E
    \end{cases}
\end{equation}
amb $f\colon \real \to E$. Sabem que el semigrup de l'equació homogènia ve donat per
$T_t g = e^{tA}g$, que satisfà la propietat del semigrup: $T_s \circ
T_t = e^{sA} e^{tA} = e^{(s+t)A} = T_{s+t}$. La solució del problema no homogeni
ve donada per la fórmula de Duhamel o de variació de les constants:
\[
    u(t) = e^{tA} g + \int_0^t e^{(t-s)A} f(s) \dif s.
\]
\begin{obs}
    S'anomena tamb\'e fórmula de variació de les constants perquè es pot obtenir
    amb el mètode de variació de les constants de EDOs. En efecte, si busquem la solució $u$ de la forma
    $u(t) = e^{tA} g(t)$, volem que
    \[
        Ae^{At} g(t) + f(t) \overset{\eqref{eq:EDONHBanach}}{=} u'(t) = Ae^{tA} g(t) + e^{tA} g'(t).
    \]
    I per tant
    \[
        g'(t) = e^{-tA} f(t),
    \]
    d'on
    \[
        g(t) = g(0) + \int_0^t e^{-sA} f(s) \dif s.
    \]
    I finalment
    \[
        u(t) = e^{tA}g + \int_0^t e^{(t-s)A} f(s) \dif s.
    \]
\end{obs}
%TODO val la pena posar explicacio de per que no funciona per al transport??
Volem finalment resoldre EDPs de transport no lineals, com ara
\begin{enumerate}[i)]
    \item
        \[
            \begin{cases}
                u_t + c u_x = u^2, & x \in \real, t \in I, \\
                u(x, 0) = g(x), & x \in \real,
            \end{cases}
        \]
    \item
        \[
            \begin{cases}
                (u_t + cu_x)(x, t) = \frac{1}{x}\int_0^x \cos(u(y, t))
                \dif y, & x , t \in  \real, \\
                u(x, 0) = g(x), & x \in \real.
            \end{cases}
        \]
\end{enumerate}
El primer cas ja el sabem resoldre pel mètode de les característiques (veure
problema 5 de la primera llista). El segon, però, no dona una EDO sobre la recta
característica. Ara b\'e, com sabem resoldre el problema homogeni lineal i en
sabem descriure el semigrup, podem utilitzar la fórmula de Duhamel. \'Es a dir,
voldríem trobar solucions de
\[
    u(x, t) = (T_t g)(x) + \int_0^t (T_{t-s} F(u(\cdot, s)))(x) =: (Nu)(x, t)\dif s,
\]
on $(T_t v)(x) = v(x-ct)$ és el semigrup pel problema lineal homogeni $v_t + c v_x = 0$. Si definim $E = \C^0_b(\real)$, $\tilde{E} =
\C^0_b(\overline{I}, \real),$\footnote{
    La tria de l'espai de Banach sobre el que treballem \'es rellevant. En aquest
    cas, el fet que fixem l'espai de funcions contínues farà que puguem veure que
    \'es una contracció m\'es fàcilment però nom\'es obtindrem una solució integral
    (o generalitzada). No obtenim  en la demostració una solució clàssica $\C^1$ (sense usar arguments suplementaris com els d'EDOs, usant l'equació del punt fix que resolem). Podríem, però, treballar
    a $\C^1_b$ i trobar solucions clàssiques, si bé la demostració es complica doncs la norma $\C^1$ inclou més termes.
} i $\overline{I} = [-\varepsilon, \varepsilon]$, aleshores
tenim que $F$ \'es
\begin{align*}
    F \colon E &\to E \\
    w &\mapsto \frac{1}{x}\int_0^x \cos(w(y)) \dif y
\end{align*}
i a l'origen $F(w)(0) = \cos(w(0))$.
Busquem aleshores un punt fix de $N \colon \tilde{E} \to \tilde{E}$.

Procedirem igual que hem fet anteriorment per aplicar el teorema del punt
fix de Banach. En primer lloc cal veure que, com hem afirmat,
$N$ envia elements de $\tilde{E}$ a $\tilde{E}$. Sigui $u \in \tilde{E}$. En primer
lloc veiem que $N(u)$ \'es fitada sobre $I \times \real$. Sabem que $T_t$ \'es lineal
i continu, i per tant \'es Lipschitz. De fet, $||T_t g||_E = ||g(\cdot-ct)||_E = ||g||_E$, \'es
a dir, \'es una isometria. Tamb\'e tenim que
\[
    |F(u(\cdot, s))(x)| \leq \frac{1}{|x|} \int_0^x |\cos(u(\tilde{x}, s)))| \dif \tilde{x}  \leq \frac{|x|}{|x|}
    \leq 1.
\]
Aleshores $||N(u(\cdot, t))||_E \leq ||g||_E + \varepsilon$, \'es a dir, \'es fitat.
Com $T_t$ \'es continu
respecte de $t$ i $F$ tamb\'e, $N(u)$ tamb\'e ho serà.
%TODO revisar aixo.
Vegem ara, suposant que $F$ \'es globalment Lipschitz, que $N$ \'es una contracció.
\begin{gather*}
    ||N(u)(\cdot, t) - N(w)(\cdot, t))||_E = \left \lVert \int_0^t T_{t-s} (F(u(\cdot, s)) - F(w(\cdot, s)))
    \dif s \right \rVert_E \leq \\
    \leq \int_0^t \lVert T_{t-s} (F(u(\cdot, s)) - F(w(\cdot, s)))
    \rVert_E \dif s = \\
    = \int_0^t \lVert F(u(\cdot, s)) - F(w(\cdot, s))
    \rVert_E \dif s \leq C_L \varepsilon ||u-w||_{\tilde{E}}.
\end{gather*}
Prenent suprem per a tot $t$, $||N(u) - N(w)||_{\tilde{E}} \leq C_L \varepsilon
||u - w||_{\tilde{E}}$, i per tant $N$ \'es una contracció si $\varepsilon$ \'es
prou petit. Demostrarem ara que $F$ \'es globalment Lipschitz, com acabem de suposar.
\begin{gather*}
    ||(F(v_1) - F(v_2))(x)|| \leq \frac{1}{|x|} \int_{\min(0, x)}^{\max(0, x)}
    |\cos(v_1(y)) - \cos(v_2(y))| \dif y = \\
    = \frac{1}{|x|} \int_{\min(0, x)}^{\max(0, x)}
    |\sin \xi(y)| |v_1(y) - v_2(y)| \dif y \leq ||v_1 - v_2||_E.
\end{gather*}
Prenent suprem per a tota $x$, $||F(v_1) - F(v_2)||_E \leq ||v_1 - v_2||_E$, \'es a dir,
$F$ \'es Lipschitz i per tant $N$ \'es una contracció i podem trobar un punt fix
$Nu = u$. Remarquem que aquest punt fix pertany a $\tilde{E}$ i per tant no \'es
necessàriament diferenciable, \'es a dir, nom\'es obtenim una solució integral
o generalitzada.
