\chapter{Espais de Sobolev i problemes de contorn}

\section{Espais de Sobolev en dimensió 1}

\begin{defi}
    Sigui $\Omega \subset \real^n$ un obert, i sigui $1 \leq p \leq \infty$. Definim
    l'espai $L_{loc}^p (\Omega)$ com l'espai de les funcions mesurables que restringides a qualsevol
    compacte $K \subset \Omega$ són de $L^p (K)$.
\end{defi}

D'ara en endavant, ens centrarem en l'espai $L_{loc}^1 (\Omega)$. Observem que aquests espais
contenen moltes més funcions que els espais $L^p$ estàndard: sense anar més lluny, tots els polinomis
en $n$ variables pertanyen a $L_{loc}^1 (\Omega)$, però cap pertany a $L^1 (\Omega)$ (excepte el 0).

\begin{obs}
    La funció $f(x) = 1/x$ no pertany a $L_{loc}^1 (I)$ si $I \subset
    \mathbb{R}$ és un interval obert que contingui $x = 0$. Això és degut a que
    la singularitat de $x = 0$ no és integrable.
\end{obs}

En diverses demostracions d'aquest tema s'usa el concepte de successió
regularitzadora. Tot i que s'ha introduït a classe de problemes, en recordem la
definició a continuació. Intuïtivament, es tracta de successions que tendeixen a
la delta de Dirac.

\begin{defi}[successió regularitzadora]
    Una successió regularitzadora és una successió de funcions $\rho_n \in
    \C_c^{\infty} (\real)$ tal que $\forall n$ compleixen $\rho_n (x) \geq 0$,
    $\supp (\rho_n) \subset (-1/n,1/n)$, i $\int_{\real} \rho_n =  1$.
\end{defi}

\begin{defi}[derivada feble]
    Donada $u \in L_{loc}^1(I)$ (amb $I \subset \real$ interval), es diu que
    $g \in L_{loc}^1(I)$ és derivada feble de $u$ (i s'escriu $u' = g$) si
    \[
        \int_I u \varphi' = - \int_I g \varphi
    \]
    per a tota $\varphi \in \C_c^{1} (I)$. Aquestes funcions $\varphi$ es diuen
    funcions de prova o de test.
\end{defi}

\begin{obs}
    Les dues integrals de la definició existeixen perquè tant $u$ com $g$ són
    integrables en el suport de $\varphi$, que és compacte. Aquesta definició és
    una generalització de la fórmula d'integració per parts quan $u \in
    \C^{1}(I)$.
\end{obs}

\begin{obs}
    A la definició podríem haver pres $\C_c^{\infty}$ en comptes de $\C_c^{1}$,
    i no canviaria res.  Si $\varphi \in \C_c^{1}$ i considerem una successió
    regularitzadora $\eta_{\varepsilon}$, llavors $\eta_{\varepsilon} \ast
    \varphi \in \C_c^{\infty}$ i per a $\varepsilon \to 0$ es té
    $\eta_{\varepsilon} \varphi \to \varphi$ en norma $\C^1$.
\end{obs}

\begin{lema}
    Sigui $v \in L_{loc}^1 (I)$ tal que per a tota $\varphi \in \C_c^1(I)$ es té
    \[
        \int_{I} v \varphi = 0.
    \]
    Aleshores, $v = 0$ quasi pertot.
\end{lema}

%Miquel o Velasco, feu-ho si us fa il·lusió, però la demostració que fa el Brézis utilitza
%alguns resultats previs i em fa pal fer-ho tot.
La demostració d'aquest lema es troba al llibre de Brézis (coro\lgem{}ari 4.24, pàgina 110)
referenciat a la bibliografia de l'assignatura. No obstant, el presentem perquè serà necessari
per a la següent proposició.

\begin{prop}
    \begin{enumerate}[i)]
        \item[]
        \item Si $u$ és de classe $\C^1$ a $I \subset \real$, aleshores $\partial_x u$ també és la derivada
        feble de $u$. També és cert si $u$ és $\C^1$ a trossos a $I$.
        \item Per a tota $u \in L_{loc}^1(I)$, si existeix una derivada feble $g \in L_{loc}^1(I)$ llavors és única.
        \item Si $u \in L_{loc}^1 (I)$ i $u'(x) = 0$ quasi per a tota $x \in I$, aleshores existeix $C \in \real$
        tal que $u(x) = C$ quasi per a tota $x \in I$. 
    \end{enumerate}
    \label{derivadafeble}
\end{prop}
\begin{proof}
    \begin{enumerate}[i)]
        \item[]
        \item Si $u$ és de classe $\C^1$, el resultat és conseqüència de la
            fórmula d'integració per parts, utilitzant que totes les funcions
            $\varphi$ s'anu\l.len als extrems de l'interval. El cas de les
            funcions $\C^1$ a trossos es deixa com a exercici.

        \item Suposem que $g_1,g_2$ són derivades febles de la funció $u$. Llavors, per la definició, s'hauria
        de satisfer que
        \[
            \int_I (g_1 - g_2) \varphi = 0
        \]
        per a tota $\varphi \in \C_c^1(I)$. Pel lema anterior, deduïm que $g_1 = g_2$ quasi pertot (és a dir, són
        la mateixa funció a $L_{loc}^1$).

        \item Considerem una funció $\psi \in \C_c^1(I)$ tal que $\int_I \psi = 1$. Si sospitem que $u(x) = C$
        quasi per tot $x$, aleshores és raonable identificar $C = \int_I u \psi$, com veurem tot seguit.
        
        Sigui $w \in \C_c^1(I)$ una funció qualsevol. Pel lema anterior, ens serà suficient demostrar que
        \[  
            \int_I \lp u - \lp\int_I u \psi \rp\rp w = 0.
        \]
        Reordenant els termes, veiem que això és equivalent a
        \[
            \int_I u \lp w - \lp \int_I w \rp\psi \rp = 0.
        \]
        Si aconseguim veure que $h = w - \lp \int_I w \rp\psi$ és la derivada
        d'una certa funció $\varphi \in \C_c^1(I)$, ja haurem acabat (per la
        hipòtesi de l'enunciat). D'una banda, $h$ és una funció contínua i amb
        suport compacte, ja que $w,\psi$ ho són. D'altra banda, hauríem de poder
        garantir que $\varphi$ s'anu\l.la als extrems de l'interval, però això
        ho deduïm de:
        \[
            \int_I h = \int_I w - \lp \int_I w \rp \lp \int_I \psi\rp = 0.
        \]
        Per tant, existeix $\varphi \in \C_c^1(I)$ tal que $\varphi' = h$, i se segueix el resultat.
    \end{enumerate}
\end{proof}

\begin{example}
    Considerem la funció $u(x) = \operatorname{sign}(x)$ a l'interval $[-1,1]$. Suposem que té una
    derivada feble $u' = g$ en aquest interval. Considerem ara totes les funcions $\varphi$ 
    derivables amb continuïtat però amb suport només a $(-1,0)$. Llavors, hauríem de tenir:
    \[
        \int_{-1}^0 (-1) \varphi' = \varphi(-1) - \varphi(0) = 0 = \int_{-1}^0 g \varphi.
    \]
    Pel lema, hauríem de tenir $g = 0$ (gariebé arreu) en l'interval $(-1,0)$. Anàlogament, també hauríem
    de tenir $g = 0$ a $(0,1)$, és a dir, que $g = 0$ quasi pertot. Però això és una contradicció, ja que
    existeixen funcions $\varphi$ tal que
    \[
        \int_{-1}^1 u \varphi' \neq 0,
    \]
    com, per exemple, $\varphi(x) = x^2$. Per tant, $u$ no admet derivada feble.
\end{example}

\begin{defi}
    Sigui $I$ un interval obert (no necessàriament fitat), i $1 \leq p \leq \infty$. Definim l'espai de Sobolev
    \[
        W^{k,p} (I) = \{ u \in L^p(I) \text{ tal que } \exists g_i \in L^p(I) | g_i = u^{(i)}, \, \forall 1 \leq i \leq k\}
    \]
    Les derivades que apareixen a la definició s'entenen en sentit feble. Per $p = 2$ acostumem a notar $H^1(I): = W^{1,2} (I)$.

\end{defi}

En l'espai de Sobolev $W^{1,p}$ definim la norma $\|u\|_{W^{1,p}} = \|u\|_{L^p}
+ \|u'\|_{L^p}$. A vegades també usarem la norma equivalent $\|u\|'_{W^{1,p}} =
(\|u\|^p_{L^p} + \|u'\|^p_{L^p})^{1/p}$, sobretot en el cas de $H^1$, quan
aquesta norma prové del producte escalar $(u,v)_{H^1} = (u,v)_{L^2} +
(u',v')_{L^2}$. 

En l'espai de Sobolev $W^{k,p}$ definim anàlogament $\|u\|_{W^{k,p}} = \sum_{i =
0}^k \|u^{(i)}\|_{L^p}$.

\begin{teo*} 
    $W^{1,p}$ és un espai de Banach. Si $1 \leq p < \infty$ és separable, i si
    $1 < p < \infty$ és reflexiu. L'espai $H^1$ és de Hilbert.
\end{teo*}

\begin{proof}
    
    Observem que l'operador $T:W^{1,p} \longrightarrow L^p \times L^p$, tal que
    $Tu = (u,u')$, és una isometria. Podem mirar-nos $W^{1,p}$ com a subespai de
    $L^p \times L^p$ mitjançant la identificació isomètrica $T$. Com que $L^p
    \times L^p$ és un espai de Banach, per veure que $W^{1,p}$ és complet n'hi
    ha prou amb veure que $T(W^{1,p})$ és un tancat de $L^p \times L^p$. 

    En efecte, observem que podem identificar $W^{1,p}$ amb 
    \[
        \bigcap_{\varphi \in \C_c ^1(I)} \left \{(f,g) \in L^p\times L^p | \int_I f\varphi' + g\varphi = 0 \right \}
    \]
    Cada un d'aquests conjunts és tancat, perquè és el nucli de la forma lineal
    $(f,g) \mapsto \int_I f\varphi' + g\varphi$. Així doncs, $W^{1,p}$ vist dins
    de $L^p \times L^p$ és tancat per ser intersecció de tancats, i per tant és
    complet.

    Igualment, tot subespai tancat d'un espai reflexiu és reflexiu. Com que
    $L^p\times L^p$ és reflexiu, per $1 < p < \infty$, $W^{1,p}$ també és
    reflexiu per aquests valors de $p$.  Com que $L^p$ és separable per $1 \leq
    p < \infty$, també ho és $L^p \times L^p$, i com que tot subconjunt d'un
    espai separable és separable, $W^{1,p}$ és separable per aquests valors de
    $p$.

    $H^1(I)$ és de Hilbert, ja que hem vist que té una norma que prové d'un producte escalar, i és complet amb una norma equivalent.
\end{proof}

\begin{lema}
    Sigui $g \in L_{loc}^p (I)$, amb $ 1 \leq p \leq \infty$, i $y_0 \in I$.
    Sigui $v(x) = \int_{y_0}^x g(s) \dif s$. Aleshores $v$ és contínua i
    $\forall \varphi \in \C_c^1(I)$ es té $\int_I v\varphi' = -\int_I g
    \varphi$.
\end{lema}

\begin{proof}
    La pròpia definició de $v$ ens garanteix que es contínua. Sigui $I = (a,b)$.
    \[
        \int_I v\varphi' = \int_I \left [ \int_{y_0}^x g(t) \dif t \right] \varphi'(x) \dif x =
        -\int_a^{y_0} \dif x \int_x ^{y_0} g(t) \varphi'(x) \dif t +
        \int_{y_0}^b \dif x \int_{y_0} ^x g(t) \varphi'(x) \dif t
    \]
    Aplicant el Teorema de Fubini a cada terme, l'expressió anterior queda
    \[
        -\int_a^{y_0} g(t)\dif t \int_a^t \varphi'(x) \dif x + \int_{y_0}^b g(t) \dif t
        \int_{t} ^b \varphi'(x) \dif x = -\int_I g(t)\varphi(t) \dif t
    \]
    Així doncs tenim efectivament
    \[
    \int_I v\varphi' = -\int_I g\varphi
    \]
\end{proof}

\begin{teo}[Encabiment de Sobolev]
    Sigui $u \in W^{1,p} (I)$, amb $1 \leq p \leq \infty$. Aleshores,
    \begin{enumerate}[i)]
        \item $\exists ! \tilde{u}$ funció contínua a $\Bar{I}$ tal que $u(x) =
            \tilde{u}(x)$ q.p.t $x \in I$.
        \item $\exists$ una constant $K > 0$ independent de $u$ tal que $\|
            \tilde{u} \|_{\C(I)} \leq K\|u\|_{W^{1,p} (I)}$.
        \item Si $p < \infty$ i $I$ és no fitat, aleshores $\tilde{u}
            \rightarrow 0$ quan $|x| \rightarrow \infty$.
        \item Si $1 < p$, $I$ és fitat i $(u_n)$ és una successió fitada a
            $W^{1,p} (I)$, aleshores $(\tilde{u_n})$ és equicontínua i fitada, i
            per tant té una parcial convergent a $\C(\Bar{I})$.
    \end{enumerate}
\end{teo}

\begin{proof}
        \begin{enumerate}[i)]
        \item[]
        \item Sigui $u \in W^{1,p} (I)$ i $g = u'$. Definim $v(x) = \int_{y_0}^x
            g(s) \dif s$. Pel lema que acabem de veure, $g$ és la derivada de $v$.
            Aleshores, com que la diferència entre les derivades de $u$ i $v$ és
            zero, per la proposició \ref{derivadafeble}, la diferència entre $u$
            i $v$ ha de ser una constant g.a. Com que $v$ és contínua fins als
            extrems, i les constants també, $u = \tilde{u} := v+C$ q.p.t $x \in
            I$.

        \item Suposem $p < \infty$, i que $I$ és de longitud $l < \infty$.
            Donats $x,y \in \Bar{I}$, tindrem 
            \[
                |\tilde{u}(x)|-|\tilde{u}(y)| \leq |\tilde{u}(x)-\tilde{u}(y)| \leq
                \left \lvert \int_x^y u'(s) \dif s \right \rvert \leq
                \int_I |u'(s)| \dif s = \|u'\|_{L^1(I)}
            \]
            La norma $L^1$ de $u'$ està ben definida perquè, com que $I$ és fitat,
            $u \in L^p \implies u \in L^1$.

            La desigualtat que hem obtingut es pot reescriure com a $|\tilde{u}(x)|
            \leq |\tilde{u}(y)| + \|u'\|_{L^1(I)}$. Integrem aquesta desigualtat
            respecte $y$ i obtenim 
            \[
                l|\tilde{u}(x)| \leq \|u\|_{L^1(I)} + l\|u'\|_{L^1(I)} \leq
                \max \{1,l\} (\|u\|_{L^1(I)} + \|u'\|_{L^1(I)} )
            \]

            Com que $I$ és fitat, la norma $L^1(I)$ està fitada per la norma
            $L^p(I)$ amb una constant $K = K(l,p)$ que depèn només de $l$ i de $p$.
            Això prova el cas en què $I$ és fitat.

            Sigui ara $I$ no fitat. Prenem un interval auxiliar $I_1 \subset
            \bar{I}$ de longitud $l = 1$ tq $x \in I$. Aleshores $I_1$ és fitat i
            usant que $I_1 \subset I$ tenim 
            \[
                |\tilde{u}(x)| \leq K(1,p) \left [ \left ( \int_{I_1} |u|^p \right)^{1/p}
                + \left ( \int_{I_1} |u'|^p \right )^{1/p} \right ] \leq
                K(1,p) (\|u\|_{L^p(I)} + \|u'\|_{L^p(I)})
            \]

            En el cas $p = \infty$ l'argument és el mateix, però s'escriu de manera
            diferent. Es deixa com a exercici per al lector.
        \item Farem el cas $x \to \infty$. El cas $x \to -\infty$ és anàleg.
            Prenem l'interval $I_x = [x-1/2, x+1/2]$, i usem la desigualtat
            obtinguda en $(ii)$ \[ |\tilde{u}(x)| \leq K(1,p) \left [ \left (
                \int_{I_x} |u|^p \right )^{1/p} + \left ( \int_{I_x} |u'|^p
            \right )^{1/p} \right ] \]

            Com que $u, u' \in L^p$, és clar que $\int_{I_x} |u|^p \to 0$  i
            $\int_{I_x} |u'|^p \to 0$ quan $x \to \infty$. Per tant,
            efectivament $\tilde{u}(x) \to 0$ quan $x \to \infty$.

        \item Pel teorema d'Arzelà-Ascoli, una successió puntualment
            fitada i equicontínua definida en un compacte admet una
            parcial convergent. És clar que $\bar{I}$ és compacte, i per
            $(ii)$, $(\tilde{u_n})$ és puntualment fitada. Per tant,
            només ens queda veure l'equicontinuitat. Per hipòtesi
            $(u_n)$ és fitada en $W^{1,p}$, és a dir, $\exists \, M > 0
            \text{ tal que }\|u_n\|_{W^{1,p}} \leq M$. Aleshores, usant
            la desigualtat de Hölder, i prenent $x < y$ tenim
            \[
                |\tilde{u}(x)|-|\tilde{u}(y)| \leq \int_x^y |u'_n (s)| \dif s \leq
                \left( \int_x ^y |u'_n (s)|^p \dif s \right )^{1/p} \left ( \int_x
                ^y 1^q \dif s \right )^{1/q} \leq M |x-y|^{1/q}
            \]
            Com que $p > 1$ tenim $q < \infty$ i per tant aquesta desigualtat
            garanteix l'equicontinuïtat desitjada.

    \end{enumerate}
\end{proof}

\begin{obs}
    El resultat que acabem de demostrar és rellevant. Ens diu que $W^{1,p}$ està
    encabit dins de $\C(I)$, i a més a més aquest encabiment és continu $(ii)$ i
    compacte $(iv)$. A més a més, $(iii)$ és un resultat sorprenent, perquè no
    és cert en $L^p$. A partir d'ara anomenarem sovint $\tilde{u}$ al
    representant continu de $u$.
\end{obs}

\begin{lema}
    Sigui $I = (0,1)$ i sigui $\eta \in \C^1(\real)$ una funció qualsevol satisfent 
    \[
        \eta(x)  = \begin{cases}
                       1 &\text{si } x < 1/4\\
                       0 &\text{si } x > 3/4
                   \end{cases}
    \]
    Donada $f$ definida en $I$, anomenem $\tilde{f}$ a la següent funció
    definida en $(0, \infty)$.
    \[
        \tilde{f}(x)  = \begin{cases}
                            f(x) &\text{si } 0< x < 1\\
                            0 &\text{si } x > 1
                        \end{cases}
    \]
    Aleshores, donada $u \in W^{1,p}(I)$, es té $\eta \tilde{u} \in
    W^{1,p}(0,\infty)$ i $(\eta \tilde{u})' = \eta ' \tilde{u} + \eta
    \tilde{u'}$.

    \label{LemaEta}
\end{lema}

\begin{proof}
Sigui $\varphi \in \C_c ^1 ((0, \infty))$. Aleshores, 
\[
    \int_0 ^{\infty} \eta \tilde{u} \varphi' = \int_0 ^1 \eta u \varphi' =
    \int_0 ^1 u [(\eta \varphi)' - \eta' \varphi] = \int_0 ^1 u(\eta \varphi)' -
    \int_0^1 u \eta' \varphi
\]

Com que $\eta \varphi \in \C_c^1 ((0,1))$, emprant la definició de derivada
feble tenim que
\[
\int_0 ^{\infty} \eta \tilde{u} \varphi' = - \int_0 ^{\infty} [\tilde{u'} \eta +
\tilde{u} \eta'] \varphi
\]
\end{proof}

\begin{teo}[Teorema de prolongació]
    Sigui $1 \leq p \leq \infty$. Existeix un operador de prolongació lineal i
    fitat $P: W^{1,p}(I) \longrightarrow W^{1,p}(\real)$, no necessàriament
    únic, tal que $u \mapsto v$, amb $v(x) = v(y) \, \forall x \in I$ i de
    manera que $\exists C = C(I)$ tal que $\|v\|_ {L^p(\real)} \leq C\|u\|_
    {L^p(I)}$, i $\|v\|_ {W^{1,p}(\real)} \leq C\|u\|_ {W^{1,p}(I)}$, on la
    constant $C$ pot dependre de $I$ però no de $u$. A més a més, si $I$ és
    fitat, $v$ tindrà suport compacte.
\end{teo}

\begin{proof}
    Vegem en primer lloc el cas en què I és no fitat. Qualsevol interval no
    fitat i diferent de $\real$ es redueix al cas $I = (0, \infty)$ fent un
    canvi de coordenades adient. Definim l'extensió per reflexió i veurem que
    compleix les propietats desitjades.
    \[
        P(u)(x)  = \begin{cases}
                    u(x) &\text{si $x \geq 0$}\\
                    u(-x) &\text{si $x < 0$}
                    \end{cases}
    \]
    És clar que $\|P(u)\|_{L^p(\real)} \leq 2\|u\|_{L^p(I)}$. Observem que la
    funció $v \in L^p(\real)$ definida per
    \[
        v(x)  = \begin{cases}
                    u'(x) &\text{si $x \geq 0$}\\
                    -u'(-x) &\text{si $x < 0$}
                    \end{cases}
    \]
    és la derivada de $P(u)$. En efecte,

    \[
    P(u)(x)-P(u)(0) = \int_0 ^x v(t)\dif t \, \, \forall x \in \real
    \]
    En definitiva, $P(u) \in W^{1,p}(\real)$ i tenim $\|P(u)\|_{L^p(\real)} \leq
    C\|u\|_{L^p(I)}$, amb $C = 2$.

    Vegem ara el cas en què l'interval és fitat. Sense pèrdua de generalitat,
    podem suposar que $I = (0,1)$. Utilitzant la mateixa notació del lema
    \ref{LemaEta}, podem escriure 
    \[
    u = \eta u + (1-\eta) u
    \]
    I podem estendre $\eta u$ a $(0, \infty)$ per $\eta \tilde{u}$, i aleshores
    a $\real$ en virtut del cas $I = (0, \infty)$ que hem fet en primer lloc.

    Així, obtenim una funció $v_1 \in W^{1,p}(\real)$ que estén $\eta u$ a
    $\real$ i tal que
    \[
        \begin{cases}
            \|v_1\|_{L^p(\real)} \leq C \|u \|_{L^p (I)} \\
            \|v_1\|_{W^{1,p}(\real)} \leq C \|u \|_{W^{1,p} (I)} \\
        \end{cases}
    \]
    Pel mateix procediment, estenem $(1-\eta)u$ a $(-\infty,1)$ per zero a
    $(-\infty,0)$ i després estenem a tot $\real$ per reflexió respecte $x = 1$.
    D'aquesta manera obtenim $v_2 \in W^{1,p}(\real)$ que estén $(1-\eta) u$ a
    $\real$ i tal que
    \[
        \begin{cases}
            \|v_2\|_{L^p(\real)} \leq C \|u \|_{L^p (I)} \\
            \|v_2\|_{W^{1,p}(\real)} \leq C \|u \|_{W^{1,p} (I)} \\
        \end{cases}
    \]
    Aleshores, $Pu = v_1 + v_2$ satisfà les condicions del Teorema.
\end{proof}

A continuació veurem un resultat que ens permetrà aproximar les funcions de
$W^{1,p} (I)$ per funcions $\C^{\infty}$ en un cert sentit. Abans, però, cal
introduir alguns conceptes que són necessaris per la demostració. En primer
lloc, enunciarem el següent lema sobre convolució, que no demostrarem. La
demostració es pot trobar a la pàgina 211 del Brézis.

\begin{lema}
    Sigui $\rho \in L^1 (\real)$, i $v \in W^{1,p}(\real)$, amb $1 \leq p \leq
    \infty$. Aleshores, $\rho \ast v \in W^{1,p} (\real)$ i $(\rho \ast v)' =
    \rho \ast v'$. 
    \label{lemaConv}
\end{lema}

\begin{defi}[funcions de truncació]
    Fixem una funció $\zeta \in \C_c ^{\infty} (\real)$ tal que $0 \leq \zeta \leq 1$ i 
    \[
        \zeta(x) = \begin{cases}
                        1 &\text{si $|x| < 1$}\\
                        0 &\text{si $|x| \geq 2$}
                    \end{cases}
    \]
    Anomenem funció de truncació a una funció que compleixi aquestes propietats.
\end{defi}

Donada una funció de truncació $\zeta$, definim la seqüència $\zeta_n (x) =
\zeta(x/n)$. Pel Teorema de la convergència dominada, si $f \in L^p(\real)$,
aleshores $\zeta_n f \to f$ amb norma $L^p (\real)$. Intuïtivament, la successió
$\zeta_n$ és una successió que tendeix a la funció constant igual a 1.



\begin{teo*}
    Sigui $u \in W^{1,p}(I)$, amb $1 \leq p < \infty$. Aleshores $\exists$
    una successió $(u_n) \in \C_c^{\infty} (\real)$ tal que $u_n|_I \to u$ amb
    la norma de $W^{1,p}(I)$. \label{densitat}
\end{teo*}

\begin{proof}
    Usant el teorema de prolongació que acabem de veure, podem suposar que $I =
    \real$. Prenem ara una successió regularitzadora $\rho_n$, i una funció de
    truncació $\zeta$. Veurem que la successió $u_n = \zeta_n (\rho_n \ast u)$
    convergeix a $u$ en $W^{1,p}(\real)$. En primer lloc, tenim $\|u_n -
    u\|_{L^p} \to 0$, ja que $u_n -u = \zeta_n ((\rho_n \ast u)-u) + (\zeta_n u
    - u)$, i, per tant, tal com volíem
    \[
        \|u_n -u \|_{L^p} \leq \|(\rho_n \ast u) -u \|_{L^p} + \|\zeta_n u - u \|_{L^p} \to 0
    \]
    Utilitzant ara el lema \ref{lemaConv} tenim
    \[
        u'_n = \zeta'_n (\rho_n \ast u) + \zeta_n (\rho_n \ast u')
    \]
    I, per tant,
    \[
        \|u'_n -u' \|_{L^p} \leq \|\zeta'_n (\rho_n \ast u)\|_{L^p} + \|\zeta_n
        (\rho_n \ast u') - u'\|_{L^p}
    \]
    Per la definició de la seqüència $\zeta_n$, $\|\zeta'_n\|_{L^p} =
    \|\zeta'\|_{L^p}/n$. Per altra banda, sumant i restant $\zeta_n u'$ i 
    usant la desigualtat triangular, tenim
    \[
        \|\zeta_n (\rho_n \ast u') - u'\|_{L^p} = \|\zeta_n (\rho_n \ast u') -
        \zeta_n u'\|_{L^p} + \|\zeta_n u' - u'\|_{L^p}
    \]
    Així doncs, si anomenem $C = \| \zeta'_n \|_{\infty}$, i usem que $\zeta_n
    (x) < 1$, obtenim finalment
    \[
        \|u'_n -u' \|_{L^p} \leq \frac{C}{n} \|u\|_{L^p} + \|\rho_n \ast u' -
        u'\|_{L^p} + \| \zeta_n u' -u' \|_{L^p} \to 0
    \]
    En definitiva, hem provat que $u_n \to u$ i $u'_n \to u'$ en $L^p$, i per
    tant, $u_n \to u$ en $W^{1,p}$.
\end{proof}

\begin{obs}
    Aquest resultat pot portar confusió. Cal remarcar que no diu que $u$ pugui
    aproximar-se per una successió $u_n \in \C_c ^{\infty} (I)$, encara que
    d'entrada pugui semblar equivalent.
\end{obs}

\begin{col}
    Siguin $u,v \in W^{1,p} (I)$, amb $1 \leq p \leq \infty$. Aleshores, $uv \in W^{1,p} (I)$ i 
    \[
        (uv)' = u'v + uv'
    \]
    A més a més, es satisfà la fórmula d'integració per parts, $\forall x,y \in \Bar{I}$ tenim
    \[
        \int_y^x u'v = u(x)v(x) - u(y)v(y) - \int_y^x uv'
    \]
\end{col}

\begin{proof}
    Per l'encabiment de Sobolev, podem suposar $u \in L^{\infty}(I)$, i per tant
    $uv \in L^p$. Queda veure doncs que $(uv)' \in L^p$. Considerem en primer
    lloc el cas $p < \infty$. Siguin $(u_n), \, (v_n) \in \C_c^1 (\real)$
    seqüències tals que $u_n|_I \to u$, $v_n|_I \to v$ en $W^{1,p}(I)$.
    Aleshores, de nou per l'encabiment de Sobolev, tenim $u_n|_I \to u$, $v_n|_I
    \to v$ en $L^{\infty}(I)$ i en $L^p (I)$.

    Per tant, $u_n v_n \to uv$ en $L^{\infty} (I)$ i també en $L^p(I)$. Així doncs, tenim
    \[
        (u_n v_n)' = u'_n v_n + u_n v'_n \to u'v +uv'
    \]
    Així doncs, hem que $uv \in W^{1,p} (I)$ i també la regla del producte per la derivació.

    Fem ara el cas $p = \infty$. Siguin $u,v \in W^{1,\infty}(I)$. Aleshores $uv
    \in L^{\infty}(I)$ i $u'v+uv' \in L^{\infty}(I)$. Ens queda veure que
    $u'v+uv'$ és la derivada de $uv$.

    Sigui $\varphi \in \C_c^1 (I)$, i fixem un interval $J \subset I$ obert i
    fitat tal que $\supp(\varphi) \subset J$. Aleshores, com que $J$ és
    fitat, $u,v \in W^{1,p}(I)$ $\forall p < \infty$ i pel cas $p < \infty$ que
    ja hem fet, tenim
    \[
        \int_J uv \varphi' = -\int_J u'v + uv' \varphi
    \]
    I, per tant, 
    \[
        \int_I uv \varphi' = -\int_I u'v + uv' \varphi
    \]
    Integrant la regla del producte obtenim la fórmula d'integració per parts.
\end{proof}

Observem que $\C_c^1(I) \subset W^{1,p}, \, \, \forall 1 \leq p \leq \infty$, ja
que el suport compacte garanteix que tota funció de $\C_c^1(I)$ i la seva
derivada siguin integrables. Això ens permet fer la definició següent: 

\begin{defi}
    Sigui $1 \leq p < \infty$. Anomenem $W_0^{1,p}(I)$ a la clausura de
    $\C_c^1(I)$ en $W^{1,p}(I)$. Si $p = 2$, anomenarem $H_0^1 = W_0 ^{1,2}$.
\end{defi}

$W_0 ^{1,p}$ és un espai de Banach prenent com a norma la restricció de la norma
de $W^{1,p}(I)$ a $W_0 ^{1,p}(I)$. Anàlogament, l'espai $H_0^1$ és un espai de
Hilbert amb la restricció del producte escalar de $H^1(I)$. Observem també que
$W_0^{1,p}(\real) = W^{1,p}(\real)$.

%\begin{obs}
%   Usant una seqüència regularitzadora, es pot veure que $\C_c^{\infty}$ és
%   dens a $W_0^{1,p}$, cosa que fa que a vegades es defineixi $W_0^{1,p}$ com
%   la clausura de $\C_c^{\infty}$. Usant també regularitzadors es pot veure que
%   si $u \in W^{1,p}(I) \cap \C_c(I)$, aleshores $u \in W_0 ^{1,p}$.
%\end{obs}


\begin{prop}
    Sigui $u \in W^{1,p}(I)$. Aleshores, $u \in W_0^{1,p}(I)$ si i només si
    $\tilde{u}$ s'anu\l.la als extrems.
\end{prop}

\begin{proof}
    Si $u \in W_0^{1,p}(I)$, aleshores $u = \lim_{x \to \infty} \varphi_n$, amb
    $\varphi_n \in \C_c^1(I)$. Així doncs, les $\varphi_n$ s'anu\l.len als
    extrems. Com que avaluar la funció en els extrems és continu, commuta amb el
    límit i, per tant, $u$ s'anu\l.la als extrems de $I$.

    Recíprocament, suposem que $\tilde{u} \in W^{1,p}$ s'anu\l.la als extrems.
    Vegem en primer lloc el cas d'un interval $I$ fitat. Podem estendre
    $\tilde{u}$ a fora de $I$ per $u_0 \in W_0^{1,p}(\real)$
    \[
        u_0 = \begin{cases}
                \tilde{u}(x) &\text{si $x \in I$}\\
                0 &\text{si $x \notin I$}
                \end{cases}
    \]

    Ara aproximem $u_0$ per la successió $u_n (x) = u_0 ((1+1/n)x) \in
    W_0^{1,p}(I)$, que encongeixen el suport de $u_0$, de manera que són
    funcions a suport compacte i $\supp(u_n) \subset I$. Regularitzem
    aquesta successió utilitzant funcions regularitzadores, 
    \[
        \rho_{m(n)} \ast u_0((1+1/n)x) \in \C_c^{\infty} (I) \to u
    \]
    D'aquesta manera, $u$ és limit de funcions $\C_c^{\infty}$, i per tant $u
    \in W_0^{1,p} (I)$. El cas en què $I$ és no fitat es pot reduir a $I =
    (0,\infty)$, i es procedeix de manera similar.
\end{proof}

\begin{teo}[Desigualtat de Poincaré]
    Sigui $I$ un interval fitat. Aleshores existeix una constant $C$ depenent de
    $|I|$ tal que 
    \[
        \|u\|_{W^{1,p}(I)} \leq C \|u'\|_{L^p(I)} \, \, \, \forall u \in W_0^{1,p}(I)
    \]
\end{teo}

\begin{proof}
    Sigui $I = (a,b)$ i $u \in W_0^{1,p}(I)$. Com que $u(a) = 0$, 
    \[
        |u(x)| = |u(x)-u(a)| = \left | \int_a^x u'(t) \dif t \right | \leq \|u'\|_ {L^1}
    \]

    Prenent suprems, tenim que $\|u\|_{L^{\infty}(I)} \leq \|u'\|_{L^1(I)}$. Com
    que $I$ és fitat, si $p < q$, $\exists C$ tal que $\|f\|_p \leq C\|f\|_q$.
    Aleshores,

    \[
        \|u\|_{W^{1,p}(I)} = \|u\|_{L^p(I)} + \|u'\|_{L^p(I)} \leq
        \|u'\|_{L^1(I)} + \|u'\|_{L^p(I)} \leq C(\|u'\|_{L^p(I)}) 
    \] 
\end{proof}

\begin{obs}
Com a conseqüència d'aquesta desigualtat, en $W_0^{1,p}(I)$ la norma
$\|u'\|_{L^p(I)}$ és equivalent a la norma habitual. A més a més, en $H_0^1$ el
producte escalar $\langle u,v\rangle_0 = \int_I u'v'$ dóna un producte escalar
equivalent a l'habitual, ja que, donat $0 < \alpha < 1$
\[
    \|u\|^2_{H_0^1} = \int_I u^2 + (u')^2 \geq \int_I (u')^2 \geq \alpha\int_I
    (u')^2 + \frac{(1-\alpha)}{C}\int_I u^2 \geq \min \left \{\alpha,
    \frac{(1-\alpha)}{C}\right \} \|u\|^2_{H_0^1} 
\]
\end{obs}




\section{Problemes de contorn en dimensió 1}

A continuació presentem una sèrie de problemes de contorn a l'interval $I =
(0,1)$. L'anàlisi és vàlid per a qualsevol interval, ja que en tenim prou amb un
canvi de variable per reduir-nos a l'interval $(0,1)$. Per a cada problema de
contorn farem l'anàlisi següent:

\begin{enumerate}
\item Definirem la noció de solució feble del problema.
\item Definirem la noció de solució variacional del problema.
\item Donarem condicions per a l'existència i unicitat de solucions
\item Discutirem propietats de regularitat
\item Si les condicions de contorn són homogènies, definirem l'operador solució i provarem que és lineal i continu.
\end{enumerate} 

\subsection*{Exemple 1}

Considerem en primer lloc el següent problema:
\begin{equation}
\label{eq:ex1}
	\begin{cases}
		-u'' + u = f; \\
		u(0) = 0, \; u(1) = 0
	\end{cases}
\end{equation}

\begin{defi}
    Sigui $f \in \C(\Bar{I})$. Aleshores, si $u \in \C^2(\Bar{I})$ és solució
    del problema de contorn, diem que $u$ és solució clàssica.
\end{defi}

Intentem ara generalitzar aquest problema de contorn. Observem que, si $u$ és una solució clàssica del problema, 
prenent $\varphi \in \C_c^1(I)$, tenim $\int_I -u''\varphi + \int_I u\varphi = \int_I f\varphi$. 
Integrem el primer terme per parts. Tenint en compte que el suport de $\varphi$ és compacte, 
els termes de contorn de la fórmula d'integració per parts s'anu\l.len, i ens queda

\[
	\int_I u' \varphi' + \int_I u \varphi = \int_I f \varphi \, \, \forall \varphi \in \C_c^1(I)
\]
A més a més, com que les condicions de contorn ens asseguren que $u$ val zero als extrems, $u \in H_0^1(I)$.
Aquesta igualtat es pot reescriure com a 
\[
	\langle u, \varphi \rangle_{H_0^1(I)} = \langle f, \varphi \rangle_{L^1(I)}  \, \, \forall \varphi \in \C_c^1(I)
\]
Això ens suggereix definir solució feble com $u \in H_0^1(I)$ satisfent aquesta igualtat. No obstant, el plantejament es pot
fer una mica més general, seguint les següents observacions:

\begin{obs}	
	Com que les funcions de $\C_c^1(I)$ són denses a $H^1_0(I)$, i els funcionals $\langle u, \varphi \rangle_{H_0^1(I)}$ i $\langle f, \varphi \rangle_{L^1(I)}$ són continus en $\varphi$ en la norma de $H^1_0$, demanar que $u$ satisfaci 
	\[
		\langle u, \varphi \rangle_{H_0^1(I)} = \langle f, \varphi \rangle_{L^2(I)}  
	\]
	$\forall \varphi \in \C_c^1(I)$ és equivalent a demanar-ho $\forall \varphi \in H_0^1(I)$.
\end{obs}

\begin{obs}	
	Quan hem plantejat el problema inicialment, no hem especificat cap condició sobre $f$, 
	si bé en la definició de solució clàssica hem demanat que $f \in \C(\Bar{I})$. 
	No obstant, en la formulació feble del problema que hem vist fins al moment, només cal que 
	$f \in L^2(I)$ perquè tingui sentit el producte escalar $\langle f, \varphi \rangle_{L^2(I)}$. 
	
    Més en general, podríem substituir $f$ per $F \in H^1_0(I)'$, i el producte escalar pel producte
    de dualitat $\langle F, \varphi \rangle_{H^1_0(I)',H^1_0(I)}$. 
\end{obs}

Ara sí, podem arribar a la definició de solució feble amb tota la generalitat necessària.

\begin{defi}
	Donada $F \in H^1_0(I)'$, direm que $u \in H_0^1(I)$ és una solució feble del problema
	\begin{equation}
	\label{eq:ex1feble}
		\begin{cases}
			-u'' + u = F \\
			u(0) = u(1) = 0
		\end{cases}
	\end{equation}
	si satisfà la següent igualtat $\forall v \in H_0^1(I)$
	\[
		\langle u, \varphi \rangle_{H_0^1(I)} = \langle F, \varphi \rangle_{H^1_0(I)',H^1_0(I)}.
	\]
\end{defi}

\begin{obs}
	Encara que la nomenclatura ens pot confondre, la definició que hem fet de derivada feble no és equivalent a que $u$ satisfaci $-u'' + u = f$ entenent les $u''$ com a derivades febles.
\end{obs}

\begin{obs}
	Atenent a la deducció que hem fet, és clar que tota solució clàssica del problema també és solució feble.
\end{obs}

Un cop tenim definida la noció de solució feble (el primer punt que ens havíem proposat), podem observar algunes propietats fonamentals:

\begin{teo*}
	Donada $F \in H_0^1 (I)$, existeix una única solució feble $u \in H_0^1(I)$ del problema \eqref{eq:ex1feble}.
	A més, aquesta solució és la que minimitza la forma variacional
	\[
		\frac{1}{2} \int_{I} \lp(v')^2 + v^2 \rp -  \langle  F, v\rangle_{H^1_0(I)',H^1_0(I)},
	\]
	entre totes les $v \in H_0^1(I)$. Addicionalment, $\|u\|_{H_0^1 (I)} = \|F \|_{H_0^1 (I)'}$, i per tant la dependència
	de $u$ respecte $F$ és lineal i contínua.
	
	Si el segon terme prové d'una funció $f \in L^2(I)$, tindrem a més que $u \in H^2(I) \cap H_0^1(I)$ i se satisfarà que
	$-u''+u = f$ en el sentit de derivades febles. Si $f \in C(\Bar{I})$, $u$ també serà solució clàssica.
\end{teo*}
\begin{proof}
	L'existència i unicitat se segueix del teorema de Riesz-Fréchet (\ref{teo:riesz}), i també el fet que 
	$\|u\|_{H_0^1 (I)} = \|F \|_{H_0^1 (I)'}$. La forma variacional es dedueix del teorema de Lax-Milgram (\ref{teo:L-M}),
	ja que $\langle u,v \rangle_{H_0^1}$ és una forma bilineal, simètrica, contínua i coerciva.
	
	Pel que fa a la regularitat de la solució, si tenim $f \in L^2(I)$, de la definició de solució feble tindrem:
	\[
		\int_I u' v' = -\int_I (u-f) v; \hspace{5mm} \forall v \in H_0^1(I).
	\]
	Com que $u-f \in L^2(I)$, concloem que $u'$ admet una derivada feble, que és $u-f$, i per tant $u \in H^2(I)$.
	No detallarem el cas $f \in \C(\Bar{I})$.
\end{proof}

Així, hem acabat l'anàlisi que havíem plantejat al principi. 

\subsection*{Exemple 2}
Considerem ara la següent variant del problema:
\[
	\begin{cases}
		-u'' + u = f \\
		u(0) = \alpha, \, u(1) = \beta
	\end{cases}
\]
Aquest cas es redueix a l'anterior \eqref{eq:ex1} escrivint $u = u_0 + v$, on $u_0 (x)$ és una funció auxiliar
que satisfà les condicions de contorn (per exemple, una recta), i $v$ és una
solució de l'equació amb condicions de contorn nu\l.les.

\subsection*{Exemple 3}
Estudiem el següent problema de Sturm-Liouville:
\[
	\begin{cases}
		-(p(x)u')' + q(x)u = f \\
		u(0) = 0, \, u(1) = 0
	\end{cases}
\]
\begin{defi}
	La funció $u \in H_0^1(I)$ serà solució feble del problema anterior si
	per a tota $v \in H_0^1(I)$ se satisfà:
	\[
		\int_I p(x) u'v' + \int_I q(x) u v = \int_I fv.
	\]
\end{defi}

El desenvolupament és anàleg a l'anterior. Per a poder aplicar el teorema de Lax-Milgram, necessitem imposar
algunes condicions de fitació sobre $p$ i $q$.

\begin{prop}
	Si les funcions $p(x)$, $q(x)$ són fitades amb $p(x) \geq p_0 > 0$ i $q(x) \geq q_0$, amb $q_0$ positiu o poc
	negatiu (en un sentit que es precisarà a la demostració), llavors la forma bilineal
	\[
		a(u,v) = \int_I p(x) u'v' + \int_I q(x) u v
	\]
	és contínua i coerciva.
\end{prop}
\begin{proof}
	Que és contínua es dedueix ràpidament del fet que $p$ i $q$ són fitades i d'aplicar Cauchy-Schwarz amb $u$ i $v$.
	Pel que fa a la coercivitat:
	\[
		a(v,v) \geq p_0 \|v'\|^2_{L^2} + q_0 \|v\|^2_{L^2}  = (p_0 - \varepsilon) \|v'\|^2_{L^2} + \varepsilon \|v'\|^2_{L^2} + q_0 \|v\|^2_{L^2}.
	\]
	Aplicant la desigualtat de Poincaré, és a dir, que existeix $C > 0$ tal que $C\|v'\|_{L^2}^2 \geq \|v \|_{L^2}^2$, obtenim:
	\[
		a(v,v) \geq (p_0 - \varepsilon) \|v'\|^2_{L^2} + \lp \frac{\varepsilon}{C} + q_0\rp \|v\|^2_{L^2}. 
	\]
	Si escollim $\varepsilon > 0$ tal que $p_0 - \varepsilon > \delta > 0$ i $\frac{\varepsilon}{C} + q_0 > \delta > 0$
	(admetent així que $q_0$ pugui ser una mica negatiu), acabarem concloent que $a(v,v) > \delta \|v\|^2_{H^1}$, com volíem.
\end{proof}

Si se satisfan les hipòtesis que hem dit, podrem aplicar el teorema de Lax-Milgram per a afirmar que hi haurà existència
i unicitat de solucions, i podrem obtenir la forma variacional del problema.

\subsection*{Exemple 4}
Ara, canviem les condicions de contorn per condicions de Neumann:
\begin{equation}
\label{eq:ex4}
	\begin{cases}
		-u'' + u = f; \\
		u'(0) = \alpha, \; u'(1) = \beta
	\end{cases}
\end{equation}
Això va diferent. Aquí, en comptes d'imposar les condicions de contorn en l'espai en el que treballem
(com fèiem en $H_0^1(I)$), imposarem les condicions dins la mateixa equació. Notem que les condicions 
$u'(0) = \alpha, \; u'(1) = \beta$ no tenen sentit a $H^1(I)$ (sí que tindrien sentit a $H^2(I)$, ja que per l'encabiment
de Sobolev es pot prendre un representant continu de la derivada).

Suposant que tenim una solució clàssica, si multipliquem l'equació per una funció de prova $\varphi \in \C^1 ([0,1])$ 
(no demanem que s'anu\l.li als extrems, ja que perdríem les condicions de contorn), s'obté:
\[
	\alpha \varphi(0) - \beta \varphi(1) + \int_I u' \varphi' + \int_I u \varphi = \int_i f \varphi.
\]
Inspirant-nos en això, podem definir solució feble:
\begin{defi}
	Donada $f \in L^2 (I)$, una solució feble $u \in H^1(I)$ de l'equació \eqref{eq:ex4} és una funció que satisfà
	\[
		\int_I u' v + \int_I u v = -\alpha v(0) + \beta v(1) + \int_i f v,
	\]
	per a tota $v \in H^1(I)$.
\end{defi}

El membre de l'esquerra és una forma bilineal, simètrica, contínua i coerciva, i el membre de la dreta és una forma lineal contínua.
Per tant, podem aplicar Lax-Milgram per afirmar existència i unicitat de solucions. La forma variacional és que $u$ minimitzi:
\[
	\frac{1}{2} \int_I  \lp (v')^2 + v^2 \rp + \alpha v(0) - \beta v(0) - \int_I fv,
\]
per a $v \in H^1(0,1)$.

\begin{obs}
	En comptes de $f \in L^2 (I)$, més en general podríem haver pres $\langle F,v \rangle_{(H^1)',H_1}$ per a certa $F \in H^1 (I)'$.
\end{obs}

Respecte a la regularitat, si suposem que $f \in L^2(I)$ i prenem $v \in \C_c^1(I)$, de la definició de solució feble sortirà que
$u'$ té derivada feble $u-f \in L^2(I)$. Falta veure que es compleixen les condicions de contorn (que ara tenen sentit perquè 
$u \in H^2(I)$). Integrant per parts a la definició de solució feble, tenint en compte que $-u'' + u = f$, obtenim:
\[
	u'(0) v(0) - u'(1) v(1) = \alpha v(0) - \beta v(1).
\]
Com que $v \in H^1(0,1)$ és arbitrària (i, per tant, $v(0)$ i $v(1)$ també), concloem que $u'(0) = \alpha$ i $u'(1) = \beta$.

\subsection*{Exemple 5}
Un problema ``mixt'':
\[
	\begin{cases}
		-u'' + u = f; \\
		u(0) = 0, \; u'(1) = 0
	\end{cases}
\]
Introduïm un nou espai, el de les funcions que s'anu\l.len a l'esquerra:
\[
	H_e^1(a,b) = \lc u \in H^1(a,b) \, | \,u(a) = 0 \rc
\]
Treballant en aquest nou espai, es fa com en els casos anteriors.

\subsection*{Exemple 6}
Introduïm ara condicions de tercera classe:
\[
	\begin{cases}
		-u'' + u = f; \\
		u'(0) - ku(0) = 0, \; u'(1) = 0
	\end{cases}
\]
D'una banda, treballem en l'espai $H_d^1(I)$ de funcions que s'anu\l.len en $x = 1$. Suposant que tenim
una solució clàssica i multiplicant per una funció de prova $\varphi \in \C^1([0,1))$, obtenim:
\[
	u'(0) \varphi(0) + \int_I \lp u' \varphi' + u \varphi \rp = \int_I f \varphi. 
\]
Amb això en ment, i aplicant les condicions de contorn, definim solució feble:

\begin{defi}
	Donada $f \in L^2(I)$, una solució feble $u \in H_d^1(I)$ del problema anterior és tal que:
	\[
		k u(0)v(0) + \int_I \lp u'v' + uv \rp = \int_I fv
	\]
	per a tota $v \in H_d^1(I)$.
\end{defi}

Si $k \geq 0$, es podrà aplicar el teorema de Lax-Milgram, i es podrà donar la forma variacional corresponent.

\subsection*{Exemple 7}
Posem condicions de contorn periòdiques:
\[
	\begin{cases}
		-u'' + u = f; \\
		u(0) - u(1) = 0, \; u'(0) - u'(1) = 0
	\end{cases}
\]
Es treballa amb l'espai de funcions tals que $u(0) = u(1)$, i es procedeix igual que sempre.

\subsection*{Exemple 8}
Comentem finalment un problema una mica diferent, ara en l'interval $I = \real$:
\[
	\begin{cases}
		-u'' + Ru' + Ku = f \\
		\lim_{|x| \to \infty} u(x) = 0.
	\end{cases}
\]
La solució feble serà $u \in H^1(\real)$ tal que
\[
	\int u'v' + R \int  u'v + K \int uv = \int fv,
\] 
per a tota $v \in H^1(\real)$. Notem que el membre de l'esquerra és una forma bilineal contínua en $H^1(\real)$,
però que no serà simètrica si $R \neq 0$. Serà coerciva si $K > 0$, ja que $\int v' v = 0$ (la primitiva és $v^2$), i
\[
	\int (v')^2 + K \int v^2 \geq \min \{ 1,K \} \|v\|_{H^1}.
\]
Notem que en no ser una forma simètrica, no admet una forma variacional.
